
package com.loklvokl.lokl.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class UserProfile {


    @SerializedName("addhar_counter")
    @Expose
    private int addhar_counter;

    @SerializedName("pan_counter")
    @Expose
    private int pan_counter;

    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("first_name")
    @Expose
    private String firstName;

    @SerializedName("mobile")
    @Expose
    private String mobile;

    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("dob")
    @Expose
    private String dob;
    @SerializedName("profile_image")
    @Expose
    private String profileImage;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("aadhar_document")
    @Expose
    private List<AadharDocument> aadharDocument = null;
    @SerializedName("pan_document")
    @Expose
    private List<PanDocument> panDocument = null;


    public int getAddhar_counter() {
        return addhar_counter;
    }

    public void setAddhar_counter(int addhar_counter) {
        this.addhar_counter = addhar_counter;
    }

    public int getPan_counter() {
        return pan_counter;
    }

    public void setPan_counter(int pan_counter) {
        this.pan_counter = pan_counter;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<AadharDocument> getAadharDocument() {
        return aadharDocument;
    }

    public void setAadharDocument(List<AadharDocument> aadharDocument) {
        this.aadharDocument = aadharDocument;
    }

    public List<PanDocument> getPanDocument() {
        return panDocument;
    }

    public void setPanDocument(List<PanDocument> panDocument) {
        this.panDocument = panDocument;
    }
}
