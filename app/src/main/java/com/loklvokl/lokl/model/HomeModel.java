
package com.loklvokl.lokl.model;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class HomeModel {

    @SerializedName("homeslider")
    @Expose
    private List<Homeslider> homeslider = null;
    @SerializedName("collection")
    @Expose
    private List<Collection> collection = null;
    @SerializedName("ExplorerCategory")
    @Expose
    private List<ExplorerCategory> explorerCategory = null;
    @SerializedName("productdeal")
    @Expose
    private List<Productdeal> productdeal = null;
    @SerializedName("spacialoffer")
    @Expose
    private List<Spacialoffer> spacialoffer = null;

    public List<HomeProduct> getHome_product() {
        return home_product;
    }

    public void setHome_product(List<HomeProduct> home_product) {
        this.home_product = home_product;
    }

    @SerializedName("home_product")
    @Expose
    private List<HomeProduct> home_product = null;

    public List<Homeslider> getHomeslider() {
        return homeslider;
    }

    public void setHomeslider(List<Homeslider> homeslider) {
        this.homeslider = homeslider;
    }

    public List<Collection> getCollection() {
        return collection;
    }

    public void setCollection(List<Collection> collection) {
        this.collection = collection;
    }

    public List<ExplorerCategory> getExplorerCategory() {
        return explorerCategory;
    }

    public void setExplorerCategory(List<ExplorerCategory> explorerCategory) {
        this.explorerCategory = explorerCategory;
    }

    public List<Productdeal> getProductdeal() {
        return productdeal;
    }

    public void setProductdeal(List<Productdeal> productdeal) {
        this.productdeal = productdeal;
    }

    public List<Spacialoffer> getSpacialoffer() {
        return spacialoffer;
    }

    public void setSpacialoffer(List<Spacialoffer> spacialoffer) {
        this.spacialoffer = spacialoffer;
    }

}
