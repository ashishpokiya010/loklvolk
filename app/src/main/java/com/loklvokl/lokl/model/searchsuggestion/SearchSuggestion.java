package com.loklvokl.lokl.model.searchsuggestion;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class SearchSuggestion{

	@SerializedName("response")
	private List<SearchItems> response;

	public void setResponse(List<SearchItems> response){
		this.response = response;
	}

	public List<SearchItems> getResponse(){
		return response;
	}
}