
package com.loklvokl.lokl.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AadharDocument {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("aadhar_image")
    @Expose
    private String aadharImage;
    @SerializedName("document_approval_status")
    @Expose
    private String documentApprovalStatus;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAadharImage() {
        return aadharImage;
    }

    public void setAadharImage(String aadharImage) {
        this.aadharImage = aadharImage;
    }

    public String getDocumentApprovalStatus() {
        return documentApprovalStatus;
    }

    public void setDocumentApprovalStatus(String documentApprovalStatus) {
        this.documentApprovalStatus = documentApprovalStatus;
    }

}
