package com.loklvokl.lokl.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class UserOrderDetails {
    @SerializedName("order_product")
    @Expose
    private List<UserOrderDetailProduct> orderProduct = null;

    public List<UserOrderDetailProduct> getOrderProduct() {
        return orderProduct;
    }

    public void setOrderProduct(List<UserOrderDetailProduct> orderProduct) {
        this.orderProduct = orderProduct;
    }
}
