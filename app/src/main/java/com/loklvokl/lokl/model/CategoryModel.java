
package com.loklvokl.lokl.model;

import java.io.Serializable;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CategoryModel implements Serializable {

    @SerializedName("main_category")
    @Expose
    private List<MainCategory> mainCategory = null;

    public List<MainCategory> getMainCategory() {
        return mainCategory;
    }

    public void setMainCategory(List<MainCategory> mainCategory) {
        this.mainCategory = mainCategory;
    }

}
