
package com.loklvokl.lokl.model;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PrivacyModel {

    @SerializedName("response")
    @Expose
    private List<ResponsePrivacy> response = null;

    public List<ResponsePrivacy> getResponse() {
        return response;
    }

    public void setResponse(List<ResponsePrivacy> response) {
        this.response = response;
    }

    public class ResponsePrivacy {

        @SerializedName("error")
        @Expose
        private Boolean error;

        @SerializedName("Privicay_Policy")
        @Expose
        private String privicayPolicy;

        public String getTermsCondition() {
            return termsCondition;
        }

        public void setTermsCondition(String termsCondition) {
            this.termsCondition = termsCondition;
        }

        @SerializedName("data")
        @Expose
        private String help;

        public String getHelp() {
            return help;
        }

        public void setHelp(String help) {
            this.help = help;
        }

        @SerializedName("Terms & Conditions")
        @Expose
        private String termsCondition;

        public String getFaq() {
            return faq;
        }

        public void setFaq(String faq) {
            this.faq = faq;
        }

        @SerializedName("faq")
        @Expose
        private String faq;

        public Boolean getError() {
            return error;
        }

        public void setError(Boolean error) {
            this.error = error;
        }

        public String getPrivicayPolicy() {
            return privicayPolicy;
        }

        public void setPrivicayPolicy(String privicayPolicy) {
            this.privicayPolicy = privicayPolicy;
        }

    }

}


