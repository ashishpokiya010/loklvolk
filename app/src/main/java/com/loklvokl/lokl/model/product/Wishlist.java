
package com.loklvokl.lokl.model.product;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Wishlist {


    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("product_image")
    @Expose
    private String product_image;

    public String getProduct_image() {
        return product_image;
    }

    public void setProduct_image(String product_image) {
        this.product_image = product_image;
    }

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("produc_id")
    @Expose
    private String producId;
    @SerializedName("product_name")
    @Expose
    private String productName;
    @SerializedName("product_selling_price")
    @Expose
    private int productSellingPrice;
    @SerializedName("mrp")
    @Expose
    private String mrp;
    @SerializedName("off")
    @Expose
    private String off;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProducId() {
        return producId;
    }

    public void setProducId(String producId) {
        this.producId = producId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public int getProductSellingPrice() {
        return productSellingPrice;
    }

    public void setProductSellingPrice(int productSellingPrice) {
        this.productSellingPrice = productSellingPrice;
    }

    public String getMrp() {
        return mrp;
    }

    public void setMrp(String mrp) {
        this.mrp = mrp;
    }

    public String getOff() {
        return off;
    }

    public void setOff(String off) {
        this.off = off;
    }

}
