package com.loklvokl.lokl.model.sizesbycategory;

import com.google.gson.annotations.SerializedName;

public class SizeDetailsItem{

	@SerializedName("size")
	private String size;

	public void setSize(String size){
		this.size = size;
	}

	public String getSize(){
		return size;
	}
}