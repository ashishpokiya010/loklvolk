package com.loklvokl.lokl.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PincodeModel {

    public List<Pincode> getPincode() {
        return pincode;
    }

    public void setPincode(List<Pincode> pincode) {
        this.pincode = pincode;
    }

    @SerializedName("pincode")
    @Expose
    private List<Pincode> pincode = null;


    public class Pincode {

        @SerializedName("error")
        @Expose
        private boolean error;

        @SerializedName("message")
        @Expose
        private String massage;


        public boolean getError() {
            return error;
        }

        public void setError(boolean error) {
            this.error = error;
        }

        public String getMassage() {
            return massage;
        }

        public void setMassage(String massage) {
            this.massage = massage;
        }
    }
}
