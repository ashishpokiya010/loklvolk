
package com.loklvokl.lokl.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProductSize {

    @SerializedName("size_value")
    @Expose
    private String size_value;

    public String getSize_value() {
        return size_value;
    }

    public void setSize_value(String size_value) {
        this.size_value = size_value;
    }
}
