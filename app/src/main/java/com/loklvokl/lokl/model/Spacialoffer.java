
package com.loklvokl.lokl.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Spacialoffer {

    @SerializedName("bannar_id")
    @Expose
    private String bannarId;
    @SerializedName("bannar_image")
    @Expose
    private String bannarImage;

    public String getBannarId() {
        return bannarId;
    }

    public void setBannarId(String bannarId) {
        this.bannarId = bannarId;
    }

    public String getBannarImage() {
        return bannarImage;
    }

    public void setBannarImage(String bannarImage) {
        this.bannarImage = bannarImage;
    }

}
