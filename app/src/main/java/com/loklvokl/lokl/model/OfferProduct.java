
package com.loklvokl.lokl.model;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OfferProduct {

    @SerializedName("offer_request")
    @Expose
    private List<OfferRequest> offerRequest = null;

    public List<OfferRequest> getOfferRequest() {
        return offerRequest;
    }

    public void setOfferRequest(List<OfferRequest> offerRequest) {
        this.offerRequest = offerRequest;
    }

}
