
package com.loklvokl.lokl.model;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CollectionProduct {

    @SerializedName("collection")
    @Expose
    private List<CollectionPro> collection = null;

    public List<CollectionPro> getCollection() {
        return collection;
    }

    public void setCollection(List<CollectionPro> collection) {
        this.collection = collection;
    }

}
