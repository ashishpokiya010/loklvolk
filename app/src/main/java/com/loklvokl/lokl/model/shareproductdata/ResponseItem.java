package com.loklvokl.lokl.model.shareproductdata;

import com.google.gson.annotations.SerializedName;

public class ResponseItem{

	@SerializedName("data")
	private String data;

	public void setData(String data){
		this.data = data;
	}

	public String getData(){
		return data;
	}
}