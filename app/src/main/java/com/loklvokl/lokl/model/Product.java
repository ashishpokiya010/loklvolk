
package com.loklvokl.lokl.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Product {

    @SerializedName("product_id")
    @Expose
    private String productId;
    @SerializedName("product_name")
    @Expose
    private String productName;
    @SerializedName("mrp")
    @Expose
    private String productMrp;
    @SerializedName("product_selling_price")
    @Expose
    private String productSellingPrice;
    @SerializedName("off")
    @Expose
    private String off;
    @SerializedName("product_image")
    @Expose
    private String productImage;

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductMrp() {
        return productMrp;
    }

    public void setProductMrp(String productMrp) {
        this.productMrp = productMrp;
    }

    public String getProductSellingPrice() {
        return productSellingPrice;
    }

    public void setProductSellingPrice(String productSellingPrice) {
        this.productSellingPrice = productSellingPrice;
    }

    public String getOff() {
        return off;
    }

    public void setOff(String off) {
        this.off = off;
    }

    public String getProductImage() {
        return productImage;
    }

    public void setProductImage(String productImage) {
        this.productImage = productImage;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    private String color = "#FFFFFF";

    @SerializedName("id")
    @Expose
    private String bannar_id = "-1";

    public String getBannar_id() {
        return bannar_id;
    }

    public void setBannar_id(String bannar_id) {
        this.bannar_id = bannar_id;
    }

    public String getBannar_image() {
        return bannar_image;
    }

    public void setBannar_image(String bannar_image) {
        this.bannar_image = bannar_image;
    }

    @SerializedName("bannar_image")
    @Expose
    private String bannar_image;

}
