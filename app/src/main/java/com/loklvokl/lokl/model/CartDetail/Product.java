
package com.loklvokl.lokl.model.CartDetail;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Product {

    @SerializedName("product_id")
    @Expose
    private String productId;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("color")
    @Expose
    private String color;
    @SerializedName("product_selling_price")
    @Expose
    private Integer productSellingPrice;
    @SerializedName("mrp")
    @Expose
    private String mrp;
    @SerializedName("off")
    @Expose
    private String off;


    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Integer getProductSellingPrice() {
        return productSellingPrice;
    }

    public void setProductSellingPrice(Integer productSellingPrice) {
        this.productSellingPrice = productSellingPrice;
    }

    public String getMrp() {
        return mrp;
    }

    public void setMrp(String mrp) {
        this.mrp = mrp;
    }

    public String getOff() {
        return off;
    }

    public void setOff(String off) {
        this.off = off;
    }


    @SerializedName("product_name")
    @Expose
    private String prodcutName;

    @SerializedName("qty")
    @Expose
    private String qty = "0";

    @SerializedName("description")
    @Expose
    private String description = "";

    public String getCart_id() {
        return cart_id;
    }

    public void setCart_id(String cart_id) {
        this.cart_id = cart_id;
    }

    @SerializedName("cart_id")
    @Expose
    private String cart_id;

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    @SerializedName("size")
    @Expose
    private String size;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getProdcutName() {
        return prodcutName;
    }

    public void setProdcutName(String prodcutName) {
        this.prodcutName = prodcutName;
    }

}
