package com.loklvokl.lokl.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class UserProfileUpdate {
    @SerializedName("docu_massage")
    @Expose
    private String docuMassage;
    @SerializedName("error")
    @Expose
    private String error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("mobile")
    @Expose
    private String mobile;
    @SerializedName("profile_image")
    @Expose
    private String profileImage;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("dob")
    @Expose
    private String dob;
    @SerializedName("aadhar_document")
    @Expose
    private List<AadharDocument> aadharDocument = null;
    @SerializedName("pan_document")
    @Expose
    private List<PanDocument> panDocument = null;
    @SerializedName("addhar_counter")
    @Expose
    private int addharCounter;
    @SerializedName("pan_counter")
    @Expose
    private int panCounter;

    public String getDocuMassage() {
        return docuMassage;
    }

    public void setDocuMassage(String docuMassage) {
        this.docuMassage = docuMassage;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public List<AadharDocument> getAadharDocument() {
        return aadharDocument;
    }

    public void setAadharDocument(List<AadharDocument> aadharDocument) {
        this.aadharDocument = aadharDocument;
    }

    public List<PanDocument> getPanDocument() {
        return panDocument;
    }

    public void setPanDocument(List<PanDocument> panDocument) {
        this.panDocument = panDocument;
    }

    public int getAddharCounter() {
        return addharCounter;
    }

    public void setAddharCounter(int addharCounter) {
        this.addharCounter = addharCounter;
    }

    public int getPanCounter() {
        return panCounter;
    }

    public void setPanCounter(int panCounter) {
        this.panCounter = panCounter;
    }

}
