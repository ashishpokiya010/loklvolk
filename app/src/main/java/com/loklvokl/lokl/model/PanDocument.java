
package com.loklvokl.lokl.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PanDocument {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("pan_image")
    @Expose
    private String panImage;
    @SerializedName("document_approval_status")
    @Expose
    private String documentApprovalStatus;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPanImage() {
        return panImage;
    }

    public void setPanImage(String panImage) {
        this.panImage = panImage;
    }

    public String getDocumentApprovalStatus() {
        return documentApprovalStatus;
    }

    public void setDocumentApprovalStatus(String documentApprovalStatus) {
        this.documentApprovalStatus = documentApprovalStatus;
    }

}
