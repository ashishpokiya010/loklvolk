
package com.loklvokl.lokl.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProductColor {

    @SerializedName("color_name")
    @Expose
    private String color_name;


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @SerializedName("code")
    @Expose
    private String code;

    public String getColor() {
        return color_name;
    }

    public void setColor(String color) {
        this.color_name = color_name;
    }

}
