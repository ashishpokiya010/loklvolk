
package com.loklvokl.lokl.model;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Response {

    @SerializedName("error")
    @Expose
    private Boolean error;
    @SerializedName("message")
    @Expose
    private String message = "";
    @SerializedName("user")
    @Expose
    private List<User> user = new ArrayList<>();

    public List<User> getUserdata() {
        return userdata;
    }

    public void setUserdata(List<User> userdata) {
        this.userdata = userdata;
    }

    @SerializedName("userdata")
    @Expose
    private List<User> userdata = new ArrayList<>();

    public Boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<User> getUser() {
        return user;
    }

    public void setUser(List<User> user) {
        this.user = user;
    }

}
