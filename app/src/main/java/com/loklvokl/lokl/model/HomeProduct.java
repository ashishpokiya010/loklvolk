
package com.loklvokl.lokl.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class HomeProduct {

    @SerializedName("home_product_title_name")
    @Expose
    private String home_product_title_name = "";

    @SerializedName("product")
    @Expose
    private List<Product> product = new ArrayList<>();

    public String getHome_product_title_name() {
        return home_product_title_name;
    }

    public void setHome_product_title_name(String home_product_title_name) {
        this.home_product_title_name = home_product_title_name;
    }

    public List<Product> getProduct() {
        return product;
    }

    public void setProduct(List<Product> product) {
        this.product = product;
    }

}
