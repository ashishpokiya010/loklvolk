
package com.loklvokl.lokl.model;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProductDetailModel {

    @SerializedName("product_detail")
    @Expose
    private List<ProductDetail> productDetail = null;

    public List<ProductDetail> getProductDetail() {
        return productDetail;
    }

    public void setProductDetail(List<ProductDetail> productDetail) {
        this.productDetail = productDetail;
    }

    public List<Product> getSimilar_product() {
        return similar_product;
    }

    public void setSimilar_product(List<Product> similar_product) {
        this.similar_product = similar_product;
    }

    @SerializedName("similar_product")
    @Expose
    private List<Product> similar_product = new ArrayList<>();

}
