package com.loklvokl.lokl.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CatBanner {

    @SerializedName("response")
    @Expose
    private List<HomeSliderRequest> homeSliderRequest = null;

    public List<HomeSliderRequest> getHomeSliderRequest() {
        return homeSliderRequest;
    }

    public void setHomeSliderRequest(List<HomeSliderRequest> homeSliderRequest) {
        this.homeSliderRequest = homeSliderRequest;
    }

}
