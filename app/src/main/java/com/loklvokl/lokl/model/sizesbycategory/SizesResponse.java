package com.loklvokl.lokl.model.sizesbycategory;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class SizesResponse{

	@SerializedName("size_details")
	private List<SizeDetailsItem> sizeDetails;

	public void setSizeDetails(List<SizeDetailsItem> sizeDetails){
		this.sizeDetails = sizeDetails;
	}

	public List<SizeDetailsItem> getSizeDetails(){
		return sizeDetails;
	}
}