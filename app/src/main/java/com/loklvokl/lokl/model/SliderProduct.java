package com.loklvokl.lokl.model;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SliderProduct {

    @SerializedName("home_slider_request")
    @Expose
    private List<HomeSliderRequest> homeSliderRequest = null;

    public List<HomeSliderRequest> getHomeSliderRequest() {
        return homeSliderRequest;
    }

    public void setHomeSliderRequest(List<HomeSliderRequest> homeSliderRequest) {
        this.homeSliderRequest = homeSliderRequest;
    }

}
