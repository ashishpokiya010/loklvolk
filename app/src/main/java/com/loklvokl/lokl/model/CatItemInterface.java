package com.loklvokl.lokl.model;

public interface CatItemInterface {

    public void ItemClick(int adapterPosition, String id, String mainCatName);

    public void SeeAllClick(int adapterPosition, String id, String mainCatName);
}
