package com.loklvokl.lokl.model.product;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class WishListModel {
    @SerializedName("wishlist")
    @Expose
    private List<Wishlist> wishlist = null;

    public List<Action> getAction() {
        return action;
    }

    public void setAction(List<Action> action) {
        this.action = action;
    }

    @SerializedName("action")
    @Expose
    private List<Action> action = null;

    public List<Wishlist> getWishlist() {
        return wishlist;
    }

    public void setWishlist(List<Wishlist> wishlist) {
        this.wishlist = wishlist;
    }
}
