package com.loklvokl.lokl.model.product;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class UpdateStockModel {

    @SerializedName("user_product")
    @Expose
    private List<UserProduct> userProduct = null;

    public List<UserProduct> getUserProduct() {
        return userProduct;
    }

    public void setUserProduct(List<UserProduct> userProduct) {
        this.userProduct = userProduct;
    }


}
