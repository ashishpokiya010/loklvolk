
package com.loklvokl.lokl.model.product;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProductColor {

    @SerializedName("color")
    @Expose
    private Object color;
    @SerializedName("colors_code")
    @Expose
    private Object colorsCode;

    public Object getColor() {
        return color;
    }

    public void setColor(Object color) {
        this.color = color;
    }

    public Object getColorsCode() {
        return colorsCode;
    }

    public void setColorsCode(Object colorsCode) {
        this.colorsCode = colorsCode;
    }

}
