
package com.loklvokl.lokl.model.CartDetail;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Cart {

    public String getPromocode() {
        return promocode;
    }

    public void setPromocode(String promocode) {
        this.promocode = promocode;
    }

    @SerializedName("promocode")
    @Expose
    private String promocode = "";

    public Integer getDiscount() {
        return discount;
    }

    public void setDiscount(Integer discount) {
        this.discount = discount;
    }

    @SerializedName("discount")
    @Expose
    private Integer discount = 0;
    @SerializedName("price")
    @Expose
    private Integer price;

    public Integer getShipppingCharge() {
        return shipppingCharge;
    }

    public void setShipppingCharge(Integer shipppingCharge) {
        this.shipppingCharge = shipppingCharge;
    }

    @SerializedName("shipping charges")
    @Expose
    private Integer shipppingCharge;
    @SerializedName("Total Amount")
    @Expose
    private Integer totalAmount = 0;
    @SerializedName("product")
    @Expose
    private List<Product> product = null;

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Integer getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Integer totalAmount) {
        this.totalAmount = totalAmount;
    }

    public List<Product> getProduct() {
        return product;
    }

    public void setProduct(List<Product> product) {
        this.product = product;
    }

}
