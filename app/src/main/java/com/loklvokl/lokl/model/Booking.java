
package com.loklvokl.lokl.model;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Booking {

    @SerializedName("booking")
    @Expose
    private List<Booking_> booking = null;

    public List<Booking_> getBooking() {
        return booking;
    }

    public void setBooking(List<Booking_> booking) {
        this.booking = booking;
    }

    public class Booking_ {

        @SerializedName("error")
        @Expose
        private Boolean error;
        @SerializedName("message")
        @Expose
        private String message;
        @SerializedName("order_id")
        @Expose
        private Integer orderId;
        @SerializedName("delivery_estimated_date")
        @Expose
        private String deliveryEstimatedDate;

        public Boolean getError() {
            return error;
        }

        public void setError(Boolean error) {
            this.error = error;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public Integer getOrderId() {
            return orderId;
        }

        public void setOrderId(Integer orderId) {
            this.orderId = orderId;
        }

        public String getDeliveryEstimatedDate() {
            return deliveryEstimatedDate;
        }

        public void setDeliveryEstimatedDate(String deliveryEstimatedDate) {
            this.deliveryEstimatedDate = deliveryEstimatedDate;
        }

    }
}

