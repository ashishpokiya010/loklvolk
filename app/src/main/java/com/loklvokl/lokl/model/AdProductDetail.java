
package com.loklvokl.lokl.model;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AdProductDetail {

    @SerializedName("Product_color")
    @Expose
    private List<ProductColor2> productColor = null;
    @SerializedName("size_details")
    @Expose
    private List<SizeDetail> sizeDetails = null;

    public List<ProductColor2> getProductColor() {
        return productColor;
    }

    public void setProductColor(List<ProductColor2> productColor) {
        this.productColor = productColor;
    }

    public List<SizeDetail> getSizeDetails() {
        return sizeDetails;
    }

    public void setSizeDetails(List<SizeDetail> sizeDetails) {
        this.sizeDetails = sizeDetails;
    }

}
