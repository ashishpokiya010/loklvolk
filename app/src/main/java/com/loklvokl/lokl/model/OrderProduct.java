
package com.loklvokl.lokl.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OrderProduct {


    @SerializedName("error")
    @Expose
    private String error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("color")
    @Expose
    private String color;
    @SerializedName("order_id")
    @Expose
    private String orderId;
    @SerializedName("product_id")
    @Expose
    private String productId;
    @SerializedName("product_name")
    @Expose
    private String productName;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("prod_qty")
    @Expose
    private String prodQty;
    @SerializedName("order_date")
    @Expose
    private String orderDate;
    @SerializedName("product_image")
    @Expose
    private String productImage;
    @SerializedName("payment_type")
    @Expose
    private String paymentType;
    @SerializedName("delivery estimatedate")
    @Expose
    private String deliveryEstimatedate;
    @SerializedName("order_status")
    @Expose
    private String orderStatus;

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    @SerializedName("rating")
    @Expose
    private String rating;

    @SerializedName("order_details_id")
    @Expose
    private String order_details_id;
    @SerializedName("user_id")
    @Expose
    private String user_id;

    public String getOrderId() {
        return orderId;
    }

    public String getOrder_details_id() {
        return order_details_id;
    }

    public void setOrder_details_id(String order_details_id) {
        this.order_details_id = order_details_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getProdQty() {
        return prodQty;
    }

    public void setProdQty(String prodQty) {
        this.prodQty = prodQty;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public String getProductImage() {
        return productImage;
    }

    public void setProductImage(String productImage) {
        this.productImage = productImage;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getDeliveryEstimatedate() {
        return deliveryEstimatedate;
    }

    public void setDeliveryEstimatedate(String deliveryEstimatedate) {
        this.deliveryEstimatedate = deliveryEstimatedate;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }


}
