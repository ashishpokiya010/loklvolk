
package com.loklvokl.lokl.model;

import java.io.Serializable;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MainCategory implements Serializable {

    @SerializedName("main_cat_id")
    @Expose
    private String mainCatId;
    @SerializedName("main_cat_name")
    @Expose
    private String mainCatName;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("cat_slider")
    @Expose
    private List<CatSlider> catSlider = null;
    @SerializedName("category")
    @Expose
    private List<Category> category = null;

    public String getMainCatId() {
        return mainCatId;
    }

    public void setMainCatId(String mainCatId) {
        this.mainCatId = mainCatId;
    }

    public String getMainCatName() {
        return mainCatName;
    }

    public void setMainCatName(String mainCatName) {
        this.mainCatName = mainCatName;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public List<CatSlider> getCatSlider() {
        return catSlider;
    }

    public void setCatSlider(List<CatSlider> catSlider) {
        this.catSlider = catSlider;
    }

    public List<Category> getCategory() {
        return category;
    }

    public void setCategory(List<Category> category) {
        this.category = category;
    }

}
