package com.loklvokl.lokl.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MySoldOrderModel {


    public String getOrder_details_id() {
        return order_details_id;
    }

    public void setOrder_details_id(String order_details_id) {
        this.order_details_id = order_details_id;
    }

    @SerializedName("order_details_id")
    @Expose
    private String order_details_id;

    @SerializedName("order_status_color")
    @Expose
    private String order_status_color;

    @SerializedName("error")
    @Expose
    private String error;

    @SerializedName("message")
    @Expose
    private String message;


    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getProd_qty() {
        return prod_qty;
    }

    public void setProd_qty(String prod_qty) {
        this.prod_qty = prod_qty;
    }

    public String getOrder_date() {
        return order_date;
    }

    public void setOrder_date(String order_date) {
        this.order_date = order_date;
    }

    public String getProduct_image() {
        return product_image;
    }

    public void setProduct_image(String product_image) {
        this.product_image = product_image;
    }

    public String getPayment_type() {
        return payment_type;
    }

    public void setPayment_type(String payment_type) {
        this.payment_type = payment_type;
    }

    public String getOrder_status() {
        return order_status;
    }

    public void setOrder_status(String order_status) {
        this.order_status = order_status;
    }

    public String getOrder_accepted_date() {
        return order_accepted_date;
    }

    public void setOrder_accepted_date(String order_accepted_date) {
        this.order_accepted_date = order_accepted_date;
    }

    public String getReady_to_dispatch() {
        return ready_to_dispatch;
    }

    public void setReady_to_dispatch(String ready_to_dispatch) {
        this.ready_to_dispatch = ready_to_dispatch;
    }

    public String getShipped_date() {
        return shipped_date;
    }

    public void setShipped_date(String shipped_date) {
        this.shipped_date = shipped_date;
    }

    public String getOrder_status_color() {
        return order_status_color;
    }

    public void setOrder_status_color(String order_status_color) {
        this.order_status_color = order_status_color;
    }

    @SerializedName("order_id")
    @Expose
    private String order_id;
    @SerializedName("product_id")
    @Expose
    private String product_id;
    @SerializedName("product_name")
    @Expose
    private String product_name;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("prod_qty")
    @Expose
    private String prod_qty;
    @SerializedName("order_date")
    @Expose
    private String order_date;
    @SerializedName("product_image")
    @Expose
    private String product_image;
    @SerializedName("payment_type")
    @Expose
    private String payment_type;
    @SerializedName("order_status")
    @Expose
    private String order_status;
    @SerializedName("order_accepted_date")
    @Expose
    private String order_accepted_date;
    @SerializedName("ready_to_dispatch")
    @Expose
    private String ready_to_dispatch;
    @SerializedName("shipped_date")
    @Expose
    private String shipped_date;

}
