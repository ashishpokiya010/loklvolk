
package com.loklvokl.lokl.model;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CollectionPro {

    @SerializedName("collection_id")
    @Expose
    private String collectionId;
    @SerializedName("product")
    @Expose
    private List<Product> product = null;

    public String getCollectionId() {
        return collectionId;
    }

    public void setCollectionId(String collectionId) {
        this.collectionId = collectionId;
    }

    public List<Product> getProduct() {
        return product;
    }

    public void setProduct(List<Product> product) {
        this.product = product;
    }

}
