package com.loklvokl.lokl.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SoldOrderDetailsModle {
    @SerializedName("order_product")
    @Expose
    private List<OrderProductSold> orderProduct = null;

    public List<OrderProductSold> getOrderProduct() {
        return orderProduct;
    }

    public void setOrderProduct(List<OrderProductSold> orderProduct) {
        this.orderProduct = orderProduct;
    }

}
