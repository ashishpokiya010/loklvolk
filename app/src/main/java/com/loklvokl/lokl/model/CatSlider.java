
package com.loklvokl.lokl.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class CatSlider implements Serializable {

    @SerializedName("cat_slider_id")
    @Expose
    private String catSliderId;
    @SerializedName("cat_slider_image")
    @Expose
    private String catSliderImage;

    public String getCatSliderId() {
        return catSliderId;
    }

    public void setCatSliderId(String catSliderId) {
        this.catSliderId = catSliderId;
    }

    public String getCatSliderImage() {
        return catSliderImage;
    }

    public void setCatSliderImage(String catSliderImage) {
        this.catSliderImage = catSliderImage;
    }

}
