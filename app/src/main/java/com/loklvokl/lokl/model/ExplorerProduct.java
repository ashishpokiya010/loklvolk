package com.loklvokl.lokl.model;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ExplorerProduct {

    @SerializedName("category_product")
    @Expose
    private List<CategoryProduct> categoryProduct = null;

    public List<CategoryProduct> getCategoryProduct() {
        return categoryProduct;
    }

    public void setCategoryProduct(List<CategoryProduct> categoryProduct) {
        this.categoryProduct = categoryProduct;
    }

}
