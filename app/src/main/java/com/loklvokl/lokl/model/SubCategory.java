
package com.loklvokl.lokl.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class SubCategory implements Serializable {

    @SerializedName("sub_cat_id")
    @Expose
    private String subCatId;
    @SerializedName("sub_cat_name")
    @Expose
    private String subCatName;
    @SerializedName("image")
    @Expose
    private String image;

    public SubCategory(String mainCatId, String mainCatName, String image) {
        this.subCatId = mainCatId;
        this.subCatName = mainCatName;
        this.image = image;
    }

    public SubCategory() {
    }

    public String getSubCatId() {
        return subCatId;
    }

    public void setSubCatId(String subCatId) {
        this.subCatId = subCatId;
    }

    public String getSubCatName() {
        return subCatName;
    }

    public void setSubCatName(String subCatName) {
        this.subCatName = subCatName;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    private String id = "1";

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
