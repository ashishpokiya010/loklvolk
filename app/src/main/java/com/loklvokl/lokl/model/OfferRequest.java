
package com.loklvokl.lokl.model;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OfferRequest {

    @SerializedName("offer_id")
    @Expose
    private String offerId;
    @SerializedName("no_page")
    @Expose
    private Integer totalPages;
    @SerializedName("bannar_image")
    @Expose
    private String bannarImage;
    @SerializedName("product")
    @Expose
    private List<Product> product = null;

    public String getOfferId() {
        return offerId;
    }

    public void setOfferId(String offerId) {
        this.offerId = offerId;
    }

    public Integer getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(Integer totalPages) {
        this.totalPages = totalPages;
    }

    public String getBannarImage() {
        return bannarImage;
    }

    public void setBannarImage(String bannarImage) {
        this.bannarImage = bannarImage;
    }

    public List<Product> getProduct() {
        return product;
    }

    public void setProduct(List<Product> product) {
        this.product = product;
    }

    public String getOffer_product_title_name() {
        return offer_product_title_name;
    }

    public void setOffer_product_title_name(String offer_product_title_name) {
        this.offer_product_title_name = offer_product_title_name;
    }

    @SerializedName("offer_product_title_name")
    @Expose
    private String offer_product_title_name;

}
