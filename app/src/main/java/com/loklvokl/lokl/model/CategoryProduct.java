package com.loklvokl.lokl.model;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CategoryProduct {

    @SerializedName("explorer_category_id")
    @Expose
    private String explorerCategoryId;
    @SerializedName("product")
    @Expose
    private List<Product> product = null;

    public String getExplorerCategoryId() {
        return explorerCategoryId;
    }

    public void setExplorerCategoryId(String explorerCategoryId) {
        this.explorerCategoryId = explorerCategoryId;
    }

    public List<Product> getProduct() {
        return product;
    }

    public void setProduct(List<Product> product) {
        this.product = product;
    }

}
