
package com.loklvokl.lokl.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class EditProfileResponse {

    @SerializedName("user_profile")
    @Expose
    public UserProfileUpdate userProfileUpdate;
    public UserProfileUpdate getUserProfile() {
        return userProfileUpdate;
    }

    public void setUserProfile(UserProfileUpdate userProfile) {
        this.userProfileUpdate = userProfile;
    }
}
