package com.loklvokl.lokl.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserOrderDetailProduct {

    @SerializedName("error")
    @Expose
    private String error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("order_details_id")
    @Expose
    private String orderDetailsId;
    @SerializedName("order_id")
    @Expose
    private String orderId;
    @SerializedName("product_id")
    @Expose
    private String productId;
    @SerializedName("product_name")
    @Expose
    private String productName;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("prod_qty")
    @Expose
    private String prodQty;
    @SerializedName("prod_size")
    @Expose
    private String prodSize;
    @SerializedName("total_price")
    @Expose
    private int totalPrice;
    @SerializedName("order_date")
    @Expose
    private String orderDate;
    @SerializedName("product_image")
    @Expose
    private String productImage;
    @SerializedName("rating")
    @Expose
    private String rating;
    @SerializedName("payment_type")
    @Expose
    private String paymentType;
    @SerializedName("delivery_estimatedate")
    @Expose
    private String deliveryEstimatedate;
    @SerializedName("shipping_charge")
    @Expose
    private int shippingCharge;
    @SerializedName("cod_charge")
    @Expose
    private int codCharge;
    @SerializedName("courier_id")
    @Expose
    private String courierId;
    @SerializedName("courier_name")
    @Expose
    private String courierName;
    @SerializedName("track_link")
    @Expose
    private String trackLink;
    @SerializedName("order_status")
    @Expose
    private String orderStatus;
    @SerializedName("order_status_color")
    @Expose
    private String order_status_color;
    @SerializedName("order_accepted_date")
    @Expose
    private String orderAcceptedDate;
    @SerializedName("ready_to_dispatch")
    @Expose
    private String readyToDispatch;
    @SerializedName("shipped_date")
    @Expose
    private String shippedDate;
    @SerializedName("shipping_address")
    @Expose
    private String shippingAddress;
    @SerializedName("track_number")
    @Expose
    private String track_number;

    @SerializedName("cancle_status")
    @Expose
    private boolean cancle_status;
    @SerializedName("return_status")
    @Expose
    private boolean return_status;

    public boolean isCancle_status() {
        return cancle_status;
    }

    public void setCancle_status(boolean cancle_status) {
        this.cancle_status = cancle_status;
    }

    public boolean isReturn_status() {
        return return_status;
    }

    public void setReturn_status(boolean return_status) {
        this.return_status = return_status;
    }


    public String getOrder_status_color() {
        return order_status_color;
    }

    public void setOrder_status_color(String order_status_color) {
        this.order_status_color = order_status_color;
    }

    public String getTrack_number() {
        return track_number;
    }

    public void setTrack_number(String track_number) {
        this.track_number = track_number;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getOrderDetailsId() {
        return orderDetailsId;
    }

    public void setOrderDetailsId(String orderDetailsId) {
        this.orderDetailsId = orderDetailsId;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getProdQty() {
        return prodQty;
    }

    public void setProdQty(String prodQty) {
        this.prodQty = prodQty;
    }

    public String getProdSize() {
        return prodSize;
    }

    public void setProdSize(String prodSize) {
        this.prodSize = prodSize;
    }

    public int getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(int totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public String getProductImage() {
        return productImage;
    }

    public void setProductImage(String productImage) {
        this.productImage = productImage;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getDeliveryEstimatedate() {
        return deliveryEstimatedate;
    }

    public void setDeliveryEstimatedate(String deliveryEstimatedate) {
        this.deliveryEstimatedate = deliveryEstimatedate;
    }

    public int getShippingCharge() {
        return shippingCharge;
    }

    public void setShippingCharge(int shippingCharge) {
        this.shippingCharge = shippingCharge;
    }

    public int getCodCharge() {
        return codCharge;
    }

    public void setCodCharge(int codCharge) {
        this.codCharge = codCharge;
    }

    public String getCourierId() {
        return courierId;
    }

    public void setCourierId(String courierId) {
        this.courierId = courierId;
    }

    public String getCourierName() {
        return courierName;
    }

    public void setCourierName(String courierName) {
        this.courierName = courierName;
    }

    public String getTrackLink() {
        return trackLink;
    }

    public void setTrackLink(String trackLink) {
        this.trackLink = trackLink;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getStatusColor() {
        return order_status_color;
    }

    public void setStatusColor(String color) {
        this.order_status_color = color;
    }

    public String getOrderAcceptedDate() {
        return orderAcceptedDate;
    }

    public void setOrderAcceptedDate(String orderAcceptedDate) {
        this.orderAcceptedDate = orderAcceptedDate;
    }

    public String getReadyToDispatch() {
        return readyToDispatch;
    }

    public void setReadyToDispatch(String readyToDispatch) {
        this.readyToDispatch = readyToDispatch;
    }

    public String getShippedDate() {
        return shippedDate;
    }

    public void setShippedDate(String shippedDate) {
        this.shippedDate = shippedDate;
    }

    public String getShippingAddress() {
        return shippingAddress;
    }

    public void setShippingAddress(String shippingAddress) {
        this.shippingAddress = shippingAddress;
    }
}
