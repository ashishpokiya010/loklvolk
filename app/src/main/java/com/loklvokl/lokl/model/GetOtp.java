package com.loklvokl.lokl.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetOtp {

    @SerializedName("response")
    private List<Response> response;

    public List<Response> getResponse() {
        return response;
    }

    public void setResponse(List<Response> response) {
        this.response = response;
    }

    public static class Response {

        @Expose
        @SerializedName("otp")
        private int otp;
        @Expose
        @SerializedName("message")
        private String message;
        @Expose
        @SerializedName("mobile")
        private String mobile;
        @Expose
        @SerializedName("error")
        private boolean error;

        public int getOtp() {
            return otp;
        }

        public void setOtp(int otp) {
            this.otp = otp;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public boolean getError() {
            return error;
        }

        public void setError(boolean error) {
            this.error = error;
        }
    }

}
