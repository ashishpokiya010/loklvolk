
package com.loklvokl.lokl.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class OfferProductHome {

    @SerializedName("offer_product")
    @Expose
    private List<OfferRequest> offerRequest = null;

    public List<OfferRequest> getOfferRequest() {
        return offerRequest;
    }

    public void setOfferRequest(List<OfferRequest> offerRequest) {
        this.offerRequest = offerRequest;
    }

}
