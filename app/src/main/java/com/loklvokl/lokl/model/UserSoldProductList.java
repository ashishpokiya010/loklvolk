package com.loklvokl.lokl.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class UserSoldProductList {

    @SerializedName("order_product")
    @Expose
    private List<MySoldOrderModel> soldorderProduct = null;

    public List<MySoldOrderModel> getOrderProduct() {
        return soldorderProduct;
    }

    public void setOrderProduct(List<MySoldOrderModel> orderProduct) {
        this.soldorderProduct = orderProduct;
    }
}
