package com.loklvokl.lokl.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OrderProductSold {
    @SerializedName("error")
    @Expose
    private String error;
    @SerializedName("message")
    @Expose
    private String message;


    @SerializedName("courier_name")
    @Expose
    private String courier_name;

    @SerializedName("track_number")
    @Expose
    private String track_number;
    @SerializedName("dispatch_status")
    @Expose
    private String dispatch_status;


    @SerializedName("shipping_address")
    @Expose
    private String prod_shipping_address;
    @SerializedName("prod_size")
    @Expose
    private String prod_size;
    @SerializedName("order_id")
    @Expose
    private String orderId;
    @SerializedName("product_id")
    @Expose
    private String productId;
    @SerializedName("product_name")
    @Expose
    private String productName;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("prod_qty")
    @Expose
    private String prodQty;
    @SerializedName("order_date")
    @Expose
    private String orderDate;
    @SerializedName("product_image")
    @Expose
    private String productImage;
    @SerializedName("payment_type")
    @Expose
    private String paymentType;
    @SerializedName("delivery_estimatedate")
    @Expose
    private String deliveryEstimatedate;
    @SerializedName("order_status")
    @Expose
    private String orderStatus;
    @SerializedName("order_status_color")
    @Expose
    private String orderStatusColor;
    @SerializedName("order_accepted_date")
    @Expose
    private String orderAcceptedDate;
    @SerializedName("ready_to_dispatch")
    @Expose
    private String readyToDispatch;
    @SerializedName("shipped_date")
    @Expose
    private String shippedDate;

    public String getCourier_name() {
        return courier_name;
    }

    public void setCourier_name(String courier_name) {
        this.courier_name = courier_name;
    }

    public String getTrack_number() {
        return track_number;
    }

    public void setTrack_number(String track_number) {
        this.track_number = track_number;
    }

    public String getDispatch_status() {
        return dispatch_status;
    }

    public void setDispatch_status(String dispatch_status) {
        this.dispatch_status = dispatch_status;
    }

    public String getProd_size() {
        return prod_size;
    }

    public void setProd_size(String prod_size) {
        this.prod_size = prod_size;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProd_shipping_address() {
        return prod_shipping_address;
    }

    public void setProd_shipping_address(String prod_shipping_address) {
        this.prod_shipping_address = prod_shipping_address;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getProdQty() {
        return prodQty;
    }

    public void setProdQty(String prodQty) {
        this.prodQty = prodQty;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public String getProductImage() {
        return productImage;
    }

    public void setProductImage(String productImage) {
        this.productImage = productImage;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getDeliveryEstimatedate() {
        return deliveryEstimatedate;
    }

    public void setDeliveryEstimatedate(String deliveryEstimatedate) {
        this.deliveryEstimatedate = deliveryEstimatedate;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getOrderStatusColor() {
        return orderStatusColor;
    }

    public void setOrderStatusColor(String orderStatusColor) {
        this.orderStatusColor = orderStatusColor;
    }

    public String getOrderAcceptedDate() {
        return orderAcceptedDate;
    }

    public void setOrderAcceptedDate(String orderAcceptedDate) {
        this.orderAcceptedDate = orderAcceptedDate;
    }

    public String getReadyToDispatch() {
        return readyToDispatch;
    }

    public void setReadyToDispatch(String readyToDispatch) {
        this.readyToDispatch = readyToDispatch;
    }

    public String getShippedDate() {
        return shippedDate;
    }

    public void setShippedDate(String shippedDate) {
        this.shippedDate = shippedDate;
    }
}
