package com.loklvokl.lokl.model.searchsuggestion;

import com.google.gson.annotations.SerializedName;

public class SearchItems {

	@SerializedName("data")
	private String data;

	public void setData(String data){
		this.data = data;
	}

	public String getData(){
		return data;
	}
}