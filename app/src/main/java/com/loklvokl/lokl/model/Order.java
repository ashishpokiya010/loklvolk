package com.loklvokl.lokl.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Order implements Serializable {

    @SerializedName("user_id")
    String user_id;

    @SerializedName("address_id")
    String address_id;

    @SerializedName("total_qty")
    String total_qty;

    @SerializedName("price")
    String price;

    @SerializedName("discount")
    String discount;

    @SerializedName("total_price")
    String total_price;

    @SerializedName("payment_type")
    String payment_type;

    @SerializedName("device_id")
    String device_id;


    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getAddress_id() {
        return address_id;
    }

    public void setAddress_id(String address_id) {
        this.address_id = address_id;
    }

    public String getTotal_qty() {
        return total_qty;
    }

    public void setTotal_qty(String total_qty) {
        this.total_qty = total_qty;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getTotal_price() {
        return total_price;
    }

    public void setTotal_price(String total_price) {
        this.total_price = total_price;
    }

    public String getPayment_type() {
        return payment_type;
    }

    public void setPayment_type(String payment_type) {
        this.payment_type = payment_type;
    }

    public String getDevice_id() {
        return device_id;
    }

    public void setDevice_id(String device_id) {
        this.device_id = device_id;
    }


    public String getShipppingCharge() {
        return shipppingCharge;
    }

    public void setShipppingCharge(String shipppingCharge) {
        this.shipppingCharge = shipppingCharge;
    }

    String shipppingCharge;

    public String getPromo_code() {
        return promo_code;
    }

    public void setPromo_code(String promo_code) {
        this.promo_code = promo_code;
    }

    String promo_code = "";

}
