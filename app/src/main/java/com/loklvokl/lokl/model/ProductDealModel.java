
package com.loklvokl.lokl.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ProductDealModel {

    @SerializedName("productdeal")
    @Expose
    private List<Productdeal> productdeal = null;

    public List<Productdeal> getProductdeal() {
        return productdeal;
    }

    public void setProductdeal(List<Productdeal> productdeal) {
        this.productdeal = productdeal;
    }

}
