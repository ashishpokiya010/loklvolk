
package com.loklvokl.lokl.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProductMoreDetails {

    @SerializedName("generic_name")
    @Expose
    private String generic_name;

    @SerializedName("posted_by")
    @Expose
    private String posted_by;

    @SerializedName("condonation")
    @Expose
    private String condonation;

    @SerializedName("country_of_region")
    @Expose
    private String country_of_region;

    public String getGeneric_name() {
        return generic_name;
    }

    public void setGeneric_name(String generic_name) {
        this.generic_name = generic_name;
    }

    public String getPosted_by() {
        return posted_by;
    }

    public void setPosted_by(String posted_by) {
        this.posted_by = posted_by;
    }

    public String getCondonation() {
        return condonation;
    }

    public void setCondonation(String condonation) {
        this.condonation = condonation;
    }

    public String getCountry_of_region() {
        return country_of_region;
    }

    public void setCountry_of_region(String country_of_region) {
        this.country_of_region = country_of_region;
    }
}
