
package com.loklvokl.lokl.model;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Productdeal {

    @SerializedName("title")
    @Expose
    private String title = "";

    @SerializedName("bg_color")
    @Expose
    private String bg_color = "#FFFFFF";

    @SerializedName("id")
    @Expose
    private String bannar_id = "-1";

    @SerializedName("no_page")
    @Expose
    private Integer totalPages;

    @SerializedName("bannar_image")
    @Expose
    private String bannar_image = "";

    @SerializedName("product")
    @Expose
    private List<Product> product = new ArrayList<>();

    public String getBannar_id() {
        return bannar_id;
    }

    public void setBannar_id(String bannar_id) {
        this.bannar_id = bannar_id;
    }

    public String getBg_color() {
        return bg_color;
    }

    public void setBg_color(String bg_color) {
        this.bg_color = bg_color;
    }

    public String getBannar_image() {
        return bannar_image;
    }

    public void setBannar_image(String bannar_image) {
        this.bannar_image = bannar_image;
    }

    public Integer getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(Integer totalPages) {
        this.totalPages = totalPages;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<Product> getProduct() {
        return product;
    }

    public void setProduct(List<Product> product) {
        this.product = product;
    }

}
