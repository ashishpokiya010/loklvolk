package com.loklvokl.lokl.activtiy;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginBehavior;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.loklvokl.lokl.R;
import com.loklvokl.lokl.model.DysplayProfileModel;
import com.loklvokl.lokl.model.SignUpResponse;
import com.loklvokl.lokl.model.User;
import com.loklvokl.lokl.model.UserProfile;
import com.loklvokl.lokl.util.AnalyticsApplication;
import com.loklvokl.lokl.util.ApiClient;
import com.loklvokl.lokl.util.ApiInterface;
import com.loklvokl.lokl.util.Constant;
import com.loklvokl.lokl.util.SharedPref;
import com.truecaller.android.sdk.ITrueCallback;
import com.truecaller.android.sdk.TrueError;
import com.truecaller.android.sdk.TrueException;
import com.truecaller.android.sdk.TrueProfile;
import com.truecaller.android.sdk.TruecallerSDK;
import com.truecaller.android.sdk.TruecallerSdkScope;
import com.truecaller.android.sdk.clients.VerificationCallback;
import com.truecaller.android.sdk.clients.VerificationDataBundle;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    private static final int RC_SIGN_IN = 123;
    TextView tv_sign_up, btn_login, txt_forgotpassword;

    EditText ed_mobile, ed_password;
    ProgressBar progressBar;
    ApiInterface apiService;
    ApiClient apiClient;
    SharedPref sharedPref;
    LoginButton loginButton;
    CallbackManager callbackManager;
    CardView googleButton, facebookButton;
    GoogleSignInOptions gso;
    GoogleSignInClient mGoogleSignInClient;
    SignInButton sign_in_button;
    Tracker mTracker;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);


        gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

        sharedPref = new SharedPref(this);
        apiClient = new ApiClient();
        apiService = ApiClient.getClient().create(ApiInterface.class);
        init();

        AnalyticsApplication application = (AnalyticsApplication) getApplication();
        mTracker = application.getDefaultTracker();
    }


    private boolean idValid(String email) {
        Pattern pattern = Patterns.EMAIL_ADDRESS;
        return pattern.matcher(email).matches();
    }

    public void init() {
        tv_sign_up = findViewById(R.id.btn_sign);
        tv_sign_up.setOnClickListener(this);

        txt_forgotpassword = findViewById(R.id.txt_forgotpassword);
        txt_forgotpassword.setOnClickListener(this);
        btn_login = findViewById(R.id.btn_login);
        btn_login.setOnClickListener(this);

        progressBar = findViewById(R.id.progressBar);
        ed_mobile = findViewById(R.id.ed_mobile);
        ed_password = findViewById(R.id.ed_password);
        loginButton = findViewById(R.id.loginButton);
        loginButton.setLoginBehavior(LoginBehavior.WEB_ONLY);

        loginButton.setOnClickListener(this);
        sign_in_button = findViewById(R.id.sign_in_button);

        googleButton = findViewById(R.id.googleButton);
        googleButton.setOnClickListener(this);
        facebookButton = findViewById(R.id.facebookButton);
        facebookButton.setOnClickListener(this);

        callbackManager = CallbackManager.Factory.create();
        Log.d("API123", " start");
        loginButton.setPermissions(Arrays.asList("email", "public_profile"));
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                // App code
                //loginResult.getAccessToken();
                //loginResult.getRecentlyDeniedPermissions()
                //loginResult.getRecentlyGrantedPermissions()
                boolean loggedIn = AccessToken.getCurrentAccessToken() == null;

                getUserProfile(AccessToken.getCurrentAccessToken());
                Log.e("log_fberror0", ">succ");


            }

            @Override
            public void onCancel() {
                // App codes
                Log.e("log_fberror1", ">cancel");
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
                Log.e("log_fberror2", ">" + exception);

            }
        });
    }

    @Override
    public void onClick(View view) {
        if (view == tv_sign_up) {
            Intent i = new Intent(LoginActivity.this, SignupActivity.class);
            startActivityForResult(i, 11);
        } else if (view == btn_login) {
            performSignIn();

        } else if (view == txt_forgotpassword) {
            Intent i = new Intent(LoginActivity.this, ForgotActivity.class);
            startActivity(i);
        } else if (view == loginButton) {
            loginButton.setPermissions(Arrays.asList("email", "public_profile"));
//            loginButton.setReadPermissions(Arrays.asList("email", "public_profile", "user_mobile_phone", "mobile_phone"));

        } else if (view == facebookButton) {
            loginButton.performClick();

        } else if (view == googleButton) {
//            sign_in_button.performClick();
            signIn();
        }
    }

    private void signIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        TruecallerSDK.getInstance().onActivityResultObtained(this, resultCode, data);

        callbackManager.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 11) {
            if (resultCode == RESULT_OK) {
                setResult(RESULT_OK);
                finish();
            }
        } else if (requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);

            // Signed in successfully, show authenticated UI.
            updateUI(account);
        } catch (ApiException e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.e("APITAG", "signInResult:failed code=" + e.getStatusCode());
            updateUI(null);
        }
    }

    private void updateUI(GoogleSignInAccount account) {
        if (account != null) {
            Log.e("APITAG", ">" + account.getDisplayName());

            loginWithSocial(account.getDisplayName(), account.getEmail(), account.getId(), "", false);

        }
    }

    private void getUserProfile(AccessToken currentAccessToken) {
        GraphRequest request = GraphRequest.newMeRequest(
                currentAccessToken, new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        Log.e("TAG", object.toString());
                        try {
                            String first_name = object.getString("first_name");
                            String last_name = object.getString("last_name");
                            String email = object.getString("email");
                            String id = object.getString("id");
                            String image_url = "https://graph.facebook.com/" + id + "/picture?type=normal";

//                            txtUsername.setText("First Name: " + first_name + "\nLast Name: " + last_name);
//                            txtEmail.setText(email);
//                            Picasso.with(MainActivity.this).load(image_url).into(imageView);


                            String name = first_name + " " + last_name;
                            loginWithSocial(name, email, id, image_url, true);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                });

        Bundle parameters = new Bundle();
        parameters.putString("fields", "first_name,last_name,email,id");
        request.setParameters(parameters);
        request.executeAsync();

    }

    private void performSignIn() {
        String mobile = ed_mobile.getText().toString();
        String pass = ed_password.getText().toString();

        if (mobile.isEmpty()) {
            Toast.makeText(this, "Mobile is required.", Toast.LENGTH_SHORT).show();
        } else if (mobile.length() != 10) {
            Toast.makeText(this, "Mobile is not valid.", Toast.LENGTH_SHORT).show();
        } else if (pass.isEmpty()) {
            Toast.makeText(this, "Password is required.", Toast.LENGTH_SHORT).show();
        } else {
            progressBar.setVisibility(View.VISIBLE);

            apiService = apiClient.getClient().create(ApiInterface.class);

            Call<SignUpResponse> call = apiService.requestLogin(mobile, pass, Constant.getDeviceId(this));
            apiClient.enqueue(call, new Callback<SignUpResponse>() {
                @Override
                public void onResponse(Call<SignUpResponse> call, Response<SignUpResponse> response) {
                    Log.e("TAG", "data: " + response.body());
                    //progressBar.setVisibility(View.GONE);
                    SignUpResponse body = response.body();

                    if (body.getResponse() != null && body.getResponse().size() > 0) {
                        if (!body.getResponse().get(0).getError()) {
                            User user = body.getResponse().get(0).getUser().get(0);
                            displayProfile(user.getId(), body.getResponse().get(0).getMessage());

                            sharedPref.setStr("MOBILE", user.getMobile());
                            //sharedPref.setStr("NAME", user.getUsername());
                            //sharedPref.setStr("LNAME", user.getLast_name());
                            //sharedPref.setStr("EMAIL", user.getEmail());
                            //sharedPref.setStr("IMAGE", user.getProfile_image());
                            //sharedPref.setStr("ID", user.getId());
                            //sharedPref.setBool("IS_LOGIN", true);
                            //sharedPref.setStr("CITY", user.getCity());
                            //sharedPref.setStr("GENDER", user.getGender());
                            //sharedPref.setStr("DOB", user.getDob());
                            //setResult(RESULT_OK);
                            //finish();

                            // for analitics
                            mTracker.setScreenName("Login Screen");
                            mTracker.send(new HitBuilders.ScreenViewBuilder().build());
                            mTracker.send(new HitBuilders.EventBuilder()
                                    .setCategory("Login")
                                    .setAction("Login")
                                    .build());
                        } else {
                            progressBar.setVisibility(View.GONE);
                            Toast.makeText(LoginActivity.this, body.getResponse().get(0).getMessage(),
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                }

                @Override
                public void onFailure(Call<SignUpResponse> call, Throwable t) {
                    // Log error here since request failed
                    Log.e("TAG", t.toString());
                }
            });
        }
    }

    private void displayProfile(String user_id, String message) {
        String mobile = ed_mobile.getText().toString();
        String pass = ed_password.getText().toString();

        if (mobile.isEmpty()) {
            Toast.makeText(this, "Mobile is required.", Toast.LENGTH_SHORT).show();
        } else if (mobile.length() != 10) {
            Toast.makeText(this, "Mobile is not valid.", Toast.LENGTH_SHORT).show();
        } else if (pass.isEmpty()) {
            Toast.makeText(this, "Password is required.", Toast.LENGTH_SHORT).show();
        } else {
            //progressBar.setVisibility(View.VISIBLE);

            apiService = apiClient.getClient().create(ApiInterface.class);

            // Call<DysplayProfileModel> call = apiService.diplayProfile("93");
            Call<DysplayProfileModel> call = apiService.diplayProfile(user_id);

            apiClient.enqueue(call, new Callback<DysplayProfileModel>() {
                @Override
                public void onResponse(Call<DysplayProfileModel> call, Response<DysplayProfileModel> response) {
                    Log.e("TAG", "data: " + response.body());
                    progressBar.setVisibility(View.GONE);
                    DysplayProfileModel body = response.body();

                    if (body.getUserProfile() != null) {
                        if (body.getUserProfile().getUserId() != null) {
                            Toast.makeText(LoginActivity.this, message, Toast.LENGTH_SHORT).show();
                            UserProfile user = body.getUserProfile();
                            sharedPref.setStr("MOBILE", user.getMobile());
                            sharedPref.setStr("NAME", user.getUsername());
                            sharedPref.setStr("LNAME", user.getLastName());
                            sharedPref.setStr("EMAIL", user.getEmail());
                            sharedPref.setStr("IMAGE", user.getProfileImage());
                            sharedPref.setStr("ID", user.getUserId());
                            sharedPref.setBool("IS_LOGIN", true);
                            sharedPref.setStr("CITY", user.getCity());
                            sharedPref.setStr("GENDER", user.getGender());
                            sharedPref.setStr("DOB", user.getDob());
                            sharedPref.setStr("FNAME", user.getFirstName());
                            sharedPref.setAdharList("ADHAR_LIST", user.getAadharDocument());
                            sharedPref.setPanList("PAN_LIST", user.getPanDocument());
                            sharedPref.setInt("PAN_COUNT", user.getPan_counter());
                            sharedPref.setInt("ADHAR_COUNT", user.getAddhar_counter());
                            // sharedPref.set("FNAME", user.getKycDetails().get);
                            setResult(RESULT_OK);
                            finish();
                        }
                    }
                }

                @Override
                public void onFailure(Call<DysplayProfileModel> call, Throwable t) {
                    // Log error here since request failed
                    Log.e("TAG", t.toString());
                }
            });
        }
    }


    private void loginWithSocial(String name, String email, String id, String image_url, Boolean isFb) {
        progressBar.setVisibility(View.VISIBLE);

        apiService = apiClient.getClient().create(ApiInterface.class);

        Call<SignUpResponse> call = isFb ? apiService.requestSocial(name, email, id, "", Constant.getDeviceId(this)) :
                apiService.requestSocial(name, email, "", id, Constant.getDeviceId(this));

        apiClient.enqueue(call, new Callback<SignUpResponse>() {
            @Override
            public void onResponse(Call<SignUpResponse> call, Response<SignUpResponse> response) {
                Log.e("TAG", "data: " + response.body());
                progressBar.setVisibility(View.GONE);
                SignUpResponse body = response.body();

                if (body.getResponse() != null && body.getResponse().size() > 0) {
                    if (!body.getResponse().get(0).getError()) {
                        User user = body.getResponse().get(0).getUser().get(0);
                        sharedPref.setStr("MOBILE", user.getMobile());
                        sharedPref.setStr("NAME", user.getUsername());
                        sharedPref.setStr("LNAME", user.getLast_name());
                        sharedPref.setStr("EMAIL", user.getEmail());
                        sharedPref.setStr("IMAGE", user.getProfile_image());
                        sharedPref.setStr("ID", user.getId());
                        sharedPref.setBool("IS_LOGIN", true);
                        sharedPref.setStr("CITY", user.getCity());
                        sharedPref.setStr("GENDER", user.getGender());
                        sharedPref.setStr("DOB", user.getDob());
                        setResult(RESULT_OK);
                        finish();
                    }
                    Toast.makeText(LoginActivity.this, body.getResponse().get(0).getMessage(), Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<SignUpResponse> call, Throwable t) {
                // Log error here since request failed
                Log.e("TAG", t.toString());
            }
        });
    }
}
