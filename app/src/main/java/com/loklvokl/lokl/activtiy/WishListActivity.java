package com.loklvokl.lokl.activtiy;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.loklvokl.lokl.R;
import com.loklvokl.lokl.adapter.WishListAdapter;
import com.loklvokl.lokl.model.product.WishListModel;
import com.loklvokl.lokl.model.product.Wishlist;
import com.loklvokl.lokl.util.ApiClient;
import com.loklvokl.lokl.util.ApiInterface;
import com.loklvokl.lokl.util.Constant;
import com.loklvokl.lokl.util.SharedPref;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WishListActivity extends BaseActivity implements View.OnClickListener, WishListAdapter.onDeleteClick {

    ImageView back_btn;
    TextView title_text;
    ProgressBar progressBar;
    ApiClient apiClient;
    SharedPref sharedPref;
    ApiInterface apiService;
    RecyclerView recycle_view;
    WishListAdapter wishlistAdapter;
    //EditText searchView;
    List<Wishlist> wishlistarray = new ArrayList<>();
    LinearLayout list_layout, no_data_txt;
    ImageView imgDetailCart;
    public static String userId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.wishlist_activity);

        sharedPref = new SharedPref(this);
        apiClient = new ApiClient();
        apiService = ApiClient.getClient().create(ApiInterface.class);
        userId = sharedPref.getStr("ID");

        init();
        loadDAta();
    }

    private void init() {
        findViewById(R.id.img_detail_noti).setVisibility(View.GONE);
        findViewById(R.id.img_detail_wishlist).setVisibility(View.GONE);

        title_text = findViewById(R.id.title_text);
        title_text.setText("My Wishlist");
        back_btn = findViewById(R.id.back_btn);
        back_btn.setOnClickListener(this);
        progressBar = findViewById(R.id.progressBar);
        // searchView = findViewById(R.id.searchView);
        recycle_view = findViewById(R.id.recycle_view);
        list_layout = findViewById(R.id.list_layout);
        no_data_txt = findViewById(R.id.list_no_data_found);
        imgDetailCart = findViewById(R.id.img_detail_cart);
        imgDetailCart.setOnClickListener(this);

        recycle_view.setLayoutManager(new GridLayoutManager(this, 1));

    }


    private void loadDAta() {
        progressBar.setVisibility(View.VISIBLE);

        apiService = apiClient.getClient().create(ApiInterface.class);
        //Call<WishListModel> call = apiService.getWishList("124");
        Call<WishListModel> call = apiService.getWishList(sharedPref.getStr("ID"), Constant.getDeviceId(this));

        apiClient.enqueue(call, new Callback<WishListModel>() {
            @Override
            public void onResponse(Call<WishListModel> call, Response<WishListModel> response) {
                progressBar.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    if (response != null) {
                        WishListModel detailModel = response.body();
                        wishlistarray.addAll(detailModel.getWishlist());
                        if (wishlistarray.size() > 0) {
                            wishlistAdapter = new WishListAdapter(WishListActivity.this, wishlistarray,
                                    WishListActivity.this);
                            recycle_view.setAdapter(wishlistAdapter);
                            no_data_txt.setVisibility(View.GONE);
                            list_layout.setVisibility(View.VISIBLE);
                        } else {
                            no_data_txt.setVisibility(View.VISIBLE);
                            list_layout.setVisibility(View.GONE);
                        }
                        System.out.println("SIISISIS" + wishlistarray.size());
                    } else {
                        // Constant.toast(getApplicationContext(), response.body().getWishlist().get(0).get);
                        no_data_txt.setVisibility(View.VISIBLE);
                        list_layout.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onFailure(Call<WishListModel> call, Throwable t) {
                // Log error here since request failed
            }
        });
    }

    private void deleteData(String productId) {
        progressBar.setVisibility(View.VISIBLE);
        wishlistarray = new ArrayList<>();
        apiService = apiClient.getClient().create(ApiInterface.class);
        //Call<WishListModel> call = apiService.getWishList("124");
        Call<WishListModel> call = apiService.deleteWishListItem(sharedPref.getStr("ID"), Constant.getDeviceId(this),
                "delete", productId);

        apiClient.enqueue(call, new Callback<WishListModel>() {
            @Override
            public void onResponse(Call<WishListModel> call, Response<WishListModel> response) {
                progressBar.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    if (response != null) {
                        WishListModel detailModel = response.body();
                        wishlistarray.addAll(detailModel.getWishlist());
                        if (wishlistarray.size() > 0) {
                            wishlistAdapter = new WishListAdapter(WishListActivity.this, wishlistarray,
                                    WishListActivity.this);
                            recycle_view.setAdapter(wishlistAdapter);
                            no_data_txt.setVisibility(View.GONE);
                            list_layout.setVisibility(View.VISIBLE);
                        } else {
                            no_data_txt.setVisibility(View.VISIBLE);
                            list_layout.setVisibility(View.GONE);
                        }
                        System.out.println("SIISISIS" + wishlistarray.size());
                    } else {
                        // Constant.toast(getApplicationContext(), response.body().getWishlist().get(0).get);
                        no_data_txt.setVisibility(View.VISIBLE);
                        list_layout.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onFailure(Call<WishListModel> call, Throwable t) {
                // Log error here since request failed
            }
        });
    }

    @Override
    public void onClick(View view) {
        if (view == back_btn) {
            finish();
        }
        if(view == imgDetailCart){
            Intent intent = new Intent(this, CartActivity.class);
            startActivity(intent);
        }
    }


    @Override
    public void onDeleteClick(Wishlist wishlist) {
        if (wishlist != null) {
            deleteData(wishlist.getId());
        }
    }
}