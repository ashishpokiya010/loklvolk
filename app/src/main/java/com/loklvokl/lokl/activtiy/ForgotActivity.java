package com.loklvokl.lokl.activtiy;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.loklvokl.lokl.R;
import com.loklvokl.lokl.model.SignUpResponse;
import com.loklvokl.lokl.model.User;
import com.loklvokl.lokl.util.ApiClient;
import com.loklvokl.lokl.util.ApiInterface;
import com.loklvokl.lokl.util.SharedPref;

import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgotActivity extends AppCompatActivity implements View.OnClickListener {
    TextView tv_sign_up, btn_login;

    EditText ed_mobile;
    ProgressBar progressBar;
    ApiInterface apiService;
    ApiClient apiClient;


    SharedPref sharedPref;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        setContentView(R.layout.forgot_activity);

        sharedPref = new SharedPref(this);
        apiClient = new ApiClient();
        apiService = ApiClient.getClient().create(ApiInterface.class);

        init();
    }


    private void performSignIn() {
        String mobile = ed_mobile.getText().toString();

        if (mobile.isEmpty()) {
            Toast.makeText(this, "Mobile is required.", Toast.LENGTH_SHORT).show();
        } else {
            progressBar.setVisibility(View.VISIBLE);

            apiService = apiClient.getClient().create(ApiInterface.class);

            Call<SignUpResponse> call = apiService.requestLogin(mobile, "", "");
            apiClient.enqueue(call, new Callback<SignUpResponse>() {
                @Override
                public void onResponse(Call<SignUpResponse> call, Response<SignUpResponse> response) {
                    Log.d("TAG", "data: " + response.body());
                    progressBar.setVisibility(View.GONE);
                    SignUpResponse body = response.body();

                    if (body.getResponse() != null && body.getResponse().size() > 0) {

                        Toast.makeText(ForgotActivity.this, body.getResponse().get(0).getMessage(), Toast.LENGTH_SHORT).show();
                        if (!body.getResponse().get(0).getError()) {
                            User user = body.getResponse().get(0).getUser().get(0);
                            sharedPref.setStr("MOBILE", user.getMobile());
                            sharedPref.setStr("NAME", user.getUsername());
                            sharedPref.setStr("LNAME", user.getLast_name());
                            sharedPref.setStr("IMAGE", user.getProfile_image());
                            sharedPref.setStr("EMAIL", user.getEmail());
                            sharedPref.setStr("ID", user.getId());
                            sharedPref.setStr("CITY", user.getCity());
                            sharedPref.setStr("GENDER", user.getGender());
                            sharedPref.setStr("DOB", user.getDob());
                        }
                    }
                }

                @Override
                public void onFailure(Call<SignUpResponse> call, Throwable t) {
                    // Log error here since request failed
                    Log.e("TAG", t.toString());
                }
            });

        }

    }

    private boolean idValid(String email) {
        Pattern pattern = Patterns.EMAIL_ADDRESS;
        return pattern.matcher(email).matches();
    }

    public void init() {
        tv_sign_up = findViewById(R.id.btn_sign);
        tv_sign_up.setOnClickListener(this);

        btn_login = findViewById(R.id.btn_login);
        btn_login.setOnClickListener(this);

        progressBar = findViewById(R.id.progressBar);
        ed_mobile = findViewById(R.id.ed_mobile);
    }

    @Override
    public void onClick(View view) {
        if (view == tv_sign_up) {
            finish();
//            Intent i = new Intent(ForgotActivity.this, SignupActivity.class);
//            startActivity(i);
        } else if (view == btn_login) {
//            performSignIn();
            Intent i = new Intent(ForgotActivity.this, ForgotActivity2.class);
            startActivity(i);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
