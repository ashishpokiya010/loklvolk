package com.loklvokl.lokl.activtiy;

import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.loklvokl.lokl.R;
import com.loklvokl.lokl.adapter.ProductDealAdapter;
import com.loklvokl.lokl.model.ProductDealModel;
import com.loklvokl.lokl.model.Product;
import com.loklvokl.lokl.util.ApiClient;
import com.loklvokl.lokl.util.ApiInterface;
import com.loklvokl.lokl.util.EndlessRecyclerOnScrollListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DealListActivity extends BaseActivity implements View.OnClickListener {

    private static final String TAG = "Product_list";
    RecyclerView recyclerView;
    ProgressBar progressBar;
    StaggeredGridLayoutManager staggeredGridLayoutManager;
    ProductDealAdapter productDealAdapter;
    List<Product> products = new ArrayList<>();
    String productId = "0";
    int currentPage = 1, totalPages;
    int cartCount;
    TextView empty_text, title_text;
    ImageView back_btn;
    ImageView imgDealsSearch, imgDealsCart, imgDealsWishlist;
    RelativeLayout layout_sort;
    TextView txtSelectedSort;
    ApiInterface apiService;
    Boolean isCheckedChange = false;
    String selectedSort = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_list);
        apiService = ApiClient.getClient().create(ApiInterface.class);

        init();
        getINtentData();
        loadProductData(false);

    }

    private void getINtentData() {
        productId = getIntent().getStringExtra("BANNER_ID");
        cartCount = sharedPref.getInt("CART_COUNT");
        if (getIntent().hasExtra("TITLE")) {
            title_text.setText(getIntent().getStringExtra("TITLE"));
        }
    }

    private void loadProductData(Boolean isSort) {
        progressBar.setVisibility(View.VISIBLE);
        layout_sort.setVisibility(View.GONE);

        Map<String, String> data = new HashMap<>();
        data.put("deal_id", productId);
        data.put("pageno", String.valueOf(currentPage));
        if(!selectedSort.equals("popular")) {
            data.put(selectedSort, "");
        }
        Call<ProductDealModel> call = apiService.getDealData(data);
        call.enqueue(new Callback<ProductDealModel>() {
            @Override
            public void onResponse(Call<ProductDealModel> call, Response<ProductDealModel> response) {
                Log.d(TAG, "data: " + response.body());
                if (isSort){
                    products.clear();
                }
                progressBar.setVisibility(View.GONE);
                ProductDealModel sliderRequest = response.body();

                if (sliderRequest != null) {
                    products.addAll(sliderRequest.getProductdeal().get(0).getProduct());
                    totalPages = sliderRequest.getProductdeal().get(0).getTotalPages();
                }
                if (products != null && products.size() > 0) {
                    empty_text.setVisibility(View.GONE);
                    layout_sort.setVisibility(View.VISIBLE);
                    if(productDealAdapter == null) {
                        setProductData();
                    } else {
                        if(isSort) {
                            //On sorting of data
                            productDealAdapter.notifyDataSetChanged();
                        } else {
                            //On load more data
                            productDealAdapter.notifyItemRangeChanged(0, products.size());
                        }
                    }
                } else {
                    empty_text.setVisibility(View.VISIBLE);
                    layout_sort.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<ProductDealModel> call, Throwable t) {
                // Log error here since request failed
                Log.e(TAG, t.toString());
                empty_text.setVisibility(View.VISIBLE);
                layout_sort.setVisibility(View.GONE);
            }
        });
    }

    private void setProductData() {

        productDealAdapter = new ProductDealAdapter(this, products, false);
        recyclerView.setAdapter(productDealAdapter);
    }

    private void init() {
        findViewById(R.id.img_home_noti).setVisibility(View.GONE);

        title_text = findViewById(R.id.title_text);
        back_btn = findViewById(R.id.back_btn);
        imgDealsSearch = findViewById(R.id.img_home_search);
        imgDealsCart = findViewById(R.id.img_home_cart);
        imgDealsWishlist = findViewById(R.id.img_home_wishlist);
        back_btn.setVisibility(View.VISIBLE);

        back_btn.setOnClickListener(this);
        imgDealsCart.setOnClickListener(this);
        imgDealsSearch.setOnClickListener(this);
        imgDealsWishlist.setOnClickListener(this);

        empty_text = findViewById(R.id.empty_text);
        progressBar = findViewById(R.id.progressBar);
        recyclerView = findViewById(R.id.recyclerView);
        layout_sort = findViewById(R.id.layout_sort);
        txtSelectedSort = findViewById(R.id.txt_selected_sort);

        staggeredGridLayoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(staggeredGridLayoutManager);
        //recyclerView.setLayoutManager(new GridLayoutManager(this, 2));

        recyclerView.addOnScrollListener(new EndlessRecyclerOnScrollListener(staggeredGridLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                if(page <= totalPages) {
                    currentPage = page;
                    loadProductData(false);
                }
            }
        });

    }

    @Override
    public void onClick(View view) {
        if (view == back_btn) {
            finish();
        } else if (view == imgDealsCart) {
            Intent intent = new Intent(this, CartActivity.class);
            startActivity(intent);

        } else if (view == imgDealsSearch) {
            Intent intent = new Intent(this, SearchActivity.class);
            startActivity(intent);
        } else if (view == imgDealsWishlist) {
            Intent intent = new Intent(this, WishListActivity.class);
            startActivity(intent);
        }
    }

    public void onSortClick(View view){
        showSortDialog();
    }

    AlertDialog alertDialog;
    private void showSortDialog() {
        final AlertDialog.Builder alert = new AlertDialog.Builder(DealListActivity.this,
                R.style.AppThemeDialogAlert);
        View mView = getLayoutInflater().inflate(R.layout.popup_sort, null);
        RadioGroup rg_sort = mView.findViewById(R.id.group_product_condition);
        RadioButton rb_popular = mView.findViewById(R.id.rb_sort_popular);
        RadioButton rb_latest = mView.findViewById(R.id.rb_sort_latest);
        RadioButton rb_lowhigh = mView.findViewById(R.id.rb_sort_lowtohigh);
        RadioButton rb_hightlow = mView.findViewById(R.id.rb_sort_hightolow);

        rb_latest.setChecked(false);
        rb_lowhigh.setChecked(false);
        rb_hightlow.setChecked(false);

        alert.setView(mView);
        alertDialog = alert.create();
        alertDialog.setCanceledOnTouchOutside(true);

        Log.d("SortClick Collection: "," - - Here  - "+selectedSort);
        if(selectedSort.equalsIgnoreCase("latest-arrival")){
            rb_latest.setChecked(true);
        }else if (selectedSort.equalsIgnoreCase("low-to-high")){
            rb_lowhigh.setChecked(true);
        } else if (selectedSort.equalsIgnoreCase("high-to-low")){
            rb_hightlow.setChecked(true);
        } else if (selectedSort.equalsIgnoreCase("popular")){
            rb_popular.setChecked(true);
        }
        rg_sort.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton selectedRadioButton = mView.findViewById(checkedId);
                isCheckedChange = true;
                if(selectedRadioButton.getText().toString().contains("Latest Arrivals")) {
                    selectedSort = "latest-arrival";
                } else if (selectedRadioButton.getText().toString().contains("Low - High")){
                    selectedSort = "low-to-high";
                } else if (selectedRadioButton.getText().toString().contains("High - Low")){
                    selectedSort = "high-to-low";
                } else  if (selectedRadioButton.getText().toString().contains("Popular")){
                    selectedSort = "popular";
                }
                Log.d("SortClick Collection: "," - - Here  - "+selectedSort);
                txtSelectedSort.setText(selectedRadioButton.getText());
                alertDialog.dismiss();
                currentPage = 1;
                loadProductData(true);

            }
        });
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.show();

    }
}