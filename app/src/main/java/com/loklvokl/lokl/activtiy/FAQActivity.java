package com.loklvokl.lokl.activtiy;

import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.loklvokl.lokl.R;
import com.loklvokl.lokl.model.PrivacyModel;
import com.loklvokl.lokl.util.ApiClient;
import com.loklvokl.lokl.util.ApiInterface;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FAQActivity extends BaseActivity implements View.OnClickListener{

    private static final String TAG = "PRIVACY";
    ImageView back_btn;
    TextView textView, title_text;
    ProgressBar progressBar;
    ApiInterface apiService;
    String title = "FAQ";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_privacy);

        init();
        loadData();
    }

    private void init() {
        back_btn = findViewById(R.id.back_btn);
        back_btn.setOnClickListener(this);
        title_text = findViewById(R.id.title_text);
        textView = findViewById(R.id.textView);
        progressBar = findViewById(R.id.progressBar);

        if (getIntent().hasExtra("TITLE")) {
            title = getIntent().getStringExtra("TITLE");
        }
        title_text.setText(title);
    }

    private void loadData() {
        apiService = ApiClient.getClient().create(ApiInterface.class);

        progressBar.setVisibility(View.VISIBLE);
        Call<PrivacyModel> call = apiService.getFaq();
        call.enqueue(new Callback<PrivacyModel>() {
            @Override
            public void onResponse(Call<PrivacyModel> call, Response<PrivacyModel> response) {
                Log.d(TAG, "data: " + response);
                progressBar.setVisibility(View.GONE);
                PrivacyModel sliderRequest = response.body();

                textView.setText(Html.fromHtml(sliderRequest.getResponse().get(0).getFaq()));

            }

            @Override
            public void onFailure(Call<PrivacyModel> call, Throwable t) {
                // Log error here since request failed
                Log.e(TAG, t.toString());
            }
        });
    }

    @Override
    public void onClick(View view) {
        if (view == back_btn) {
            finish();

        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}