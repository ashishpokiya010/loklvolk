package com.loklvokl.lokl.activtiy;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.loklvokl.lokl.R;
import com.loklvokl.lokl.adapter.UserOrderAdapter;
import com.loklvokl.lokl.model.DataModel;
import com.loklvokl.lokl.model.UserOrderDetailProduct;
import com.loklvokl.lokl.model.UserOrderDetails;
import com.loklvokl.lokl.util.ApiClient;
import com.loklvokl.lokl.util.ApiInterface;
import com.loklvokl.lokl.util.Constant;
import com.loklvokl.lokl.util.SharedPref;
import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserOrderDetailActivity extends BaseActivity implements View.OnClickListener {

    ApiClient apiClient;
    SharedPref sharedPref;
    ApiInterface apiService;

    ImageView back_btn;
    ImageView image_copy, image, status_icn;

    TextView title_text;
    ProgressBar progressBar;
    RecyclerView recycle_view;
    UserOrderAdapter orderAdapter;
    LinearLayout list_layout, no_data_txt, btnCopy_tracking;
    LinearLayout layout_product_rating, layout_product_courier;

    String orderid_without_hash, tracking_link;
    String userId, order_detail_id, rating;
    TextView orderid_txt, name_txt, color_txt, size_txt, qty_txt, price_txt, status_txt;
    Button btn_cancle, btn_return, btn_track_order;
    RatingBar orderRatingBar;
    TextView courier_name_values, tracking_number_values, product_charge_values,
            delevery_charge_values, cod_charge_values, total_amount_values, shipping_adress_values;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_order_detail);

        sharedPref = new SharedPref(this);
        apiClient = new ApiClient();
        apiService = ApiClient.getClient().create(ApiInterface.class);

        if (getIntent() != null) {
            userId = sharedPref.getStr("ID");
            rating = getIntent().getStringExtra("Rating");
            order_detail_id = getIntent().getStringExtra("OrderDetailId");
        }
        init();
        loadData();
    }

    private void init() {
        findViewById(R.id.img_detail_noti).setVisibility(View.GONE);
        findViewById(R.id.img_detail_wishlist).setVisibility(View.GONE);
        findViewById(R.id.img_detail_cart).setVisibility(View.GONE);

        title_text = findViewById(R.id.title_text);
        orderid_txt = findViewById(R.id.orderid_txt);
        image_copy = findViewById(R.id.image_copy);
        image = findViewById(R.id.image);
        status_icn = findViewById(R.id.status_icn);
        name_txt = findViewById(R.id.name_txt);
        color_txt = findViewById(R.id.color_txt);
        size_txt = findViewById(R.id.size_txt);
        size_txt = findViewById(R.id.size_txt);
        qty_txt = findViewById(R.id.qty_txt);
        price_txt = findViewById(R.id.price_txt);
        status_txt = findViewById(R.id.status_txt);

        btn_cancle = findViewById(R.id.btn_cancle);
        btn_return = findViewById(R.id.btn_return);
        orderRatingBar = findViewById(R.id.orderRatingBar);
        btnCopy_tracking = findViewById(R.id.btnCopy_tracking);
        courier_name_values = findViewById(R.id.courier_name_values);
        tracking_number_values = findViewById(R.id.tracking_number_values);
        btn_track_order = findViewById(R.id.btn_track_order);

        layout_product_rating = findViewById(R.id.layout_product_rating);
        layout_product_courier = findViewById(R.id.layout_product_courier);

        product_charge_values = findViewById(R.id.product_charge_values);
        delevery_charge_values = findViewById(R.id.delevery_charge_values);
        cod_charge_values = findViewById(R.id.cod_charge_values);
        total_amount_values = findViewById(R.id.total_amount_values);

        shipping_adress_values = findViewById(R.id.shipping_adress_values);

        title_text.setText("Order Details");
        back_btn = findViewById(R.id.back_btn);
        back_btn.setOnClickListener(this);
        image_copy.setOnClickListener(this);
        btn_track_order.setOnClickListener(this);
        btnCopy_tracking.setOnClickListener(this);
        btn_return.setOnClickListener(this);
        btn_cancle.setOnClickListener(this);
        orderRatingBar.setOnClickListener(this);


        progressBar = findViewById(R.id.progressBar);
        list_layout = findViewById(R.id.list_layout);

    }

    public void onResume(){
        super.onResume();
        loadData();
    }
    private void loadData() {
        progressBar.setVisibility(View.VISIBLE);
        list_layout.setVisibility(View.GONE);
        apiService = apiClient.getClient().create(ApiInterface.class);

        Call<UserOrderDetails> call = apiService.getUserOrderDetail(order_detail_id, rating, userId);

        apiClient.enqueue(call, new Callback<UserOrderDetails>() {
            @Override
            public void onResponse(Call<UserOrderDetails> call, Response<UserOrderDetails> response) {
                progressBar.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    String responseData = response.body().getOrderProduct().get(0).getError();
                    if (responseData == null) {
                        System.out.println("NULL>>>");
                        //no_data_txt.setVisibility(View.GONE);
                        list_layout.setVisibility(View.VISIBLE);
                        UserOrderDetails detailModel = response.body();
                        //orderProducts.addAll(detailModel.getOrderProduct().get(0));
                        //orderAdapter = new UserOrderAdapter(MyOrderActivity.this, orderProducts);
                        //recycle_view.setAdapter(orderAdapter);
                        setData(detailModel);
                    } else {
                        Constant.toast(getApplicationContext(), response.body().getOrderProduct().get(0).getMessage());
                        //no_data_txt.setVisibility(View.VISIBLE);
                        list_layout.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onFailure(Call<UserOrderDetails> call, Throwable t) {
                // Log error here since request failed
            }
        });
    }

    private void cancelOrder() {
        progressBar.setVisibility(View.VISIBLE);
        apiService = apiClient.getClient().create(ApiInterface.class);
        System.out.println("USSSID" + userId);
        Call<DataModel> call = apiService.getUserOrderCancel(userProductResponse.getOrderId(), userId);

        apiClient.enqueue(call, new Callback<DataModel>() {
            @Override
            public void onResponse(Call<DataModel> call, Response<DataModel> response) {
                progressBar.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    String responseData = response.body().getData().get(0).getError();
                    if (responseData != null) {
                        //no_data_txt.setVisibility(View.GONE);
                        DataModel detailModel = response.body();
                        Constant.toast(getApplicationContext(), response.body().getData().get(0).getMessage());
                    } else {
                        Constant.toast(getApplicationContext(), response.body().getData().get(0).getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<DataModel> call, Throwable t) {
                // Log error here since request failed
            }
        });
    }

    UserOrderDetailProduct userProductResponse;

    public void setData(UserOrderDetails userOrderDetails) {
        userProductResponse = userOrderDetails.getOrderProduct().get(0);

        if (userProductResponse.getProductImage() != null) {
            Picasso.get().load("https://" + userProductResponse.getProductImage())
                    .resize(0, 250)
                    .into(image, new com.squareup.picasso.Callback() {
                        @Override
                        public void onSuccess() {
                        }

                        @Override
                        public void onError(Exception e) {
                        }
                    });
        }
        status_icn.setColorFilter(Color.parseColor(userProductResponse.getStatusColor()));
        orderid_without_hash = userProductResponse.getOrderId();
        tracking_link = userProductResponse.getTrackLink();
        orderid_txt.setText("Order #" + userProductResponse.getOrderId());
        name_txt.setText(userProductResponse.getProductName());
        size_txt.setText("Size: " + userProductResponse.getProdSize());
        qty_txt.setText("Qty: " + userProductResponse.getProdQty());
        price_txt.setText("Rs " + userProductResponse.getPrice());
        status_txt.setText(userProductResponse.getOrderStatus());
        courier_name_values.setText(userProductResponse.getCourierName());
        tracking_number_values.setText(userProductResponse.getTrack_number());
        // size_txt.setText(userProductResponse.getProdSize());
        product_charge_values.setText("₹" + userProductResponse.getPrice() + "");
        delevery_charge_values.setText("₹" + userProductResponse.getShippingCharge() + "");
        cod_charge_values.setText("₹" + userProductResponse.getCodCharge() + "");
        total_amount_values.setText("₹" + userProductResponse.getTotalPrice() + "");

        shipping_adress_values.setText(Html.fromHtml(userProductResponse.getShippingAddress()));

        /*if (userProductResponse.getOrderStatus().equals("")) {
            btn_cancle.setVisibility(View.INVISIBLE);
        } else {
            btn_cancle.setVisibility(View.VISIBLE);
        }*/

        if(userProductResponse.getOrderStatus().contains("Pending") ||
                userProductResponse.getOrderStatus().contains("Accepted Order") ||
                userProductResponse.getOrderStatus().contains("Dispatched") ||
                userProductResponse.getOrderStatus().contains("Shipped")){
            btn_cancle.setVisibility(View.VISIBLE);

            if (userProductResponse.getOrderStatus().contains("Shipped")) {
                layout_product_courier.setVisibility(View.VISIBLE);
                btn_cancle.setTextColor(getResources().getColor(R.color.text_dark));
                btn_cancle.setBackgroundTintList(null);
            } else {
                layout_product_courier.setVisibility(View.GONE);
            }
            layout_product_rating.setVisibility(View.GONE);
        }
        else if (userProductResponse.getOrderStatus().contains("Cancel")  &&
                !userProductResponse.isCancle_status()) {

            btn_cancle.setVisibility(View.VISIBLE);
            btn_cancle.setText(userProductResponse.getOrderStatus());
            btn_cancle.setTextColor(getResources().getColor(R.color.text_dark));
            btn_cancle.setBackgroundTintList(null);
            btn_cancle.setEnabled(false);

            layout_product_courier.setVisibility(View.GONE);
            layout_product_rating.setVisibility(View.GONE);

        } else {
            layout_product_rating.setVisibility(View.VISIBLE);
            layout_product_courier.setVisibility(View.VISIBLE);
            btn_cancle.setVisibility(View.GONE);
        }


        if (userProductResponse.getOrderStatus().contains("Delivered")) {
            btn_return.setVisibility(View.VISIBLE);
        } else if(userProductResponse.getOrderStatus().contains("Return") ){
            btn_return.setVisibility(View.VISIBLE);
            btn_return.setText(userProductResponse.getOrderStatus());
            btn_return.setTextColor(getResources().getColor(R.color.text_dark));
            btn_return.setBackgroundTintList(null);
            btn_return.setEnabled(false);
        } else {
            btn_return.setVisibility(View.GONE);
        }

/*
    0 = "pending"
    1 = "Accepted Order"
    2 = "Dispatched Order"
    3 = "Shipped Order"
    4 = "Delivered Order"
    5 = "Rejected Order"
    6-0 = "Cancel Order Request"
    6-1 = "Cancel Order Accepted"
    6-3 = "User Cancelled Order"
    7-0= "Return Order Request"
    7-1 = "Return Order Accepted"
    7-3= "Return Order Processing"
    7-4 = "Return Order Completed"
    8 = "Courier Return Order
*/


        if (userProductResponse.getRating() != null) {
            try {
                orderRatingBar.setRating(Integer.parseInt(userProductResponse.getRating()));
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        }
        orderRatingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {

            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating1, boolean fromUser) {
                System.out.println("RATTTTT" + rating);
                int ratingItem = Math.round(rating1);
                rating = ratingItem + "";
                loadData();
            }
        });
    }

    @Override
    public void onClick(View view) {
        if (view == back_btn) {
            this.finish();
        }
        if (view == image_copy) {
            ClipboardManager clipboard = (ClipboardManager) this.getSystemService(Context.CLIPBOARD_SERVICE);
            ClipData clip = ClipData.newPlainText("copyy", orderid_without_hash);
            clipboard.setPrimaryClip(clip);
            Toast.makeText(getApplicationContext(), "Order Id Copied", Toast.LENGTH_SHORT).show();
        }
        if (view == btn_track_order) {
            System.out.println("TrackingLInk" + tracking_link);

            Uri webpage = Uri.parse(tracking_link);
            if (!tracking_link.startsWith("http://") && !tracking_link.startsWith("https://")) {
                webpage = Uri.parse("http://" + tracking_link);
            }
            Intent intent = new Intent(Intent.ACTION_VIEW, webpage);
            if (intent.resolveActivity(getPackageManager()) != null) {
                startActivity(intent);
            }
        }
        if (view == btnCopy_tracking) {
            if (userProductResponse.getTrack_number() != null) {
                ClipboardManager clipboard = (ClipboardManager) this.getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("copy1", userProductResponse.getTrack_number());
                clipboard.setPrimaryClip(clip);
                Toast.makeText(getApplicationContext(), " Copied", Toast.LENGTH_SHORT).show();
            }
        }
        if (view == btn_return) {
            if (!userProductResponse.isReturn_status()) {
                new AlertDialog.Builder(UserOrderDetailActivity.this)
                        .setMessage("Return window has been closed")
                        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .show();
            } else {
                Intent i= new Intent(UserOrderDetailActivity.this, UserReturnOrderActivity.class);
                i.putExtra("order_id", userProductResponse.getOrderId());
                i.putExtra("product_name", userProductResponse.getProductName());
                i.putExtra("product_image", userProductResponse.getProductImage());
                i.putExtra("product_size", userProductResponse.getProdSize());
                i.putExtra("product_qty", userProductResponse.getProdQty());
                i.putExtra("product_price", userProductResponse.getPrice());
                startActivity(i);
            }
        }
        if (view == btn_cancle) {
           if(userProductResponse.getOrderStatus().contains("Shipped")){
                Toast.makeText(getApplicationContext(), "Shipped order can't be canceled", Toast.LENGTH_SHORT).show();
            }else {
                new AlertDialog.Builder(UserOrderDetailActivity.this, R.style.CustomAlertDialog)
                        .setMessage("Do you want to cancel order?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                cancelOrder();
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int i) {
                                dialog.dismiss();
                            }
                        })
                        .show();
            }
        }
    }
}
