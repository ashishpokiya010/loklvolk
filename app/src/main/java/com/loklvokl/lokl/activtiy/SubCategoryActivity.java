package com.loklvokl.lokl.activtiy;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loklvokl.lokl.R;
import com.loklvokl.lokl.adapter.AllSubCategoryAdapter;
import com.loklvokl.lokl.adapter.SliderAdapter;
import com.loklvokl.lokl.model.CatItemInterface;
import com.loklvokl.lokl.model.CatSlider;
import com.loklvokl.lokl.model.CategoryModel;
import com.loklvokl.lokl.model.Homeslider;
import com.loklvokl.lokl.model.SubCategory;
import com.loklvokl.lokl.util.ApiClient;
import com.loklvokl.lokl.util.ApiInterface;

import java.util.ArrayList;
import java.util.List;

public class SubCategoryActivity extends BaseActivity implements CatItemInterface {

    private static final String TAG = "Category";
    ProgressBar progressBar;
    ApiInterface apiService;
    CategoryModel categoryModel;
    LinearLayout tab_layout;
    RecyclerView recycle_view;
    GridLayoutManager layoutManagerProductDeal;
    TextView empty_text;
    RelativeLayout pager_layout;
    ViewPager viewPager;
    TabLayout indicator;
    //RelativeLayout wishlist_layout;
    ImageView imgCatWishlist, imgCatSearch, imgCatCart;
    List<SubCategory> productsSeeAll = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub_category);

        apiService = ApiClient.getClient().create(ApiInterface.class);

        init();
        loadData();

    }

    private void init() {
        empty_text = findViewById(R.id.empty_text);
        progressBar = findViewById(R.id.progressBar);
        tab_layout = findViewById(R.id.tab_layout);
        recycle_view = findViewById(R.id.recycle_view);
        recycle_view.setNestedScrollingEnabled(false);
        pager_layout = findViewById(R.id.pager_layout);
        indicator = findViewById(R.id.indicator);
        viewPager = findViewById(R.id.viewPager);
        imgCatWishlist = findViewById(R.id.img_category_wishlist);
        imgCatSearch = findViewById(R.id.img_category_search);
        imgCatCart = findViewById(R.id.img_category_cart);

        imgCatWishlist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SubCategoryActivity.this, WishListActivity.class);
                startActivity(intent);
            }
        });

        imgCatSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SubCategoryActivity.this, SearchActivity.class);
                startActivity(intent);
            }
        });
        imgCatCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SubCategoryActivity.this, CartActivity.class);
                startActivity(intent);
            }
        });
    }

    private void loadData() {

        productsSeeAll = new Gson().fromJson(getIntent().getStringExtra("SUBCATEGORY"), new TypeToken<List<SubCategory>>() {
        }.getType());

        setupTabLayout();
        setCategoryData("0");

    }


    private void setCategoryData(String mainId) {

        if (productsSeeAll.size() > 0) {
            empty_text.setVisibility(View.GONE);
        } else {
            empty_text.setVisibility(View.VISIBLE);
        }
        progressBar.setVisibility(View.GONE);

        List<SubCategory> products = new ArrayList<>();
        products.addAll(productsSeeAll);

        AllSubCategoryAdapter productDealAdapter = new AllSubCategoryAdapter(this, products);
        layoutManagerProductDeal = new GridLayoutManager(this, 3);
        layoutManagerProductDeal.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                if ("0".equals(products.get(position).getId())) {
                    return 3;
                }
                return 1;
            }
        });
        recycle_view.setLayoutManager(layoutManagerProductDeal);
        recycle_view.setAdapter(productDealAdapter);
    }

    List<TextView> textViewList = new ArrayList<>();

    private void setupTabLayout() {
        TextView title_text = findViewById(R.id.title_text);
        title_text.setText(getIntent().getStringExtra("TITLE"));

        ImageView back_btn = findViewById(R.id.back_btn);
        back_btn.setVisibility(View.VISIBLE);
        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void updateTextBg(View view) {
        for (TextView textView : textViewList) {
            textView.setBackgroundResource(R.drawable.category_tab_unselector);
        }
        view.setBackgroundResource(R.drawable.category_tab_selector);
    }


    private void setupSliderBanner(List<CatSlider> catSlider) {
        List<Homeslider> homesliders = new ArrayList<>();
        if (catSlider != null && catSlider.size() > 0) {
            for (CatSlider catSlider1 : catSlider) {
                homesliders.add(new Homeslider(catSlider1.getCatSliderId(), catSlider1.getCatSliderImage()));
            }
            pager_layout.setVisibility(View.VISIBLE);
            viewPager.setAdapter(new SliderAdapter(this, homesliders));
            indicator.setupWithViewPager(viewPager, true);

        } else {
            pager_layout.setVisibility(View.GONE);
        }

    }


    @Override
    public void ItemClick(int adapterPosition, String id, String mainCatName) {
        Intent intent = new Intent(this, CategoryProductActivity.class);
        intent.putExtra("ID", id);
        intent.putExtra("TITLE", mainCatName);
        intent.putExtra("ISBANNER", false);
        startActivity(intent);
    }

    @Override
    public void SeeAllClick(int adapterPosition, String id, String mainCatName) {

    }
}