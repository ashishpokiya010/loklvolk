package com.loklvokl.lokl.activtiy;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.text.Spannable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.textfield.TextInputLayout;
import com.loklvokl.lokl.R;
import com.loklvokl.lokl.adapter.CorierAdapter;
import com.loklvokl.lokl.model.Courier;
import com.loklvokl.lokl.model.CourierModel;
import com.loklvokl.lokl.model.OrderProductSold;
import com.loklvokl.lokl.model.SoldOrderDetailsModle;
import com.loklvokl.lokl.model.commonresponse.CommonResponse;
import com.loklvokl.lokl.util.ApiClient;
import com.loklvokl.lokl.util.ApiInterface;
import com.loklvokl.lokl.util.Constant;
import com.loklvokl.lokl.util.SharedPref;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserSoldOrderDetailActivity extends BaseActivity
        implements View.OnClickListener, CorierAdapter.onCountryClick {

    SharedPref sharedPref;
    ApiInterface apiService;
    ApiClient apiClient;

    LinearLayout layout_shipping_details, layout_order_accept_decline;
    Button btn_order_accept, btn_order_decline;
    ImageView back_btn, imageView, img_more, img_copy_address;
    TextView title_text, text_name, textViewColor, textViewSize, textViewQty, textprice, text_cod, txt_shipping_address;
    ProgressBar progressBar;
    LinearLayout list_layout, bottomLayout1;
    CardView layout_shipping_address;
    String order_details_id, c_seller_id, userId, orderId;
    RelativeLayout btnCourier;
    private ArrayList<Courier> couriersDataArrayList;
    TextView txtCourierName;
    EditText edit_other_courier_name, edit_other_tracking_number;

    TextInputLayout text_input_layout_courier_name, text_input_layout_tracking_number;

    Button btn_confirm_shipping;
    String courier_id = "", track_number = "", other_courier_name = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_sold_order_detail);

        sharedPref = new SharedPref(this);
        apiClient = new ApiClient();
        apiService = ApiClient.getClient().create(ApiInterface.class);

        if (getIntent() != null) {
            userId = sharedPref.getStr("ID");
            //userId = getIntent().getStringExtra("UserId");
            order_details_id = getIntent().getStringExtra("order_details_id");
            c_seller_id = getIntent().getStringExtra("c_seller_id");
            orderId = getIntent().getStringExtra("order_id");
        }

        init();
        loadDAta();
        loadCourierData();
    }

    private void init() {
        findViewById(R.id.img_detail_noti).setVisibility(View.GONE);
        findViewById(R.id.img_detail_cart).setVisibility(View.GONE);
        findViewById(R.id.img_detail_wishlist).setVisibility(View.GONE);

        layout_shipping_details = findViewById(R.id.layout_shipping_details);
        layout_order_accept_decline = findViewById(R.id.layout_order_accept_decline);
        layout_shipping_address = findViewById(R.id.layout_shipping_address);
        title_text = findViewById(R.id.title_text);
        back_btn = findViewById(R.id.back_btn);
        back_btn.setOnClickListener(this);
        progressBar = findViewById(R.id.progressBar);
        list_layout = findViewById(R.id.list_layout);
        imageView = findViewById(R.id.imageView);
        img_more = findViewById(R.id.iv_more);
        txt_shipping_address = findViewById(R.id.txt_shipping_address);
        img_copy_address = findViewById(R.id.img_copy_address);
        btn_order_accept = findViewById(R.id.btn_accept_order);
        btn_order_decline = findViewById(R.id.btn_decline_order);
        text_name = findViewById(R.id.text_name);
        textViewColor = findViewById(R.id.textViewColor);
        textViewSize = findViewById(R.id.textViewSize);
        textViewQty = findViewById(R.id.textViewQty);
        textprice = findViewById(R.id.textprice);
        text_cod = findViewById(R.id.text_cod);
        btnCourier = findViewById(R.id.btnCourier);
        txtCourierName = findViewById(R.id.txtCourierName);
        edit_other_courier_name = findViewById(R.id.edit_other_courier_name);
        edit_other_tracking_number = findViewById(R.id.edit_other_tracking_number);
        text_input_layout_tracking_number = findViewById(R.id.text_input_layout_tracking_number);
        text_input_layout_courier_name = findViewById(R.id.text_input_layout_courier_name);
        btn_confirm_shipping = findViewById(R.id.btn_confirm_shipping);
        bottomLayout1 = findViewById(R.id.bottomLayout1);


        edit_other_courier_name.setVisibility(View.GONE);
        //edit_other_tracking_number.setVisibility(View.GONE);
        text_input_layout_courier_name.setVisibility(View.GONE);
        //text_input_layout_tracking_number.setVisibility(View.GONE);
        btnCourier.setOnClickListener(this);
        btn_confirm_shipping.setOnClickListener(this);
        img_more.setOnClickListener(this);
        img_copy_address.setOnClickListener(this);
        btn_order_accept.setOnClickListener(this);
        btn_order_decline.setOnClickListener(this);

        bottomLayout1.setVisibility(View.GONE);

    }


    OrderProductSold orderProductSold;

    public void setData(SoldOrderDetailsModle soldOrderDetails) {

        orderProductSold = soldOrderDetails.getOrderProduct().get(0);
        if (orderProductSold.getProductImage() != null) {
            Picasso.get().load("https://" + orderProductSold.getProductImage())
                    .resize(0, 250)
                    .into(imageView, new com.squareup.picasso.Callback() {
                        @Override
                        public void onSuccess() {
                        }

                        @Override
                        public void onError(Exception e) {
                        }
                    });
        }

        //  status_icn.setColorFilter(Color.parseColor(userProductResponse.getStatusColor()));
        //  orderid_without_hash = userProductResponse.getOrderId();
        //  tracking_link = userProductResponse.getTrackLink();

        title_text.setText("Order Id " + "#"+orderProductSold.getOrderId());
        text_name.setText(orderProductSold.getProductName());
        txt_shipping_address.setText(Html.fromHtml(orderProductSold.getProd_shipping_address().trim()));

        if (orderProductSold.getProd_size() != null && !orderProductSold.getProd_size().isEmpty()) {
            textViewSize.setText("Size: " + orderProductSold.getProd_size());
        } else {
            textViewSize.setVisibility(View.GONE);
        }
        if (orderProductSold.getProdQty() != null && !orderProductSold.getProdQty().isEmpty()) {
            textViewQty.setText("Qty: " + orderProductSold.getProdQty());
        } else {
            textViewQty.setVisibility(View.GONE);
        }

        textprice.setText("Rs " + orderProductSold.getPrice());
        text_cod.setText(orderProductSold.getPaymentType());


        if (orderProductSold.getCourier_name() != null) {
            txtCourierName.setText(orderProductSold.getCourier_name());
            txtCourierName.setEnabled(false);
            btnCourier.setClickable(false);

            btn_confirm_shipping.setEnabled(false);
            btn_confirm_shipping.setAlpha(0.5f);

        } else {
            txtCourierName.setEnabled(true);
            btnCourier.setClickable(true);

            btn_confirm_shipping.setEnabled(true);
            btn_confirm_shipping.setAlpha(1.0f);
        }
        if (orderProductSold.getTrack_number() != null) {
            edit_other_tracking_number.setText(orderProductSold.getTrack_number());
            edit_other_tracking_number.setEnabled(false);
            edit_other_tracking_number.setClickable(false);
            btn_confirm_shipping.setEnabled(false);
            btn_confirm_shipping.setAlpha(0.5f);
        } else {
            edit_other_tracking_number.setText("");
            edit_other_tracking_number.setEnabled(true);
            edit_other_tracking_number.setClickable(true);
            btn_confirm_shipping.setEnabled(true);
            btn_confirm_shipping.setAlpha(1.0f);
        }

        if(orderProductSold.getOrderStatus().contains("Pending")){
            layout_shipping_details.setVisibility(View.GONE);
            layout_order_accept_decline.setVisibility(View.VISIBLE);
        }
        else if(orderProductSold.getOrderStatus().contains("Accepted")){
            layout_shipping_details.setVisibility(View.VISIBLE);
            layout_order_accept_decline.setVisibility(View.VISIBLE);
            btn_order_accept.setText("Order Accepted");
            btn_order_accept.setBackground(getResources().getDrawable(R.drawable.button_accept_bg));
            btn_order_decline.setVisibility(View.GONE);
        }
        else if(orderProductSold.getOrderStatus().contains("Rejected")){
            layout_shipping_details.setVisibility(View.GONE);
            layout_order_accept_decline.setVisibility(View.VISIBLE);
            btn_order_decline.setText("Order Cancelled");
            btn_order_decline.setBackground(getResources().getDrawable(R.drawable.button_decline_bg));
            btn_order_accept.setVisibility(View.GONE);
        } else {
            layout_shipping_details.setVisibility(View.VISIBLE);
            layout_order_accept_decline.setVisibility(View.GONE);
        }
    }

    @Override
    public void onClick(View view) {
        if (view == back_btn) {
            this.finish();
        }
        if(view == btn_order_accept){
            if(orderProductSold.getOrderStatus().contains("Pending")) {
                new AlertDialog.Builder(UserSoldOrderDetailActivity.this, R.style.CustomAlertDialog)
                        .setMessage("Want to accept order?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                orderAccepted();
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int i) {
                                dialog.dismiss();
                            }
                        })
                        .show();
            } else {
                Constant.toast(UserSoldOrderDetailActivity.this, "Order has already been accepted");
            }
        }
        if(view == btn_order_decline){

            if(orderProductSold.getOrderStatus().contains("Pending")) {
                new AlertDialog.Builder(UserSoldOrderDetailActivity.this, R.style.CustomAlertDialog)
                        .setMessage("Want to cancel order?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                orderDeclined();
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int i) {
                                dialog.dismiss();
                            }
                        })
                        .show();
                /*final AlertDialog.Builder builder = new AlertDialog.Builder(UserSoldOrderDetailActivity.this,R.style.CustomAlertDialog);
                ViewGroup viewGroup = findViewById(android.R.id.content);
                View dialogView = LayoutInflater.from(this).inflate(R.layout.alert_dialog_accept_decline, viewGroup, false);
                Button btn_yes=dialogView.findViewById(R.id.btn_yes);
                Button btn_cancel=dialogView.findViewById(R.id.btn_cancle);
                builder.setView(dialogView);
                final AlertDialog alertDialog = builder.create();
                btn_cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialog.dismiss();
                    }
                });
                btn_yes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        orderDeclined();
                    }
                });
                alertDialog.show();*/
            } else {
                Constant.toast(UserSoldOrderDetailActivity.this, "Order has already been declined");
            }
        }
        if(view == img_more){
            if (img_more.getRotation() == 90) {
                img_more.setRotation(270);
                layout_shipping_address.setVisibility(View.VISIBLE);
            } else if (img_more.getRotation() == 270) {
                img_more.setRotation(90);
                layout_shipping_address.setVisibility(View.GONE);
            }
        }
        if(view == img_copy_address){
            ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
            ClipData clip = ClipData.newPlainText("shipping_address", ""+txt_shipping_address.getText());
            clipboard.setPrimaryClip(clip);
            Toast.makeText(UserSoldOrderDetailActivity.this, "Address copied", Toast.LENGTH_SHORT).show();
        }
        if (view == btnCourier) {
            if (couriersDataArrayList != null && couriersDataArrayList.size() > 0)
                showCourierPicker();
            else {
                Constant.toast(this, "courier api not working");
            }
        }
        if (view == btn_confirm_shipping) {
            if (txtCourierName.getText().toString().equals("Select Courier")) {
                Toast.makeText(this, "Please select courier name", Toast.LENGTH_SHORT).show();
            } else if (txtCourierName.getText().toString().equals("Other")) {
                if (edit_other_courier_name.getText().toString().trim().length() <= 0) {
                    Toast.makeText(this, "Please enter other courier name", Toast.LENGTH_SHORT).show();
                }
            } else if (edit_other_tracking_number.getText().toString().trim().length() <= 0) {
                Toast.makeText(this, "Tracking number is required.", Toast.LENGTH_SHORT).show();
            } else {
                submitData();
            }

           /* if ((txtCourierName.getText().toString().equals("Select Courier")) ||
                    (edit_other_courier_name.getText().toString().trim().length() == 0) &&
                            (edit_other_tracking_number.getText().toString().trim().length() == 0)) {
                Constant.toast(this, "Please Select courier ");
            } else {

            }*/
        }
    }

    private void loadDAta() {
        track_number = edit_other_tracking_number.getText().toString().trim();
        other_courier_name = edit_other_courier_name.getText().toString().trim();

        System.out.println("CURIER_ID" + courier_id);
        System.out.println("CURIER_ID" + track_number);
        System.out.println("CURIER_ID" + track_number);
        list_layout.setVisibility(View.GONE);
        bottomLayout1.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
        apiService = apiClient.getClient().create(ApiInterface.class);

        Call<SoldOrderDetailsModle> call = apiService.getUserSoldOrderDetails(order_details_id, c_seller_id,
                courier_id, track_number, other_courier_name);

        apiClient.enqueue(call, new Callback<SoldOrderDetailsModle>() {
            @Override
            public void onResponse(Call<SoldOrderDetailsModle> call, Response<SoldOrderDetailsModle> response) {
                progressBar.setVisibility(View.GONE);

                if (response.isSuccessful()) {
                    String responseData = response.body().getOrderProduct().get(0).getError();

                    System.out.println("NULL>>>" + response.body().getOrderProduct());
                    if (responseData == null) {
                        list_layout.setVisibility(View.VISIBLE);
                        bottomLayout1.setVisibility(View.VISIBLE);
                        SoldOrderDetailsModle detailModel = response.body();
                        setData(detailModel);
                    } else {
                        Constant.toast(getApplicationContext(), response.body().getOrderProduct().get(0).getMessage());
                        list_layout.setVisibility(View.GONE);
                        bottomLayout1.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onFailure(Call<SoldOrderDetailsModle> call, Throwable t) {
                // Log error here since request failed
            }
        });
    }

    private void orderAccepted() {

        progressBar.setVisibility(View.VISIBLE);
        apiService = apiClient.getClient().create(ApiInterface.class);

        Call<CommonResponse> call = apiService.orderAccepted(orderId);

        apiClient.enqueue(call, new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {
                progressBar.setVisibility(View.GONE);

                if (response.isSuccessful()) {
                    System.out.println("NULL>>>" + response.body().getResponse().get(0));
                    if (response.body() == null) {
                        Constant.toast(UserSoldOrderDetailActivity.this, "Unable to accept order");
                    } else {
                        Constant.toast(UserSoldOrderDetailActivity.this, response.body().getResponse().get(0).getMessage());
                        if(!response.body().getResponse().get(0).isError()){
                            loadDAta();
                        } else {
                            Constant.toast(UserSoldOrderDetailActivity.this, response.body().getResponse().get(0).getMessage());
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {
                Constant.toast(UserSoldOrderDetailActivity.this, "Failed to get response");
            }
        });
    }

    private void orderDeclined() {

        progressBar.setVisibility(View.VISIBLE);
        apiService = apiClient.getClient().create(ApiInterface.class);

        Call<CommonResponse> call = apiService.orderDeclined(orderId);

        apiClient.enqueue(call, new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {
                progressBar.setVisibility(View.GONE);

                if (response.isSuccessful()) {
                    System.out.println("NULL>>>" + response.body().getResponse().get(0));
                    if (response.body() == null) {
                        Constant.toast(UserSoldOrderDetailActivity.this, "Unable to accept order");
                    } else {
                        Constant.toast(UserSoldOrderDetailActivity.this, response.body().getResponse().get(0).getMessage());
                        if(!response.body().getResponse().get(0).isError()){
                            loadDAta();
                        } else {
                            Constant.toast(UserSoldOrderDetailActivity.this, response.body().getResponse().get(0).getMessage());
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {
                Constant.toast(UserSoldOrderDetailActivity.this, "Failed to get response");
            }
        });
    }


    private void submitData() {
        track_number = edit_other_tracking_number.getText().toString().trim();
        other_courier_name = edit_other_courier_name.getText().toString().trim();

        System.out.println("CURIER_ID" + courier_id);
        System.out.println("CURIER_ID" + track_number);
        System.out.println("CURIER_ID" + track_number);

        progressBar.setVisibility(View.VISIBLE);
        apiService = apiClient.getClient().create(ApiInterface.class);

        Call<SoldOrderDetailsModle> call = apiService.getUserSoldOrderDetails(order_details_id, c_seller_id,
                courier_id, track_number, other_courier_name);

        apiClient.enqueue(call, new Callback<SoldOrderDetailsModle>() {
            @Override
            public void onResponse(Call<SoldOrderDetailsModle> call, Response<SoldOrderDetailsModle> response) {
                progressBar.setVisibility(View.GONE);

                if (response.isSuccessful()) {
                    String responseData = response.body().getOrderProduct().get(0).getError();

                    System.out.println("NULL>>>" + response.body().getOrderProduct());
                    if (responseData == null) {
                        //      Toast.makeText(UserSoldOrderDetailActivity.this, response.body().getOrderProduct().get(0).getMessage(),
                        //             Toast.LENGTH_SHORT).show();
                        finish();
                    } else {
                        Constant.toast(getApplicationContext(), response.body().getOrderProduct().get(0).getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<SoldOrderDetailsModle> call, Throwable t) {
                // Log error here since request failed
            }
        });
    }

    private void loadCourierData() {
        couriersDataArrayList = new ArrayList<>();
        progressBar.setVisibility(View.VISIBLE);
        apiService = apiClient.getClient().create(ApiInterface.class);
        Call<CourierModel> call = apiService.getCourierCompnanyList();

        apiClient.enqueue(call, new Callback<CourierModel>() {
            @Override
            public void onResponse(Call<CourierModel> call, Response<CourierModel> response) {
                progressBar.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    CourierModel detailModel = response.body();
                    if (detailModel.getCourier().size() > 0) {
                        couriersDataArrayList.addAll(detailModel.getCourier());
                    } else {
                        Constant.toast(UserSoldOrderDetailActivity.this, "Something went wrong!");
                    }
                    System.out.println("NULL>>>" + detailModel.getCourier());
                }
            }

            @Override
            public void onFailure(Call<CourierModel> call, Throwable t) {
                // Log error here since request failed
            }
        });
    }

    AlertDialog alertDialog;

    private void showCourierPicker() {
        final AlertDialog.Builder alert = new AlertDialog.Builder(UserSoldOrderDetailActivity.this,
                R.style.AppThemeDialogAlert);
        View mView = getLayoutInflater().inflate(R.layout.popup_courer_picker, null);
        RecyclerView recyclerView = mView.findViewById(R.id.recyclerviewCountry);
        alert.setView(mView);

        alertDialog = alert.create();
        alertDialog.setCanceledOnTouchOutside(true);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        CorierAdapter countryAdapter = new CorierAdapter(UserSoldOrderDetailActivity.this,
                couriersDataArrayList, UserSoldOrderDetailActivity.this);
        recyclerView.setAdapter(countryAdapter);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.show();

    }

    @Override
    public void onCountryItemClick(Courier courierData) {
        System.out.println("CLICKED");
        if (alertDialog != null && alertDialog.isShowing())
            alertDialog.dismiss();

        if (courierData.getName().contains("other")) {
            // edit_other_tracking_number.setVisibility(View.VISIBLE);
            edit_other_courier_name.setVisibility(View.VISIBLE);

            //text_input_layout_tracking_number.setVisibility(View.VISIBLE);
            text_input_layout_courier_name.setVisibility(View.VISIBLE);
            txtCourierName.setText("Other");

        } else {
            edit_other_courier_name.setText("");
            //edit_other_tracking_number.setText("");

            //edit_other_tracking_number.setVisibility(View.GONE);
            edit_other_courier_name.setVisibility(View.GONE);
            //text_input_layout_tracking_number.setVisibility(View.GONE);
            text_input_layout_courier_name.setVisibility(View.GONE);
            txtCourierName.setText(courierData.getName());
            courier_id = courierData.getId();
        }

        Log.d("tag", courierData.getName());

    }
}
