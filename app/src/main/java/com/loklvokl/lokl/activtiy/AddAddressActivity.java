package com.loklvokl.lokl.activtiy;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loklvokl.lokl.R;
import com.loklvokl.lokl.model.Address;
import com.loklvokl.lokl.model.AddressResponse;
import com.loklvokl.lokl.util.ApiClient;
import com.loklvokl.lokl.util.ApiInterface;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddAddressActivity extends BaseActivity implements View.OnClickListener {


    ImageView back_btn;
    TextView title_text;
    ProgressBar progressBar;
    Button btn_continue;

    EditText ed_username, ed_alternatmob, ed_email, ed_address1, ed_address2, ed_pincode, ed_city, ed_state;
    String address_id = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_address);

        init();
        getIntentData();
    }

    private void getIntentData() {
        if (getIntent().hasExtra("ADDRESS_ID")) {
            address_id = getIntent().getStringExtra("ADDRESS_ID");
        }
        if (getIntent().hasExtra("ADDRESS")) {
            String addressStr = getIntent().getStringExtra("ADDRESS");

            Address address = new Gson().fromJson(addressStr, new TypeToken<Address>() {
            }.getType());

            ed_username.setText(address.getFullName());
            ed_alternatmob.setText(address.getMobile());
//            ed_email.setText(address.getEmail());
            ed_address1.setText(address.getArea());
            ed_address2.setText(address.getLandmark());
            ed_pincode.setText(address.getPincode());
            ed_city.setText(address.getCity());
            ed_state.setText(address.getState());

        }

    }

    private void init() {
        progressBar = findViewById(R.id.progressBar);
        back_btn = findViewById(R.id.back_btn);
        back_btn.setOnClickListener(this);
        title_text = findViewById(R.id.title_text);
        title_text.setText("Add New Address");

        btn_continue = findViewById(R.id.btn_continue);
        btn_continue.setOnClickListener(this);

        ed_username = findViewById(R.id.ed_username);
        ed_alternatmob = findViewById(R.id.ed_alternatmob);
        ed_email = findViewById(R.id.ed_email);
        ed_address1 = findViewById(R.id.ed_address1);
        ed_address2 = findViewById(R.id.ed_address2);
        ed_pincode = findViewById(R.id.ed_pincode);
        ed_city = findViewById(R.id.ed_city);
        ed_state = findViewById(R.id.ed_state);

    }

    @Override
    public void onClick(View view) {
        if (view == back_btn) {
            finish();

        } else if (view == btn_continue) {
            performAddAddress();
        }
    }

    private void performAddAddress() {
        String fullname = ed_username.getText().toString();
        String mobile = ed_alternatmob.getText().toString();
//        String email = ed_email.getText().toString();
        String pincode = ed_pincode.getText().toString();
        String area = ed_address1.getText().toString();
        String landmark = ed_address2.getText().toString();
        String city = ed_city.getText().toString();
        String state = ed_state.getText().toString();
        String user_id = sharedPref.getStr("ID");

        if (fullname.isEmpty()) {
            Toast.makeText(this, "Full name is required.", Toast.LENGTH_SHORT).show();
        } else if (mobile.isEmpty()) {
            Toast.makeText(this, "Mobile number is required.", Toast.LENGTH_SHORT).show();
        } else if (mobile.length() != 10) {
            Toast.makeText(this, "Mobile number is not valid.", Toast.LENGTH_SHORT).show();
        } else if (area.isEmpty()) {
            Toast.makeText(this, "Flat/House/Office/Building is required.", Toast.LENGTH_SHORT).show();
        } else if (landmark.isEmpty()) {
            Toast.makeText(this, "Landmark is required.", Toast.LENGTH_SHORT).show();
        } else if (pincode.isEmpty()) {
            Toast.makeText(this, "Pincode is required.", Toast.LENGTH_SHORT).show();
        } else if (city.isEmpty()) {
            Toast.makeText(this, "City is required.", Toast.LENGTH_SHORT).show();
        } else if (state.isEmpty()) {
            Toast.makeText(this, "Area is required.", Toast.LENGTH_SHORT).show();
        } else {

            progressBar.setVisibility(View.VISIBLE);

            ApiClient apiClient = new ApiClient();
            ApiInterface apiService = apiClient.getClient().create(ApiInterface.class);

            Call<AddressResponse> call;
            if (address_id.isEmpty()) {
                call = apiService.requestAddAddress(user_id, fullname, mobile, pincode,
                        area, landmark, city, state);
            } else {
                call = apiService.requestEditAddress(address_id, user_id, fullname, mobile, pincode,
                        area, landmark, city, state);
            }
            apiClient.enqueue(call, new Callback<AddressResponse>() {
                @Override
                public void onResponse(Call<AddressResponse> call, Response<AddressResponse> response) {
                    Log.d("TAG", ": " + new Gson().toJson(response.body()));
                    progressBar.setVisibility(View.GONE);

                    AddressResponse body = response.body();

                    if (body.getAddress() != null && body.getAddress().size() > 0) {
                        if (body.getAddress().get(0).getError()) {
                            Toast.makeText(AddAddressActivity.this, body.getAddress().get(0).getMassage(), Toast.LENGTH_SHORT).show();
                            setResult(RESULT_CANCELED);
                        } else {
                            Toast.makeText(AddAddressActivity.this, body.getAddress().get(0).getMassage(), Toast.LENGTH_SHORT).show();
                            setResult(RESULT_OK);
                            finish();
                        }
                    }
                }

                @Override
                public void onFailure(Call<AddressResponse> call, Throwable t) {
                    // Log error here since request failed
                    Log.e("TAG", t.toString());
                }
            });
        }

    }
}