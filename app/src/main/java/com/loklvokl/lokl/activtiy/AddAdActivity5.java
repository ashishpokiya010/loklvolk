package com.loklvokl.lokl.activtiy;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.loklvokl.lokl.R;
import com.loklvokl.lokl.model.MainCategory;
import com.loklvokl.lokl.util.ApiClient;
import com.loklvokl.lokl.util.ApiInterface;

public class AddAdActivity5 extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "AddAd";
    ProgressBar progressBar;
    ApiInterface apiService;
    MainCategory mainCategory = new MainCategory();
    String image1, image2, image3, image4, adtitle, color_id, size_id, main_cat, sub_cat1, sub_cat2, productDiscription;
    Button btn_next;
    EditText ed_price, ed_sell_price;
    ImageView back_btn;
    TextView title_text;
    private String productCondition, countryOfRegion;
    RadioGroup group_product_condition, group_country_region;
    private RadioButton rb_condition_one, rb_india;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_ad5);

        init();
        getINtentData();
    }

    private void getINtentData() {
        if (getIntent().hasExtra("DATA")) {
            mainCategory = (MainCategory) getIntent().getSerializableExtra("DATA");
        }
        image1 = getIntent().getStringExtra("IMAGE1");
        image2 = getIntent().getStringExtra("IMAGE2");
        image3 = getIntent().getStringExtra("IMAGE3");
        image4 = getIntent().getStringExtra("IMAGE4");

        adtitle = getIntent().getStringExtra("AD_TITLE");
        color_id = getIntent().getStringExtra("COLOR_ID");
        size_id = getIntent().getStringExtra("SIZE_ID");

        main_cat = getIntent().getStringExtra("MAIN_CAT");
        sub_cat1 = getIntent().getStringExtra("SUB_CAT1");
        sub_cat2 = getIntent().getStringExtra("SUB_CAT2");
        productDiscription = getIntent().getStringExtra("product_discription");

    }

    private void init() {
        apiService = ApiClient.getClient().create(ApiInterface.class);
        progressBar = findViewById(R.id.progressBar);
        back_btn = findViewById(R.id.back_btn);
        title_text = findViewById(R.id.title_text);
        title_text.setText("Enter Price");

        group_product_condition = findViewById(R.id.group_product_condition);
        group_country_region = findViewById(R.id.group_country_region);
        back_btn.setOnClickListener(this);

        ed_price = findViewById(R.id.ed_price);
        ed_sell_price = findViewById(R.id.ed_sell_price);
        btn_next = findViewById(R.id.btn_next);

        btn_next.setOnClickListener(this);

        ed_price.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (productCondition != null && countryOfRegion != null
                        && ed_sell_price.getText().toString().trim().length() > 0 && ed_sell_price.getText().toString().trim().length() > 0) {
                    btn_next.setEnabled(true);
                    btn_next.setAlpha(1.0f);
                } else {
                    btn_next.setEnabled(false);
                    btn_next.setAlpha(0.5f);
                }

            }
        });

        ed_sell_price.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (productCondition != null && countryOfRegion != null
                        && ed_sell_price.getText().toString().trim().length() > 0 && ed_sell_price.getText().toString().trim().length() > 0) {
                    btn_next.setEnabled(true);
                    btn_next.setAlpha(1.0f);
                } else {
                    btn_next.setEnabled(false);
                    btn_next.setAlpha(0.5f);
                }
            }
        });

        group_product_condition.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton radioButton = (RadioButton) findViewById(checkedId);
                productCondition = radioButton.getText().toString();

                if (productCondition != null && countryOfRegion != null
                        && ed_sell_price.getText().toString().trim().length() > 0 && ed_sell_price.getText().toString().trim().length() > 0) {
                    btn_next.setEnabled(true);
                    btn_next.setAlpha(1.0f);

                } else {
                    btn_next.setEnabled(false);
                    btn_next.setAlpha(0.5f);
                }

            }
        });

        group_country_region.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton radioButton = (RadioButton) findViewById(checkedId);
                countryOfRegion = radioButton.getText().toString();

                if (productCondition != null && countryOfRegion != null
                        && ed_sell_price.getText().toString().trim().length() > 0 && ed_sell_price.getText().toString().trim().length() > 0) {
                    btn_next.setEnabled(true);
                    btn_next.setAlpha(1.0f);

                } else {
                    btn_next.setEnabled(false);
                    btn_next.setAlpha(0.5f);
                }

            }
        });

    }

    private void loadData() {

    }

    @Override
    public void onClick(View view) {
        if (view == back_btn) {
            onBackPressed();
        } else if (view == btn_next) {
            if (ed_price.getText().toString().isEmpty()) {
                Toast.makeText(this, "Enter Market price", Toast.LENGTH_SHORT).show();

            } else if (ed_sell_price.getText().toString().isEmpty()) {
                Toast.makeText(this, "Enter Selling price", Toast.LENGTH_SHORT).show();

            } else if (Integer.parseInt(ed_price.getText().toString()) < Integer.parseInt(ed_sell_price.getText().toString())) {
                Toast.makeText(this, "Selling price is not more than Market price!!", Toast.LENGTH_SHORT).show();
            } else if (productCondition == null) {
                Toast.makeText(this, "Please select product condition", Toast.LENGTH_SHORT).show();
            } else if (countryOfRegion == null) {
                Toast.makeText(this, "Please select country of region", Toast.LENGTH_SHORT).show();
            } else {
                if (Integer.parseInt(ed_price.getText().toString()) < 149) {
                    Toast.makeText(this, "Minimum market price is 149", Toast.LENGTH_SHORT).show();
                } else if (Integer.parseInt(ed_sell_price.getText().toString()) < 149) {
                    Toast.makeText(this, "Minimum selling price is 149", Toast.LENGTH_SHORT).show();
                } else {
                    Intent intent = new Intent(this, AddAdActivity6.class);
                    intent.putExtra("IMAGE1", image1);
                    intent.putExtra("IMAGE2", image2);
                    intent.putExtra("IMAGE3", image3);
                    intent.putExtra("IMAGE4", image4);
                    intent.putExtra("AD_TITLE", adtitle);
                    intent.putExtra("COLOR_ID", color_id);
                    intent.putExtra("SIZE_ID", size_id);
                    intent.putExtra("M_PRICE", ed_price.getText().toString());
                    intent.putExtra("S_PRICE", ed_sell_price.getText().toString());
                    intent.putExtra("MAIN_CAT", main_cat);
                    intent.putExtra("SUB_CAT1", sub_cat1);
                    intent.putExtra("SUB_CAT2", sub_cat2);
                    intent.putExtra("product_discription", productDiscription);
                    intent.putExtra("productCondition", productCondition);
                    intent.putExtra("countryOfRegion", countryOfRegion);
                    startActivityForResult(intent, 112);
                    overridePendingTransition(R.anim.enter, R.anim.exit);
                    System.out.println("SECCLING_PRICE" + ed_sell_price.getText().toString());
                }
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 112 && resultCode == RESULT_OK) {
            setResult(RESULT_OK);
            finish();
        }
    }
}