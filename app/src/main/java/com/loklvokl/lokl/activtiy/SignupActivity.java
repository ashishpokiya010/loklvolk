package com.loklvokl.lokl.activtiy;

import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.loklvokl.lokl.R;
import com.loklvokl.lokl.model.SignUpResponse;
import com.loklvokl.lokl.model.User;
import com.loklvokl.lokl.util.AnalyticsApplication;
import com.loklvokl.lokl.util.ApiClient;
import com.loklvokl.lokl.util.ApiInterface;
import com.loklvokl.lokl.util.Constant;
import com.loklvokl.lokl.util.SharedPref;

import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignupActivity extends AppCompatActivity implements View.OnClickListener {

    TextView btn_login, btn_sign, privacy_text, terms_text;

    EditText ed_alternatmob, ed_username, ed_email, ed_password, ed_password_c, ed_lastname;
    ProgressBar progressBar;
    ApiInterface apiService;
    ApiClient apiClient;
    SharedPref sharedPref;
    Tracker mTracker;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        setContentView(R.layout.signup_activity);

        sharedPref = new SharedPref(this);
        apiClient = new ApiClient();
        apiService = ApiClient.getClient().create(ApiInterface.class);

        init();

        AnalyticsApplication application = (AnalyticsApplication) getApplication();
        mTracker = application.getDefaultTracker();
    }

    private void init() {
        terms_text = findViewById(R.id.terms_text);
        terms_text.setOnClickListener(this);
        terms_text.setPaintFlags(terms_text.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        privacy_text = findViewById(R.id.privacy_text);
        privacy_text.setOnClickListener(this);
        privacy_text.setPaintFlags(privacy_text.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        btn_login = findViewById(R.id.btn_login);
        btn_login.setOnClickListener(this);
        btn_sign = findViewById(R.id.btn_sign);
        btn_sign.setOnClickListener(this);

        progressBar = findViewById(R.id.progressBar);
        ed_alternatmob = findViewById(R.id.ed_alternatmob);
        ed_username = findViewById(R.id.ed_username);
        ed_email = findViewById(R.id.ed_email);
        ed_password = findViewById(R.id.ed_password);
        ed_password_c = findViewById(R.id.ed_password_c);
        ed_lastname = findViewById(R.id.ed_lastname);
    }

    @Override
    public void onClick(View view) {
        if (view == btn_login) {
            finish();
        } else if (view == btn_sign) {
            performSignUp();
        } else if (view == terms_text) {
            openPrivacy("Terms of Services");
        } else if (view == privacy_text) {
            openPrivacy("Privacy Policy");

        }

    }

    private void openPrivacy(String b) {
        Intent intent = new Intent(this, PrivacyActivity.class);
        intent.putExtra("TITLE", b);
        startActivity(intent);
    }

    private void performSignUp() {
        String mobile = ed_alternatmob.getText().toString();
        String username = ed_username.getText().toString();
        String lastname = ed_lastname.getText().toString();
        String email = ed_email.getText().toString();
        String pass = ed_password.getText().toString();
        String pass_c = ed_password_c.getText().toString();

        if (mobile.isEmpty()) {
            Toast.makeText(this, "Mobile number is required.", Toast.LENGTH_SHORT).show();
        } else if (mobile.length() != 10) {
            Toast.makeText(this, "Mobile number is not valid.", Toast.LENGTH_SHORT).show();
        }
        /*else if (username.isEmpty()) {
            Toast.makeText(this, "First name is required.", Toast.LENGTH_SHORT).show();
        } else if (lastname.isEmpty()) {
            Toast.makeText(this, "Last name is required.", Toast.LENGTH_SHORT).show();
        } else if (!email.isEmpty() && !idValid(email)) {
            Toast.makeText(this, "Email is not valid format.", Toast.LENGTH_SHORT).show();
        }*/
        else if (pass.isEmpty()) {
            Toast.makeText(this, "Password is required.", Toast.LENGTH_SHORT).show();
        } else if (!pass.equals(pass_c)) {
            Toast.makeText(this, "Password is not matched.", Toast.LENGTH_SHORT).show();
        } else {
            progressBar.setVisibility(View.VISIBLE);

            apiService = apiClient.getClient().create(ApiInterface.class);

            Call<SignUpResponse> call = apiService.requestSignup(username, lastname, email, mobile, pass, Constant.getDeviceId(this));
            apiClient.enqueue(call, new Callback<SignUpResponse>() {
                @Override
                public void onResponse(Call<SignUpResponse> call, Response<SignUpResponse> response) {
                    Log.d("TAG", "data: " + response.body());
                    progressBar.setVisibility(View.GONE);
                    SignUpResponse body = response.body();

                    if (body.getResponse() != null && body.getResponse().size() > 0) {
                        Toast.makeText(SignupActivity.this, body.getResponse().get(0).getMessage(), Toast.LENGTH_SHORT).show();

                        if (!body.getResponse().get(0).getError()) {

                            // for analitics
                            mTracker.setScreenName("SignUp Screen");
                            mTracker.send(new HitBuilders.ScreenViewBuilder().build());
                            mTracker.send(new HitBuilders.EventBuilder()
                                    .setCategory("Signup")
                                    .setAction("New_User_Registered")
                                    .build());


                            User user = body.getResponse().get(0).getUser().get(0);
                            sharedPref.setStr("MOBILE", user.getMobile());
                            sharedPref.setStr("NAME", user.getUsername());
                            sharedPref.setStr("LNAME", user.getLast_name());
                            sharedPref.setStr("IMAGE", user.getProfile_image());
                            sharedPref.setStr("EMAIL", user.getEmail());
                            sharedPref.setStr("ID", user.getId());
                            sharedPref.setBool("IS_LOGIN", true);

                            sharedPref.setStr("CITY", user.getCity());
                            sharedPref.setStr("GENDER", user.getGender());
                            sharedPref.setStr("DOB", user.getDob());
                            setResult(RESULT_OK);
                            finish();
                            MainActivity.tabHost.setCurrentTab(4);
                        }
                    }
                }

                @Override
                public void onFailure(Call<SignUpResponse> call, Throwable t) {
                    // Log error here since request failed
                    Log.e("TAG", t.toString());
                }
            });

        }

    }

    private boolean idValid(String email) {
        Pattern pattern = Patterns.EMAIL_ADDRESS;
        return pattern.matcher(email).matches();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
