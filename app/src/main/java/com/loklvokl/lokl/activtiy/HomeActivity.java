package com.loklvokl.lokl.activtiy;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;
import androidx.viewpager.widget.ViewPager;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.material.tabs.TabLayout;
import com.loklvokl.lokl.R;
import com.loklvokl.lokl.adapter.ExplorerAdapter;
import com.loklvokl.lokl.adapter.ProductDealAdapter;
import com.loklvokl.lokl.adapter.SliderAdapter;
import com.loklvokl.lokl.adapter.CollectionAdapter;
import com.loklvokl.lokl.model.BannerInterface;
import com.loklvokl.lokl.model.CollectInterface;
import com.loklvokl.lokl.model.Collection;
import com.loklvokl.lokl.model.ExplorerCategory;
import com.loklvokl.lokl.model.HomeProduct;
import com.loklvokl.lokl.model.ItemInterface;
import com.loklvokl.lokl.model.Product;
import com.loklvokl.lokl.model.Productdeal;
import com.loklvokl.lokl.model.shareproductdata.ShareProduct;
import com.loklvokl.lokl.util.AnalyticsApplication;
import com.loklvokl.lokl.util.ApiInterface;
import com.loklvokl.lokl.model.HomeModel;
import com.loklvokl.lokl.model.Homeslider;
import com.loklvokl.lokl.model.Spacialoffer;
import com.loklvokl.lokl.util.ApiClient;
import com.loklvokl.lokl.util.SharedPref;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeActivity extends BaseActivity implements
        BannerInterface, ItemInterface, CollectInterface, View.OnClickListener {

    private static final String TAG = "TAG";
    ApiInterface apiService;
    ApiClient apiClient;
    SharedPref sharedPref;

    private ProgressBar progressBar;

    ViewPager viewPager, viewPager_spacialoffer;
    TabLayout indicator;
    HomeModel homeModel = null;
    RecyclerView recycle_horizontal, recycle_collection, recycle_product_deal, recycle_home_product;
    NestedScrollView scrollView;
    GridLayoutManager layoutManagerProductDeal, layoutManagerProductHome;
    StaggeredGridLayoutManager staggeredGridLayoutManager;
    //RelativeLayout cart_layout, wishlist_layout;
    ImageView imgHomeCart, imgHomeWishlist;
    private TextView /*collection_seeall,*/ explore_seell, searchView;

    ExplorerAdapter explorerAdapter;
    List<ExplorerCategory> explorerCategories = new ArrayList<>();
    CollectionAdapter collectionAdapter;
    List<Collection> collections = new ArrayList<>();
    LinearLayout home_product_layout;
    Typeface face;
    TextView title_text;
    Tracker mTracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_activity);


        apiClient = new ApiClient();
        apiService = ApiClient.getClient().create(ApiInterface.class);
        sharedPref = new SharedPref(this);

        face = Typeface.createFromAsset(getAssets(),
                "Moonshiner-Regular.otf");

        init();
        loadData();

        // for analitics
        AnalyticsApplication application = (AnalyticsApplication) getApplication();
        mTracker = application.getDefaultTracker();
        mTracker.setScreenName("Home Screen");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("Home")
                .setAction("Home")
                .build());

        PackageInfo pinfo = null;
        try {
            pinfo = getPackageManager().getPackageInfo(getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        String versionName = pinfo.versionName;
        sharedPref.setVersionName(versionName);
    }

    private void init() {
        findViewById(R.id.img_home_search).setVisibility(View.GONE);
        searchView = findViewById(R.id.searchView);
        searchView.setOnClickListener(this);
        explore_seell = findViewById(R.id.explore_seell);
        explore_seell.setOnClickListener(this);
        //collection_seeall = findViewById(R.id.collection_seeall);
        //collection_seeall.setOnClickListener(this);

        imgHomeCart = findViewById(R.id.img_home_cart);
        imgHomeCart.setOnClickListener(this);
        imgHomeWishlist = findViewById(R.id.img_home_wishlist);
        imgHomeWishlist.setOnClickListener(this);

        title_text = findViewById(R.id.title_text);


        progressBar = findViewById(R.id.progressBar);

        scrollView = findViewById(R.id.scrollView);
        viewPager = findViewById(R.id.viewPager);
        viewPager_spacialoffer = findViewById(R.id.viewPager_spacialoffer);
        indicator = findViewById(R.id.indicator);
        recycle_horizontal = findViewById(R.id.recycle_horizontal);
        recycle_horizontal.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        recycle_horizontal.setNestedScrollingEnabled(false);

        recycle_collection = findViewById(R.id.recycle_collection);
        recycle_collection.setLayoutManager(new GridLayoutManager(this, 3));
        recycle_collection.setNestedScrollingEnabled(false);

        recycle_product_deal = findViewById(R.id.recycle_product_deal);
        layoutManagerProductDeal = new GridLayoutManager(this, 2);
        recycle_product_deal.setNestedScrollingEnabled(false);

        home_product_layout = findViewById(R.id.home_product_layout);
        recycle_home_product = findViewById(R.id.recycle_home_product);
        layoutManagerProductHome = new GridLayoutManager(this, 2);
        staggeredGridLayoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);

        title_text.setTypeface(face);
        title_text.setTextSize(30);

    }

    private void loadData() {

        scrollView.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);

        apiService = apiClient.getClient().create(ApiInterface.class);
        Call<HomeModel> call = apiService.getHomeData();

        apiClient.enqueue(call, new Callback<HomeModel>() {
            @Override
            public void onResponse(Call<HomeModel> call, Response<HomeModel> response) {
                Log.d(TAG, "data received: " + response.body());
                progressBar.setVisibility(View.GONE);
                scrollView.setVisibility(View.VISIBLE);

                homeModel = response.body();

                setSlider();
                setExploreCategory();
                setCollection();
                setProductDeal();
                setSpecialOffer();
                setHomeProduct();
                loadShareText();
            }

            @Override
            public void onFailure(Call<HomeModel> call, Throwable t) {
                // Log error here since request failed
                Log.e(TAG, t.toString());
            }
        });
    }

    private void loadShareText() {

        progressBar.setVisibility(View.VISIBLE);

        apiService = apiClient.getClient().create(ApiInterface.class);
        Call<ShareProduct> call = apiService.getShareData();

        call.enqueue(new Callback<ShareProduct>() {
            @Override
            public void onResponse(Call<ShareProduct> call, Response<ShareProduct> response) {
                Log.d(TAG, "data received: " + response.body().getResponse().get(0).getData());
                progressBar.setVisibility(View.GONE);

                if(response.isSuccessful() && response.code() == 200){
                    sharedPref.setShareText(response.body().getResponse().get(0).getData());
                }

            }

            @Override
            public void onFailure(Call<ShareProduct> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                Log.e(TAG, t.toString());
            }
        });
    }

    private void setHomeProduct() {
        home_product_layout.setVisibility(View.VISIBLE);

        List<Product> products = new ArrayList<>();
        for (HomeProduct productdeal : homeModel.getHome_product()) {
            /*Product product = new Product();
            product.setProductId("0");
            product.setProductName(productdeal.getHome_product_title_name());

            product.setBannar_id("");
            product.setBannar_image("");
            products.add(product);*/

            products.addAll(productdeal.getProduct());

        }
        ProductDealAdapter adapter = new ProductDealAdapter(this, products, false);

        /*layoutManagerProductHome.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                if ("0".equals(products.get(position).getProductId())) {
                    return 2;
                }
                return 1;
            }
        });*/
        recycle_home_product.setLayoutManager(staggeredGridLayoutManager);
        recycle_home_product.setAdapter(adapter);

    }

    private void setSpecialOffer() {
        if (homeModel.getSpacialoffer() != null) {
            List<Homeslider> homesliders = new ArrayList<>();
            for (Spacialoffer spacialoffer : homeModel.getSpacialoffer()) {
                homesliders.add(new Homeslider(spacialoffer.getBannarId(), spacialoffer.getBannarImage()));
            }
            viewPager_spacialoffer.setAdapter(new SliderAdapter(this, homesliders));
        } else {
            viewPager_spacialoffer.setVisibility(View.GONE);
        }
//        if (homesliders.size() > 0) {
//            Timer timer = new Timer();
//            timer.scheduleAtFixedRate(new SliderTimer(viewPager_spacialoffer, homesliders.size()), 4000, 6000);
//        }
    }

    private void setProductDeal() {
        List<Product> products = new ArrayList<>();
        for (Productdeal productdeal : homeModel.getProductdeal()) {
            String bgColor = productdeal.getBg_color();
            Product product = new Product();
            product.setProductId("0");
            product.setColor(bgColor);
            product.setProductName(productdeal.getTitle());

            product.setBannar_id(productdeal.getBannar_id());
            product.setBannar_image(productdeal.getBannar_image());
            products.add(product);

            for (Product product1 : productdeal.getProduct()) {
                product1.setColor(bgColor);
                products.add(product1);
            }
        }
        ProductDealAdapter productDealAdapter = new ProductDealAdapter(this, products, true);
        layoutManagerProductDeal.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                if ("0".equals(products.get(position).getProductId())) {
                    return 2;
                }
                return 1;
            }
        });
        recycle_product_deal.setLayoutManager(layoutManagerProductDeal);
        recycle_product_deal.setAdapter(productDealAdapter);
    }

    private void setCollection() {
        if (homeModel.getCollection() != null) {
            /*for (int i = 0; i < Math.min(6, homeModel.getCollection().size()); i++) {
                collections.add(homeModel.getCollection().get(i));
            }*/
            collections.addAll(homeModel.getCollection());
            collectionAdapter = new CollectionAdapter(this, collections);
            recycle_collection.setAdapter(collectionAdapter);
        } else {
            findViewById(R.id.collection_layout).setVisibility(View.GONE);
        }
    }

    private void setExploreCategory() {
        explorerCategories.clear();
//        for (int i = 0; i < Math.min(6, homeModel.getExplorerCategory().size()); i++) {
//            explorerCategories.add(homeModel.getExplorerCategory().get(i));
//        }
        explorerCategories.addAll(homeModel.getExplorerCategory());
        explorerAdapter = new ExplorerAdapter(this, explorerCategories);
        recycle_horizontal.setAdapter(explorerAdapter);
    }

    private void setSlider() {

//        Gson gson = new Gson();
//        try {
//            homeModel = gson.fromJson(String.valueOf(new JSONObject(tempRes)), HomeModel.class);
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }

        viewPager.setAdapter(new SliderAdapter(this, homeModel.getHomeslider()));
        indicator.setupWithViewPager(viewPager, true);

        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new SliderTimer(viewPager, homeModel.getHomeslider().size()), 4000, 6000);

    }

    @Override
    public void bannerClick(String id) {
        Intent intent = new Intent(this, ProductListActivity.class);
        intent.putExtra("BANNER_ID", id);
        startActivity(intent);
    }


    @Override
    public void ItemClick(int adapterPosition, String id, String mainCatName) {
        Intent intent = new Intent(this, ExplorerProductActivity.class);
        intent.putExtra("ID", id);
        intent.putExtra("TITLE", mainCatName);
        startActivity(intent);
    }

    @Override
    public void collectionClick(String id, String title) {
        Intent intent = new Intent(this, CollectionProductActivity.class);
        intent.putExtra("ID", id);
        intent.putExtra("TITLE", title);
        startActivity(intent);
    }

    @Override
    public void onClick(View view) {
        if (view == imgHomeCart) {
            Intent intent = new Intent(this, CartActivity.class);
            startActivity(intent);

        } /*else if (view == collection_seeall) {
            if (collections.size() > 6) {
                collections.clear();
                for (int i = 0; i < Math.min(6, homeModel.getCollection().size()); i++) {
                    collections.add(homeModel.getCollection().get(i));
                }
            } else {
                collections.clear();
                collections.addAll(homeModel.getCollection());
            }
            collectionAdapter.notifyDataSetChanged();

        }*/ else if (view == explore_seell) {
//            explorerCategories.clear();
//            explorerCategories.addAll(homeModel.getExplorerCategory());
//            explorerAdapter.notifyDataSetChanged();
            MainActivity.tabHost.setCurrentTab(1);
        } else if (view == searchView) {
            Intent intent = new Intent(this, SearchActivity.class);
            startActivity(intent);
        } else if (view == imgHomeWishlist) {
            Intent intent = new Intent(this, WishListActivity.class);
            startActivity(intent);
        }
    }

    private class SliderTimer extends TimerTask {

        private ViewPager viewPag;
        int sizes;

        public SliderTimer(ViewPager viewPag, int sizes) {
            this.sizes = sizes;
            this.viewPag = viewPag;
        }

        @Override
        public void run() {
            HomeActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (viewPag.getCurrentItem() < sizes - 1) {
                        viewPag.setCurrentItem(viewPag.getCurrentItem() + 1);
                    } else {
                        viewPag.setCurrentItem(0);
                    }
                }
            });
        }
    }
}
