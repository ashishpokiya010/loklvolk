package com.loklvokl.lokl.activtiy;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import com.loklvokl.lokl.R;
import com.loklvokl.lokl.adapter.ProductDealAdapter;
import com.loklvokl.lokl.model.OfferProduct;
import com.loklvokl.lokl.model.Product;
import com.loklvokl.lokl.util.ApiClient;
import com.loklvokl.lokl.util.ApiInterface;
import com.loklvokl.lokl.util.EndlessRecyclerOnScrollListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OffersActivity extends BaseActivity {

    private static final String TAG = "Product_list";
    RecyclerView recyclerView;
    StaggeredGridLayoutManager staggeredGridLayoutManager;
    ProgressBar progressBar;
    ProductDealAdapter productDealAdapter;
    List<Product> products = new ArrayList<>();
    String productId = "";
    int currentPage = 1, totalPages;
    ImageView back_btn;
    TextView empty_text;
    RelativeLayout layout_sort;
    TextView txtSelectedSort;
    ApiInterface apiService;
    ImageView imgOfferWishlist, imgOfferSearch, imgOfferCart;

    String selectedSort = "popular";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offfers);
        apiService = ApiClient.getClient().create(ApiInterface.class);

        init();
        getINtentData();
        loadProductData(false);

    }

    private void getINtentData() {
        if (getIntent().hasExtra("BANNER_ID")) {
            productId = getIntent().getStringExtra("BANNER_ID");
            back_btn.setVisibility(View.VISIBLE);
        }
    }

    public void loadProductData(Boolean isSort) {
        progressBar.setVisibility(View.VISIBLE);
        layout_sort.setVisibility(View.GONE);
        Map<String, String> data = new HashMap<>();
        data.put("offer_id", productId);
        data.put("pageno", String.valueOf(currentPage));
        if(!selectedSort.equals("popular")) {
            data.put(selectedSort, "");
        }
        Call<OfferProduct> call = apiService.getOfferData(data);
        call.enqueue(new Callback<OfferProduct>() {
            @Override
            public void onResponse(Call<OfferProduct> call, Response<OfferProduct> response) {
                progressBar.setVisibility(View.GONE);
                if(isSort){
                    products.clear();
                }
                OfferProduct sliderRequest = response.body();

                if (sliderRequest != null) {
                    products.addAll(sliderRequest.getOfferRequest().get(0).getProduct());
                    totalPages = sliderRequest.getOfferRequest().get(0).getTotalPages();
                }
                Log.d("onScrolled call : ","Here "+products.size());
                if (products != null && products.size() > 0) {
                    empty_text.setVisibility(View.GONE);
                    layout_sort.setVisibility(View.VISIBLE);
                    if(productDealAdapter == null) {
                        setProductData();
                    } else {
                        if(isSort) {
                            //On sorting of data
                            productDealAdapter.notifyDataSetChanged();
                        } else {
                            //On load more data
                            productDealAdapter.notifyItemRangeChanged(0, products.size());
                        }
                    }
                } else {
                    empty_text.setVisibility(View.VISIBLE);
                    layout_sort.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<OfferProduct> call, Throwable t) {
                // Log error here since request failed
                Log.e(TAG, t.toString());
                empty_text.setVisibility(View.VISIBLE);
                layout_sort.setVisibility(View.GONE);
            }
        });
    }

    private void setProductData() {

        productDealAdapter = new ProductDealAdapter(this, products, false);
        recyclerView.setAdapter(productDealAdapter);
        Log.d("onScrolled : "," - - "+products.size()+" - "+totalPages);
    }

    private void init() {
        empty_text = findViewById(R.id.empty_text);
        progressBar = findViewById(R.id.progressBar);
        recyclerView = findViewById(R.id.recyclerView);
        back_btn = findViewById(R.id.back_btn);
        imgOfferCart = findViewById(R.id.img_offer_cart);
        imgOfferSearch = findViewById(R.id.img_offer_search);
        imgOfferWishlist = findViewById(R.id.img_offer_wishlist);

        layout_sort = findViewById(R.id.layout_sort);
        txtSelectedSort = findViewById(R.id.txt_selected_sort);

        //recyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        staggeredGridLayoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(staggeredGridLayoutManager);


        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        imgOfferSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(OffersActivity.this, SearchActivity.class);
                startActivity(intent);
            }
        });
        imgOfferWishlist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(OffersActivity.this, WishListActivity.class);
                startActivity(intent);
            }
        });
        imgOfferCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(OffersActivity.this, CartActivity.class);
                startActivity(intent);
            }
        });

        recyclerView.addOnScrollListener(new EndlessRecyclerOnScrollListener(staggeredGridLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                if(page <= totalPages) {
                    currentPage = page;
                    loadProductData(false);
                }
            }
        });

    }

    public void onSortOfferClick(View view){
        showSortDialog();
    }

    AlertDialog alertDialog;
    private void showSortDialog() {
        final AlertDialog.Builder alert = new AlertDialog.Builder(OffersActivity.this,
                R.style.AppThemeDialogAlert);
        View mView = getLayoutInflater().inflate(R.layout.popup_sort, null);
        RadioGroup rg_sort = mView.findViewById(R.id.group_product_condition);
        RadioButton rb_popular = mView.findViewById(R.id.rb_sort_popular);
        RadioButton rb_latest = mView.findViewById(R.id.rb_sort_latest);
        RadioButton rb_lowhigh = mView.findViewById(R.id.rb_sort_lowtohigh);
        RadioButton rb_hightlow = mView.findViewById(R.id.rb_sort_hightolow);

        rb_latest.setChecked(false);
        rb_lowhigh.setChecked(false);
        rb_hightlow.setChecked(false);

        alert.setView(mView);
        alertDialog = alert.create();
        alertDialog.setCanceledOnTouchOutside(true);

        Log.d("SortClick Collection: "," - - Here  - "+selectedSort);
        if(selectedSort.equalsIgnoreCase("latest-arrival")){
            rb_latest.setChecked(true);
        }else if (selectedSort.equalsIgnoreCase("low-to-high")){
            rb_lowhigh.setChecked(true);
        } else if (selectedSort.equalsIgnoreCase("high-to-low")){
            rb_hightlow.setChecked(true);
        } else if (selectedSort.equalsIgnoreCase("popular")){
            rb_popular.setChecked(true);
        }
        rg_sort.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton selectedRadioButton = mView.findViewById(checkedId);
                if(selectedRadioButton.getText().toString().contains("Latest Arrivals")) {
                    selectedSort = "latest-arrival";
                } else if (selectedRadioButton.getText().toString().contains("Low - High")){
                    selectedSort = "low-to-high";
                } else if (selectedRadioButton.getText().toString().contains("High - Low")){
                    selectedSort = "high-to-low";
                } else  if (selectedRadioButton.getText().toString().contains("Popular")){
                    selectedSort = "popular";
                }
                Log.d("SortClick Collection: "," - - Here  - "+selectedSort);
                txtSelectedSort.setText(selectedRadioButton.getText());
                alertDialog.dismiss();
                currentPage = 1;
                loadProductData(true);
            }
        });
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.show();

    }
}