package com.loklvokl.lokl.activtiy;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;

import com.facebook.login.LoginManager;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.loklvokl.lokl.R;
import com.loklvokl.lokl.util.SharedPref;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

public class ProfileActivity extends BaseActivity implements View.OnClickListener {

    TextView logout_txt, txt_name, txt_email, txt_phone, btn_login, txt_version;
    LinearLayout privacy_txt, faq_txt, address_txt, order_txt, help_txt;
    LinearLayout update_stock, sold_order_txt, wishlist_txt;
    SharedPref sharedPref;
    int ATTRIB_LOGIN = 101, ATTRIB_EDIT = 102;
    RelativeLayout layout_user;
    LinearLayout main_profile;
    ImageView imgProfileCart;
    ImageView img_user;
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        main_profile = findViewById(R.id.main_profile);
        main_profile.setVisibility(View.VISIBLE);

        sharedPref = new SharedPref(this);
        init();
    }

    private void updateData() {
        Boolean isLogin = sharedPref.getBool("IS_LOGIN");
        txt_email.setVisibility(View.GONE);
        txt_phone.setVisibility(View.GONE);
        txt_name.setText("Welcome User");

        if (!isLogin) {
            layout_user.setVisibility(View.VISIBLE);
            logout_txt.setVisibility(View.GONE);
            btn_login.setText("Login");
            img_user.setImageResource(R.drawable.profile_default_icon);
        } else {
            btn_login.setText("Edit");
            main_profile.setVisibility(View.VISIBLE);
            logout_txt.setVisibility(View.VISIBLE);
            layout_user.setVisibility(View.VISIBLE);

            String full_name = sharedPref.getStr("NAME") + " " + sharedPref.getStr("LNAME");
            txt_name.setText(full_name);

            if (!sharedPref.getStr("EMAIL").trim().isEmpty()) {
                txt_email.setVisibility(View.VISIBLE);
                txt_email.setText(sharedPref.getStr("EMAIL"));
            }
            if (!sharedPref.getStr("MOBILE").trim().isEmpty()) {
                txt_phone.setVisibility(View.VISIBLE);
                txt_phone.setText(sharedPref.getStr("MOBILE"));
            }

            if (!sharedPref.getStr("IMAGE").isEmpty()) {
                progressBar.setVisibility(View.VISIBLE);
                Picasso.get().load("https://" + sharedPref.getStr("IMAGE"))
                        .resize(0, 250)
                        .into(img_user, new Callback() {
                            @Override
                            public void onSuccess() {
                                progressBar.setVisibility(View.GONE);
                            }

                            @Override
                            public void onError(Exception e) {
                                progressBar.setVisibility(View.GONE);
                            }
                        });
            } else {
                img_user.setImageResource(R.drawable.profile_default_icon);
            }
        }
    }

    private void init() {
        progressBar = findViewById(R.id.progressBar);
        progressBar.setVisibility(View.GONE);
        imgProfileCart = findViewById(R.id.img_profile_cart);
        txt_name = findViewById(R.id.txt_name);
        txt_email = findViewById(R.id.txt_email);
        txt_phone = findViewById(R.id.txt_phone);
        layout_user = findViewById(R.id.layout_user);
        update_stock = findViewById(R.id.update_stock);
        sold_order_txt = findViewById(R.id.sold_order_txt);
        wishlist_txt = findViewById(R.id.wishlist_txt);
        help_txt = findViewById(R.id.help_txt);
        wishlist_txt.setOnClickListener(this);
        imgProfileCart.setOnClickListener(this);

        btn_login = findViewById(R.id.btn_login);
        btn_login.setOnClickListener(this);
        logout_txt = findViewById(R.id.logout_txt);
        logout_txt.setOnClickListener(this);
        txt_version = findViewById(R.id.version_txt);

        img_user = findViewById(R.id.img_user);
        privacy_txt = findViewById(R.id.privacy_txt);
        privacy_txt.setOnClickListener(this);
        faq_txt = findViewById(R.id.faq_txt);
        faq_txt.setOnClickListener(this);
        address_txt = findViewById(R.id.address_txt);
        address_txt.setOnClickListener(this);
        order_txt = findViewById(R.id.order_txt);
        order_txt.setOnClickListener(this);
        update_stock.setOnClickListener(this);
        sold_order_txt.setOnClickListener(this);
        help_txt.setOnClickListener(this);

        txt_version.setText("Version "+sharedPref.getVersionName());
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateData();
    }

    @Override
    public void onClick(View view) {
        if (view == btn_login) {
            if (btn_login.getText().equals("Edit")) {
                Intent i = new Intent(ProfileActivity.this, EditActivity.class);
                startActivityForResult(i, ATTRIB_EDIT);
            } else {
                Intent i = new Intent(ProfileActivity.this, LoginActivity.class);
                startActivityForResult(i, ATTRIB_LOGIN);
            }
        } else if(view == imgProfileCart){
            Intent intent = new Intent(ProfileActivity.this, CartActivity.class);
            startActivity(intent);
        } else if (view == logout_txt) {
            openConfirmDialog();

        } else if (view == privacy_txt) {
            Intent i = new Intent(ProfileActivity.this, PrivacyActivity.class);
            startActivity(i);

        } else if (view == faq_txt) {
            Intent i = new Intent(ProfileActivity.this, FAQActivity.class);
            i.putExtra("TITLE", "FAQ's");
            startActivity(i);

        } else if (view == layout_user) {
            Intent i = new Intent(ProfileActivity.this, EditActivity.class);
            startActivity(i);

        } else if (view == address_txt) {
            Intent i = new Intent(ProfileActivity.this, AddressActivity.class);
            i.putExtra("FROM_PROFILE", true);
            startActivity(i);

        } else if (view == order_txt) {
            if (sharedPref.getBool("IS_LOGIN")) {
                Intent i = new Intent(ProfileActivity.this, MyOrderActivity.class);
                i.putExtra("FROM_PROFILE", true);
                startActivity(i);
            } else {
                Intent intent = new Intent(this, LoginActivity.class);
                startActivityForResult(intent, ATTRIB_LOGIN);
            }

        } else if (view == sold_order_txt) {
            if (sharedPref.getBool("IS_LOGIN")) {
                Intent i = new Intent(ProfileActivity.this, MySoldOrderActivity.class);
                i.putExtra("FROM_PROFILE", true);
                startActivity(i);
            } else {
                Intent intent = new Intent(this, LoginActivity.class);
                startActivityForResult(intent, ATTRIB_LOGIN);
            }

        } else if (view == update_stock) {
            if (sharedPref.getBool("IS_LOGIN")) {
                Intent i = new Intent(ProfileActivity.this, UpdateStockActivity.class);
                i.putExtra("FROM_PROFILE", true);
                startActivity(i);
            } else {
                Intent intent = new Intent(this, LoginActivity.class);
                startActivityForResult(intent, ATTRIB_LOGIN);
            }
        } else if (view == wishlist_txt) {
            Intent i = new Intent(ProfileActivity.this, WishListActivity.class);
            i.putExtra("FROM_PROFILE", true);
            startActivity(i);
        } else if (view == help_txt) {
            Intent i = new Intent(ProfileActivity.this, HelpActivity.class);
            i.putExtra("TITLE", "Help");
            startActivity(i);
        }
    }

    AlertDialog alertDialog;

    private void openConfirmDialog() {

        final AlertDialog.Builder alert = new AlertDialog.Builder(ProfileActivity.this,
                R.style.AppThemeDialogAlert);
        View mView = getLayoutInflater().inflate(R.layout.alert_dialog_logout, null);
        // RecyclerView recyclerView = mView.findViewById(R.id.recyclerviewCountry);
        Button btnCancel, btnOk;
        btnCancel = mView.findViewById(R.id.btn_cancle);
        btnOk = mView.findViewById(R.id.btn_ok);
        alert.setView(mView);

        alertDialog = alert.create();
        alertDialog.setCanceledOnTouchOutside(true);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.show();

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sharedPref.clearData();
                MainActivity.tabHost.setCurrentTab(0);
                Toast.makeText(ProfileActivity.this, "Logout Successfully.", Toast.LENGTH_SHORT).show();

                LoginManager.getInstance().logOut();
                GoogleSignInOptions gso = new GoogleSignInOptions.
                        Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).
                        build();

                GoogleSignInClient googleSignInClient = GoogleSignIn.getClient(ProfileActivity.this, gso);
                googleSignInClient.signOut();
                alertDialog.dismiss();

                Intent intent = new Intent(ProfileActivity.this, SplashActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            }
        });

       /* new AlertDialog.Builder(this)
                .setTitle("Logout")
                .setMessage("Are you sure you want to logout?")
                // .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {

                    }
                })
                .setNegativeButton(android.R.string.no, null).show();*/
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == ATTRIB_LOGIN) {
            Log.e("log_log", ">" + resultCode);
            if (resultCode == RESULT_OK) {
                logout_txt.setVisibility(View.VISIBLE);
                layout_user.setVisibility(View.VISIBLE);
                btn_login.setText("Login");
            } else {
                MainActivity.tabHost.setCurrentTab(0);
                btn_login.setText("Edit");
            }
        }
    }

    @Override
    public void onBackPressed(){
        Intent intent = new Intent(ProfileActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
        super.onBackPressed();
    }
}