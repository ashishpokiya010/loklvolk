package com.loklvokl.lokl.activtiy;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.IntentSender;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.credentials.Credential;
import com.google.android.gms.auth.api.credentials.HintRequest;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.loklvokl.lokl.R;
import com.loklvokl.lokl.model.CheckLogin;
import com.loklvokl.lokl.model.DysplayProfileModel;
import com.loklvokl.lokl.model.GetOtp;
import com.loklvokl.lokl.model.SignUpResponse;
import com.loklvokl.lokl.model.TrueCallerVerify;
import com.loklvokl.lokl.model.User;
import com.loklvokl.lokl.model.UserProfile;
import com.loklvokl.lokl.receiver.AppSignatureHashHelper;
import com.loklvokl.lokl.util.ApiClient;
import com.loklvokl.lokl.util.ApiInterface;
import com.loklvokl.lokl.util.Constant;
import com.loklvokl.lokl.util.Progress;
import com.loklvokl.lokl.util.SharedPref;
import com.truecaller.android.sdk.ITrueCallback;
import com.truecaller.android.sdk.TrueError;
import com.truecaller.android.sdk.TrueException;
import com.truecaller.android.sdk.TrueProfile;
import com.truecaller.android.sdk.TruecallerSDK;
import com.truecaller.android.sdk.TruecallerSdkScope;
import com.truecaller.android.sdk.clients.VerificationCallback;
import com.truecaller.android.sdk.clients.VerificationDataBundle;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddMobileNumberActivity extends AppCompatActivity implements View.OnClickListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    private static final int CREDENTIAL_PICKER_REQUEST = 1;
    private static final int RC_HINT = 1000;

    private static final String TAG = "AddMobileNumberActivity";
    private ImageView back_btn;
    private TextView title_text, text;
    private EditText ed_mobile;
    private ConstraintLayout cl_verify, cl_otp_verify;
    private GoogleApiClient googleApiClient;
    private boolean isShowOnce = false;
    private ConstraintLayout cl_add_mobile_number, cl_verification_process, cl_verify_otp;
    private EditText et_otp_one, et_otp_two, et_otp_three, et_otp_four;

    private ProgressDialog progressDialog;
    private ApiInterface apiService;
    private int verificationCallbackType;
    private CountDownTimer timer;
    private SharedPref sharedPref;
    private boolean
            isFailure = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_mobile_number);

//        initTrueCaller();
        showHint();

        bind();
        init();
        listner();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.d(TAG, "Connected");
    }

    @Override
    public void onConnectionSuspended(int cause) {
        Log.d(TAG, "GoogleApiClient is suspended with cause code: " + cause);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    private void init() {
        apiService = ApiClient.getClient().create(ApiInterface.class);
        sharedPref = new SharedPref(this);

        isFailure = getIntent().getBooleanExtra("isFailure", false);

        AppSignatureHashHelper appSignatureHashHelper = new AppSignatureHashHelper(this);
        Log.e("TAG", "init: " + appSignatureHashHelper.getAppSignatures().get(0));

    }

    private void listner() {
        cl_verify.setOnClickListener(this);
        cl_otp_verify.setOnClickListener(this);
        back_btn.setOnClickListener(this);

        et_otp_one.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.toString().length() == 1) {
                    et_otp_two.requestFocus();
                }
            }
        });

        et_otp_two.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.toString().length() == 1) {
                    et_otp_three.requestFocus();
                } else if (editable.toString().length() == 0) {
                    et_otp_one.requestFocus();
                }
            }
        });

        et_otp_three.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.toString().length() == 1) {
                    et_otp_four.requestFocus();
                } else if (editable.toString().length() == 0) {
                    et_otp_two.requestFocus();
                }
            }
        });

        et_otp_four.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                if (editable.toString().length() == 1) {
                    cl_otp_verify.callOnClick();
                } else if (editable.toString().length() == 0) {
                    et_otp_three.requestFocus();
                }
            }
        });

    }

    private void bind() {
        back_btn = findViewById(R.id.back_btn);
        text = findViewById(R.id.text);
        text.setText(Html.fromHtml(getResources().getString(R.string.terms_policy)));
        text.setMovementMethod(LinkMovementMethod.getInstance());

        et_otp_one = findViewById(R.id.et_otp_one);
        et_otp_two = findViewById(R.id.et_otp_two);
        et_otp_three = findViewById(R.id.et_otp_three);
        et_otp_four = findViewById(R.id.et_otp_four);

        cl_add_mobile_number = findViewById(R.id.cl_add_mobile_number);
        cl_verification_process = findViewById(R.id.cl_verification_process);
        cl_verify_otp = findViewById(R.id.cl_verify_otp);
        title_text = findViewById(R.id.title_text);
        ed_mobile = findViewById(R.id.ed_mobile);
        title_text.setText("");
        cl_verify = findViewById(R.id.cl_verify);
        cl_otp_verify = findViewById(R.id.cl_otp_verify);

        cl_add_mobile_number.setVisibility(View.VISIBLE);
        cl_verification_process.setVisibility(View.GONE);
        cl_verify_otp.setVisibility(View.GONE);
        clearOtpView();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.cl_otp_verify:

                String one = et_otp_one.getText().toString().trim();
                String two = et_otp_two.getText().toString().trim();
                String three = et_otp_three.getText().toString().trim();
                String four = et_otp_four.getText().toString().trim();

                if (!TextUtils.isEmpty(one) && !TextUtils.isEmpty(two) && !TextUtils.isEmpty(three) && !TextUtils.isEmpty(four)) {
                    String otp = one + two + three + four;
//                    final TrueProfile profile = new TrueProfile.Builder("", "").build();
//                    TruecallerSDK.getInstance().verifyOtp(profile, otp, apiCallback);
                } else {
                    Toast.makeText(this, "please enter valid otp", Toast.LENGTH_SHORT).show();
                }

                break;

            case R.id.cl_verify:

                String mobile = ed_mobile.getText().toString();

                if (mobile.isEmpty()) {
                    Toast.makeText(this, "Mobile is required.", Toast.LENGTH_SHORT).show();
                } else if (mobile.length() != 10) {
                    Toast.makeText(this, "Mobile is not valid.", Toast.LENGTH_SHORT).show();
                } else {
//                    initTrueCaller();

                    if (isFailure) {
                        getOtp(mobile);
                    } /*else {
                        TruecallerSDK.getInstance().getUserProfile(AddMobileNumberActivity.this);
                    }*/
                }
                break;

            case R.id.back_btn:

                onBackPressed();

                break;
        }
    }

    public void getOtp(String mobile) {
        progressDialog = null;
        progressDialog = Progress.showProgressDialog(this, "please wait.", false);
        progressDialog.show();
        Call<GetOtp> call = apiService.sendOTP(mobile);

        call.enqueue(new Callback<GetOtp>() {
            @Override
            public void onResponse(Call<GetOtp> call, Response<GetOtp> response) {
                Log.d(TAG, "data: " + response.body());

                GetOtp otp = response.body();
                if (otp.getResponse().get(0).getError()) {
                    Toast.makeText(AddMobileNumberActivity.this, otp.getResponse().get(0).getMessage(), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(AddMobileNumberActivity.this, otp.getResponse().get(0).getMessage(), Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(AddMobileNumberActivity.this, EnterOtpActivity.class);
                    intent.putExtra("number", ed_mobile.getText().toString());
                    intent.putExtra("otp", String.valueOf(otp.getResponse().get(0).getOtp()));
                    startActivity(intent);
                }

                if (progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

            }

            @Override
            public void onFailure(Call<GetOtp> call, Throwable t) {

                if (progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                Toast.makeText(AddMobileNumberActivity.this, "Fail! Try again.", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_HINT) {
            if (resultCode == RESULT_OK) {
                Credential cred = data.getParcelableExtra(Credential.EXTRA_KEY);
                ed_mobile.setText(cred.getId().replace("+91", ""));
            } else {
//                ui.focusPhoneNumber();
            }
        } /*else {
            TruecallerSDK.getInstance().onActivityResultObtained(this, resultCode, data);
        }*/
    }

    private void showHint() {

        googleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .enableAutoManage(this, this)
                .addApi(Auth.CREDENTIALS_API)
                .build();
        googleApiClient.connect();

        HintRequest hintRequest = new HintRequest.Builder()
                .setPhoneNumberIdentifierSupported(true)
                .build();

        PendingIntent intent = Auth.CredentialsApi.getHintPickerIntent(googleApiClient, hintRequest);
        try {
            startIntentSenderForResult(intent.getIntentSender(), RC_HINT, null, 0, 0, 0);
        } catch (IntentSender.SendIntentException e) {
            Log.e(TAG, "Could not start hint picker Intent", e);
        }
    }


//    private void initTrueCaller() {
//
//        TruecallerSdkScope trueScope = new TruecallerSdkScope.Builder(this, new ITrueCallback() {
//            @Override
//            public void onSuccessProfileShared(@NonNull TrueProfile trueProfile) {
//                Log.e("TAG", "onSuccessProfileShared: " + trueProfile);
//            }
//
//            @Override
//            public void onFailureProfileShared(@NonNull TrueError trueError) {
//                Log.e("TAG", "onFailureProfileShared: " + trueError);
//            }
//
//            @Override
//            public void onVerificationRequired(TrueError trueError) {
//                TruecallerSDK.getInstance().requestVerification("IN", ed_mobile.getText().toString(), apiCallback, AddMobileNumberActivity.this);
//            }
//        })
//                .consentMode(TruecallerSdkScope.CONSENT_MODE_BOTTOMSHEET)
//                .buttonColor(getResources().getColor(R.color.colorPrimary))
//                .buttonTextColor(Color.WHITE)
//                .loginTextPrefix(TruecallerSdkScope.LOGIN_TEXT_PREFIX_TO_GET_STARTED)
//                .loginTextSuffix(TruecallerSdkScope.LOGIN_TEXT_SUFFIX_PLEASE_VERIFY_MOBILE_NO)
//                .ctaTextPrefix(TruecallerSdkScope.CTA_TEXT_PREFIX_USE)
//                .buttonShapeOptions(TruecallerSdkScope.BUTTON_SHAPE_ROUNDED)
//                .privacyPolicyUrl("http://loklvokl.co.in/privacy-policy")
//                .termsOfServiceUrl("http://loklvokl.co.in/tc")
//                .footerType(TruecallerSdkScope.FOOTER_TYPE_NONE)
//                .consentTitleOption(TruecallerSdkScope.SDK_CONSENT_TITLE_LOG_IN)
//                .sdkOptions(TruecallerSdkScope.SDK_OPTION_WITHOUT_OTP)
//                .build();
//        TruecallerSDK.init(trueScope);
//    }
//
//    private final VerificationCallback apiCallback = new VerificationCallback() {
//
//        @Override
//        public void onRequestSuccess(int requestCode, @Nullable VerificationDataBundle extras) {
//
//            Log.e("TAG", "onRequestSuccess: " + requestCode);
//
//            if (requestCode == VerificationCallback.TYPE_MISSED_CALL_INITIATED) {
//                verificationCallbackType = VerificationCallback.TYPE_MISSED_CALL_INITIATED;
//
//                cl_add_mobile_number.setVisibility(View.GONE);
//                cl_verification_process.setVisibility(View.VISIBLE);
//                cl_verify_otp.setVisibility(View.GONE);
//
//                clearOtpView();
//
//                String ttl = extras.getString(VerificationDataBundle.KEY_TTL);
//
//                if (ttl != null) {
//                    showCountDownTimer(Double.parseDouble(ttl) * 1000);
//                }
//
//                Log.e("TAG", "onRequestSuccess: TYPE_MISSED_CALL_INITIATED");
//                Log.e("TAG", "onRequestSuccess: TYPE_MISSED_CALL_INITIATED" + extras.getString(VerificationDataBundle.KEY_TTL));
//
//            } else if (requestCode == VerificationCallback.TYPE_MISSED_CALL_RECEIVED) {
//
//                final TrueProfile profile = new TrueProfile.Builder("", "").build();
//                TruecallerSDK.getInstance().verifyMissedCall(profile, apiCallback);
//
//            } else if (requestCode == VerificationCallback.TYPE_OTP_INITIATED) {
//
//                verificationCallbackType = VerificationCallback.TYPE_OTP_INITIATED;
//
//                cl_add_mobile_number.setVisibility(View.GONE);
//                cl_verification_process.setVisibility(View.GONE);
//                cl_verify_otp.setVisibility(View.VISIBLE);
//
//                clearOtpView();
//
//
//                String ttl = extras.getString(VerificationDataBundle.KEY_TTL);
//                if (ttl != null) {
//                    showCountDownTimer(Double.parseDouble(ttl) * 1000);
//                }
//
//                Log.e("TAG", "onRequestSuccess: TYPE_OTP_INITIATED");
//                Log.e("TAG", "onRequestSuccess: TYPE_OTP_INITIATED" + extras.getString(VerificationDataBundle.KEY_OTP));
//
//            } else if (requestCode == VerificationCallback.TYPE_OTP_RECEIVED) {
//                fillOtp(extras.getString(VerificationDataBundle.KEY_OTP));
//            } else if (requestCode == VerificationCallback.TYPE_PROFILE_VERIFIED_BEFORE) {
//
//                checkLogin(extras.getProfile(), extras.getProfile().phoneNumber.replace("+91", ""));
//
//            } else {
//                dismissCountDownTimer();
//
//                verifyProfile(extras, extras.getString(VerificationDataBundle.KEY_ACCESS_TOKEN));
//
//            }
//
//        }
//
//        @Override
//        public void onRequestFailure(int i, @NonNull TrueException e) {
//            Log.e("TAG", "onRequestFailure: " + i);
//
//            String mobile = ed_mobile.getText().toString();
//            getOtp(mobile);
//        }
//
//    };

    public void verifyProfile(VerificationDataBundle extras, String token) {

        progressDialog = null;
        progressDialog = Progress.showProgressDialog(this, "please wait.", false);
        progressDialog.show();

        Call<TrueCallerVerify> call = apiService.verifyTrueCall(token, getResources().getString(R.string.partnerKey));

        call.enqueue(new Callback<TrueCallerVerify>() {
            @Override
            public void onResponse(Call<TrueCallerVerify> call, Response<TrueCallerVerify> response) {

                if (response.isSuccessful()) {

                    TrueCallerVerify trueCallerVerify = response.body();

                    if (trueCallerVerify.getCode() == 200) {
                        checkLogin(extras.getProfile(), String.valueOf(trueCallerVerify.getPhoneNumber()).replace("+91", ""));
                    } else {
                        cl_add_mobile_number.setVisibility(View.VISIBLE);
                        cl_verification_process.setVisibility(View.GONE);
                        cl_verify_otp.setVisibility(View.GONE);
                        clearOtpView();
                    }
                } else {
                    cl_add_mobile_number.setVisibility(View.VISIBLE);
                    cl_verification_process.setVisibility(View.GONE);
                    cl_verify_otp.setVisibility(View.GONE);
                    clearOtpView();

                }

                if (progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<TrueCallerVerify> call, Throwable t) {


                if (progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                Toast.makeText(AddMobileNumberActivity.this, "Fail! Try again.", Toast.LENGTH_SHORT).show();
            }
        });


    }

    public void checkLogin(TrueProfile profile, String number) {
        progressDialog = null;
        progressDialog = Progress.showProgressDialog(this, "please wait.", false);
        progressDialog.show();

        Call<CheckLogin> call = apiService.checkLogin(number, "", "");

        call.enqueue(new Callback<CheckLogin>() {
            @Override
            public void onResponse(Call<CheckLogin> call, Response<CheckLogin> response) {
                if (progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                CheckLogin body = response.body();
                if (body.getResponse().get(0).isRegister()) {
                    String userId = body.getResponse().get(0).getUser_id();
//                    sharedPref.setStr("MOBILE", number);
//                    sharedPref.setStr("ID", userId);
//                    sharedPref.setBool("IS_LOGIN", true);
//
//                    Intent i = new Intent(AddMobileNumberActivity.this, MainActivity.class);
//                    startActivity(i);
//                    finish();

                    displayProfile(userId);



                } else {
                    registerUser(profile, number);
                }


            }

            @Override
            public void onFailure(Call<CheckLogin> call, Throwable t) {

                if (progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                Toast.makeText(AddMobileNumberActivity.this, "Fail! Try again.", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void displayProfile(String user_id) {

        progressDialog = null;
        progressDialog = Progress.showProgressDialog(this, "please wait.", false);
        progressDialog.show();

        Call<DysplayProfileModel> call = apiService.diplayProfile(user_id);

        call.enqueue(new Callback<DysplayProfileModel>() {
            @Override
            public void onResponse(Call<DysplayProfileModel> call, Response<DysplayProfileModel> response) {
                DysplayProfileModel body = response.body();

                if (body.getUserProfile() != null) {
                    if (body.getUserProfile().getUserId() != null) {
//                        Toast.makeText(SplashActivity.this, body., Toast.LENGTH_SHORT).show();
                        UserProfile user = body.getUserProfile();
                        sharedPref.setStr("MOBILE", user.getMobile());
                        sharedPref.setStr("NAME", user.getUsername());
                        sharedPref.setStr("LNAME", user.getLastName());
                        sharedPref.setStr("EMAIL", user.getEmail());
                        sharedPref.setStr("IMAGE", user.getProfileImage());
                        sharedPref.setStr("ID", user.getUserId());
                        sharedPref.setBool("IS_LOGIN", true);
                        sharedPref.setStr("CITY", user.getCity());
                        sharedPref.setStr("GENDER", user.getGender());
                        sharedPref.setStr("DOB", user.getDob());
                        sharedPref.setStr("FNAME", user.getFirstName());
                        sharedPref.setAdharList("ADHAR_LIST", user.getAadharDocument());
                        sharedPref.setPanList("PAN_LIST", user.getPanDocument());
                        sharedPref.setInt("PAN_COUNT", user.getPan_counter());
                        sharedPref.setInt("ADHAR_COUNT", user.getAddhar_counter());
                        // sharedPref.set("FNAME", user.getKycDetails().get);


                        Intent i = new Intent(AddMobileNumberActivity.this, MainActivity.class);
                        startActivity(i);
                        finish();


                    }
                }


                if (progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

            }

            @Override
            public void onFailure(Call<DysplayProfileModel> call, Throwable t) {
                Log.e("TAG", t.toString());

                if (progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
            }
        });

    }

    public void registerUser(TrueProfile profile, String number) {

        progressDialog = null;
        progressDialog = Progress.showProgressDialog(this, "please wait.", false);
        progressDialog.show();

        Call<SignUpResponse> call = apiService.requestSignup(profile.firstName, profile.lastName, profile.email, number, number, Constant.getDeviceId(this));

        call.enqueue(new Callback<SignUpResponse>() {
            @Override
            public void onResponse(Call<SignUpResponse> call, Response<SignUpResponse> response) {
                if (progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                SignUpResponse body = response.body();

                if (body.getResponse() != null && body.getResponse().size() > 0) {
                    Toast.makeText(AddMobileNumberActivity.this, body.getResponse().get(0).getMessage(), Toast.LENGTH_SHORT).show();

                    if (!body.getResponse().get(0).getError()) {

                        User user = body.getResponse().get(0).getUser().get(0);
                        sharedPref.setStr("MOBILE", user.getMobile());
                        sharedPref.setStr("NAME", user.getUsername());
                        sharedPref.setStr("LNAME", user.getLast_name());
                        sharedPref.setStr("IMAGE", user.getProfile_image());
                        sharedPref.setStr("EMAIL", user.getEmail());
                        sharedPref.setStr("ID", user.getId());
                        sharedPref.setBool("IS_LOGIN", true);
                        sharedPref.setStr("CITY", user.getCity());
                        sharedPref.setStr("GENDER", user.getGender());
                        sharedPref.setStr("DOB", user.getDob());

                        Intent i = new Intent(AddMobileNumberActivity.this, MainActivity.class);
                        startActivity(i);
                        finish();
                    }
                }
            }

            @Override
            public void onFailure(Call<SignUpResponse> call, Throwable t) {


                if (progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                Toast.makeText(AddMobileNumberActivity.this, "Fail! Try again.", Toast.LENGTH_SHORT).show();
            }
        });

    }

    public void fillOtp(String otp) {

        String[] chars = new String[otp.length()];
        for (int i = 0; i < otp.length(); i++) {
            chars[i] = Character.toString(otp.charAt(i));
        }
        et_otp_one.setText(chars[0]);
        et_otp_two.setText(chars[1]);
        et_otp_three.setText(chars[2]);
        et_otp_four.setText(chars[3]);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        dismissCountDownTimer();
        TruecallerSDK.clear();
    }

    public void dismissCountDownTimer() {
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }

    @Override
    public void onRequestPermissionsResult(final int requestCode, @NonNull final String[] permissions,
                                           @NonNull final int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    public void showCountDownTimer(Double ttl) {

        timer = new CountDownTimer(ttl.longValue(), 1000) {
            @Override
            public void onTick(final long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                cl_add_mobile_number.setVisibility(View.VISIBLE);
                cl_verification_process.setVisibility(View.GONE);
                cl_verify_otp.setVisibility(View.GONE);
                clearOtpView();
            }
        };
        timer.start();
    }

    public void clearOtpView() {
        et_otp_one.setText("");
        et_otp_two.setText("");
        et_otp_three.setText("");
        et_otp_four.setText("");
    }

}