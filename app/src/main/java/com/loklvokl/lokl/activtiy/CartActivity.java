package com.loklvokl.lokl.activtiy;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.loklvokl.lokl.R;
import com.loklvokl.lokl.adapter.CartAdapter;
import com.loklvokl.lokl.model.CartDetail.CartDetail;
import com.loklvokl.lokl.model.CartDetail.Product;
import com.loklvokl.lokl.model.Order;
import com.loklvokl.lokl.model.ProductDetail;
import com.loklvokl.lokl.util.ApiClient;
import com.loklvokl.lokl.util.ApiInterface;
import com.loklvokl.lokl.util.Constant;
import com.loklvokl.lokl.util.SharedPref;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CartActivity extends BaseActivity implements View.OnClickListener, CartAdapter.CartInterface {

    private static final String TAG = "cart_detail";
    ApiInterface apiService;
    ApiClient apiClient;

    ProgressBar progressBar;
    RelativeLayout top_layout, layout_discount;
    RecyclerView recycle_view;
    TextView empty_txt, txt_total, txt_price, txt_price_title, txt_delivery, txt_discount;
    ImageView back_btn;
    CartAdapter cartAdapter;
    SharedPref sharedPref;
    EditText edit_promo;
    TextView text_apply;
    Button btn_continue;
    int total_qty = 0;
    private static final int ATTRIB_ADDRESS = 101;
    private static final int ATTRIB_LOGIN = 102;
    String promo_code = "";
    private int totalAmount;
    private int MAX_CART_VALUE = 5000;

    public static ArrayList<ProductDetail> productDetails = new ArrayList<>();
    private List<Product> products = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);

        sharedPref = new SharedPref(this);
        apiClient = new ApiClient();
        apiService = ApiClient.getClient().create(ApiInterface.class);

        init();
        loadData();
        setData();
    }

    private void setData() {
//        String st = sharedPref.getStr("CART");
//        productDetails = new Gson().fromJson(st, new TypeToken<ArrayList<ProductDetail>>() {
//        }.getType());
//        if (productDetails != null && productDetails.size() > 0) {
//            empty_txt.setVisibility(View.GONE);
//            top_layout.setVisibility(View.VISIBLE);
//            cartAdapter = new CartAdapter(this, productDetails);
//            recycle_view.setAdapter(cartAdapter);
//
//            setPriceData();
//        } else {
//            empty_txt.setVisibility(View.VISIBLE);
//            top_layout.setVisibility(View.GONE);
//        }
    }


    private void init() {
        btn_continue = findViewById(R.id.btn_continue);
        btn_continue.setOnClickListener(this);
        text_apply = findViewById(R.id.text_apply);
        text_apply.setOnClickListener(this);
        edit_promo = findViewById(R.id.edit_promo);
        back_btn = findViewById(R.id.back_btn);
        back_btn.setOnClickListener(this);
        progressBar = findViewById(R.id.progressBar);
        top_layout = findViewById(R.id.top_layout);
        empty_txt = findViewById(R.id.empty_txt);
        txt_total = findViewById(R.id.txt_total);
        txt_price = findViewById(R.id.txt_price);
        txt_price_title = findViewById(R.id.txt_price_title);
        txt_delivery = findViewById(R.id.txt_delivery);
        txt_discount = findViewById(R.id.txt_discount);
        layout_discount = findViewById(R.id.layout_discount);

        recycle_view = findViewById(R.id.recycle_view);
        recycle_view.setLayoutManager(new GridLayoutManager(this, 1));

    }

    private void loadData() {

        progressBar.setVisibility(View.VISIBLE);
        top_layout.setVisibility(View.GONE);

        apiService = apiClient.getClient().create(ApiInterface.class);

        Call<CartDetail> call = apiService.getCartDetail(sharedPref.getStr("ID"), Constant.getDeviceId(this));
        apiClient.enqueue(call, new Callback<CartDetail>() {
            @Override
            public void onResponse(Call<CartDetail> call, Response<CartDetail> response) {
                Log.d(TAG, "dataCart: " + new Gson().toJson(response.body().getCart()));
                progressBar.setVisibility(View.GONE);
                top_layout.setVisibility(View.VISIBLE);

                CartDetail detailModel = response.body();

                if (products != null && detailModel.getCart() != null && detailModel.getCart().get(0).getProduct() != null
                        && detailModel.getCart().get(0).getProduct().size() > 0) {
                    products.addAll(detailModel.getCart().get(0).getProduct());

                    empty_txt.setVisibility(View.GONE);
                    top_layout.setVisibility(View.VISIBLE);
                    cartAdapter = new CartAdapter(CartActivity.this, products);
                    recycle_view.setAdapter(cartAdapter);

                    updateCartCount(detailModel.getCart().get(0).getProduct().size());

                    total_qty = 0;
                    for (Product product : detailModel.getCart().get(0).getProduct()) {
                        total_qty = total_qty + Integer.parseInt(product.getQty());
                    }
                    txt_price_title.setText("Price (" + total_qty + " item)");
                } else {
                    empty_txt.setVisibility(View.VISIBLE);
                    top_layout.setVisibility(View.GONE);
                    updateCartCount(0);
                }
                txt_price.setText("" + detailModel.getCart().get(0).getPrice());
                txt_delivery.setText("" + detailModel.getCart().get(0).getShipppingCharge());
                totalAmount = detailModel.getCart().get(0).getTotalAmount();
                txt_total.setText("" + totalAmount);
                txt_discount.setText("" + detailModel.getCart().get(0).getDiscount());
                layout_discount.setVisibility(detailModel.getCart().get(0).getDiscount() != 0 ? View.VISIBLE : View.GONE);
            }

            @Override
            public void onFailure(Call<CartDetail> call, Throwable t) {
                // Log error here since request failed
                Log.e(TAG, t.toString());
            }
        });
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(View view) {
        if (view == back_btn) {
            finish();

        } else if (view == text_apply) {
            if (edit_promo.getText().toString().isEmpty()) {
                Toast.makeText(this, "Promo code cant be empty!", Toast.LENGTH_SHORT).show();
            } else
                callQtyUpdate("", edit_promo.getText().toString(), "");

        } else if (view == btn_continue) {
            if (sharedPref.getBool("IS_LOGIN")) {
                Log.e(TAG, "onClick: " + totalAmount);
                if (totalAmount > MAX_CART_VALUE) {
                    Toast.makeText(this, "cart value is high", Toast.LENGTH_SHORT).show();
                    return;
                } else {
                    Intent intent = new Intent(this, AddressActivity.class);
                    intent.putExtra("ORDER", orderData());
                    startActivityForResult(intent, ATTRIB_ADDRESS);
                }

            } else {
                Intent intent = new Intent(this, LoginActivity.class);
                startActivityForResult(intent, ATTRIB_LOGIN);
            }
        }
    }

    private Order orderData() {
        Order order = new Order();
        order.setTotal_qty(total_qty + "");
        order.setPrice(txt_price.getText().toString());
        order.setDiscount(txt_discount.getText().toString());
        order.setTotal_price(txt_total.getText().toString());
        order.setShipppingCharge(txt_delivery.getText().toString());
        order.setPromo_code(promo_code);
        return order;
    }

    @Override
    public void performPlusQty(int adapterPosition, String qty) {
//        productDetails.get(adapterPosition).setQty(productDetails.get(adapterPosition).getQty() + 1);
//        cartAdapter.notifyDataSetChanged();
//        setPriceData();

        callQtyUpdate(products.get(adapterPosition).getCart_id(), qty, "update");
    }

    @Override
    public void performMinusQty(int adapterPosition, String qty) {
//        int val = productDetails.get(adapterPosition).getQty() - 1;
//        productDetails.get(adapterPosition).setQty(Math.max(val, 1));
//        cartAdapter.notifyDataSetChanged();
//        setPriceData();

        callQtyUpdate(products.get(adapterPosition).getCart_id(), qty, "update");
    }

    @Override
    public void performDelete(int adapterPosition, String id) {
//        productDetails.remove(adapterPosition);
//        cartAdapter.notifyDataSetChanged();
//        setPriceData();
//        if (productDetails.size() <= 0) {
//            empty_txt.setVisibility(View.VISIBLE);
//        }
        AlertDialog alertDialog = new AlertDialog.Builder(CartActivity.this).create();
        alertDialog.setTitle(products.get(adapterPosition).getProdcutName());
        alertDialog.setMessage("Remove product from cart?");
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "CANCEL",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "REMOVE",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        callQtyUpdate(products.get(adapterPosition).getCart_id(), "", "delete");
                    }
                });
        alertDialog.show();

    }

    private void callQtyUpdate(String cartId, String qty, String action) {
        progressBar.setVisibility(View.VISIBLE);

        apiService = apiClient.getClient().create(ApiInterface.class);

        Call<CartDetail> call = action.isEmpty() ? apiService.getCartDetailPromo(sharedPref.getStr("ID"), Constant.getDeviceId(this), qty)
                : apiService.getCartDetailAction(sharedPref.getStr("ID"), Constant.getDeviceId(this), cartId, qty, action);

        apiClient.enqueue(call, new Callback<CartDetail>() {
            @Override
            public void onResponse(Call<CartDetail> call, Response<CartDetail> response) {
                Log.d(TAG, "dataCart: " + new Gson().toJson(response.body().getCart()));
                progressBar.setVisibility(View.GONE);

                CartDetail detailModel = response.body();
                products.clear();

                if (detailModel.getCart() != null && detailModel.getCart().get(0).getProduct() != null
                        && detailModel.getCart().get(0).getProduct().size() > 0) {

                    products.addAll(detailModel.getCart().get(0).getProduct());

                    empty_txt.setVisibility(View.GONE);
                    top_layout.setVisibility(View.VISIBLE);
                    cartAdapter = new CartAdapter(CartActivity.this, products);
                    recycle_view.setAdapter(cartAdapter);
                    updateCartCount(detailModel.getCart().get(0).getProduct().size());

                    total_qty = 0;
                    for (Product product : detailModel.getCart().get(0).getProduct()) {
                        total_qty = total_qty + Integer.parseInt(product.getQty());
                    }
                    txt_price_title.setText("Price (" + total_qty + " item)");
                } else {
                    empty_txt.setVisibility(View.VISIBLE);
                    top_layout.setVisibility(View.GONE);
                    updateCartCount(0);
                }
                txt_price.setText("" + detailModel.getCart().get(0).getPrice());
                txt_delivery.setText("" + detailModel.getCart().get(0).getShipppingCharge());
                totalAmount = detailModel.getCart().get(0).getTotalAmount();
                txt_total.setText("" + totalAmount);
                txt_discount.setText("" + detailModel.getCart().get(0).getDiscount());
                layout_discount.setVisibility(detailModel.getCart().get(0).getDiscount() != 0 ? View.VISIBLE : View.GONE);
                if (action.isEmpty()) {
                    promo_code = qty;
                    Toast.makeText(CartActivity.this, detailModel.getCart().get(0).getPromocode(), Toast.LENGTH_SHORT).show();
                    hideKeyboard();
                }
            }

            @Override
            public void onFailure(Call<CartDetail> call, Throwable t) {
                // Log error here since request failed
                Log.e(TAG, t.toString());
            }
        });
    }

    private void hideKeyboard() {
        edit_promo.clearFocus();
        InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(edit_promo.getWindowToken(), 0);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ATTRIB_ADDRESS && resultCode == RESULT_OK) {
            finish();
        }
    }
}