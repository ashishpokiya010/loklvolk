package com.loklvokl.lokl.activtiy;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageDecoder;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.loklvokl.lokl.R;
import com.loklvokl.lokl.adapter.ImageAdapter;
import com.loklvokl.lokl.model.DataModel;
import com.loklvokl.lokl.util.ApiClient;
import com.loklvokl.lokl.util.ApiInterface;
import com.loklvokl.lokl.util.Constant;
import com.loklvokl.lokl.util.SharedPref;
import com.squareup.picasso.Picasso;
import com.yalantis.ucrop.UCrop;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class UserReturnOrderActivity extends AppCompatActivity implements View.OnClickListener, ImageAdapter.AdInterface{

    public static final int RequestPermissionCodeImage = 700;
    public static final int RequestPermissionCodeCamera = 800;

    ApiClient apiClient;
    SharedPref sharedPref;
    ApiInterface apiService;

    String str_user_id, str_order_id, str_name, str_image, str_size, str_qty, str_price, str_return_reason;
    List<String> image_paths = new ArrayList<>();
    public static List<Bitmap> image_bitmap_list;

    ProgressBar progressBar;
    Button btn_add_image, btn_submit_return_req;
    ImageView img_product, back_btn;
    RecyclerView recycle_view;
    TextView title_text, txt_product_name, txt_product_size, txt_product_qty, txt_product_price;
    RadioGroup rdgroup;
    RadioButton rb_selected;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_return_order);

        sharedPref = new SharedPref(this);
        apiClient = new ApiClient();
        apiService = ApiClient.getClient().create(ApiInterface.class);

        Intent intent = getIntent();
        if(intent != null) {
            str_user_id = sharedPref.getStr("ID");
            str_order_id = intent.getStringExtra("order_id");
            str_name = intent.getStringExtra("product_name");
            str_image = intent.getStringExtra("product_image");
            str_size = intent.getStringExtra("product_size");
            str_qty = intent.getStringExtra("product_qty");
            str_price = intent.getStringExtra("product_price");
        }

        init();
    }

    private void init() {
        findViewById(R.id.img_detail_noti).setVisibility(View.GONE);
        findViewById(R.id.img_detail_wishlist).setVisibility(View.GONE);
        findViewById(R.id.img_detail_cart).setVisibility(View.GONE);

        image_bitmap_list = new ArrayList<>();

        title_text = findViewById(R.id.title_text);
        back_btn = findViewById(R.id.back_btn);
        img_product = findViewById(R.id.image);
        rdgroup = findViewById(R.id.rdg_return_reason);
        btn_add_image = findViewById(R.id.btn_add_image);
        recycle_view = findViewById(R.id.recycle_view);
        txt_product_name = findViewById(R.id.name_txt);
        txt_product_size = findViewById(R.id.size_txt);
        txt_product_qty = findViewById(R.id.qty_txt);
        txt_product_price = findViewById(R.id.price_txt);
        btn_submit_return_req = findViewById(R.id.btn_submit_return);
        progressBar = findViewById(R.id.progressBar);

        btn_add_image.setOnClickListener(this);
        back_btn.setOnClickListener(this);
        btn_submit_return_req.setOnClickListener(this);

        title_text.setText("Return for #"+str_order_id);
        txt_product_name.setText(""+str_name);
        txt_product_size.setText("Size: "+str_size);
        txt_product_qty.setText("Qty: "+str_qty);
        txt_product_price.setText("Rs " + str_price);

        Picasso.get().load("https://" + str_image)
                .resize(0, 250)
                .into(img_product, new com.squareup.picasso.Callback() {
                    @Override
                    public void onSuccess() {
                    }

                    @Override
                    public void onError(Exception e) {
                    }
                });
    }

    @Override
    public void onClick(View view) {

        if(view == btn_add_image){
            if (checkAndRequestPermissions()) {
                Intent intent = new Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(intent, 200);
            } else {
                RequestMultiplePermissionImage();
            }
        }

        if(view == btn_submit_return_req){
            int selectedId = rdgroup.getCheckedRadioButtonId();
            // find the radiobutton by returned id
            rb_selected = (RadioButton) findViewById(selectedId);

            Log.d("checked selected : "," - - "+rb_selected.getText());
            str_return_reason = rb_selected.getText().toString();

            /*String image1 = "", image2 = "", image3 = "", image4 = "", image5 = "";

            if (image_bitmap_list.size() == 1) {
                image1 = convertToString(image_bitmap_list.get(0));
            }
            if (image_bitmap_list.size() == 2) {

                image1 = convertToString(image_bitmap_list.get(0));
                image2 = convertToString(image_bitmap_list.get(1));

                System.out.println("FILENAMAMAM" + image1);
                System.out.println("FILENAMAMAM2" + image2);
                System.out.println("BITMAPNAME" + image_bitmap_list.get(0));
                System.out.println("BITMAPNAME" + image_bitmap_list.get(1));
            }
            if (image_bitmap_list.size() == 3) {
                image1 = convertToString(image_bitmap_list.get(0));
                image2 = convertToString(image_bitmap_list.get(1));
                image3 = convertToString(image_bitmap_list.get(2));

            }
            if (image_bitmap_list.size() == 4) {
                image1 = convertToString(image_bitmap_list.get(0));
                image2 = convertToString(image_bitmap_list.get(1));
                image3 = convertToString(image_bitmap_list.get(2));
                image4 = convertToString(image_bitmap_list.get(3));
            }
            if (image_bitmap_list.size() == 5) {
                image1 = convertToString(image_bitmap_list.get(0));
                image2 = convertToString(image_bitmap_list.get(1));
                image3 = convertToString(image_bitmap_list.get(2));
                image4 = convertToString(image_bitmap_list.get(3));
                image5 = convertToString(image_bitmap_list.get(4));
            }*/
            returnOrder();
            /*new AlertDialog.Builder(UserReturnOrderActivity.this, R.style.CustomAlertDialog)
                    .setMessage("Do you want to return this order?")
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            returnOrder();
                            dialogInterface.dismiss();
                        }
                    })
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    });*/
        }

        if(view == back_btn){
            this.finish();
        }
    }

    private String convertToString(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        byte[] imgByte = byteArrayOutputStream.toByteArray();
        return Base64.encodeToString(imgByte, Base64.DEFAULT);
    }


    private void returnOrder() {

        String image1 = "", image2 = "", image3 = "", image4 = "", image5 = "";

        if (image_bitmap_list.size() == 1) {
            image1 = convertToString(image_bitmap_list.get(0));
        }
        if (image_bitmap_list.size() == 2) {

            image1 = convertToString(image_bitmap_list.get(0));
            image2 = convertToString(image_bitmap_list.get(1));

            System.out.println("FILENAMAMAM" + image1);
            System.out.println("FILENAMAMAM2" + image2);
            System.out.println("BITMAPNAME" + image_bitmap_list.get(0));
            System.out.println("BITMAPNAME" + image_bitmap_list.get(1));
        }
        if (image_bitmap_list.size() == 3) {
            image1 = convertToString(image_bitmap_list.get(0));
            image2 = convertToString(image_bitmap_list.get(1));
            image3 = convertToString(image_bitmap_list.get(2));

        }
        if (image_bitmap_list.size() == 4) {
            image1 = convertToString(image_bitmap_list.get(0));
            image2 = convertToString(image_bitmap_list.get(1));
            image3 = convertToString(image_bitmap_list.get(2));
            image4 = convertToString(image_bitmap_list.get(3));
        }
        if (image_bitmap_list.size() == 5) {
            image1 = convertToString(image_bitmap_list.get(0));
            image2 = convertToString(image_bitmap_list.get(1));
            image3 = convertToString(image_bitmap_list.get(2));
            image4 = convertToString(image_bitmap_list.get(3));
            image5 = convertToString(image_bitmap_list.get(4));
        }
        progressBar.setVisibility(View.VISIBLE);
        apiService = apiClient.getClient().create(ApiInterface.class);
        System.out.println("USSSID" + str_user_id);
        Call<DataModel> call = apiService.getUserOrderReturn(str_order_id, str_user_id, str_return_reason,
                ""+image1, ""+image2, ""+image3, ""+image4, ""+image5);

        apiClient.enqueue(call, new Callback<DataModel>() {
            @Override
            public void onResponse(Call<DataModel> call, Response<DataModel> response) {
                progressBar.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    assert response.body() != null;
                    String responseData = response.body().getData().get(0).getError();
                    if (responseData != null) {
                        //no_data_txt.setVisibility(View.GONE);
                        Constant.toast(getApplicationContext(), response.body().getData().get(0).getMessage());
                        UserReturnOrderActivity.this.finish();
                    } else {
                        Constant.toast(getApplicationContext(), response.body().getData().get(0).getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<DataModel> call, Throwable t) {
                // Log error here since request failed
            }
        });
    }
    public boolean checkAndRequestPermissions() {
        int OnePermissionResult = ContextCompat.checkSelfPermission(getApplicationContext(), READ_EXTERNAL_STORAGE);
        int TwoPermissionResult = ContextCompat.checkSelfPermission(getApplicationContext(), WRITE_EXTERNAL_STORAGE);
        int ThreePermissionResult = ContextCompat.checkSelfPermission(getApplicationContext(), CAMERA);
        return OnePermissionResult == PackageManager.PERMISSION_GRANTED &&
                TwoPermissionResult == PackageManager.PERMISSION_GRANTED &&
                ThreePermissionResult == PackageManager.PERMISSION_GRANTED;
    }


    private void RequestMultiplePermissionImage() {
        ActivityCompat.requestPermissions(UserReturnOrderActivity.this, new String[]
                {
                        READ_EXTERNAL_STORAGE,
                        WRITE_EXTERNAL_STORAGE,
                        CAMERA,
                }, RequestPermissionCodeImage);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case RequestPermissionCodeImage:
                if (grantResults.length > 0) {
                    boolean ReadinternalPermission = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    boolean WriteExternalStorageStatePermission = grantResults[1] == PackageManager.PERMISSION_GRANTED;
                    boolean CameraStorageStatePermission = grantResults[2] == PackageManager.PERMISSION_GRANTED;
                    if (WriteExternalStorageStatePermission && ReadinternalPermission && CameraStorageStatePermission) {
                        Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(intent, 201);
                    }
                }
                break;
            case RequestPermissionCodeCamera:
                if (grantResults.length > 0) {
                    boolean ReadinternalPermission = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    boolean WriteExternalStorageStatePermission = grantResults[1] == PackageManager.PERMISSION_GRANTED;
                    boolean CameraStorageStatePermission = grantResults[2] == PackageManager.PERMISSION_GRANTED;
                    if (WriteExternalStorageStatePermission && ReadinternalPermission && CameraStorageStatePermission) {
                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        File f = new File(android.os.Environment.getExternalStorageDirectory(), "temp.jpg");
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
                        startActivityForResult(intent, 101);
                    }
                }
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        System.out.println("Image_Size>>>>" + requestCode);

        if (resultCode == RESULT_OK) {
            if (requestCode == 100) {
                File f = new File(Environment.getExternalStorageDirectory().toString());
                for (File temp : f.listFiles()) {
                    if (temp.getName().equals("temp.jpg")) {
                        f = temp;
                        break;
                    }
                }
                Uri uri = Uri.fromFile(f);
                System.out.println("UUUU" + uri);
                cropImage(uri);
            } else if (requestCode == 101) {
                File f = new File(Environment.getExternalStorageDirectory().toString());
                for (File temp : f.listFiles()) {
                    if (temp.getName().equals("temp.jpg")) {
                        f = temp;
                        break;
                    }
                }
                Uri uri = Uri.fromFile(f);
                System.out.println("UUUU" + uri);
                cropImage(uri);
            } else if (requestCode == 200) {
                Uri selectedImage = data.getData();
                cropImage(selectedImage);
            } else if (requestCode == 201) {
                Uri selectedImage = data.getData();
                cropImage(selectedImage);
            } else if (requestCode == 112) {
                setResult(RESULT_OK);
                finish();
            } else if (resultCode == RESULT_OK && requestCode == UCrop.REQUEST_CROP) {
                final Uri selectedImage = UCrop.getOutput(data);
                System.out.println("Image_Size>>>>" + selectedImage);
                Bitmap bitmap = loadFromUri(selectedImage);
                getDropboxIMGSize(selectedImage);


                long fileSizeInBytes = byteSizeOf(bitmap);
                long fileSizeInKB = fileSizeInBytes / 1024;
                long fileSizeInMB = fileSizeInKB / 1024;

                File file = new File(selectedImage.getPath());
                if (fileSizeInMB > 2) {
                    Toast.makeText(this, "Image size is more than 2MB", Toast.LENGTH_SHORT).show();
                } else {
                    if (image_bitmap_list.size() <= 4) {
                        image_paths.add(file.getAbsolutePath());
                        image_bitmap_list.add(bitmap);
                        setImages(image_bitmap_list);
                    } else {
                        Toast.makeText(this, "You can only add 5 image! ", Toast.LENGTH_SHORT).show();
                    }
                }

            } else if (resultCode == UCrop.RESULT_ERROR) {
                final Throwable cropError = UCrop.getError(data);
                Log.d("tag", "" + cropError.toString());
                Toast.makeText(this, "Error", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void setImages(List<Bitmap> bitmap) {
        ImageAdapter imageAdapter = new ImageAdapter(this, bitmap);
        recycle_view.setLayoutManager(new LinearLayoutManager(this,
                LinearLayoutManager.HORIZONTAL, false));
        recycle_view.setAdapter(imageAdapter);
    }

    public static int byteSizeOf(Bitmap bitmap) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            return bitmap.getAllocationByteCount();
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR1) {
            return bitmap.getByteCount();
        } else {
            return bitmap.getRowBytes() * bitmap.getHeight();
        }
    }

    private void getDropboxIMGSize(Uri uri) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(new File(uri.getPath()).getAbsolutePath(), options);
        int imageHeight = options.outHeight;
        int imageWidth = options.outWidth;

        System.out.println("HEIGHT_WIDTH" + imageHeight);
        System.out.println("HEIGHT_WIDTH" + imageWidth);

    }

    public Bitmap loadFromUri(Uri photoUri) {
        Bitmap bitmap = null;
        Bitmap imageBitmap = null;
        try {
            // check version of Android on device
            if (Build.VERSION.SDK_INT > 27) {
                // on newer versions of Android, use the new decodeBitmap method
                ImageDecoder.Source source = ImageDecoder.createSource(this.getContentResolver(), photoUri);
                bitmap = ImageDecoder.decodeBitmap(source);
            } else {
                // support older versions of Android by using getBitmap
                bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), photoUri);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        imageBitmap = bitmap.copy(Bitmap.Config.ARGB_8888, true);
        return imageBitmap;
    }

    private static final String SAMPLE_CROPPED_IMAGE_NAME = "SampleCropImage";
    private int IMAGE_COMPRESSION = 100;
    private boolean lockAspectRatio = false, setBitmapMaxWidthHeight = false;
    private int ASPECT_RATIO_X = 1, ASPECT_RATIO_Y = 1, bitmapMaxWidth = 500, bitmapMaxHeight = 500;

    private void cropImage(Uri sourceUri) {

        File f = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/Pictures/" + SAMPLE_CROPPED_IMAGE_NAME + ".jpg");

        Uri destinationUri = Uri.fromFile(f);
        UCrop.Options options = new UCrop.Options();
        options.setCompressionQuality(IMAGE_COMPRESSION);
        options.setToolbarColor(ContextCompat.getColor(this, R.color.colorPrimary));
        options.setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimary));
        options.setActiveWidgetColor(ContextCompat.getColor(this, R.color.colorPrimary));
        options.setToolbarTitle("Edit");

        if (!lockAspectRatio)
            options.withAspectRatio(ASPECT_RATIO_X, ASPECT_RATIO_Y);

        if (!setBitmapMaxWidthHeight)
            options.withMaxResultSize(bitmapMaxWidth, bitmapMaxHeight);

        UCrop.of(sourceUri, destinationUri)
                .withOptions(options)
                .start(this);
    }

    @Override
    public void itemClick(int position) {
        System.out.println("Remove item");
        image_bitmap_list.remove(position);
        image_paths.remove(position);
        setImages(image_bitmap_list);
    }

    @Override
    public void itemClickAll(int position) {
        System.out.println("itemAll");
        if (image_bitmap_list.size() > 0) {
            //top_img.setImageBitmap(image_bitmap_list.get(position));
        }
    }
}