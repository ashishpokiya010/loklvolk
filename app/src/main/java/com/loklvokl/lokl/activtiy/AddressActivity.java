package com.loklvokl.lokl.activtiy;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.loklvokl.lokl.R;
import com.loklvokl.lokl.adapter.AddressAdapter;
import com.loklvokl.lokl.model.Address;
import com.loklvokl.lokl.model.AddressResponse;
import com.loklvokl.lokl.model.Order;
import com.loklvokl.lokl.util.ApiClient;
import com.loklvokl.lokl.util.ApiInterface;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddressActivity extends BaseActivity implements AddressAdapter.AddressInterface, View.OnClickListener {

    private static final String TAG = "cartclass";
    private static final int ATTRIB_ADDRESS = 101;
    private static final int ATTRIB_PAYMENT = 102;
    RecyclerView recycle_view;
    ProgressBar progressBar;
    RelativeLayout top_layout;
    ApiInterface apiService;
    ApiClient apiClient;
    AddressAdapter addressAdapter;
    Button btn_continue;
    LinearLayout add_layout;
    List<Address> addresses = new ArrayList<>();
    ImageView back_btn;
    TextView title_text;
    String address_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_address);

        apiClient = new ApiClient();
        apiService = ApiClient.getClient().create(ApiInterface.class);

        init();
        getIntentData();
        loadData();
    }

    private void getIntentData() {
        if (getIntent().hasExtra("FROM_PROFILE")) {
            if (getIntent().getBooleanExtra("FROM_PROFILE", false))
                btn_continue.setVisibility(View.GONE);

        }
    }

    private void loadData() {

        progressBar.setVisibility(View.VISIBLE);
        top_layout.setVisibility(View.GONE);
        btn_continue.setEnabled(false);
        btn_continue.setAlpha(0.5f);

        apiService = apiClient.getClient().create(ApiInterface.class);

        String userId = sharedPref.getStr("ID");
        Call<AddressResponse> call = apiService.getUserAdress(userId);
        apiClient.enqueue(call, new Callback<AddressResponse>() {
            @Override
            public void onResponse(Call<AddressResponse> call, Response<AddressResponse> response) {
                Log.d(TAG, ": " + new Gson().toJson(response.body()));
                progressBar.setVisibility(View.GONE);
                top_layout.setVisibility(View.VISIBLE);

                AddressResponse body = response.body();

                if (body.getAddress() != null && body.getAddress().size() > 0) {
                    if (body.getAddress().get(0).getError()) {
                        Toast.makeText(AddressActivity.this, body.getAddress().get(0).getMassage(), Toast.LENGTH_SHORT).show();
                        recycle_view.setVisibility(View.GONE);
                        btn_continue.setEnabled(false);
                        btn_continue.setAlpha(0.5f);
                    } else {
                        recycle_view.setVisibility(View.VISIBLE);
                        addresses.clear();

                        addresses.addAll(body.getAddress());
                        addressAdapter = new AddressAdapter(AddressActivity.this, addresses);
                        recycle_view.setAdapter(addressAdapter);
                        addresses.get(0).setSelected(true);
                        address_id = addresses.get(0).getAddressId();
                        btn_continue.setEnabled(true);
                        btn_continue.setAlpha(1.0f);
                    }
                } else {
                    btn_continue.setAlpha(0.5f);
                    btn_continue.setEnabled(false);
                    recycle_view.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<AddressResponse> call, Throwable t) {
                // Log error here since request failed
                Log.e(TAG, t.toString());
            }
        });
    }


    private void init() {
        back_btn = findViewById(R.id.back_btn);
        back_btn.setOnClickListener(this);
        title_text = findViewById(R.id.title_text);
        title_text.setText("Delivery Address");

        add_layout = findViewById(R.id.add_layout);
        add_layout.setOnClickListener(this);
        btn_continue = findViewById(R.id.btn_continue);
        btn_continue.setOnClickListener(this);
        recycle_view = findViewById(R.id.recycle_view);
        recycle_view.setLayoutManager(new GridLayoutManager(this, 1));

        progressBar = findViewById(R.id.progressBar);
        top_layout = findViewById(R.id.top_layout);
    }

    @Override
    public void onItemClick(int adapterPosition) {
        address_id = addresses.get(adapterPosition).getAddressId();
        for (int i = 0; i < addresses.size(); i++) {
            Address address = addresses.get(i);
            address.setSelected(false);
            if (address.getAddressId().equals(addresses.get(adapterPosition).getAddressId())) {
                address.setSelected(true);
            }
            addresses.set(i, address);
        }
        addressAdapter.notifyDataSetChanged();
    }

    @Override
    public void onEditClick(int adapterPosition) {
        Intent intent = new Intent(this, AddAddressActivity.class);
        intent.putExtra("ADDRESS_ID", addresses.get(adapterPosition).getAddressId());
        intent.putExtra("ADDRESS", new Gson().toJson(addresses.get(adapterPosition)));
        startActivityForResult(intent, ATTRIB_ADDRESS);
    }

    @Override
    public void onClick(View view) {
        if (view == btn_continue) {
            Intent intent = new Intent(this, PaymentActivity.class);
            intent.putExtra("ORDER", orderData());
            startActivityForResult(intent, ATTRIB_PAYMENT);

        } else if (view == add_layout) {
            Intent intent = new Intent(this, AddAddressActivity.class);
            startActivityForResult(intent, ATTRIB_ADDRESS);

        } else if (view == back_btn) {
            finish();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ATTRIB_ADDRESS && resultCode == RESULT_OK) {
            loadData();

        } else if (requestCode == ATTRIB_PAYMENT && resultCode == RESULT_OK) {
            setResult(RESULT_OK);
            finish();
        }
    }

    private Order orderData() {
        Order order = (Order) getIntent().getSerializableExtra("ORDER");
        order.setAddress_id(address_id);
        return order;
    }
}