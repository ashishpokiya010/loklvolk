package com.loklvokl.lokl.activtiy;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.loklvokl.lokl.R;
import com.loklvokl.lokl.adapter.AdCatAdapter;
import com.loklvokl.lokl.model.CategoryModel;
import com.loklvokl.lokl.model.MainCategory;
import com.loklvokl.lokl.model.SubCategory;
import com.loklvokl.lokl.util.ApiClient;
import com.loklvokl.lokl.util.ApiInterface;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddAdActivity extends AppCompatActivity implements View.OnClickListener, AdCatAdapter.AdInterface {

    private static final String TAG = "AddAd";
    ProgressBar progressBar;
    ApiInterface apiService;
    AdCatAdapter adCatAdapter;
    RecyclerView recycle_view;
    ImageView back_btn;
    TextView title_text;
    CategoryModel categoryModel;
    String image1, image2, image3, image4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_ad);

        init();
        getINtentData();
        loadData();
    }

    private void getINtentData() {
        if (getIntent().hasExtra("TITLE")) {
            title_text.setText(getIntent().getStringExtra("TITLE"));
        }
        image1 = getIntent().getStringExtra("IMAGE1");
        image2 = getIntent().getStringExtra("IMAGE2");
        image3 = getIntent().getStringExtra("IMAGE3");
        image4 = getIntent().getStringExtra("IMAGE4");

        System.out.println("IMA1" + image1.toString());
        System.out.println("IMA1" + image2.toString());
        System.out.println("IMA1" + image3.toString());
        System.out.println("IMA1" + image4.toString());
    }

    private void init() {
        apiService = ApiClient.getClient().create(ApiInterface.class);
        progressBar = findViewById(R.id.progressBar);
        back_btn = findViewById(R.id.back_btn);
        back_btn.setOnClickListener(this);
        title_text = findViewById(R.id.title_text);
        title_text.setText("Select Category");


        recycle_view = findViewById(R.id.recycle_view);
        recycle_view.setLayoutManager(new GridLayoutManager(this, 3));
    }

    private void loadData() {

        progressBar.setVisibility(View.VISIBLE);
        Call<CategoryModel> call = apiService.getAllCategoryData();
        call.enqueue(new Callback<CategoryModel>() {
            @Override
            public void onResponse(Call<CategoryModel> call, Response<CategoryModel> response) {
                Log.d(TAG, "data: " + response.body());
                progressBar.setVisibility(View.GONE);

                categoryModel = response.body();

                List<SubCategory> subCategory = new ArrayList<>();
                for (MainCategory mainCategory : categoryModel.getMainCategory()) {
                    subCategory.add(new SubCategory(mainCategory.getMainCatId(), mainCategory.getMainCatName(), mainCategory.getImage()));
                }

                adCatAdapter = new AdCatAdapter(AddAdActivity.this, subCategory);
                recycle_view.setAdapter(adCatAdapter);
            }

            @Override
            public void onFailure(Call<CategoryModel> call, Throwable t) {
                // Log error here since request failed
                progressBar.setVisibility(View.GONE);
                Log.e(TAG, t.toString());
            }
        });
    }

    @Override
    public void onClick(View view) {
        if (view == back_btn) {
            finish();
        }
    }

    @Override
    public void itemClick(int position) {
        Intent intent = new Intent(this, AddAdActivity2.class);
        intent.putExtra("TITLE", categoryModel.getMainCategory().get(position).getMainCatName());
        intent.putExtra("DATA", categoryModel.getMainCategory().get(position));
        intent.putExtra("IMAGE1", image1);
        intent.putExtra("IMAGE2", image2);
        intent.putExtra("IMAGE3", image3);
        intent.putExtra("IMAGE4", image4);
        startActivityForResult(intent, 111);
        overridePendingTransition(R.anim.enter, R.anim.exit);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 111 && resultCode == RESULT_OK) {
            setResult(RESULT_OK);
            finish();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
    }
}