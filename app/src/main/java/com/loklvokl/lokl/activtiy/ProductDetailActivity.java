package com.loklvokl.lokl.activtiy;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.loklvokl.lokl.R;
import com.loklvokl.lokl.adapter.ColorAdapter;
import com.loklvokl.lokl.adapter.ImagePagerAdapter;
import com.loklvokl.lokl.adapter.ProductSimilarAdapter;
import com.loklvokl.lokl.adapter.SizeAdapter;
import com.loklvokl.lokl.adapter.WishListAdapter;
import com.loklvokl.lokl.cart.ModelCart;
import com.loklvokl.lokl.model.CartResponse;
import com.loklvokl.lokl.model.Homeslider;
import com.loklvokl.lokl.model.ItemInterface;
import com.loklvokl.lokl.model.PincodeModel;
import com.loklvokl.lokl.model.Product;
import com.loklvokl.lokl.model.ProductColor;
import com.loklvokl.lokl.model.ProductDetail;
import com.loklvokl.lokl.model.ProductDetailModel;
import com.loklvokl.lokl.model.ProductImage;
import com.loklvokl.lokl.model.ProductMoreDetails;
import com.loklvokl.lokl.model.ProductSize;
import com.loklvokl.lokl.model.product.WishListModel;
import com.loklvokl.lokl.model.product.Wishlist;
import com.loklvokl.lokl.util.ApiClient;
import com.loklvokl.lokl.util.ApiInterface;
import com.loklvokl.lokl.util.Constant;
import com.loklvokl.lokl.util.SharedPref;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductDetailActivity extends BaseActivity implements View.OnClickListener, ItemInterface {
    private static final String TAG = "Product_detail";
    String productId = "0";
    ProgressBar progressBar;
    ApiInterface apiService;
    TextView text_name;
    ImageView back_btn, share_img, iv_more;
    TextView textViewPrice, textViewPrice0, textOff, textViewDesc, rating, textSoldby, tv_more_details;

    LinearLayout top_layout, coloLayout, sizeLayout, layout_similar;
    ViewPager viewPager;
    TabLayout indicator;
    ApiClient apiClient;
    EditText etPincode;
    TextView tvCheckPincode;
    RecyclerView recycleColor, recycleSize, recycle_similar;
    ProductDetail productDetail = new ProductDetail();
    Button btn_addcart, btn_buynow;
    SharedPref sharedPref;
    String color_id = "", size_id = "";
    ImageView img_add_wishlist;

    ImageView imgDetailCart;
    List<Wishlist> wishlistarray = new ArrayList<>();

    public CopyOnWriteArrayList<ModelCart> arrry_list_cart;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_detail);

        sharedPref = new SharedPref(this);
        apiClient = new ApiClient();
        apiService = ApiClient.getClient().create(ApiInterface.class);

        init();
        getIntentData();
        if(productId != null) {
            loadDetailData();
        }

        listSelct = new HashMap<>();
        listSelctSize = new HashMap<>();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            // Create channel to show notifications.
            String channelId  = getString(R.string.default_notification_channel_id);
            String channelName = getString(R.string.default_notification_channel_name);
            NotificationManager notificationManager =
                    getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(new NotificationChannel(channelId,
                    channelName, NotificationManager.IMPORTANCE_LOW));
        }

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
    }

    private void getIntentData() {

        Bundle extras = getIntent().getExtras();
        if(extras!=null) {
            productId = extras.getString("ID");
        }
        Log.d("productId : ", ""+productId+" - - "+extras.toString());

//        productId = "37443";
    }

    private void init() {
        btn_addcart = findViewById(R.id.btn_addcart);
        btn_addcart.setOnClickListener(this);
        btn_buynow = findViewById(R.id.btn_buynow);
        btn_buynow.setOnClickListener(this);
        imgDetailCart = findViewById(R.id.img_detail_cart);
        imgDetailCart.setOnClickListener(this);

        progressBar = findViewById(R.id.progressBar);
        text_name = findViewById(R.id.text_name);
        top_layout = findViewById(R.id.top_layout);
        coloLayout = findViewById(R.id.coloLayout);
        sizeLayout = findViewById(R.id.sizeLayout);
        layout_similar = findViewById(R.id.layout_similar);
        viewPager = findViewById(R.id.viewPager);
        indicator = findViewById(R.id.indicator);
        tv_more_details = findViewById(R.id.tv_more_details);
        iv_more = findViewById(R.id.iv_more);
        iv_more.setOnClickListener(this);

        share_img = findViewById(R.id.share_img);
        share_img.setOnClickListener(this);
        back_btn = findViewById(R.id.back_btn);
        back_btn.setOnClickListener(this);
        textViewDesc = findViewById(R.id.textViewDesc);
        textSoldby = findViewById(R.id.textSoldby);
        textViewPrice = findViewById(R.id.textViewPrice);
        textViewPrice0 = findViewById(R.id.textViewPrice0);

        tvCheckPincode = findViewById(R.id.text_check_pincode);
        etPincode = findViewById(R.id.edit_pincode);
        textOff = findViewById(R.id.textOff);
        img_add_wishlist = findViewById(R.id.img_add_wishlist);
        img_add_wishlist.setOnClickListener(this);

        tvCheckPincode.setOnClickListener(this);

        recycleColor = findViewById(R.id.recycleColor);
        LinearLayoutManager horizontalLayoutManagaer = new LinearLayoutManager(ProductDetailActivity.this, LinearLayoutManager.HORIZONTAL, false);
        recycleColor.setLayoutManager(horizontalLayoutManagaer);

        recycleSize = findViewById(R.id.recycleSize);
        LinearLayoutManager horizontalLayoutManagaerSize = new LinearLayoutManager(ProductDetailActivity.this, LinearLayoutManager.HORIZONTAL, false);
        recycleSize.setLayoutManager(horizontalLayoutManagaerSize);

        recycle_similar = findViewById(R.id.recycle_similar);
        recycle_similar.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));

    }


    private void setImageSlider(List<ProductImage> productImage) {
        List<Homeslider> homesliders = new ArrayList<>();
        for (ProductImage productImage1 : productImage) {
            homesliders.add(new Homeslider("0", productImage1.getImage()));
        }
        viewPager.setAdapter(new ImagePagerAdapter(this, homesliders));

        indicator.setupWithViewPager(viewPager, true);

    }

    private void setDetailData(ProductDetail details) {
        text_name.setText(details.getProdcutName());

        textViewPrice.setText("Rs." + details.getProductSellingPrice());
        textViewPrice0.setText("Rs." + details.getProductMrp());
        textViewPrice0.setPaintFlags(textViewPrice0.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            textViewDesc.setText(Html.fromHtml(details.getDescription(), Html.FROM_HTML_MODE_COMPACT));
        } else {
            textViewDesc.setText(Html.fromHtml(details.getDescription().trim()));
        }

        textSoldby.setText(details.getSold_by());

        if (details.getRating() != null) {
            rating.setText(details.getRating());
        }
        textOff.setText("(" + details.getOff() + " OFF)");
    }

    private void setMoreDetails(ProductDetail details) {

        tv_more_details.setText(Html.fromHtml(details.getMoreDetails()));

    }

    @Override
    public void onClick(View view) {
        if (view == back_btn) {
            finish();

        }
        if (view == iv_more) {

            if (iv_more.getRotation() == 90) {
                iv_more.setRotation(270);
                tv_more_details.setVisibility(View.VISIBLE);
            } else if (iv_more.getRotation() == 270) {
                iv_more.setRotation(90);
                tv_more_details.setVisibility(View.GONE);
            }

        } else if(view == imgDetailCart){
            Intent intent = new Intent(this, CartActivity.class);
            startActivity(intent);
        }else if (view == tvCheckPincode) {
            if (etPincode.getText().toString().length() > 0) {
                checkPincode(etPincode.getText().toString());
            } else {
                Toast.makeText(getApplicationContext(), "Please Enter Pincode", Toast.LENGTH_SHORT).show();
            }

        } else if (view == btn_addcart) {
//            openCart();
            addToCart(false);

//            final CopyOnWriteArrayList<ModelCart> arraylistProducts = new CopyOnWriteArrayList<ModelCart>();
//            ProductDetailActivity.this.runOnUiThread(new Runnable() {
//                @Override
//                public void run() {
//                    System.out.println("CART_IDDDDQQQQQQQQQQQQQQQQQQQQ"
//                            + sharedPref.getCart_id());
////                    if (Integer.parseInt(1) != 0) {
//                        if (sharedPref.getCart_id() != "") {
////                            new AsyncCartDetails().execute();
//                        } else {
//                            new AsyncAddToCart(ProductDetailActivity.this,
//                                    arraylistProducts).execute();
////                        }
////                    } else {
////                        Toast.makeText(getApplicationContext(),
////                                "Product not available", Toast.LENGTH_SHORT)
////                                .show();
//                    }
//                }
//            });
        } else if (view == btn_buynow) {
            addToCart(true);

        } else if (view == share_img) {
            String pro_name = productDetail.getProdcutName();
            String pro_image = "https://" + productDetail.getProductImage().get(0).getImage();
            String share_text = sharedPref.getShareText().replace("\"","");
            progressBar.setVisibility(View.VISIBLE);

            Picasso.get().load(pro_image).into(new Target() {
                @Override
                public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                    progressBar.setVisibility(View.GONE);
                    Intent intent = new Intent(Intent.ACTION_SEND);
                    intent.setType("image/*");
                    intent.putExtra(Intent.EXTRA_TEXT, pro_name +" \n"+share_text);
                    intent.putExtra(Intent.EXTRA_STREAM, getLocalBitmapUri(bitmap));
                    startActivity(Intent.createChooser(intent, "Share Image"));
                }

                @Override
                public void onBitmapFailed(Exception e, Drawable errorDrawable) {
                    progressBar.setVisibility(View.GONE);
                    e.printStackTrace();
                    Toast.makeText(ProductDetailActivity.this, "Fail to share Product!", Toast.LENGTH_SHORT).show();

                }

                @Override
                public void onPrepareLoad(Drawable placeHolderDrawable) {
                }
            });
        } else if (view == img_add_wishlist) {
            addToWishList();
        }
    }

    public Uri getLocalBitmapUri(Bitmap bmp) {
        Uri bmpUri = null;
        try {
            File file = new File(getExternalFilesDir(Environment.DIRECTORY_PICTURES), "share_image_" + productDetail.getProductId() + ".png");
            FileOutputStream out = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.PNG, 90, out);
            out.close();
            bmpUri = Uri.fromFile(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bmpUri;
    }




/*    private void openCart() {

        Boolean flag = true;
        if (CartActivity.productDetails == null)
            CartActivity.productDetails = new ArrayList<>();

        for (int i = 0; i < CartActivity.productDetails.size(); i++) {
            ProductDetail detail = CartActivity.productDetails.get(i);
            if (detail.getProductId().equals(productDetail.getProductId())) {
                detail.setQty(detail.getQty() + 1);
                CartActivity.productDetails.set(i, detail);
                flag = false;
                break;
            }
        }
        if (flag) {
            CartActivity.productDetails.add(productDetail);
        }
        sharedPref.setStr("CART", new Gson().toJson(CartActivity.productDetails));
        Intent intent = new Intent(ProductDetailActivity.this, CartActivity.class);
        startActivity(intent);
    }*/


    private void setSimilatProduct(List<Product> data) {

        if (data.size() > 0) {
            ProductSimilarAdapter productSimilarAdapter = new ProductSimilarAdapter(this, data);
            recycle_similar.setAdapter(productSimilarAdapter);
            layout_similar.setVisibility(View.VISIBLE);
        } else {
            layout_similar.setVisibility(View.GONE);
        }
    }

    List<ProductColor> productColors = new ArrayList<>();
    ColorAdapter colorAdapter;
    SizeAdapter sizeAdapter;

    private void setColorAdapter(List<ProductColor> productDetail) {
        if (productDetail != null && productDetail.size() > 0) {
            productColors.addAll(productDetail);
            colorAdapter = new ColorAdapter(this, productColors);
            recycleColor.setAdapter(colorAdapter);
            coloLayout.setVisibility(View.GONE);
        } else {
            coloLayout.setVisibility(View.GONE);
        }
    }

    private void setSizeAdapter(List<ProductSize> productDetailSize) {
        if (productDetailSize != null && productDetailSize.size() > 0) {
            sizeAdapter = new SizeAdapter(this, productDetailSize);
            recycleSize.setAdapter(sizeAdapter);
            sizeLayout.setVisibility(View.VISIBLE);
        } else {
            sizeLayout.setVisibility(View.GONE);
        }
    }

    private void loadWishlist() {
        progressBar.setVisibility(View.VISIBLE);

        apiService = apiClient.getClient().create(ApiInterface.class);
        Call<WishListModel> call = apiService.getWishList(sharedPref.getStr("ID"), Constant.getDeviceId(this));

        apiClient.enqueue(call, new Callback<WishListModel>() {
            @Override
            public void onResponse(Call<WishListModel> call, Response<WishListModel> response) {
                progressBar.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    if (response != null) {
                        WishListModel detailModel = response.body();
                        wishlistarray.addAll(detailModel.getWishlist());

                        for(int i = 0; i<wishlistarray.size(); i++){
                            if(wishlistarray.get(i).getProducId().equals(productDetail.getProductId())){
                                img_add_wishlist.setImageDrawable(getResources().getDrawable(R.drawable.select_favorite));
                                Log.d(TAG, "data match: " + wishlistarray.get(i).getProducId() +" - "+productDetail.getProductId());
                                break;
                            }
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<WishListModel> call, Throwable t) {
                // Log error here since request failed
            }
        });
    }

    private void loadDetailData() {

        progressBar.setVisibility(View.VISIBLE);
        top_layout.setVisibility(View.GONE);

        apiService = apiClient.getClient().create(ApiInterface.class);

        Call<ProductDetailModel> call = apiService.getProductData(productId);
        apiClient.enqueue(call, new Callback<ProductDetailModel>() {
            @Override
            public void onResponse(Call<ProductDetailModel> call, Response<ProductDetailModel> response) {
                Log.d(TAG, "data: " + response.body());
                progressBar.setVisibility(View.GONE);
                top_layout.setVisibility(View.VISIBLE);
                ProductDetailModel detailModel = response.body();

                productDetail = detailModel.getProductDetail().get(0);
                setDetailData(detailModel.getProductDetail().get(0));
                setMoreDetails(detailModel.getProductDetail().get(0));
                setImageSlider(detailModel.getProductDetail().get(0).getProductImage());
                setColorAdapter(detailModel.getProductDetail().get(0).getProductColor());
                setSizeAdapter(detailModel.getProductDetail().get(0).getProductSizes());
                setSimilatProduct(detailModel.getSimilar_product());

                loadWishlist();
            }

            @Override
            public void onFailure(Call<ProductDetailModel> call, Throwable t) {
                // Log error here since request failed
                Log.e(TAG, t.toString());
            }
        });
    }

    private void addToCart(Boolean isBuy) {
        if (size_id.isEmpty() && productDetail.getProductSizes() != null
                && productDetail.getProductSizes().size() > 0) {
            Toast.makeText(this, "Please select size first.", Toast.LENGTH_SHORT).show();
        } else {
            progressBar.setVisibility(View.VISIBLE);
            apiService = apiClient.getClient().create(ApiInterface.class);

            Call<CartResponse> call = apiService.addToCart(sharedPref.getStr("ID"), Constant.getDeviceId(this),
                    productDetail.getProductId(), "1", size_id);
            apiClient.enqueue(call, new Callback<CartResponse>() {
                @Override
                public void onResponse(Call<CartResponse> call, Response<CartResponse> response) {
                    Log.d(TAG, "data: " + response.body());
                    progressBar.setVisibility(View.GONE);
                    CartResponse body = response.body();

                    if (body.getCart() != null && body.getCart().size() > 0) {
//                    if(body.getCart().get(0).getError())
                        Toast.makeText(ProductDetailActivity.this, body.getCart().get(0).getMessage(), Toast.LENGTH_SHORT).show();
                        updateCartCount(body.getCart().get(0).getCount_cart());
                        setCardCount();
                        if (isBuy) {
                            Intent intent = new Intent(ProductDetailActivity.this, CartActivity.class);
                            startActivity(intent);
                        }
                    }
                }

                @Override
                public void onFailure(Call<CartResponse> call, Throwable t) {
                    // Log error here since request failed
                    Log.e(TAG, t.toString());
                }
            });
        }
    }

    private void addToWishList() {

        progressBar.setVisibility(View.VISIBLE);
        apiService = apiClient.getClient().create(ApiInterface.class);

        Call<CartResponse> call = apiService.addToWishlist(sharedPref.getStr("ID"),
                productDetail.getProductId(), Constant.getDeviceId(this));

        apiClient.enqueue(call, new Callback<CartResponse>() {
            @Override
            public void onResponse(Call<CartResponse> call, Response<CartResponse> response) {
                Log.d(TAG, "data: " + response.body());
                progressBar.setVisibility(View.GONE);
                CartResponse body = response.body();

                if (body.getCart() != null && body.getCart().size() > 0) {
//                    if(body.getCart().get(0).getError())
                    Log.d(TAG, "data: " + body.getCart().get(0).getMessage());
                    Toast.makeText(ProductDetailActivity.this, body.getCart().get(0).getMessage(), Toast.LENGTH_SHORT).show();
                    //  updateCartCount(body.getCart().get(0).getCount_cart());
                    // setCardCount();
                    img_add_wishlist.setImageDrawable(getResources().getDrawable(R.drawable.select_favorite));
                }
            }

            @Override
            public void onFailure(Call<CartResponse> call, Throwable t) {
                // Log error here since request failed
                Log.e(TAG, t.toString());
            }
        });
    }


    private void checkPincode(String pincode) {
        progressBar.setVisibility(View.VISIBLE);

        apiService = apiClient.getClient().create(ApiInterface.class);

        Call<PincodeModel> call = apiService.checkPincode(pincode);
        apiClient.enqueue(call, new Callback<PincodeModel>() {

            @Override
            public void onResponse(Call<PincodeModel> call, Response<PincodeModel> response) {
                Log.d(TAG, "data: " + response.body());
                progressBar.setVisibility(View.GONE);
                PincodeModel pincodeModel = response.body();
                //pincodeModel.getPincode();

                if (pincodeModel.getPincode().get(0).getError() == true) {
                    Toast.makeText(getApplicationContext(), pincodeModel.getPincode().get(0).getMassage(), Toast.LENGTH_SHORT).show();
                } else {
                    //pincode_id = etPincode.getText().toString();
                    Toast.makeText(getApplicationContext(), pincodeModel.getPincode().get(0).getMassage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<PincodeModel> call, Throwable t) {
                // Log error here since request failed
                Log.e(TAG, t.toString());
            }
        });
    }

    @Override
    public void ItemClick(int adapterPosition, String id, String colorName) {
        if (colorName.equals("")) {
            listSelctSize.clear();
            listSelctSize.put(adapterPosition, true);
            size_id = id;
//            productDetail.setSize(id);
            sizeAdapter.notifyDataSetChanged();

        } else {
            listSelct.clear();
            listSelct.put(adapterPosition, true);
            productDetail.setColor(colorName);
            colorAdapter.notifyDataSetChanged();
        }
    }

    public static Map<Integer, Boolean> listSelct = new HashMap<>();
    public static Map<Integer, Boolean> listSelctSize = new HashMap<>();
    ProgressDialog pd;

    // AshncTaskFor AddToCart...
//    public class AsyncAddToCart extends AsyncTask<Void, Void, Void> {
//
////        RegistratonBean bean;
//        Context context;
//        JSONObject cart_responce, jsonResponce;
//        String status_message, status_code, cart_id;
//        ModelCart cartModel;
//        java.util.concurrent.CopyOnWriteArrayList<ModelCart> arrayListCartDetails;
//        // CopyOnWriteArrayList<ModelCart> arrayListCartDetails;
//        ArrayList<ModelCart> arrayListProducts;
//        ArrayList<ModelCart> arrayListProductsAll;
//        int flag = 0;
//
//        public AsyncAddToCart(Context context,
//                              CopyOnWriteArrayList<ModelCart> arraylist_product) {
//            this.arrayListCartDetails = arraylist_product;
//            this.context = context;
//        }
//
//        @Override
//        protected void onPreExecute() {
//            arrayListProducts = new ArrayList<ModelCart>();
//
//            // ////////..
//            cartModel = new ModelCart();
////            String att_id = attributs_id;
////            if (att_id == null) {
////                att_id = "" + 0;
////            } else {
////                att_id = attributs_id;
////            }
////            System.out.println("ATTTTTTTTIDDD" + att_id);
//
//            cartModel.setId_product(productDetail.getProductId());
//            cartModel.setProduct_name("");
//            cartModel.setProduct_price("");
//
//            cartModel.setCurrency("");
////            cartModel.setId_product_attribute("" + att_id);
//            cartModel.setCart_quantity("1");
//
//            cartModel.setImage("");
//            cartModel.setAttribute_name(new JSONArray());
//            cartModel.setAttribute_val(new JSONArray());
//
//            arrayListProducts.add(cartModel);
//
//            for (ModelCart user1 : arrayListCartDetails) {
//                for (ModelCart user2 : arrayListProducts) {
//                    if (user1.getId_product().equals(user2.getId_product())) {
//                        if (user1.getId_product_attribute().equals(
//                                user2.getId_product_attribute())) {
//
//                            // setHere coding...
//                            arrayListCartDetails.remove(user1);
//
//                            // if (user1.getCart_quantity().equals(
//                            // user2.getCart_quantity())) {
//                            flag = 0;
//                            // }
//                        }
//                    } else {
//                    }
//                }
//            }
//            pd = new ProgressDialog(ProductDetailActivity.this);
//            pd.setCancelable(false);
//            pd.setProgressStyle(android.R.style.Widget_ProgressBar_Small_Inverse);
//            if (Constant.isNetworkAvailable(getApplicationContext())) {
//                pd.show();
//            } else {
//                Toast.makeText(getApplicationContext(),
//                        "No Network", Toast.LENGTH_SHORT)
//                        .show();
//                cancel(true);
//            }
//
//            pd.setOnCancelListener(new DialogInterface.OnCancelListener() {
//                @Override
//                public void onCancel(DialogInterface dialog) {
//                    cancel(true);
//                    pd.dismiss();
//                }
//            });
//            super.onPreExecute();
//
//        }
//
//        @Override
//        protected Void doInBackground(Void... params) {
//            System.out.println("FLAGGGGG" + flag);
//            if (flag == 0) {
//                arrayListProductsAll = new ArrayList<ModelCart>();
//
//                arrayListProductsAll.addAll(arrayListCartDetails);
//                arrayListProductsAll.addAll(arrayListProducts);
//
//                JSONObject jObject = new JSONObject();
//                JSONObject carts = new JSONObject();
//
//                JSONArray products = new JSONArray();
//                JSONObject innerObject;
//                try {
//                    carts.put(Constant.TAG_CART_ID, pref.getCart_id());
//                    carts.put(Constant.TAG_CART_CUSTOMER_ID,
//                            pref.getCustomer_id());
//                    carts.put(Constant.TAG_CART_GUEST_ID, "");
//                    carts.put(Constant.TAG_SECUREKEY, pref.getSecrate_key());
//
//                    // System.out.println("ARRAYSIZE" + arrry_list_cart.size());
//
//                    for (int i = 0; i < arrayListProductsAll.size(); i++) {
//
//                        innerObject = new JSONObject();
//                        innerObject.put(Constant.TAG_CART_PRODUCTS_ID,
//                                arrayListProductsAll.get(i).getId_product());
//
//                        innerObject.put(
//                                Constant.TAG_CART_PRODUCTS_ID_ATTRIBUTES,
//                                arrayListProductsAll.get(i)
//                                        .getId_product_attribute());
//                        innerObject.put(Constant.TAG_CART_QAUANTITY,
//                                arrayListProductsAll.get(i).getCart_quantity());
//                        products.put(i, innerObject);
//                        // }
//                    }
//                    carts.put(Constant.TAG_CART_PRODUCTS, products);
//                    jObject.put(Constant.TAG_CART, carts);
//
//                    String param = jObject.toString();
//                    // /
//                    cart_responce = jParser.makeHttpRequest(
//                            AppUrl.URL_ADD_TO_CART, "POST", param);
//
//                    System.out.println("CART" + cart_responce);
//                    //
//                    if (cart_responce != null) {
//                        try {
//                            jsonResponce = cart_responce
//                                    .getJSONObject(Constant.TAG_CART);
//                            status_code = jsonResponce
//                                    .getString(Constant.TAG_STATUS_CODE);
//                            status_message = jsonResponce
//                                    .getString(Constant.TAG_STATUS_MESSAGE);
//
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                    }
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//            return null;
//        }
//
//        @Override
//        protected void onPostExecute(Void result) {
//            super.onPostExecute(result);
//            if (pd.isShowing()) {
//                cancel(true);
//                pd.dismiss();
//                if (cart_responce != null) {
//                    String statusCose = status_code;
//                    if (statusCose == null) {
//                        statusCose = "" + 0;
//                    }
//                    if (Integer.parseInt(statusCose) == 200) {
//                        // Increasing Car Number...
//                        // cart_counts++;
//                        cart_counts = arrayListProductsAll.size();
//                        try {
//                            cart_id = jsonResponce
//                                    .getString(Constant.TAG_CART_ID_RECEIVED);
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//
//                        tvCart_Counts.setVisibility(View.VISIBLE);
//                        tvCart_Counts.setText("" + cart_counts);
//
//                        // ...
//                        pref.setCart_id(cart_id);
//                        pref.setCart_Counts(cart_counts);
//
//                        Toast.makeText(getApplicationContext(), status_message,
//                                Toast.LENGTH_SHORT).show();
//                    } else {
//                        Toast.makeText(getApplicationContext(), status_message,
//                                Toast.LENGTH_SHORT).show();
//                    }
//                }
//            }
//        }
//    }
}