package com.loklvokl.lokl.activtiy;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.loklvokl.lokl.R;
import com.loklvokl.lokl.adapter.AdharCardFileAdapter;
import com.loklvokl.lokl.adapter.PanCardAdapter;
import com.loklvokl.lokl.model.AadharDocument;
import com.loklvokl.lokl.model.EditProfileResponse;
import com.loklvokl.lokl.model.PanDocument;
import com.loklvokl.lokl.model.UserProfileUpdate;
import com.loklvokl.lokl.util.ApiClient;
import com.loklvokl.lokl.util.ApiInterface;
import com.loklvokl.lokl.util.Constant;
import com.loklvokl.lokl.util.SharedPref;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.regex.Pattern;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class EditActivity extends BaseActivity implements View.OnClickListener,
        AdharCardFileAdapter.AdInterface, PanCardAdapter.AdInterface {

    TextView title_text, btn_sign, btn_add_file_adhar_card, btn_add_file_pan_card;
    ImageView back_btn;
    EditText ed_username, ed_email, ed_lastname, ed_city, ed_dob, ed_mobile;
    String username, lastname, email, city, dob, fname, mobile;
    SharedPref sharedPref;
    ProgressBar progressBar;
    ApiInterface apiService;
    ApiClient apiClient;
    String userId = "0";
    String image_path_profile = "";
    ImageView img_user, img_plus;
    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 101;
    RadioButton radio1, radio2, radio3;
    //   RelativeLayout adhar_view, adharCard;
    int addhar_counter, pan_counter;

    //ArrayList<KycDetail> kycList;

    RecyclerView recycle_view_adhar_card, recycle_view_pan_card;

    public static final int RequestPermissionCode = 700;
    public static final int PICK_FILE_ADHAR_REQUEST = 10;
    public static final int PICK_FILE_PAN_REQUEST = 20;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);

        sharedPref = new SharedPref(this);
        apiClient = new ApiClient();
        apiService = ApiClient.getClient().create(ApiInterface.class);
        init();
        setProfileData();

        //StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        //StrictMode.setVmPolicy(builder.build());

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
    }


    private void init() {

        radio1 = findViewById(R.id.radio1);
        radio2 = findViewById(R.id.radio2);
        radio3 = findViewById(R.id.radio3);
        img_user = findViewById(R.id.img_user);
        img_plus = findViewById(R.id.img_plus);
        img_user.setOnClickListener(this);
        img_plus.setOnClickListener(this);

        back_btn = findViewById(R.id.back_btn);
        ed_mobile = findViewById(R.id.ed_mobile);
        back_btn.setOnClickListener(this);
        title_text = findViewById(R.id.title_text);
        title_text.setText("Edit Profile");

        ed_username = findViewById(R.id.ed_username);
        ed_email = findViewById(R.id.ed_email);
        ed_lastname = findViewById(R.id.ed_lastname);
        ed_city = findViewById(R.id.ed_city);
        ed_dob = findViewById(R.id.ed_dob);
        ed_dob.setOnClickListener(this);

        btn_add_file_adhar_card = findViewById(R.id.btn_add_file_adhar_card);
        btn_add_file_pan_card = findViewById(R.id.btn_add_file_pan_card);

        recycle_view_adhar_card = findViewById(R.id.recycle_view_adhar_card);
        recycle_view_pan_card = findViewById(R.id.recycle_view_pan_card);


        progressBar = findViewById(R.id.progressBar);
        btn_sign = findViewById(R.id.btn_sign);
        btn_sign.setOnClickListener(this);

        btn_add_file_adhar_card.setOnClickListener(this);
        btn_add_file_pan_card.setOnClickListener(this);

        myCalendar = Calendar.getInstance();
        date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }
        };
    }

    private void setProfileData() {
        Log.d("Profile : "," - - "+sharedPref.getStr("MOBILE")+sharedPref.getStr("FNAME"));
        ed_email.setText(sharedPref.getStr("EMAIL"));
        ed_username.setText(sharedPref.getStr("FNAME"));
        ed_lastname.setText(sharedPref.getStr("LNAME"));
        ed_mobile.setText(sharedPref.getStr("MOBILE"));
        userId = sharedPref.getStr("ID");
        ed_city.setText(sharedPref.getStr("CITY"));

        if (sharedPref.getStr("DOB") != null) {
            ed_dob.setText(sharedPref.getStr("DOB"));
        } else {
            ed_dob.setText("");
        }

        if (sharedPref.getStr("GENDER") != null && sharedPref.getStr("GENDER").equalsIgnoreCase("female")) {
            radio2.setChecked(true);
        } else if (sharedPref.getStr("GENDER") != null && sharedPref.getStr("GENDER").equalsIgnoreCase("other")) {
            radio3.setChecked(true);
        }

        if (!sharedPref.getStr("IMAGE").isEmpty()) {
            Picasso.get().load("https://" + sharedPref.getStr("IMAGE"))
                    .resize(0, 250)
                    .into(img_user, new com.squareup.picasso.Callback() {
                        @Override
                        public void onSuccess() {
                        }

                        @Override
                        public void onError(Exception e) {
                        }
                    });
        }

        setKYCdata();
    }

    ArrayList<AadharDocument> kycAdaharList;
    ArrayList<String> kycAdaharImageList;
    ArrayList<String> kycAdaharIdList;

    ArrayList<PanDocument> kycPanList;
    ArrayList<String> kycPanImageList;
    ArrayList<String> kycPanIdList;

    public void setKYCdata() {

        kycAdaharList = sharedPref.getAdharList("ADHAR_LIST");
        if (kycAdaharList == null) {
            kycAdaharList = new ArrayList<>();
        }
        kycAdaharImageList = new ArrayList<>();
        kycAdaharIdList = new ArrayList<>();
        image_path_list_adhar = new ArrayList<>();

        for (int i = 0; i < kycAdaharList.size(); i++) {
            kycAdaharImageList.add(kycAdaharList.get(i).getAadharImage());
            kycAdaharIdList.add(kycAdaharList.get(i).getId());
            image_path_list_adhar.add("");
        }
        setImagesAdahar(kycAdaharImageList);

        //

        kycPanList = sharedPref.getPanList("PAN_LIST");
        if (kycPanList == null) {
            kycPanList = new ArrayList<>();
        }
        kycPanImageList = new ArrayList<>();
        kycPanIdList = new ArrayList<>();
        image_path_list_pan = new ArrayList<>();

        for (int i = 0; i < kycPanList.size(); i++) {
            kycPanImageList.add(kycPanList.get(i).getPanImage());
            kycPanIdList.add(kycPanList.get(i).getId());
            image_path_list_pan.add("");
        }
        setImagesPan(kycPanImageList);

        addhar_counter = sharedPref.getInt("ADHAR_COUNT");
        pan_counter = sharedPref.getInt("PAN_COUNT");
//

        if (addhar_counter >= 3) {
            btn_add_file_adhar_card.setVisibility(View.GONE);
        } else {
            btn_add_file_adhar_card.setVisibility(View.VISIBLE);
        }

        if (pan_counter >= 3) {
            btn_add_file_pan_card.setVisibility(View.GONE);
        } else {
            btn_add_file_pan_card.setVisibility(View.VISIBLE);
        }
    }


    private void updateLabel() {
        String myFormat = "dd-MM-yyyy";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        ed_dob.setText(sdf.format(myCalendar.getTime()));
    }

    @Override
    public void onClick(View view) {
        if (view == back_btn) {
            finish();

        } else if (view == btn_sign) {
            updateProfile();

        } else if (view == img_user || view == img_plus) {
            if (checkAndRequestPermissions()) {
                selectImage();
            }
        } else if (view == ed_dob) {
            openDatePIcker();
        } else if (view == btn_add_file_adhar_card) {
            if (CheckingPermissionIsEnabledOrNot()) {
                getFileChooserAdharCard();
            } else {
                RequestMultiplePermission();
            }

        } else if (view == btn_add_file_pan_card) {
            if (CheckingPermissionIsEnabledOrNot()) {
                getFileChooserPan();
            } else {
                RequestMultiplePermission();
            }
        }
    }

    public boolean CheckingPermissionIsEnabledOrNot() {
        int OnePermissionResult = ContextCompat.checkSelfPermission(getApplicationContext(), READ_EXTERNAL_STORAGE);
        int TwoPermissionResult = ContextCompat.checkSelfPermission(getApplicationContext(), WRITE_EXTERNAL_STORAGE);
        int ThreePermissionResult = ContextCompat.checkSelfPermission(getApplicationContext(), CAMERA);
        return OnePermissionResult == PackageManager.PERMISSION_GRANTED &&
                TwoPermissionResult == PackageManager.PERMISSION_GRANTED &&
                ThreePermissionResult == PackageManager.PERMISSION_GRANTED;
    }

    private void RequestMultiplePermission() {
        ActivityCompat.requestPermissions(EditActivity.this, new String[]
                {READ_EXTERNAL_STORAGE,
                        WRITE_EXTERNAL_STORAGE,
                        CAMERA,
                }, RequestPermissionCode);
    }

    private void getFileChooserAdharCard() {
        String[] mimeTypes = {"image/*"};
        // String[] mimeTypes = {"image/jpeg", "image/jpg", "image/png", "application/pdf"};
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            intent.setType(mimeTypes.length == 1 ? mimeTypes[0] : "*/*");
            if (mimeTypes.length > 0) {
                intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
            }
        } else {
            String mimeTypesStr = "";
            for (String mimeType : mimeTypes) {
                mimeTypesStr += mimeType + "|";
            }
            intent.setType(mimeTypesStr.substring(0, mimeTypesStr.length() - 1));
        }
        try {
            startActivityForResult(
                    Intent.createChooser(intent, "Select a File to Upload"),
                    PICK_FILE_ADHAR_REQUEST);
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(this, "Please install a File Manager.",
                    Toast.LENGTH_SHORT).show();
        }
    }

    private void getFileChooserPan() {
        // String[] mimeTypes = {"image/jpeg", "image/jpg", "image/png", "application/pdf"};
        String[] mimeTypes = {"image/*"};
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            intent.setType(mimeTypes.length == 1 ? mimeTypes[0] : "*/*");
            if (mimeTypes.length > 0) {
                intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
            }
        } else {
            String mimeTypesStr = "";
            for (String mimeType : mimeTypes) {
                mimeTypesStr += mimeType + "|";
            }
            intent.setType(mimeTypesStr.substring(0, mimeTypesStr.length() - 1));
        }
        try {
            startActivityForResult(
                    Intent.createChooser(intent, "Select a File to Upload"),
                    PICK_FILE_PAN_REQUEST);
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(this, "Please install a File Manager.",
                    Toast.LENGTH_SHORT).show();
        }
    }

    Calendar myCalendar;
    DatePickerDialog.OnDateSetListener date;

    private void openDatePIcker() {
        new DatePickerDialog(this, date, myCalendar
                .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH)).show();
    }

    private void selectImage() {
        Intent intent = new Intent(this, ImageDialogActivity.class);
        startActivityForResult(intent, 111);
    }

    private void updateProfile() {
        username = ed_username.getText().toString();
        lastname = ed_lastname.getText().toString();
        email = ed_email.getText().toString();
        mobile = ed_mobile.getText().toString();
        String city = ed_city.getText().toString();
        dob = ed_dob.getText().toString();
        String deviceId = Constant.getDeviceId(this);

        /*if (username.isEmpty()) {
            Toast.makeText(this, "First name is required.", Toast.LENGTH_SHORT).show();
        } else if (lastname.isEmpty()) {
            Toast.makeText(this, "Last name is required.", Toast.LENGTH_SHORT).show();
        } else if (!idValid(email)) {
            Toast.makeText(this, "Email is not valid format.", Toast.LENGTH_SHORT).show();
        } */


        /*if (dob.isEmpty()) {
            Toast.makeText(this, "Date of Birth is required.", Toast.LENGTH_SHORT).show();
        } else if (city.isEmpty()) {
            Toast.makeText(this, "City is required.", Toast.LENGTH_SHORT).show();
        }
        */


        if (mobile.isEmpty()) {
            Toast.makeText(this, "Mobile number is required..", Toast.LENGTH_SHORT).show();
        } else {
            progressBar.setVisibility(View.VISIBLE);
            apiService = apiClient.getClient().create(ApiInterface.class);

            Call<EditProfileResponse> call;
            String gender = "male";

            if (radio1.isChecked()) {
                gender = "male";
            } else if (radio2.isChecked()) {
                gender = "female";
            } else {
                gender = "other";
            }


            // if (image_path.isEmpty())
            //     call = apiService.requestUpdateProfile(username, lastname, email, userId, image_path, gender, dob, city);
            //else {


            System.out.println("PATH" + image_path_list_adhar.size());

            for (int i = 0; i < image_path_list_adhar.size(); i++) {
                System.out.println("PATH1>>" + image_path_list_adhar.get(i));
            }

            File fileProfileImage = null;

            if (!image_path_profile.isEmpty()) {
                fileProfileImage = new File(image_path_profile);
            }

            MultipartBody.Part bodyProfileImage = null;
            if (fileProfileImage != null) {
                RequestBody requestFile =
                        RequestBody.create(
                                MediaType.parse("*/*"),
                                fileProfileImage
                        );
                bodyProfileImage = MultipartBody.Part.createFormData("profile_image", fileProfileImage.getName(), requestFile);
            }

            File file1 = null, file2 = null, file3 = null;

            File file4 = null, file5 = null, file6 = null;

            if (image_path_list_adhar.size() == 1) {
                //file1 = urlToFile(kycAdaharImageList.get(0));
                if (!image_path_list_adhar.get(0).equals("")) {
                    file1 = new File(image_path_list_adhar.get(0));
                }
            }
            if (image_path_list_adhar.size() == 2) {
                if (!image_path_list_adhar.get(0).equals("")) {
                    file1 = new File(image_path_list_adhar.get(0));
                }
                if (!image_path_list_adhar.get(1).equals("")) {
                    file2 = new File(image_path_list_adhar.get(1));
                }
            }
            if (image_path_list_adhar.size() == 3) {

                if (!image_path_list_adhar.get(0).equals("")) {
                    file1 = new File(image_path_list_adhar.get(0));
                }
                if (!image_path_list_adhar.get(1).equals("")) {
                    file2 = new File(image_path_list_adhar.get(1));
                }
                if (!image_path_list_adhar.get(2).equals("")) {
                    file3 = new File(image_path_list_adhar.get(2));
                }
            }

            System.out.println("PATH_SIZE" + image_path_list_pan.size());

            // for pan
            if (image_path_list_pan.size() == 1) {
                //file1 = urlToFile(kycAdaharImageList.get(0));
                if (!image_path_list_pan.get(0).equals("")) {
                    file4 = new File(image_path_list_pan.get(0));
                }
            }
            if (image_path_list_pan.size() == 2) {
                if (!image_path_list_pan.get(0).equals("")) {
                    file4 = new File(image_path_list_pan.get(0));
                }
                if (!image_path_list_pan.get(1).equals("")) {
                    file5 = new File(image_path_list_pan.get(1));
                }
            }
            if (image_path_list_pan.size() == 3) {

                if (!image_path_list_pan.get(0).equals("")) {
                    file4 = new File(image_path_list_pan.get(0));
                }
                if (!image_path_list_pan.get(1).equals("")) {
                    file5 = new File(image_path_list_pan.get(1));
                }
                if (!image_path_list_pan.get(2).equals("")) {
                    file6 = new File(image_path_list_pan.get(2));
                }
            }


            MultipartBody.Part bodyadhaar_image1 = null;
            if (file1 != null) {
                RequestBody requestFileImage1 =
                        RequestBody.create(
                                MediaType.parse("*/*"),
                                file1
                        );
                bodyadhaar_image1 = MultipartBody.Part.createFormData("adhaar_image1", file1.getName(), requestFileImage1);
            }
            //
            MultipartBody.Part bodyadhaar_image2 = null;
            if (file2 != null) {
                RequestBody requestFileImage2 =
                        RequestBody.create(
                                MediaType.parse("*/*"),
                                file2
                        );
                bodyadhaar_image2 = MultipartBody.Part.createFormData("adhaar_image2", file2.getName(), requestFileImage2);
            }
            //
            MultipartBody.Part bodyadhaar_image3 = null;
            if (file3 != null) {
                RequestBody requestFileImage3 =
                        RequestBody.create(
                                MediaType.parse("*/*"),
                                file3
                        );
                bodyadhaar_image3 = MultipartBody.Part.createFormData("adhaar_image3", file3.getName(), requestFileImage3);
            }


            // for pan
            MultipartBody.Part bodypan_image1 = null;
            if (file4 != null) {
                RequestBody requestFilepan_image1 =
                        RequestBody.create(
                                MediaType.parse("*/*"),
                                file4
                        );
                bodypan_image1 = MultipartBody.Part.createFormData("pan_image1", file4.getName(), requestFilepan_image1);
            }
            //
            MultipartBody.Part bodypan_image2 = null;
            if (file5 != null) {
                RequestBody requestFilepan_image2 =
                        RequestBody.create(
                                MediaType.parse("*/*"),
                                file5
                        );
                bodypan_image2 = MultipartBody.Part.createFormData("pan_image2", file5.getName(), requestFilepan_image2);
            }
            //
            MultipartBody.Part bodypan_image3 = null;
            if (file6 != null) {
                RequestBody requestpan_image3 =
                        RequestBody.create(
                                MediaType.parse("*/*"),
                                file6
                        );
                bodypan_image3 = MultipartBody.Part.createFormData("pan_image3", file6.getName(), requestpan_image3);
            }


            RequestBody username1 = RequestBody.create(MediaType.parse("text/plain"), username);
            RequestBody lastname1 = RequestBody.create(MediaType.parse("text/plain"), lastname);
            RequestBody email1 = RequestBody.create(MediaType.parse("text/plain"), email);
            RequestBody userId1 = RequestBody.create(MediaType.parse("text/plain"), userId);

            RequestBody city1 = RequestBody.create(MediaType.parse("text/plain"), city);
            RequestBody gender1 = RequestBody.create(MediaType.parse("text/plain"), gender);
            RequestBody dob1 = RequestBody.create(MediaType.parse("text/plain"), dob);

            RequestBody deviceId1 = RequestBody.create(MediaType.parse("text/plain"), deviceId);
            RequestBody addhar = RequestBody.create(MediaType.parse("text/plain"), "addhar");
            RequestBody pan = RequestBody.create(MediaType.parse("text/plain"), "pan");


            if (image_path_profile.isEmpty()) {
                call = apiService.requestUpdateProfileFile(username1, lastname1, email1, userId1, null, deviceId1, gender1,
                        dob1, city1, addhar, pan, bodyadhaar_image1, bodyadhaar_image2, bodyadhaar_image3, bodypan_image1,
                        bodypan_image2, bodypan_image3);
            } else {
                call = apiService.requestUpdateProfileFile(username1, lastname1, email1, userId1, bodyProfileImage, deviceId1, gender1,
                        dob1, city1, addhar, pan, bodyadhaar_image1, bodyadhaar_image2, bodyadhaar_image3, bodypan_image1,
                        bodypan_image2, bodypan_image3);
            }

            //  }

            apiClient.enqueue(call, new Callback<EditProfileResponse>() {
                @Override
                public void onResponse(Call<EditProfileResponse> call, Response<EditProfileResponse> response) {
                    Log.d("TAG", "data: " + response.body());
                    progressBar.setVisibility(View.GONE);
                    EditProfileResponse body = response.body();

//                    if (body.getResponse() != null && body.getResponse().size() > 0) {
                    Toast.makeText(EditActivity.this, body.getUserProfile().getMessage(), Toast.LENGTH_SHORT).show();

                    if (body.getUserProfile().getError() != null) {
                        UserProfileUpdate user = body.getUserProfile();
                        sharedPref.setStr("NAME", user.getUsername());
                        sharedPref.setStr("LNAME", user.getLastName());
                        sharedPref.setStr("IMAGE", user.getProfileImage());
                        sharedPref.setStr("EMAIL", user.getEmail());
                        sharedPref.setStr("CITY", user.getCity());
                        sharedPref.setStr("GENDER", user.getGender());
                        sharedPref.setStr("DOB", user.getDob());
                        sharedPref.setAdharList("ADHAR_LIST", user.getAadharDocument());
                        sharedPref.setPanList("PAN_LIST", user.getPanDocument());
                        sharedPref.setInt("PAN_COUNT", user.getPanCounter());
                        sharedPref.setInt("ADHAR_COUNT", user.getAddharCounter());
                        setResult(RESULT_OK);
                        finish();
                        MainActivity.tabHost.setCurrentTab(4);
                    }
//                    }
                }

                @Override
                public void onFailure(Call<EditProfileResponse> call, Throwable t) {
                    // Log error here since request failed
                    Log.e("TAG", t.toString());
                }
            });

        }
    }

    private void deleteKYCDoc(String imageId) {

        username = ed_username.getText().toString();
        lastname = ed_lastname.getText().toString();
        email = ed_email.getText().toString();
        mobile = ed_mobile.getText().toString();
        String city = ed_city.getText().toString();
        dob = ed_dob.getText().toString();
        String deviceId = Constant.getDeviceId(this);

        progressBar.setVisibility(View.VISIBLE);
        apiService = apiClient.getClient().create(ApiInterface.class);

        Call<EditProfileResponse> call;
        String gender = "male";

        if (radio1.isChecked()) {
            gender = "male";
        } else if (radio2.isChecked()) {
            gender = "female";
        } else {
            gender = "other";
        }
        if (image_path_profile.isEmpty())
            call = apiService.requestDeleteDoc(username, lastname, email, userId, image_path_profile, gender, dob, deviceId, city,
                    "addhar", "pan", "delete", imageId);
        else {
            File file = new File(image_path_profile);
            String mim = getMimeType(file);
            Log.e("asasa", ">" + file + "," + mim);

//                RequestBody requestFile = RequestBody.create(MediaType.parse(mim), file);
            RequestBody requestFile = RequestBody.create(MediaType.parse("image/*"), new File(image_path_profile));

            RequestBody username1 = RequestBody.create(MediaType.parse("text/plain"), username);
            RequestBody lastname1 = RequestBody.create(MediaType.parse("text/plain"), lastname);
            RequestBody email1 = RequestBody.create(MediaType.parse("text/plain"), email);
            RequestBody userId1 = RequestBody.create(MediaType.parse("text/plain"), userId);

            RequestBody city1 = RequestBody.create(MediaType.parse("text/plain"), city);
            RequestBody gender1 = RequestBody.create(MediaType.parse("text/plain"), gender);
            RequestBody dob1 = RequestBody.create(MediaType.parse("text/plain"), dob);
            RequestBody deviceId1 = RequestBody.create(MediaType.parse("text/plain"), deviceId);
            RequestBody addhar = RequestBody.create(MediaType.parse("text/plain"), "addhar");
            RequestBody pan = RequestBody.create(MediaType.parse("text/plain"), "pan");
            RequestBody delete = RequestBody.create(MediaType.parse("text/plain"), "delete");
            RequestBody imageId1 = RequestBody.create(MediaType.parse("text/plain"), imageId);

            MultipartBody.Part body =
                    MultipartBody.Part.createFormData("profile_image", file.getName(), requestFile);

            //call = apiService.requestDeleteDoc(username1, username1, email1, userId1, body, gender1, dob1, city1);

            call = apiService.requestDeleteDocFile(username1, username1, email1, userId1, body, gender1, dob1, deviceId1, city1,
                    addhar, pan, delete, imageId1);
        }

        apiClient.enqueue(call, new Callback<EditProfileResponse>() {
            @Override
            public void onResponse(Call<EditProfileResponse> call, Response<EditProfileResponse> response) {
                Log.d("Resposne_after_delete", "data: " + response.body());
                progressBar.setVisibility(View.GONE);
                EditProfileResponse body = response.body();

//                    if (body.getResponse() != null && body.getResponse().size() > 0) {
                Toast.makeText(EditActivity.this, body.getUserProfile().getMessage(), Toast.LENGTH_SHORT).show();

                if (body.getUserProfile().getError() != null) {
                    UserProfileUpdate user = body.getUserProfile();
                    sharedPref.setStr("NAME", user.getUsername());
                    sharedPref.setStr("LNAME", user.getLastName());
                    sharedPref.setStr("IMAGE", user.getProfileImage());
                    sharedPref.setStr("EMAIL", user.getEmail());
                    sharedPref.setStr("CITY", user.getCity());
                    sharedPref.setStr("GENDER", user.getGender());
                    sharedPref.setStr("DOB", user.getDob());
                    sharedPref.setAdharList("ADHAR_LIST", user.getAadharDocument());
                    sharedPref.setPanList("PAN_LIST", user.getPanDocument());
                    sharedPref.setInt("PAN_COUNT", user.getPanCounter());
                    sharedPref.setInt("ADHAR_COUNT", user.getAddharCounter());
                    setKYCdata();
                    //sharedPref.setStr("FNAME", user.get());
                    //sharedPref.setArrayList("KYC_LIST", user.get());

                    //setResult(RESULT_OK);
                    //finish();
                    //MainActivity.tabHost.setCurrentTab(4);
                }
//                    }
            }

            @Override
            public void onFailure(Call<EditProfileResponse> call, Throwable t) {
                // Log error here since request failed
                Log.e("TAG", t.toString());
            }
        });

    }


    private boolean idValid(String email) {
        Pattern pattern = Patterns.EMAIL_ADDRESS;
        return pattern.matcher(email).matches();
    }

    //  File file_pan;
    Uri fileUriAdhar, fileUripan;

    //  List<File> image_file_list_pan = new ArrayList<>();
    List<String> image_path_list_adhar = new ArrayList<>();
    List<String> image_path_list_pan = new ArrayList<>();

    @Override

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == 201) {
                File f = new File(Environment.getExternalStorageDirectory().toString());
                for (File temp : f.listFiles()) {
                    if (temp.getName().equals("temp.jpg")) {
                        f = temp;
                        break;
                    }
                }
                try {
                    Bitmap bitmap;
                    BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
                    bitmap = BitmapFactory.decodeFile(f.getAbsolutePath(), bitmapOptions);
//                    bitmap=getResizedBitmap(bitmap, 400);
                    img_user.setImageBitmap(bitmap);
                    image_path_profile = f.getAbsolutePath();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (requestCode == 200) {
                Uri selectedImage = data.getData();
                String[] filePath = {MediaStore.Images.Media.DATA};
                Cursor c = getContentResolver().query(selectedImage, filePath, null, null, null);
                c.moveToFirst();
                int columnIndex = c.getColumnIndex(filePath[0]);
                String picturePath = c.getString(columnIndex);
                c.close();
                Bitmap bitmap = fixRotate(BitmapFactory.decodeFile(picturePath), picturePath);
//                thumbnail=getResizedBitmap(thumbnail, 400);
                Log.e("path of image", picturePath + "");
                image_path_profile = picturePath;
                img_user.setImageBitmap(bitmap);

            } else if (requestCode == 111) {
                if (data.getStringExtra("SELECT").equalsIgnoreCase("CAMERA")) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    File f = new File(android.os.Environment.getExternalStorageDirectory(), "temp.jpg");
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
                    startActivityForResult(intent, 201);
                } else {
                    Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(intent, 200);
                }
            }
        }
        if (requestCode == PICK_FILE_ADHAR_REQUEST) {
            if (resultCode == Activity.RESULT_OK) {
                fileUriAdhar = data.getData();
                String imagePath = getFilePathForN(fileUriAdhar, EditActivity.this);
                System.out.println("IMAGE_URII" + fileUriAdhar);
                if (kycAdaharImageList.size() == 3) {
                    Toast.makeText(this, "You can select 3 only! ", Toast.LENGTH_SHORT).show();
                }

                if (imagePath.endsWith(".jpg") == true) {
                    if (addhar_counter < 3) {
                        kycAdaharImageList.add(fileUriAdhar.toString());
                        kycAdaharIdList.add("");
                        image_path_list_adhar.add(imagePath);
                    }
                } else if (imagePath.endsWith(".jpeg") == true) {
                    if (addhar_counter < 3) {
                        kycAdaharImageList.add(fileUriAdhar.toString());
                        kycAdaharIdList.add("");
                        image_path_list_adhar.add(imagePath);
                    }
                } else if (imagePath.endsWith(".png") == true) {
                    if (addhar_counter < 3) {
                        kycAdaharImageList.add(fileUriAdhar.toString());
                        kycAdaharIdList.add("");
                        image_path_list_adhar.add(imagePath);
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Please select only JPEG/PDF/PNG file", Toast.LENGTH_SHORT).show();
                }
                if (kycAdaharImageList.size() == 3) {
                    btn_add_file_adhar_card.setVisibility(View.GONE);
                } else {
                    btn_add_file_adhar_card.setVisibility(View.VISIBLE);
                }
                imageAdapter.notifyDataSetChanged();
            }
        }
        if (requestCode == PICK_FILE_PAN_REQUEST) {
            if (resultCode == Activity.RESULT_OK) {
                fileUripan = data.getData();
                String imagePath = getFilePathForN(fileUripan, EditActivity.this);

                System.out.println("IMAGE_URII" + fileUripan);
                if (kycPanImageList.size() == 3) {
                    Toast.makeText(this, "You can select 3 only! ", Toast.LENGTH_SHORT).show();
                }

                if (imagePath.endsWith(".jpg") == true) {
                    if (pan_counter < 3) {
                        kycPanImageList.add(fileUripan.toString());
                        kycPanIdList.add("");
                        image_path_list_pan.add(imagePath);
                    }
                } else if (imagePath.endsWith(".jpeg") == true) {
                    if (pan_counter < 3) {
                        kycPanImageList.add(fileUripan.toString());
                        kycPanIdList.add("");
                        image_path_list_pan.add(imagePath);
                    }
                } else if (imagePath.endsWith(".png") == true) {
                    if (pan_counter < 3) {
                        kycPanImageList.add(fileUripan.toString());
                        kycPanIdList.add("");
                        image_path_list_pan.add(imagePath);
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Please select only JPEG/PDF/PNG file", Toast.LENGTH_SHORT).show();
                }
                if (kycPanImageList.size() == 3) {
                    btn_add_file_pan_card.setVisibility(View.GONE);
                } else {
                    btn_add_file_pan_card.setVisibility(View.VISIBLE);
                }
                imageAdapterPanCard.notifyDataSetChanged();
            }
        }
    }

    AdharCardFileAdapter imageAdapter;
    PanCardAdapter imageAdapterPanCard;

    private void setImagesAdahar(List<String> imagesAdaharlist) {
        if (imagesAdaharlist != null) {
            recycle_view_adhar_card.setVisibility(View.VISIBLE);
            imageAdapter = new AdharCardFileAdapter(this, imagesAdaharlist);
            recycle_view_adhar_card.setLayoutManager(new LinearLayoutManager(this,
                    LinearLayoutManager.HORIZONTAL, false));
            recycle_view_adhar_card.setAdapter(imageAdapter);
        } else {
            recycle_view_adhar_card.setVisibility(View.INVISIBLE);
        }
    }

    ;

    private void setImagesPan(List<String> imagesPanlist) {
        if (imagesPanlist != null) {
            recycle_view_pan_card.setVisibility(View.VISIBLE);
            imageAdapterPanCard = new PanCardAdapter(this, imagesPanlist);
            recycle_view_pan_card.setLayoutManager(new LinearLayoutManager(this,
                    LinearLayoutManager.HORIZONTAL, false));
            recycle_view_pan_card.setAdapter(imageAdapterPanCard);
        } else {
            recycle_view_pan_card.setVisibility(View.INVISIBLE);
        }
    }

    private Bitmap fixRotate(Bitmap decodeFile, String photoPath) {
        ExifInterface ei = null;
        try {
            ei = new ExifInterface(photoPath);
        } catch (IOException e) {
            e.printStackTrace();
        }
        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                ExifInterface.ORIENTATION_UNDEFINED);

        Bitmap rotatedBitmap = decodeFile;
        switch (orientation) {

            case ExifInterface.ORIENTATION_ROTATE_90:
                rotatedBitmap = rotateImage(decodeFile, 90);
                break;

            case ExifInterface.ORIENTATION_ROTATE_180:
                rotatedBitmap = rotateImage(decodeFile, 180);
                break;

            case ExifInterface.ORIENTATION_ROTATE_270:
                rotatedBitmap = rotateImage(decodeFile, 270);
                break;

            case ExifInterface.ORIENTATION_NORMAL:
            default:
                rotatedBitmap = decodeFile;
        }

        return rotatedBitmap;
    }

    public static Bitmap rotateImage(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(),
                matrix, true);
    }

    public boolean checkAndRequestPermissions() {
        int ExtstorePermission = ContextCompat.checkSelfPermission(EditActivity.this,
                Manifest.permission.READ_EXTERNAL_STORAGE);
        int cameraPermission = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        List<String> listPermissionsNeeded = new ArrayList<>();
        if (cameraPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA);
        }
        if (ExtstorePermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded
                            .toArray(new String[listPermissionsNeeded.size()]),
                    REQUEST_ID_MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_ID_MULTIPLE_PERMISSIONS:
                if (ContextCompat.checkSelfPermission(this,
                        Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(getApplicationContext(),
                            "Profile image Requires Access to Camara.", Toast.LENGTH_SHORT)
                            .show();
                } else if (ContextCompat.checkSelfPermission(this,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(getApplicationContext(),
                            "Upload image Profile Requires Access to Your Storage.",
                            Toast.LENGTH_SHORT).show();
                } else {
                    selectImage();
                }
                break;
        }
    }

    public String getMimeType(File file) {
        Uri uri = Uri.fromFile(file);
        ContentResolver cR = getContentResolver();
        MimeTypeMap mime = MimeTypeMap.getSingleton();
        String type = mime.getExtensionFromMimeType(cR.getType(uri));
        return type == null ? "image/jpg" : type;
    }

    @Override
    public void itemClick(int position) {
        System.out.println("Remove item");
        image_path_list_adhar.remove(position);

        if (kycAdaharIdList.get(position).isEmpty()) {
            kycAdaharIdList.remove(position);
            kycAdaharImageList.remove(position);

            imageAdapter.notifyDataSetChanged();
        } else {
            deleteKYCDoc(kycAdaharIdList.get(position));
        }
    }

    @Override
    public void itemClickAll(int position) {

    }


    @Override
    public void itemClickPan(int position) {
        System.out.println("PAN_ID_REMOVED" + kycPanIdList.get(position));
        image_path_list_pan.remove(position);
        if (kycPanIdList.get(position).isEmpty()) {
            kycPanIdList.remove(position);
            kycPanImageList.remove(position);

            imageAdapterPanCard.notifyDataSetChanged();
        } else {
            deleteKYCDoc(kycPanIdList.get(position));
        }
    }

    @Override
    public void itemClickPanAll(int position) {

    }
}