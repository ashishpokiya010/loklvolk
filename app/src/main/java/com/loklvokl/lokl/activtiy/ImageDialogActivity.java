package com.loklvokl.lokl.activtiy;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;

import com.loklvokl.lokl.R;

public class ImageDialogActivity extends AppCompatActivity {


    LinearLayout camera_layout, gallery_layout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_dialog);

        camera_layout = findViewById(R.id.camera_layout);
        gallery_layout = findViewById(R.id.gallery_layout);

        camera_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.putExtra("SELECT", "CAMERA");
                setResult(RESULT_OK, intent);
                finish();
            }
        });

        gallery_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.putExtra("SELECT", "GALLERY");
                setResult(RESULT_OK, intent);
                finish();
            }
        });
    }
}