package com.loklvokl.lokl.activtiy;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import com.google.android.material.chip.ChipGroup;
import com.loklvokl.lokl.R;
import com.loklvokl.lokl.adapter.ProductDealAdapter;
import com.loklvokl.lokl.adapter.SearchSuggestionAdapter;
import com.loklvokl.lokl.model.Product;
import com.loklvokl.lokl.model.SearchModel;
import com.loklvokl.lokl.model.searchsuggestion.SearchItems;
import com.loklvokl.lokl.model.searchsuggestion.SearchSuggestion;
import com.loklvokl.lokl.util.ApiClient;
import com.loklvokl.lokl.util.ApiInterface;
import com.loklvokl.lokl.util.Constant;
import com.loklvokl.lokl.util.FlowLayout;
import com.robertlevonyan.views.chip.Chip;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchActivity extends BaseActivity implements
        View.OnClickListener, SearchSuggestionAdapter.OnItemSuggest, SearchSuggestionAdapter.OnItemClicked {

    private static final String TAG = "Product_list";
    RecyclerView recyclerView, recyclerView_Search;
    ProgressBar progressBar;
    List<SearchItems> searchItems = new ArrayList<>();
    List<SearchItems> searchTrendItems = new ArrayList<>();
    List<Product> products = new ArrayList<>();
    String productId = "0";
    TextView empty_text, title_text;
    RelativeLayout layout_sort;
    TextView txtSelectedSort;
    ImageView imgSearchWishlist, imgSeachNoti, imgSearchCart;
    ImageView back_btn;
    ProductDealAdapter productDealAdapter;
    String selectedSort = "popular";
    ApiInterface apiService;
    EditText searchView;
    FlowLayout layout_tags;
    Chip[] chip;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        apiService = ApiClient.getClient().create(ApiInterface.class);

        init();
        getINtentData();
        loadTrendSearch();

    }


    private void setTags(){

        Log.d("SearchSugg Size : "," - - "+searchTrendItems.size()+" - - "+searchTrendItems.get(0).getData());

        chip = new Chip[searchTrendItems.size()];
        for(int i=0; i<searchTrendItems.size(); i++) {
            final int index = i;
            chip[i] = new Chip(SearchActivity.this);

            chip[i].setId(i-1);
            chip[i].setText(searchTrendItems.get(i).getData()); // Set Chip label
            chip[i].setChipTextColor(getResources().getColor(R.color.text_dark)); // Set Chip label color
            chip[i].setChipBackgroundColor(getResources().getColor(R.color.white)); //Set custom background color
            chip[i].setChipIcon(ContextCompat.getDrawable(this,R.drawable.ic_trend)); //Set Icon Drawable for Chip
            chip[i].setClosable(false);//Set Chip has close button

            chip[i].setCornerRadius(25); // Set corner radius of your Chip
            chip[i].setStrokeSize(2); // Set width of stroke
            chip[i].setStrokeColor(getResources().getColor(R.color.colorPrimaryDark));// Set stroke color for your Chip

            LinearLayout.LayoutParams lparams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT);

            lparams.setMargins(0,10,10,10);
            chip[i].setLayoutParams(lparams);
            //chip[i].setLayoutParams(lparams);
            layout_tags.addView(chip[i]);

            chip[i].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //Toast.makeText(SearchActivity.this, ""+chip[index].getText(), Toast.LENGTH_LONG).show();
                    hideKeyboard();
                    loadProductData(chip[index].getText().toString());
                }
            });
        }
    }

    private void getINtentData() {
        productId = getIntent().getStringExtra("BANNER_ID");

        if (getIntent().hasExtra("TITLE")) {
            title_text.setText(getIntent().getStringExtra("TITLE"));
        }
    }

    public void loadSearchSuggestions(String str_search){
        empty_text.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);

        Call<SearchSuggestion> call = apiService.getSearchSuggestion(str_search);

        call.enqueue(new Callback<SearchSuggestion>() {
            @Override
            public void onResponse(Call<SearchSuggestion> call, Response<SearchSuggestion> response) {
                progressBar.setVisibility(View.GONE);
                if(response.isSuccessful() && response.code() == 200){
                    searchItems = response.body().getResponse();
                    for(int i=0; i<searchItems.size(); i++) {
                        Log.d("SearchSugg : "+i, " - - " +searchItems.get(i).getData());
                    }
                    if(searchItems.size()>0){
                        setSearchAdapter();
                        empty_text.setVisibility(View.GONE);
                        layout_tags.setVisibility(View.GONE);
                        recyclerView_Search.setVisibility(View.VISIBLE);
                        recyclerView.setVisibility(View.GONE);
                        layout_sort.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onFailure(Call<SearchSuggestion> call, Throwable t) {
                Log.e(TAG, t.toString());
                progressBar.setVisibility(View.GONE);
                empty_text.setVisibility(View.VISIBLE);
                recyclerView_Search.setVisibility(View.GONE);
                recyclerView.setVisibility(View.GONE);
                layout_sort.setVisibility(View.GONE);
            }
        });

    }

    public void loadTrendSearch(){
        empty_text.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);

        Call<SearchSuggestion> call = apiService.getTrendSearch();

        call.enqueue(new Callback<SearchSuggestion>() {
            @Override
            public void onResponse(Call<SearchSuggestion> call, Response<SearchSuggestion> response) {
                progressBar.setVisibility(View.GONE);
                if(response.isSuccessful() && response.code() == 200){
                    searchTrendItems = response.body().getResponse();
                    for(int i=0; i<searchTrendItems.size(); i++) {
                        Log.d("SearchSugg : "+i, " - - " +searchTrendItems.get(i).getData());
                    }
                    if(searchTrendItems.size()>0){
                        empty_text.setVisibility(View.GONE);
                        layout_tags.setVisibility(View.VISIBLE);
                        recyclerView_Search.setVisibility(View.GONE);
                        recyclerView.setVisibility(View.GONE);
                        layout_sort.setVisibility(View.GONE);
                        setTags();
                    }
                }
            }

            @Override
            public void onFailure(Call<SearchSuggestion> call, Throwable t) {
                Log.e(TAG, t.toString());
                progressBar.setVisibility(View.GONE);
                empty_text.setVisibility(View.VISIBLE);
                recyclerView_Search.setVisibility(View.GONE);
                recyclerView.setVisibility(View.GONE);
                layout_sort.setVisibility(View.GONE);
            }
        });

    }

    private void loadProductData(String str_search) {

        empty_text.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
        layout_sort.setVisibility(View.GONE);

        Map<String, String> data = new HashMap<>();
        data.put("search_data", str_search);
        data.put("device_id", Constant.getDeviceId(this));
        if(!selectedSort.equals("popular")) {
            data.put(selectedSort, "");
        }
        Call<SearchModel> call = apiService.getSearchProduct(data);
        call.enqueue(new Callback<SearchModel>() {
            @Override
            public void onResponse(Call<SearchModel> call, Response<SearchModel> response) {
                progressBar.setVisibility(View.GONE);
                products = new ArrayList<>();
                SearchModel sliderRequest = response.body();

                if (sliderRequest != null) {
                    products = sliderRequest.getProduct();
                }
                if (products != null && products.size() > 0) {
                    if(productDealAdapter == null) {
                        setProductData();
                    } else {
                        productDealAdapter.sortData(products);
                    }
                    searchView.setText(str_search);
                    searchView.clearFocus();
                    empty_text.setVisibility(View.GONE);
                    layout_tags.setVisibility(View.GONE);
                    recyclerView_Search.setVisibility(View.GONE);
                    layout_sort.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.VISIBLE);
                } else {
                    recyclerView_Search.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.GONE);
                    layout_sort.setVisibility(View.GONE);
                    empty_text.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call<SearchModel> call, Throwable t) {
                // Log error here since request failed
                Log.e(TAG, t.toString());
                progressBar.setVisibility(View.GONE);
                empty_text.setVisibility(View.VISIBLE);
                layout_sort.setVisibility(View.GONE);
                recyclerView_Search.setVisibility(View.GONE);
                recyclerView.setVisibility(View.GONE);
            }
        });
    }

    private void setProductData() {
        productDealAdapter = new ProductDealAdapter(this, products, false);
        recyclerView.setAdapter(productDealAdapter);
    }

    private void setSearchAdapter(){
        SearchSuggestionAdapter searchSuggestionAdapter = new SearchSuggestionAdapter(this, searchItems,
                SearchActivity.this, SearchActivity.this);
        recyclerView_Search.setAdapter(searchSuggestionAdapter);
    }

    private void init() {
        findViewById(R.id.img_home_search).setVisibility(View.GONE);
        title_text = findViewById(R.id.title_text);
        back_btn = findViewById(R.id.back_btn);
        imgSearchCart = findViewById(R.id.img_home_cart);
        imgSeachNoti = findViewById(R.id.img_home_noti);
        imgSearchWishlist = findViewById(R.id.img_home_wishlist);
        back_btn.setVisibility(View.VISIBLE);
        back_btn.setOnClickListener(this);
        imgSearchCart.setOnClickListener(this);
        imgSeachNoti.setOnClickListener(this);
        imgSearchWishlist.setOnClickListener(this);
        empty_text = findViewById(R.id.empty_text);
        progressBar = findViewById(R.id.progressBar);
        recyclerView_Search = findViewById(R.id.recyclerView_Search);
        recyclerView = findViewById(R.id.recyclerView);
        layout_tags = findViewById(R.id.layout_tags);

        layout_sort = findViewById(R.id.layout_sort);
        txtSelectedSort = findViewById(R.id.txt_selected_sort);

        //recyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        recyclerView.setLayoutManager(new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL));
        recyclerView_Search.setLayoutManager(new LinearLayoutManager(this));

        searchView = findViewById(R.id.searchView);
        searchView.requestFocus();

        searchView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(searchView.hasFocus() && !searchView.getText().toString().isEmpty() &&
                        searchView.getText().toString().length() > 2) {
                    loadSearchSuggestions(searchView.getText().toString());
                } else {
                    empty_text.setVisibility(View.VISIBLE);
                    recyclerView_Search.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.GONE);
                    layout_sort.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });
        searchView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    loadProductData(searchView.getText().toString());
                    hideKeyboard();
                    return true;
                }
                return false;
            }
        });

    }

    @Override
    public void onClick(View view) {
        if (view == back_btn) {
            finish();
        }
        if (view == imgSearchCart){
            Intent intent = new Intent(this, CartActivity.class);
            startActivity(intent);
        }
        if (view == imgSeachNoti){

        }
        if (view == imgSearchWishlist){
            Intent intent = new Intent(this, WishListActivity.class);
            startActivity(intent);
        }
    }

    public void onSortClick(View view){
        showSortDialog();
    }

    AlertDialog alertDialog;
    private void showSortDialog() {
        final AlertDialog.Builder alert = new AlertDialog.Builder(SearchActivity.this,
                R.style.AppThemeDialogAlert);
        View mView = getLayoutInflater().inflate(R.layout.popup_sort, null);
        RadioGroup rg_sort = mView.findViewById(R.id.group_product_condition);
        RadioButton rb_popular = mView.findViewById(R.id.rb_sort_popular);
        RadioButton rb_latest = mView.findViewById(R.id.rb_sort_latest);
        RadioButton rb_lowhigh = mView.findViewById(R.id.rb_sort_lowtohigh);
        RadioButton rb_hightlow = mView.findViewById(R.id.rb_sort_hightolow);

        rb_latest.setChecked(false);
        rb_lowhigh.setChecked(false);
        rb_hightlow.setChecked(false);

        alert.setView(mView);
        alertDialog = alert.create();
        alertDialog.setCanceledOnTouchOutside(true);

        Log.d("SortClick Collection: "," - - Here  - "+selectedSort);
        if(selectedSort.equalsIgnoreCase("latest-arrival")){
            rb_latest.setChecked(true);
        }else if (selectedSort.equalsIgnoreCase("low-to-high")){
            rb_lowhigh.setChecked(true);
        } else if (selectedSort.equalsIgnoreCase("high-to-low")){
            rb_hightlow.setChecked(true);
        } else if (selectedSort.equalsIgnoreCase("popular")){
            rb_popular.setChecked(true);
        }
        rg_sort.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton selectedRadioButton = mView.findViewById(checkedId);
                if(selectedRadioButton.getText().toString().contains("Latest Arrivals")) {
                    selectedSort = "latest-arrival";
                } else if (selectedRadioButton.getText().toString().contains("Low - High")){
                    selectedSort = "low-to-high";
                } else if (selectedRadioButton.getText().toString().contains("High - Low")){
                    selectedSort = "high-to-low";
                } else  if (selectedRadioButton.getText().toString().contains("Popular")){
                    selectedSort = "popular";
                }
                Log.d("SortClick Collection: "," - - Here  - "+selectedSort);
                txtSelectedSort.setText(selectedRadioButton.getText());
                alertDialog.dismiss();
                loadProductData(searchView.getText().toString());

            }
        });
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.show();

    }

    private void hideKeyboard() {
        searchView.setText("");
        searchView.clearFocus();
        InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(searchView.getWindowToken(), 0);
    }

    @Override
    public void onItemSuggestClick(int position) {
        searchView.setText(searchItems.get(position).getData());
    }

    @Override
    public void onItemClicked(int position) {
        hideKeyboard();
        loadProductData(searchItems.get(position).getData());
    }
}