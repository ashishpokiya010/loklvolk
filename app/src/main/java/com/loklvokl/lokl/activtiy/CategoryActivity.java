package com.loklvokl.lokl.activtiy;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.material.tabs.TabLayout;
import com.loklvokl.lokl.R;
import com.loklvokl.lokl.adapter.AllCategoryAdapter;
import com.loklvokl.lokl.adapter.SliderAdapter;
import com.loklvokl.lokl.model.BannerInterface;
import com.loklvokl.lokl.model.CatItemInterface;
import com.loklvokl.lokl.model.CatSlider;
import com.loklvokl.lokl.model.Category;
import com.loklvokl.lokl.model.CategoryModel;
import com.loklvokl.lokl.model.Homeslider;
import com.loklvokl.lokl.model.MainCategory;
import com.loklvokl.lokl.model.SubCategory;
import com.loklvokl.lokl.util.ApiClient;
import com.loklvokl.lokl.util.ApiInterface;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CategoryActivity extends BaseActivity implements BannerInterface, CatItemInterface {

    private static final String TAG = "Category";
    ProgressBar progressBar;
    ApiInterface apiService;
    CategoryModel categoryModel;
    LinearLayout tab_layout;
    RecyclerView recycle_view;
    GridLayoutManager layoutManagerProductDeal;
    TextView empty_text;
    RelativeLayout pager_layout;
    ViewPager viewPager;
    TabLayout indicator;
    ImageView imgBack, imgCatSearch, imgCatWishlist, imgCatCart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);

        apiService = ApiClient.getClient().create(ApiInterface.class);

        init();
        loadData();

    }

    private void init() {
        empty_text = findViewById(R.id.empty_text);
        progressBar = findViewById(R.id.progressBar);
        tab_layout = findViewById(R.id.tab_layout);
        recycle_view = findViewById(R.id.recycle_view);
        recycle_view.setNestedScrollingEnabled(false);
        pager_layout = findViewById(R.id.pager_layout);
        indicator = findViewById(R.id.indicator);
        viewPager = findViewById(R.id.viewPager);

        imgBack = findViewById(R.id.back_btn);
        imgCatCart = findViewById(R.id.img_category_cart);
        imgCatWishlist = findViewById(R.id.img_category_wishlist);
        imgCatWishlist.setVisibility(View.GONE);

        imgCatSearch = findViewById(R.id.img_category_search);
        imgCatSearch.setVisibility(View.GONE);
        imgCatSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(CategoryActivity.this, SearchActivity.class);
                startActivity(intent);
            }
        });
        imgCatWishlist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(CategoryActivity.this, WishListActivity.class);
                startActivity(intent);
            }
        });

        imgCatCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CategoryActivity.this, CartActivity.class);
                startActivity(intent);
            }
        });
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        imgCatWishlist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CategoryActivity.this, CartActivity.class);
                startActivity(intent);
            }
        });
    }

    private void loadData() {

        progressBar.setVisibility(View.VISIBLE);
        Call<CategoryModel> call = apiService.getAllCategoryData();
        call.enqueue(new Callback<CategoryModel>() {
            @Override
            public void onResponse(Call<CategoryModel> call, Response<CategoryModel> response) {
                Log.d(TAG, "data: " + response.body());
                progressBar.setVisibility(View.GONE);

                categoryModel = response.body();

                setupTabLayout();
                setCategoryData(categoryModel.getMainCategory().get(0).getMainCatId());
            }

            @Override
            public void onFailure(Call<CategoryModel> call, Throwable t) {
                // Log error here since request failed
                Log.e(TAG, t.toString());
            }
        });
    }

    MainCategory mainCategoryTemp;
    List<SubCategory> products = new ArrayList<>();
    AllCategoryAdapter productDealAdapter;

    private void setCategoryData(String mainId) {

        products.clear();
        for (int i = 0; i < categoryModel.getMainCategory().size(); i++) {
            MainCategory mainCategory = categoryModel.getMainCategory().get(i);
            if (mainCategory.getMainCatId().equals(mainId)) {
                mainCategoryTemp = mainCategory;

                if (mainCategory.getCategory() != null && mainCategory.getCategory().size() > 0) {
                    setupSliderBanner(mainCategory.getCatSlider());
                    for (Category category : mainCategory.getCategory()) {
                        SubCategory product = new SubCategory();
                        product.setId("0");
                        product.setSubCatId(category.getCatId());
                        product.setSubCatName(category.getCatName());
                        product.setImage(category.getImage());

                        products.add(product);
//                        products.addAll(category.getSubCategory());
                        for (int k = 0; k < (Math.min(category.getSubCategory().size(), 6)); k++) {
                            products.add(category.getSubCategory().get(k));
                        }
                    }
                    empty_text.setVisibility(View.GONE);
                } else {
                    empty_text.setVisibility(View.VISIBLE);
                }
                textViewList.get(i).setBackgroundResource(R.drawable.category_tab_selector);
            } else {
                textViewList.get(i).setBackgroundResource(R.drawable.category_tab_unselector);
            }
        }
        productDealAdapter = new AllCategoryAdapter(this, products);
        layoutManagerProductDeal = new GridLayoutManager(this, 3);
        layoutManagerProductDeal.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                if ("0".equals(products.get(position).getId())) {
                    return 3;
                }
                return 1;
            }
        });
        recycle_view.setLayoutManager(layoutManagerProductDeal);
        recycle_view.setAdapter(productDealAdapter);
    }

    List<TextView> textViewList = new ArrayList<>();

    private void setupTabLayout() {
        for (MainCategory mainCategory : categoryModel.getMainCategory()) {
            LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View rowView = inflater.inflate(R.layout.main_category_item, null);
            TextView title_text = rowView.findViewById(R.id.title_text);
            title_text.setText(mainCategory.getMainCatName());
            title_text.setTag(mainCategory.getMainCatId());
            title_text.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String mainId = (String) view.getTag();
                    setCategoryData(mainId);
                }
            });
            textViewList.add(title_text);
            tab_layout.addView(rowView);
        }
    }

    private void updateTextBg(View view) {
        for (TextView textView : textViewList) {
            textView.setBackgroundResource(R.drawable.category_tab_unselector);
        }
        view.setBackgroundResource(R.drawable.category_tab_selector);
    }


    private void setupSliderBanner(List<CatSlider> catSlider) {
        List<Homeslider> homesliders = new ArrayList<>();
        if (catSlider != null && catSlider.size() > 0) {
            for (CatSlider catSlider1 : catSlider) {
                homesliders.add(new Homeslider(catSlider1.getCatSliderId(), catSlider1.getCatSliderImage()));
            }
            pager_layout.setVisibility(View.VISIBLE);
            viewPager.setAdapter(new SliderAdapter(this, homesliders));
            indicator.setupWithViewPager(viewPager, true);

        } else {
            pager_layout.setVisibility(View.GONE);
        }

    }

    @Override
    public void onBackPressed(){
        Intent intent = new Intent(CategoryActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
        super.onBackPressed();
    }

    @Override
    public void bannerClick(String id) {
        Intent intent = new Intent(this, CategoryProductActivity.class);
        intent.putExtra("ID", id);
        intent.putExtra("TITLE", getString(R.string.app_name));
        intent.putExtra("ISBANNER", true);
        startActivity(intent);
    }

    @Override
    public void ItemClick(int adapterPosition, String id, String mainCatName) {
        Intent intent = new Intent(this, CategoryProductActivity.class);
        intent.putExtra("ID", id);
        intent.putExtra("TITLE", mainCatName);
        intent.putExtra("ISBANNER", false);
        startActivity(intent);
    }

    @Override
    public void SeeAllClick(int adapterPosition, String cat_id, String mainCatName) {
        productsSeeAll = findSubCAtArray(cat_id);
//        Intent intent = new Intent(this, SubCategoryActivity.class);
//        intent.putExtra("ID", cat_id);
//        intent.putExtra("TITLE", mainCatName);
//        intent.putExtra("SUBCATEGORY", new Gson().toJson(productsSeeAll));
//        startActivity(intent);

        int proSize = productsSeeAll.size();
        Log.e("log_", "size>" + proSize);

        for (int i = 0; i < products.size() && proSize > 6; i++) {

            if (products.get(i).getSubCatId().equals(productsSeeAll.get(6 - 1).getSubCatId())) {

                if ((i + 1) < products.size() && !products.get(i + 1).getSubCatId().equals(productsSeeAll.get(6).getSubCatId())) {
                    for (int j = proSize; j > 6; j--) {
                        products.add(i + 1, productsSeeAll.get(j - 1));
                        productDealAdapter.notifyItemInserted(i + 1);
                    }
                    break;
                }
            }
        }
    }

    private List<SubCategory> productsSeeAll = new ArrayList<>();

    private List<SubCategory> findSubCAtArray(String cat_id) {
        if (mainCategoryTemp.getCategory() != null && mainCategoryTemp.getCategory().size() > 0) {
            for (Category category : mainCategoryTemp.getCategory()) {
                if (category.getCatId().equalsIgnoreCase(cat_id)) {
                    return category.getSubCategory();
                }
            }
        }
        return productsSeeAll;
    }


}