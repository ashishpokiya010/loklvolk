package com.loklvokl.lokl.activtiy;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loklvokl.lokl.R;
import com.loklvokl.lokl.adapter.SizeAdapter;
import com.loklvokl.lokl.adapter.SizeUpdateAdapter;
import com.loklvokl.lokl.model.Courier;
import com.loklvokl.lokl.model.ItemInterface;
import com.loklvokl.lokl.model.SizeDetail;
import com.loklvokl.lokl.model.product.ProductSize;
import com.loklvokl.lokl.model.product.UpdateStockModel;
import com.loklvokl.lokl.model.product.UserProduct;
import com.loklvokl.lokl.model.sizesbycategory.SizeDetailsItem;
import com.loklvokl.lokl.model.sizesbycategory.SizesResponse;
import com.loklvokl.lokl.util.ApiClient;
import com.loklvokl.lokl.util.ApiInterface;
import com.loklvokl.lokl.util.Constant;
import com.loklvokl.lokl.util.SharedPref;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UpdateStockDetailActivity extends BaseActivity implements View.OnClickListener, ItemInterface {

    SharedPref sharedPref;
    ApiInterface apiService;
    ApiClient apiClient;
    Gson gson;

    RecyclerView recycleSize;
    ImageView back_btn, imageView;
    TextView title_text, text_name, textViewColor, textViewSize;
    ProgressBar progressBar;
    LinearLayout list_layout, bottomLayout1,sizeLayout;
    String cat_id, user_product_id, image, color, size, name, userId, mrp, selling_price, stock;
    RelativeLayout btnCourier;
    ImageView imgDetailNoti, imgDetailCart, imgDetailWishlist;
    SizeUpdateAdapter sizeAdapter;
    private ArrayList<Courier> couriersDataArrayList;
    public List<com.loklvokl.lokl.model.product.ProductSize> sizeSelectedArrayList;
    public List<SizeDetailsItem> sizeAllArrayList;
    private UserProduct userProduct;
    String size_id;
    //TextView txtCourierName;
    EditText edit_other_selling_price, edit_other_market_price, edit_other_stock;

    TextInputLayout text_input_layout_stock, text_input_layout_market_price, text_input_layout_edit_stock;

    Button btn_confirm;
    //String courier_id = "", track_number = "", other_courier_name = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.upadate_stock_detail_activity);

        sharedPref = new SharedPref(this);
        apiClient = new ApiClient();
        apiService = ApiClient.getClient().create(ApiInterface.class);
        gson = new Gson();
        listSelctSize = new HashMap<>();

        if (getIntent() != null) {

            userId = sharedPref.getStr("ID");
            //userId = getIntent().getStringExtra("UserId");
            user_product_id = getIntent().getStringExtra("user_product_id");
            image = getIntent().getStringExtra("image");
            color = getIntent().getStringExtra("color");
            size = getIntent().getStringExtra("size");
            name = getIntent().getStringExtra("name");
            selling_price = getIntent().getStringExtra("selling_price");
            stock = getIntent().getStringExtra("stock");
            mrp = getIntent().getStringExtra("mrp");
            String sizeListString = getIntent().getStringExtra("size_list");
            if(sizeListString!=null && !sizeListString.isEmpty()) {
                userProduct = gson.fromJson(sizeListString, new TypeToken<UserProduct>(){}.getType());
                sizeSelectedArrayList = userProduct.getProductSize();
                cat_id = userProduct.getSubCatId();
                for(int i=0; i<sizeSelectedArrayList.size(); i++){
                    Log.d("Sizes String Selected: ",""+sizeSelectedArrayList.get(i).getSize());
                }
            }
        }

        init();
        getSizes(cat_id);
    }

    private void init() {
        title_text = findViewById(R.id.title_text);
        back_btn = findViewById(R.id.back_btn);
        back_btn.setOnClickListener(this);

        progressBar = findViewById(R.id.progressBar);
        list_layout = findViewById(R.id.list_layout);
        imageView = findViewById(R.id.imageView);

        sizeLayout = findViewById(R.id.sizeLayout);
        recycleSize = findViewById(R.id.recycleSize);

        text_name = findViewById(R.id.text_name);
        textViewColor = findViewById(R.id.textcolor);
        textViewSize = findViewById(R.id.text_size);

        imgDetailCart = findViewById(R.id.img_detail_cart);
        imgDetailNoti = findViewById(R.id.img_detail_noti);
        btnCourier = findViewById(R.id.btnCourier);
        edit_other_selling_price = findViewById(R.id.edit_other_selling_price);
        edit_other_market_price = findViewById(R.id.edit_other_market_price);
        edit_other_stock = findViewById(R.id.edit_other_stock);

        text_input_layout_stock = findViewById(R.id.text_input_layout_stock);
        text_input_layout_market_price = findViewById(R.id.text_input_layout_market_price);
        text_input_layout_stock = findViewById(R.id.text_input_layout_stock);

        LinearLayoutManager horizontalLayoutManagaerSize = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        recycleSize.setLayoutManager(horizontalLayoutManagaerSize);

        btn_confirm = findViewById(R.id.btn_confirm);
        bottomLayout1 = findViewById(R.id.bottomLayout1);
        imgDetailWishlist = findViewById(R.id.img_detail_wishlist);

        imgDetailWishlist.setVisibility(View.GONE);
        imgDetailCart.setVisibility(View.GONE);
        imgDetailNoti.setVisibility(View.GONE);
        progressBar.setVisibility(View.GONE);
        btn_confirm.setOnClickListener(this);

        setData();
    }


    public void setData() {
        if (image != null) {
            Picasso.get().load("https://" + image)
                    .resize(0, 250)
                    .into(imageView, new com.squareup.picasso.Callback() {
                        @Override
                        public void onSuccess() {
                        }

                        @Override
                        public void onError(Exception e) {
                        }
                    });
        }


        System.out.println("SIZE" + size);
        System.out.println("COLOR" + size);

        title_text.setText("Update Product Details");
        text_name.setText(name);
        edit_other_selling_price.setText(selling_price);
        edit_other_market_price.setText(mrp);
        edit_other_stock.setText(stock);

        if (color != null) {
            textViewColor.setText("Color:" + color);
        }
        if (size != null) {
            textViewSize.setText(size);
            if(!size.equalsIgnoreCase("Free Size")){
                textViewSize.setVisibility(View.GONE);
            }
        }

        /*if(sizeArrayList!=null && sizeArrayList.size()>0 && !size.equalsIgnoreCase("Free Size")){
            sizeAdapter = new SizeUpdateAdapter(sizeArrayList, this);
            recycleSize.setAdapter(sizeAdapter);
            sizeLayout.setVisibility(View.VISIBLE);
        } else {
            sizeLayout.setVisibility(View.GONE);
        }*/

    }

    @Override
    public void onClick(View view) {
        if (view == btnCourier) {

        }
        if (view == back_btn) {
            finish();
        }
        if (view == btn_confirm) {

            if (edit_other_selling_price.getText().toString().isEmpty()) {
                Toast.makeText(this, "Enter selling price", Toast.LENGTH_SHORT).show();

            } else if (edit_other_market_price.getText().toString().isEmpty()) {
                Toast.makeText(this, "Enter market price", Toast.LENGTH_SHORT).show();

            } else if (edit_other_stock.getText().toString().isEmpty()) {
                Toast.makeText(this, "Enter Stock", Toast.LENGTH_SHORT).show();

            } else if (Integer.parseInt(edit_other_market_price.getText().toString()) < Integer.parseInt(edit_other_selling_price.getText().toString())) {
                Toast.makeText(this, "Selling price is not more than Market price!!", Toast.LENGTH_SHORT).show();

            } else if(sizeAllArrayList.size() <=1 && listSelctSize.containsValue(false)){
                Toast.makeText(this, "Select available size", Toast.LENGTH_SHORT).show();

            } else if(sizeAllArrayList.size() > 1 && Collections.frequency(listSelctSize.values(), false) == listSelctSize.size()){
                Toast.makeText(this, "Select available size", Toast.LENGTH_SHORT).show();
            } else {
                if (Integer.parseInt(edit_other_market_price.getText().toString()) < 149) {
                    Toast.makeText(this, "Minimum market price is 149", Toast.LENGTH_SHORT).show();

                } else if (Integer.parseInt(edit_other_selling_price.getText().toString()) < 149) {
                    Toast.makeText(this, "Minimum selling price is 149", Toast.LENGTH_SHORT).show();

                }else {
                    loadDAta();
                }
            }
        }
    }

    private void getSizes(String cat_id) {

        progressBar.setVisibility(View.VISIBLE);
        apiService = apiClient.getClient().create(ApiInterface.class);
        Call<SizesResponse> call = apiService.getStockSize(cat_id);

        apiClient.enqueue(call, new Callback<SizesResponse>() {
            @Override
            public void onResponse(Call<SizesResponse> call, Response<SizesResponse> response) {
                progressBar.setVisibility(View.GONE);

                if (response.isSuccessful()) {
                    String responseData = response.body().getSizeDetails().get(0).getSize();
                    System.out.println("NULL>>>" + response.body().getSizeDetails().get(0).getSize());

                    if (responseData != null) {
                        sizeAllArrayList = response.body().getSizeDetails();
                        if (sizeAllArrayList != null && sizeAllArrayList.size() > 0) {
                            for (ProductSize item : sizeSelectedArrayList) {
                                for(int j=0; j<sizeAllArrayList.size(); j++) {
                                    if (item.getSize().equals(sizeAllArrayList.get(j).getSize())) {
                                        Log.d("Sizes String get: ",""+sizeAllArrayList.get(j).getSize()+ " - "+item.getSize());
                                        listSelctSize.put(j,true);
                                    }
                                }
                            }
                            sizeAdapter = new SizeUpdateAdapter(sizeSelectedArrayList, sizeAllArrayList,
                                    UpdateStockDetailActivity.this);
                            recycleSize.setAdapter(sizeAdapter);
                            sizeLayout.setVisibility(View.VISIBLE);
                        } else {
                            sizeLayout.setVisibility(View.GONE);
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<SizesResponse> call, Throwable t) {

            }
        });
    }

    private void loadDAta() {

        //list_layout.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
        StringBuilder size0 = new StringBuilder();
        String size1;
        if(listSelctSize != null && !listSelctSize.isEmpty()) {
            for (Integer key : listSelctSize.keySet()) {
                if (listSelctSize.get(key).equals(true)) {
                    size0.append(",").append(sizeAllArrayList.get(key).getSize());
                }
            }
            size1 = size0.substring(1);
        } else {
            size0.append(",").append("Free Size");
            size1 = size0.substring(1);
        }
        apiService = apiClient.getClient().create(ApiInterface.class);
        Call<UpdateStockModel> call = apiService.getUpdateStockDetail(user_product_id, edit_other_selling_price.getText().toString(),
                edit_other_market_price.getText().toString(), edit_other_stock.getText().toString(), size1);

        apiClient.enqueue(call, new Callback<UpdateStockModel>() {
            @Override
            public void onResponse(Call<UpdateStockModel> call, Response<UpdateStockModel> response) {
                progressBar.setVisibility(View.GONE);

                if (response.isSuccessful()) {
                    String responseData = response.body().getUserProduct().get(0).getError();
                    System.out.println("NULL>>>" + responseData);
                    UpdateStockModel detailModel = response.body();

                    if (responseData.equalsIgnoreCase("false")) {
                        Constant.toast(getApplicationContext(), response.body().getUserProduct().get(0).getMessage());
                        finish();
                        setResult(RESULT_OK);
                    }
                }
            }

            @Override
            public void onFailure(Call<UpdateStockModel> call, Throwable t) {
                // Log error here since request failed
            }
        });
    }
    public static Map<Integer, Boolean> listSelctSize = new HashMap<>();
    @Override
    public void ItemClick(int adapterPosition, String id, String colorName) {
        if (colorName != null && colorName.equals("")) {
            //listSelctSize.clear();
            Boolean aBoolean = listSelctSize.get(adapterPosition);
            listSelctSize.put(adapterPosition, aBoolean == null || !aBoolean);
            size_id = id;
            Log.d("Sizes array T: "," - - "+listSelctSize.get(adapterPosition)+" - - "+adapterPosition+" - - "+listSelctSize.size());
//          productDetail.setSize(id);
            sizeAdapter.notifyDataSetChanged();
        }
    }
}