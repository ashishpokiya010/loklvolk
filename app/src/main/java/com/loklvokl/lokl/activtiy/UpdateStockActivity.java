package com.loklvokl.lokl.activtiy;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.loklvokl.lokl.R;
import com.loklvokl.lokl.adapter.ExplorerAdapter;
import com.loklvokl.lokl.adapter.UpdateStockAdapter;
import com.loklvokl.lokl.model.commonresponse.CommonResponse;
import com.loklvokl.lokl.model.product.UpdateStockModel;
import com.loklvokl.lokl.model.product.UserProduct;
import com.loklvokl.lokl.util.ApiClient;
import com.loklvokl.lokl.util.ApiInterface;
import com.loklvokl.lokl.util.Constant;
import com.loklvokl.lokl.util.SharedPref;

import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UpdateStockActivity extends BaseActivity implements View.OnClickListener, UpdateStockAdapter.onClick {

    ImageView back_btn;
    TextView title_text;
    ProgressBar progressBar;
    ApiClient apiClient;
    SharedPref sharedPref;
    ApiInterface apiService;
    RecyclerView recycle_view;
    UpdateStockAdapter updateStockAdapter;
    //EditText searchView;
    List<UserProduct> updateStocklist;
    LinearLayout list_layout, no_data_txt;
    ImageView imgDetailCart, imgDetailWishlist;
    //RelativeLayout cart_layout, wishlist_layout;
    public static String userId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.update_stock_product_activity);

        sharedPref = new SharedPref(this);
        apiClient = new ApiClient();
        apiService = ApiClient.getClient().create(ApiInterface.class);
        userId = sharedPref.getStr("ID");


        init();
        loadDAta();
    }

    private void init() {
        findViewById(R.id.img_detail_noti).setVisibility(View.GONE);
        title_text = findViewById(R.id.title_text);
        title_text.setText("Update Stock/Product");
        back_btn = findViewById(R.id.back_btn);
        back_btn.setOnClickListener(this);
        progressBar = findViewById(R.id.progressBar);
        // searchView = findViewById(R.id.searchView);
        recycle_view = findViewById(R.id.recycle_view);
        list_layout = findViewById(R.id.list_layout);
        no_data_txt = findViewById(R.id.list_no_data_found);
        imgDetailCart = findViewById(R.id.img_detail_cart);
        imgDetailCart.setOnClickListener(this);

        imgDetailWishlist = findViewById(R.id.img_detail_wishlist);
        imgDetailWishlist.setVisibility(View.GONE);

        recycle_view.setLayoutManager(new GridLayoutManager(this, 1));

    }

    private void setUpdateStockAdapter() {
        if (updateStocklist != null) {
            updateStockAdapter = new UpdateStockAdapter(UpdateStockActivity.this, updateStocklist, UpdateStockActivity.this);
            recycle_view.setAdapter(updateStockAdapter);
        }
    }

    private void loadDAta() {
        progressBar.setVisibility(View.VISIBLE);
        updateStocklist = new ArrayList<>();
        apiService = apiClient.getClient().create(ApiInterface.class);
        //Call<UpdateStockModel> call = apiService.getUpdateStockProduct("124");
        Call<UpdateStockModel> call = apiService.getUpdateStockProduct(sharedPref.getStr("ID"));

        apiClient.enqueue(call, new Callback<UpdateStockModel>() {
            @Override
            public void onResponse(Call<UpdateStockModel> call, Response<UpdateStockModel> response) {
                progressBar.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    String responseData = response.body().getUserProduct().get(0).getError();
                    System.out.println("NULL>>>" + responseData + " - - " + sharedPref.getStr("ID"));
                    if (responseData == null) {
                        no_data_txt.setVisibility(View.GONE);
                        list_layout.setVisibility(View.VISIBLE);
                        UpdateStockModel detailModel = response.body();
                        updateStocklist.addAll(detailModel.getUserProduct());
                        setUpdateStockAdapter();
                    } else {
                        Constant.toast(getApplicationContext(), response.body().getUserProduct().get(0).getMessage());
                        no_data_txt.setVisibility(View.VISIBLE);
                        list_layout.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onFailure(Call<UpdateStockModel> call, Throwable t) {
                // Log error here since request failed
            }
        });
    }


    @Override
    public void onClick(View view) {
        if (view == back_btn) {
            finish();
        }
        if (view == imgDetailCart){
            Intent intent = new Intent(this, CartActivity.class);
            startActivity(intent);
        }
    }

    /*@Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        System.out.println("Callaaa" + requestCode);
        if (requestCode == 111) {
            loadDAta();
        }
    }*/

    @Override
    public void onResume() {
        super.onResume();
        if (updateStocklist != null && !updateStocklist.isEmpty()) {
            clear();
        }
    }

    public void clear() {
        Log.d("Clear : ", " - - " + updateStocklist.size());
        updateStocklist.clear();
        updateStockAdapter.notifyDataSetChanged();
        loadDAta();
    }

    @Override
    public void onClick(UserProduct product, int position) {

        Gson gson = new Gson();

        Intent intent = new Intent(this, UpdateStockDetailActivity.class);
        intent.putExtra("user_product_id", product.getProductId());
        intent.putExtra("image", product.getProductImage());
        intent.putExtra("name", product.getProductName());
        intent.putExtra("selling_price", product.getSellingPrice());
        intent.putExtra("mrp", product.getMrp());
        intent.putExtra("stock", product.getStock());

        if (product.getProductColor().get(0).getColor() != null) {
            intent.putExtra("color", product.getProductColor().get(0).getColor().toString());
        }
        if(product.getProductSize().size()>1){
            intent.putExtra("size", product.getProductSize().get(1).getSize());
        } else {
            intent.putExtra("size", product.getProductSize().get(0).getSize());
        }
        if (updateStocklist.get(position).getProductSize() != null && !updateStocklist.get(position).getProductSize().isEmpty()) {
            String jsonUpdateStockList = gson.toJson(updateStocklist.get(position));
            intent.putExtra("size_list", jsonUpdateStockList);
        }
        startActivityForResult(intent, 111);

    }

    private void deleteProduct(UserProduct userProduct, int position) {
        progressBar.setVisibility(View.VISIBLE);
        apiService = apiClient.getClient().create(ApiInterface.class);

        Call<UpdateStockModel> call1 = apiService.deleteStockProduct(sharedPref.getStr("ID"),
                userProduct.getProductId());

        call1.enqueue(new Callback<UpdateStockModel>() {
            @Override
            public void onResponse(Call<UpdateStockModel> call, Response<UpdateStockModel> response) {
                progressBar.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    System.out.println("NULL>>>" + response.body().getUserProduct().get(0));
                    if (response.body() == null) {
                        Constant.toast(UpdateStockActivity.this, "Unable to delete product");
                    } else {
                        Constant.toast(UpdateStockActivity.this, response.body().getUserProduct().get(0).getMessage());
                        if (response.body().getUserProduct().get(0).getMessage().contains("success")) {
                            updateStocklist.remove(position);
                            updateStockAdapter.notifyItemRemoved(position);
                            updateStockAdapter.notifyItemRangeChanged(position, updateStocklist.size());
                            if (updateStocklist.isEmpty()) {
                                no_data_txt.setVisibility(View.VISIBLE);
                            }
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<UpdateStockModel> call, Throwable t) {
                Log.d("Failed: ", " - - " + t.getMessage());
            }
        });
    }

    @Override
    public void onDelete(UserProduct userProduct, int position) {
        deleteProduct(userProduct, position);
    }
}