package com.loklvokl.lokl.activtiy;


import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.TabActivity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TabHost;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.loklvokl.lokl.R;
import com.loklvokl.lokl.util.Constant;
import com.loklvokl.lokl.util.SharedPref;

public class MainActivity extends TabActivity {

    AppCompatImageView imageView;
    public static TabHost tabHost;
    SharedPref sharedPref;
    String str_Action = "";
    String productId = "", offerId = "", categoryId = "", categoryName = "", collectionId = "", collectionName = "", sliderId = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sharedPref = new SharedPref(this);
        tabHost = getTabHost();
        setTabs();

        Bundle extras = getIntent().getExtras();
        if(extras!=null) {
            str_Action = extras.getString("click_action");
            productId = extras.getString("product_id");
            offerId = extras.getString("offer_id");
            categoryId = extras.getString("cat_id");
            categoryName = extras.getString("cat_name");
            collectionId = extras.getString("collection_id");
            collectionName = extras.getString("collection_name");
            sliderId = extras.getString("slider_id");
            Log.d("Notification Payload: "," - - " + " - - "+ extras.toString());
        }


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            // Create channel to show notifications.
            String channelId  = getString(R.string.default_notification_channel_id);
            String channelName = getString(R.string.default_notification_channel_name);
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(new NotificationChannel(channelId,
                    channelName, NotificationManager.IMPORTANCE_LOW));
        }

        // Notification redirection based on click_action
        if(str_Action != null && str_Action.contains(Constant.clickAction.ProductDetailActivity.toString()) &&
                productId != null && !productId.isEmpty()){
            Log.d("Notification Detail: "," - - "+ productId);
            Intent intent = new Intent(MainActivity.this, ProductDetailActivity.class);
            intent.putExtra("ID", productId);
            startActivity(intent);
        }
        else if(str_Action != null && str_Action.contains(Constant.clickAction.OffersActivity.toString()) &&
                offerId != null && !offerId.isEmpty()){
            Log.d("Notification Offer: "," - - "+ offerId);
            Intent intent = new Intent(MainActivity.this, OffersActivity.class);
            intent.putExtra("BANNER_ID", offerId);
            startActivity(intent);
        }
        else if(str_Action != null && str_Action.contains(Constant.clickAction.CategoryProductActivity.toString()) &&
                categoryId != null && !categoryId.isEmpty() && categoryName != null && !categoryName.isEmpty()){
            Log.d("Notification Category: "," - - "+ categoryId +" - - "+categoryName);
            Intent intent = new Intent(MainActivity.this, CategoryProductActivity.class);
            intent.putExtra("ID", categoryId);
            intent.putExtra("TITLE", categoryName);
            intent.putExtra("ISBANNER", false);
            startActivity(intent);
        }
        else if(str_Action != null && str_Action.contains(Constant.clickAction.CollectionProductActivity.toString()) &&
                collectionId != null && !collectionId.isEmpty() && collectionName != null && !collectionName.isEmpty()){
            Log.d("Notification Collect: "," - - "+ collectionId +" - - "+collectionName);
            Intent intent = new Intent(MainActivity.this, CollectionProductActivity.class);
            intent.putExtra("ID", collectionId);
            intent.putExtra("TITLE", collectionName);
            startActivity(intent);
        }
        else if(str_Action != null && str_Action.contains(Constant.clickAction.SliderProductActivity.toString()) &&
                sliderId != null && !sliderId.isEmpty()){
            Log.d("Notification Slider: "," - - "+ sliderId);
            Intent intent = new Intent(this, ProductListActivity.class);
            intent.putExtra("BANNER_ID", sliderId);
            startActivity(intent);
        }
        getFCMToken();
    }

    private void getFCMToken() {
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            return;
                        }

                        // Get the Instance ID token //
                        String token = task.getResult().getToken();
                        sharedPref.setStr("FCM_TOKEN", token);
                        String msg = getString(R.string.msg_token_fmt, token);
                        Log.d("TAG", msg);

                    }
                });
    }

    private void setTabs() {
        addTab("HOME", R.drawable.tab_home, HomeActivity.class);
        addTab("CATEGORY", R.drawable.tab_category, CategoryActivity.class);
        addTab("SELL", R.drawable.tab_sell, AddAdActivity1.class);
        addTab("OFFERS", R.drawable.tab_offers, OffersTabActivity.class);
        addTab("PROFILE", R.drawable.tab_profile, ProfileActivity.class);


        imageView = findViewById(R.id.ibHome);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {

                if (sharedPref.getBool("IS_LOGIN")) {
                    Intent intent = new Intent(MainActivity.this, AddAdActivity1.class);
                    startActivity(intent);
                } else {
                    Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                    startActivity(intent);
                }
            }
        });
    }

    private void addTab(String labelId, int drawableId, Class<?> c) {
        Intent intent = new Intent(MainActivity.this, c);
        TabHost.TabSpec spec = tabHost.newTabSpec("tab" + labelId);

        View tabIndicator = LayoutInflater.from(this).inflate(
                R.layout.tab_indicator, getTabWidget(), false);
        AppCompatTextView title = tabIndicator.findViewById(R.id.title);
        title.setText(labelId);

        AppCompatImageView icon = tabIndicator.findViewById(R.id.icon);
        icon.setImageResource(drawableId);
        spec.setIndicator(tabIndicator);
        spec.setContent(intent);
        tabHost.addTab(spec);

        System.out.println("TAB_IDDD" + labelId);

        if (labelId.equals("SELL")) {
            title.setPadding(0, 0, 0, 0);
        }
    }

    boolean doubleBackToExitPressedOnce = false;

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }

}