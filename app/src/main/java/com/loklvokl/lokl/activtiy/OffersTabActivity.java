package com.loklvokl.lokl.activtiy;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import com.loklvokl.lokl.R;
import com.loklvokl.lokl.adapter.ProductDealAdapter;
import com.loklvokl.lokl.model.OfferProductHome;
import com.loklvokl.lokl.model.OfferRequest;
import com.loklvokl.lokl.model.Product;
import com.loklvokl.lokl.util.ApiClient;
import com.loklvokl.lokl.util.ApiInterface;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OffersTabActivity extends BaseActivity {

    private static final String TAG = "Product_list";
    RecyclerView recyclerView;
    ProgressBar progressBar;
    TextView empty_text;
    ApiInterface apiService;
    List<Product> products = new ArrayList<>();
    ImageView imgOfferSearch, imgOfferWishlist, imgOfferCart;
    GridLayoutManager layoutManagerProductHome;
    StaggeredGridLayoutManager layoutManager;
    RelativeLayout layout_sort;
    TextView txtSelectedSort;
    ProductDealAdapter productDealAdapter;
    String selectedSort = "popular";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offfers);
        apiService = ApiClient.getClient().create(ApiInterface.class);

        init();
        loadProductData();

    }

    private void loadProductData() {
        progressBar.setVisibility(View.VISIBLE);
        layout_sort.setVisibility(View.GONE);

        Map<String, String> data = new HashMap<>();
        if(!selectedSort.equals("popular")) {
            data.put(selectedSort, "");
        }

        Call<OfferProductHome> call = apiService.getOfferProduct(data);
        call.enqueue(new Callback<OfferProductHome>() {
            @Override
            public void onResponse(Call<OfferProductHome> call, Response<OfferProductHome> response) {
                Log.d(TAG, "data: " + response.body());
                progressBar.setVisibility(View.GONE);
                products = new ArrayList<>();
                OfferProductHome sliderRequest = response.body();
                if (sliderRequest != null) {
                    products.addAll(sliderRequest.getOfferRequest().get(0).getProduct());
                }
                if (products != null && products.size() > 0) {
                    empty_text.setVisibility(View.GONE);
                    //layout_sort.setVisibility(View.VISIBLE);
                    if(productDealAdapter == null) {
                        setProductData();
                    } else {
                        productDealAdapter.sortData(products);
                    }
                } else {
                    empty_text.setVisibility(View.VISIBLE);
                    layout_sort.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<OfferProductHome> call, Throwable t) {
                // Log error here since request failed
                Log.e(TAG, t.toString());
                progressBar.setVisibility(View.GONE);
                empty_text.setVisibility(View.VISIBLE);
                layout_sort.setVisibility(View.GONE);
            }
        });
    }

    private void setProductData() {
        productDealAdapter = new ProductDealAdapter(this, products, false);
        recyclerView.setAdapter(productDealAdapter);

    }

    private void init() {
        empty_text = findViewById(R.id.empty_text);
        progressBar = findViewById(R.id.progressBar);
        recyclerView = findViewById(R.id.recyclerView);
        layoutManagerProductHome = new GridLayoutManager(this, 2);
        layoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        layoutManager.setGapStrategy(StaggeredGridLayoutManager.GAP_HANDLING_MOVE_ITEMS_BETWEEN_SPANS);
        recyclerView.setLayoutManager(layoutManager);
        imgOfferSearch = findViewById(R.id.img_offer_search);
        imgOfferWishlist = findViewById(R.id.img_offer_wishlist);
        imgOfferCart = findViewById(R.id.img_offer_cart);

        layout_sort = findViewById(R.id.layout_sort);
        txtSelectedSort = findViewById(R.id.txt_selected_sort);


        imgOfferSearch.setVisibility(View.GONE);
        imgOfferWishlist.setVisibility(View.GONE);
        imgOfferSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(OffersTabActivity.this, SearchActivity.class);
                startActivity(intent);
            }
        });

        imgOfferCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(OffersTabActivity.this, CartActivity.class);
                startActivity(intent);
            }
        });
    }

    public void onSortOfferClick(View view){
        //showSortDialog();
    }

    AlertDialog alertDialog;
    private void showSortDialog() {
        final AlertDialog.Builder alert = new AlertDialog.Builder(OffersTabActivity.this,
                R.style.AppThemeDialogAlert);
        View mView = getLayoutInflater().inflate(R.layout.popup_sort, null);
        RadioGroup rg_sort = mView.findViewById(R.id.group_product_condition);
        RadioButton rb_popular = mView.findViewById(R.id.rb_sort_popular);
        RadioButton rb_latest = mView.findViewById(R.id.rb_sort_latest);
        RadioButton rb_lowhigh = mView.findViewById(R.id.rb_sort_lowtohigh);
        RadioButton rb_hightlow = mView.findViewById(R.id.rb_sort_hightolow);

        rb_latest.setChecked(false);
        rb_lowhigh.setChecked(false);
        rb_hightlow.setChecked(false);

        alert.setView(mView);
        alertDialog = alert.create();
        alertDialog.setCanceledOnTouchOutside(true);

        Log.d("SortClick Collection: "," - - Here  - "+selectedSort);
        if(selectedSort.equalsIgnoreCase("latest-arrival")){
            rb_latest.setChecked(true);
        }else if (selectedSort.equalsIgnoreCase("low-to-high")){
            rb_lowhigh.setChecked(true);
        } else if (selectedSort.equalsIgnoreCase("high-to-low")){
            rb_hightlow.setChecked(true);
        } else if (selectedSort.equalsIgnoreCase("popular")){
            rb_popular.setChecked(true);
        }
        rg_sort.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton selectedRadioButton = mView.findViewById(checkedId);
                if(selectedRadioButton.getText().toString().contains("Latest Arrivals")) {
                    selectedSort = "latest-arrival";
                } else if (selectedRadioButton.getText().toString().contains("Low - High")){
                    selectedSort = "low-to-high";
                } else if (selectedRadioButton.getText().toString().contains("High - Low")){
                    selectedSort = "high-to-low";
                } else  if (selectedRadioButton.getText().toString().contains("Popular")){
                    selectedSort = "popular";
                }
                Log.d("SortClick Collection: "," - - Here  - "+selectedSort);
                txtSelectedSort.setText(selectedRadioButton.getText());
                alertDialog.dismiss();
                loadProductData();

            }
        });
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.show();

    }

    @Override
    public void onBackPressed(){
        Intent intent = new Intent(OffersTabActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
        super.onBackPressed();
    }

}