package com.loklvokl.lokl.activtiy;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.loklvokl.lokl.R;
import com.loklvokl.lokl.adapter.AdCatAdapter;
import com.loklvokl.lokl.adapter.ColorAdapter;
import com.loklvokl.lokl.adapter.SizeAdapter;
import com.loklvokl.lokl.model.AdProductDetail;
import com.loklvokl.lokl.model.ItemInterface;
import com.loklvokl.lokl.model.ProductColor;
import com.loklvokl.lokl.model.ProductColor2;
import com.loklvokl.lokl.model.ProductSize;
import com.loklvokl.lokl.model.SizeDetail;
import com.loklvokl.lokl.util.ApiClient;
import com.loklvokl.lokl.util.ApiInterface;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddAdActivity4 extends AppCompatActivity implements View.OnClickListener, ItemInterface {

    private static final String TAG = "AddAd";
    private static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 10;
    ProgressBar progressBar;
    ApiInterface apiService;
    AdCatAdapter adCatAdapter;
    RecyclerView recycle_view;
    ImageView back_btn;
    TextView title_text, upload_image;
    String subcat_id;
    AdProductDetail adProductDetail;
    RecyclerView recycleColor, recycleSize;
    ColorAdapter colorAdapter;
    SizeAdapter sizeAdapter;
    LinearLayout top_layout, coloLayout, sizeLayout, layout_similar;
    public static Map<Integer, Boolean> listSelct = new HashMap<>();
    public static Map<Integer, Boolean> listSelctSize = new HashMap<>();
    ProgressDialog pd;
    ConstraintLayout main_Layout;
    ImageView img_user, img_user2, img_user3, img_user4;
    List<String> image_paths = new ArrayList<>();
    EditText ed_title, et_product_detail;
    Button btn_next;
    String main_cat, sub_cat1, sub_cat2;
    String image1, image2, image3, image4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_ad4);

        init();
        getINtentData();
        loadData();

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
    }

    private void getINtentData() {
        if (getIntent().hasExtra("TITLE")) {
            title_text.setText(getIntent().getStringExtra("TITLE"));
        }
        if (getIntent().hasExtra("ID")) {
            subcat_id = getIntent().getStringExtra("ID");

        }
        main_cat = getIntent().getStringExtra("MAIN_CAT");
        sub_cat1 = getIntent().getStringExtra("SUB_CAT1");
        sub_cat2 = getIntent().getStringExtra("SUB_CAT2");

        image1 = getIntent().getStringExtra("IMAGE1");
        image2 = getIntent().getStringExtra("IMAGE2");
        image3 = getIntent().getStringExtra("IMAGE3");
        image4 = getIntent().getStringExtra("IMAGE4");
        System.out.println("IIIIII" + image1);
        System.out.println("IIIIII" + image2);


        if (AddAdActivity1.image_bitmap_list.size() == 1) {
            img_user.setImageBitmap(AddAdActivity1.image_bitmap_list.get(0));
        }
        if (AddAdActivity1.image_bitmap_list.size() == 2) {
            img_user.setImageBitmap(AddAdActivity1.image_bitmap_list.get(0));
            img_user2.setImageBitmap(AddAdActivity1.image_bitmap_list.get(1));
        }
        if (AddAdActivity1.image_bitmap_list.size() == 3) {
            img_user.setImageBitmap(AddAdActivity1.image_bitmap_list.get(0));
            img_user2.setImageBitmap(AddAdActivity1.image_bitmap_list.get(1));
            img_user3.setImageBitmap(AddAdActivity1.image_bitmap_list.get(2));
        }
        if (AddAdActivity1.image_bitmap_list.size() == 4) {
            img_user.setImageBitmap(AddAdActivity1.image_bitmap_list.get(0));
            img_user2.setImageBitmap(AddAdActivity1.image_bitmap_list.get(1));
            img_user3.setImageBitmap(AddAdActivity1.image_bitmap_list.get(2));
            img_user4.setImageBitmap(AddAdActivity1.image_bitmap_list.get(3));
        }

        /*if (!image2.isEmpty())
            img_user2.setImageBitmap(BitmapFactory.decodeFile(image2));
        if (!image3.isEmpty())
            img_user3.setImageBitmap(BitmapFactory.decodeFile(image3));
        if (!image4.isEmpty())
            img_user4.setImageBitmap(BitmapFactory.decodeFile(image4));*/
    }

    private void init() {
        listSelct = new HashMap<>();
        listSelctSize = new HashMap<>();
        apiService = ApiClient.getClient().create(ApiInterface.class);
        progressBar = findViewById(R.id.progressBar);
        back_btn = findViewById(R.id.back_btn);
        back_btn.setOnClickListener(this);
        title_text = findViewById(R.id.title_text);
        upload_image = findViewById(R.id.upload_image);
        upload_image.setOnClickListener(this);
        img_user = findViewById(R.id.img_user);
        img_user2 = findViewById(R.id.img_user2);
        img_user3 = findViewById(R.id.img_user3);
        img_user4 = findViewById(R.id.img_user4);
        ed_title = findViewById(R.id.ed_title);
        btn_next = findViewById(R.id.btn_next);
        et_product_detail = findViewById(R.id.et_product_detail);
        btn_next.setOnClickListener(this);

        recycle_view = findViewById(R.id.recycle_view);
        recycle_view.setLayoutManager(new GridLayoutManager(this, 3));

        recycleColor = findViewById(R.id.recycleColor);
        LinearLayoutManager horizontalLayoutManagaer = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        recycleColor.setLayoutManager(horizontalLayoutManagaer);

        recycleSize = findViewById(R.id.recycleSize);
        LinearLayoutManager horizontalLayoutManagaerSize = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        recycleSize.setLayoutManager(horizontalLayoutManagaerSize);
        coloLayout = findViewById(R.id.coloLayout);
        sizeLayout = findViewById(R.id.sizeLayout);
        main_Layout = findViewById(R.id.main_Layout);

    }

    private void loadData() {
        main_Layout.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
        Call<AdProductDetail> call = apiService.performProductDetail(subcat_id);
        call.enqueue(new Callback<AdProductDetail>() {
            @Override
            public void onResponse(Call<AdProductDetail> call, Response<AdProductDetail> response) {
                Log.d(TAG, "data: " + response.body());
                progressBar.setVisibility(View.GONE);
                main_Layout.setVisibility(View.VISIBLE);

                adProductDetail = response.body();
                setDetailData();
            }

            @Override
            public void onFailure(Call<AdProductDetail> call, Throwable t) {
                // Log error here since request failed
                progressBar.setVisibility(View.GONE);
                Log.e(TAG, t.toString());
            }
        });
    }

    private void setDetailData() {

        if (adProductDetail.getSizeDetails() != null && adProductDetail.getSizeDetails().size() > 0) {
            List<ProductSize> dataSet = new ArrayList<>();
            for (SizeDetail productDetailSize : adProductDetail.getSizeDetails()) {
                ProductSize size = new ProductSize();
                size.setSize_value(productDetailSize.getSize());
                dataSet.add(size);
            }
            sizeAdapter = new SizeAdapter(this, dataSet);
            recycleSize.setAdapter(sizeAdapter);
            sizeLayout.setVisibility(View.VISIBLE);
        } else {
            sizeLayout.setVisibility(View.GONE);
        }

        if (adProductDetail.getProductColor() != null && adProductDetail.getProductColor().size() > 0) {
            List<ProductColor> dataSet = new ArrayList<>();
            for (ProductColor2 color2 : adProductDetail.getProductColor()) {
                ProductColor size = new ProductColor();
                size.setColor(color2.getColor());
                size.setCode(color2.getColorCode());
                dataSet.add(size);
            }
            colorAdapter = new ColorAdapter(this, dataSet);
            recycleColor.setAdapter(colorAdapter);
            coloLayout.setVisibility(View.VISIBLE);
        } else {
            coloLayout.setVisibility(View.GONE);
        }
    }

    @Override
    public void onClick(View view) {
        if (view == back_btn) {
            onBackPressed();

        } else if (view == upload_image) {
            if (image_paths.size() < 4) {
                if (checkAndRequestPermissions()) {
                    Intent intent = new Intent(this, ImageDialogActivity.class);
                    startActivityForResult(intent, 111);
                }
            } else {
                Toast.makeText(this, "You can Upload maximum 4 images", Toast.LENGTH_SHORT).show();
            }

        } else if (view == btn_next) {
//            if (image_paths.size() <= 0) {
//                Toast.makeText(this, "Select minimum 1 image", Toast.LENGTH_SHORT).show();
//
//            } else
            if (ed_title.getText().toString().isEmpty()) {
                Toast.makeText(this, "Please enter Ad title", Toast.LENGTH_SHORT).show();

            } else if (listSelctSize.size() <= 0) {
                Toast.makeText(this, "Please select size", Toast.LENGTH_SHORT).show();

            } else if (listSelct.size() <= 0) {
                Toast.makeText(this, "Please select color", Toast.LENGTH_SHORT).show();

            } else {
                StringBuilder size0 = new StringBuilder();
                StringBuilder color0 = new StringBuilder();
                for (Integer key : listSelctSize.keySet()) {
                    size0.append(",").append(adProductDetail.getSizeDetails().get(key).getSize());
                }
                for (Integer key : listSelct.keySet()) {
                    color0.append(",").append(adProductDetail.getProductColor().get(key).getId());
                }
                String size1 = size0.substring(1);
                String color1 = color0.substring(1);

                Intent intent = new Intent(this, AddAdActivity5.class);
//                intent.putExtra("IMAGE1", image_paths.size() > 0 ? image_paths.get(0) : "");
//                intent.putExtra("IMAGE2", image_paths.size() > 1 ? image_paths.get(1) : "");
//                intent.putExtra("IMAGE3", image_paths.size() > 2 ? image_paths.get(2) : "");
//                intent.putExtra("IMAGE4", image_paths.size() > 3 ? image_paths.get(3) : "");
                intent.putExtra("IMAGE1", image1);
                intent.putExtra("IMAGE2", image2);
                intent.putExtra("IMAGE3", image3);
                intent.putExtra("IMAGE4", image4);

                intent.putExtra("AD_TITLE", ed_title.getText().toString());
                intent.putExtra("COLOR_ID", color1);
                intent.putExtra("SIZE_ID", size1);

                intent.putExtra("MAIN_CAT", main_cat);
                intent.putExtra("SUB_CAT1", sub_cat1);
                intent.putExtra("SUB_CAT2", sub_cat2);
                intent.putExtra("product_discription", et_product_detail.getText().toString());
                startActivityForResult(intent, 112);
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        }
    }

    public boolean checkAndRequestPermissions() {
        int ExtstorePermission = ContextCompat.checkSelfPermission(AddAdActivity4.this,
                Manifest.permission.READ_EXTERNAL_STORAGE);
        int cameraPermission = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        List<String> listPermissionsNeeded = new ArrayList<>();
        if (cameraPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA);
        }
        if (ExtstorePermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded
                            .toArray(new String[listPermissionsNeeded.size()]),
                    REQUEST_ID_MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_ID_MULTIPLE_PERMISSIONS:
                if (ContextCompat.checkSelfPermission(this,
                        Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(getApplicationContext(),
                            "Profile image Requires Access to Camara.", Toast.LENGTH_SHORT)
                            .show();
                } else if (ContextCompat.checkSelfPermission(this,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(getApplicationContext(),
                            "Upload image Profile Requires Access to Your Storage.",
                            Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);

    }

    private String size_id = "", color_id = "";

    @Override
    public void ItemClick(int adapterPosition, String id, String colorName) {
        if (colorName != null && colorName.equals("")) {
//            listSelctSize.clear();
            Boolean aBoolean = listSelctSize.get(adapterPosition);
            listSelctSize.put(adapterPosition, aBoolean == null || !aBoolean);
            size_id = id;
//            productDetail.setSize(id);
            sizeAdapter.notifyDataSetChanged();

        } else {
//            listSelct.clear();
            Boolean aBoolean = listSelct.get(adapterPosition);
            listSelct.put(adapterPosition, aBoolean == null || !aBoolean);
            colorAdapter.notifyDataSetChanged();
            color_id = adProductDetail.getProductColor().get(adapterPosition).getId();
            colorAdapter.notifyDataSetChanged();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == 1) {
                File f = new File(Environment.getExternalStorageDirectory().toString());
                for (File temp : f.listFiles()) {
                    if (temp.getName().equals("temp.jpg")) {
                        f = temp;
                        break;
                    }
                }
                try {
                    Bitmap bitmap;
                    BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
                    bitmap = BitmapFactory.decodeFile(f.getAbsolutePath(), bitmapOptions);
//                    bitmap=getResizedBitmap(bitmap, 400);
                    image_paths.add(f.getAbsolutePath());
//                    img_user.setImageBitmap(bitmap);
                    updateImages(image_paths, bitmap);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (requestCode == 2) {
                Uri selectedImage = data.getData();
                String[] filePath = {MediaStore.Images.Media.DATA};
                Cursor c = getContentResolver().query(selectedImage, filePath, null, null, null);
                c.moveToFirst();
                int columnIndex = c.getColumnIndex(filePath[0]);
                String picturePath = c.getString(columnIndex);
                c.close();
                Bitmap bitmap = fixRotate(BitmapFactory.decodeFile(picturePath), picturePath);
//                thumbnail=getResizedBitmap(thumbnail, 400);
                Log.e("path of image", picturePath + "");
                image_paths.add(picturePath);
                updateImages(image_paths, bitmap);

//                img_user.setImageBitmap(bitmap);

            } else if (requestCode == 111) {

                if (data.getStringExtra("SELECT").equalsIgnoreCase("CAMERA")) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    File f = new File(android.os.Environment.getExternalStorageDirectory(), "temp.jpg");
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
                    startActivityForResult(intent, 1);
                } else {
                    Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(intent, 2);
                }

            } else if (requestCode == 112) {
                setResult(RESULT_OK);
                finish();
            }
        }
    }

    private void updateImages(List<String> image_paths, Bitmap bitmap) {

        switch (image_paths.size()) {
            case 1:
                img_user.setImageBitmap(bitmap);
                img_user.setVisibility(View.VISIBLE);
                break;
            case 2:
                img_user2.setImageBitmap(bitmap);
                img_user2.setVisibility(View.VISIBLE);
                break;
            case 3:
                img_user3.setImageBitmap(bitmap);
                img_user3.setVisibility(View.VISIBLE);
                break;
            case 4:
                img_user4.setImageBitmap(bitmap);
                img_user4.setVisibility(View.VISIBLE);
                break;
        }

    }

    private Bitmap fixRotate(Bitmap decodeFile, String photoPath) {
        ExifInterface ei = null;
        try {
            ei = new ExifInterface(photoPath);
        } catch (IOException e) {
            e.printStackTrace();
        }
        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                ExifInterface.ORIENTATION_UNDEFINED);

        Bitmap rotatedBitmap = decodeFile;
        switch (orientation) {

            case ExifInterface.ORIENTATION_ROTATE_90:
                rotatedBitmap = rotateImage(decodeFile, 90);
                break;

            case ExifInterface.ORIENTATION_ROTATE_180:
                rotatedBitmap = rotateImage(decodeFile, 180);
                break;

            case ExifInterface.ORIENTATION_ROTATE_270:
                rotatedBitmap = rotateImage(decodeFile, 270);
                break;

            case ExifInterface.ORIENTATION_NORMAL:
            default:
                rotatedBitmap = decodeFile;
        }

        return rotatedBitmap;
    }

    public static Bitmap rotateImage(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(),
                matrix, true);
    }

}