package com.loklvokl.lokl.activtiy;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.loklvokl.lokl.R;
import com.loklvokl.lokl.model.CheckLogin;
import com.loklvokl.lokl.model.DysplayProfileModel;
import com.loklvokl.lokl.model.SignUpResponse;
import com.loklvokl.lokl.model.User;
import com.loklvokl.lokl.model.UserProfile;
import com.loklvokl.lokl.util.ApiClient;
import com.loklvokl.lokl.util.ApiInterface;
import com.loklvokl.lokl.util.Constant;
import com.loklvokl.lokl.util.Progress;
import com.loklvokl.lokl.util.SharedPref;
import com.truecaller.android.sdk.ITrueCallback;
import com.truecaller.android.sdk.TrueError;
import com.truecaller.android.sdk.TrueProfile;
import com.truecaller.android.sdk.TruecallerSDK;
import com.truecaller.android.sdk.TruecallerSdkScope;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SplashActivity extends AppCompatActivity {
    SharedPref sharedPref;
    private ProgressDialog progressDialog;
    private ApiInterface apiService;
    private ConstraintLayout cl_continue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        sharedPref = new SharedPref(this);

        initTrueCaller();
        getFirebaseToken();

        cl_continue = findViewById(R.id.cl_continue);

        apiService = ApiClient.getClient().create(ApiInterface.class);


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                if (sharedPref.getBool("IS_LOGIN")) {
                    Intent i = new Intent(SplashActivity.this, MainActivity.class);
                    startActivity(i);
                    finish();
                } else {
                    cl_continue.setVisibility(View.VISIBLE);
                }

            }
        }, 2000);

        cl_continue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (sharedPref.getBool("IS_LOGIN")) {
                    Intent i = new Intent(SplashActivity.this, MainActivity.class);
                    startActivity(i);
                    finish();
                } else {
//                    Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
//                    startActivity(intent);
                    if (TruecallerSDK.getInstance().isUsable()) {
                        TruecallerSDK.getInstance().getUserProfile(SplashActivity.this);
                    } else {
//                        Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
//                        startActivity(intent);

                        Intent intent = new Intent(SplashActivity.this, AddMobileNumberActivity.class);
                        intent.putExtra("isFailure", true);
                        startActivity(intent);
                        finish();

                    }
                }

            }
        });

    }

    private void getFirebaseToken() {

        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(task -> {
                    if (!task.isSuccessful()) {
                        return;
                    }
                    String token = task.getResult().getToken();
                    Log.e("firebase Token", "getFirebaseToken: " + token);
                });
    }

    private void initTrueCaller() {

        TruecallerSdkScope trueScope = new TruecallerSdkScope.Builder(this, new ITrueCallback() {
            @Override
            public void onSuccessProfileShared(@NonNull TrueProfile trueProfile) {
                checkLogin(trueProfile, trueProfile.phoneNumber.replace("+91", ""));
            }

            @Override
            public void onFailureProfileShared(@NonNull TrueError trueError) {

                Intent intent = new Intent(SplashActivity.this, AddMobileNumberActivity.class);
                intent.putExtra("isFailure", true);
                startActivity(intent);
                finish();

            }

            @Override
            public void onVerificationRequired(TrueError trueError) {
                Log.e("TAG", "onFailureProfileShared: " + trueError);
            }
        })
                .consentMode(TruecallerSdkScope.CONSENT_MODE_POPUP)
                .buttonColor(getResources().getColor(R.color.colorPrimary))
                .buttonTextColor(Color.WHITE)
                .loginTextPrefix(/*TruecallerSdkScope.LOGIN_TEXT_PREFIX_TO_GET_STARTED|*/TruecallerSdkScope.LOGIN_TEXT_PREFIX_TO_VIEW_MORE)
                .loginTextSuffix(TruecallerSdkScope.LOGIN_TEXT_SUFFIX_PLEASE_VERIFY_MOBILE_NO)
                .ctaTextPrefix(TruecallerSdkScope.CTA_TEXT_PREFIX_USE)
                .buttonShapeOptions(TruecallerSdkScope.BUTTON_SHAPE_ROUNDED)
                .privacyPolicyUrl("http://loklvokl.co.in/privacy-policy")
                .termsOfServiceUrl("http://loklvokl.co.in/tc")
                .footerType(TruecallerSdkScope.FOOTER_TYPE_CONTINUE)
                .consentTitleOption(TruecallerSdkScope.SDK_CONSENT_TITLE_VERIFY)
                .sdkOptions(TruecallerSdkScope.SDK_OPTION_WITHOUT_OTP)
                .build();
        TruecallerSDK.init(trueScope);
    }


    public void checkLogin(TrueProfile trueProfile, String number) {

        progressDialog = null;
        progressDialog = Progress.showProgressDialog(this, "please wait.", false);
        progressDialog.show();

        Call<CheckLogin> call = apiService.checkLogin(number, "", "");

        call.enqueue(new Callback<CheckLogin>() {
            @Override
            public void onResponse(Call<CheckLogin> call, Response<CheckLogin> response) {
                if (progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                CheckLogin body = response.body();
                if (body.getResponse().get(0).isRegister()) {

                    String userId = body.getResponse().get(0).getUser_id();

                    displayProfile(userId);

//                    sharedPref.setStr("MOBILE", number);
//                    sharedPref.setStr("ID", userId);
//                    sharedPref.setBool("IS_LOGIN", true);
//
//                    Intent i = new Intent(SplashActivity.this, MainActivity.class);
//                    startActivity(i);
//                    finish();

                } else {
                    registerUser(trueProfile, number);
                }
            }

            @Override
            public void onFailure(Call<CheckLogin> call, Throwable t) {

                if (progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                Toast.makeText(SplashActivity.this, "Fail! Try again.", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void registerUser(TrueProfile trueProfile, String number) {

        progressDialog = null;
        progressDialog = Progress.showProgressDialog(this, "please wait.", false);
        progressDialog.show();

        Call<SignUpResponse> call = apiService.requestSignup(trueProfile.firstName, trueProfile.lastName, trueProfile.email, number, number, Constant.getDeviceId(this));

        call.enqueue(new Callback<SignUpResponse>() {
            @Override
            public void onResponse(Call<SignUpResponse> call, Response<SignUpResponse> response) {
                if (progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                SignUpResponse body = response.body();

                if (body.getResponse() != null && body.getResponse().size() > 0) {
                    Toast.makeText(SplashActivity.this, body.getResponse().get(0).getMessage(), Toast.LENGTH_SHORT).show();

                    if (!body.getResponse().get(0).getError()) {

                        User user = body.getResponse().get(0).getUser().get(0);
                        sharedPref.setStr("MOBILE", user.getMobile());
                        sharedPref.setStr("NAME", user.getUsername());
                        sharedPref.setStr("LNAME", user.getLast_name());
                        sharedPref.setStr("IMAGE", user.getProfile_image());
                        sharedPref.setStr("EMAIL", user.getEmail());
                        sharedPref.setStr("ID", user.getId());
                        sharedPref.setBool("IS_LOGIN", true);

                        sharedPref.setStr("CITY", user.getCity());
                        sharedPref.setStr("GENDER", user.getGender());
                        sharedPref.setStr("DOB", user.getDob());

                        Intent i = new Intent(SplashActivity.this, MainActivity.class);
                        startActivity(i);
                        finish();

                    }
                }
            }

            @Override
            public void onFailure(Call<SignUpResponse> call, Throwable t) {

                if (progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                Toast.makeText(SplashActivity.this, "Fail! Try again.", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void displayProfile(String user_id) {

        progressDialog = null;
        progressDialog = Progress.showProgressDialog(this, "please wait.", false);
        progressDialog.show();

        Call<DysplayProfileModel> call = apiService.diplayProfile(user_id);

        call.enqueue(new Callback<DysplayProfileModel>() {
            @Override
            public void onResponse(Call<DysplayProfileModel> call, Response<DysplayProfileModel> response) {
                DysplayProfileModel body = response.body();

                if (body.getUserProfile() != null) {
                    if (body.getUserProfile().getUserId() != null) {
//                        Toast.makeText(SplashActivity.this, body., Toast.LENGTH_SHORT).show();
                        UserProfile user = body.getUserProfile();
                        sharedPref.setStr("MOBILE", user.getMobile());
                        sharedPref.setStr("NAME", user.getUsername());
                        sharedPref.setStr("LNAME", user.getLastName());
                        sharedPref.setStr("EMAIL", user.getEmail());
                        sharedPref.setStr("IMAGE", user.getProfileImage());
                        sharedPref.setStr("ID", user.getUserId());
                        sharedPref.setBool("IS_LOGIN", true);
                        sharedPref.setStr("CITY", user.getCity());
                        sharedPref.setStr("GENDER", user.getGender());
                        sharedPref.setStr("DOB", user.getDob());
                        sharedPref.setStr("FNAME", user.getFirstName());
                        sharedPref.setAdharList("ADHAR_LIST", user.getAadharDocument());
                        sharedPref.setPanList("PAN_LIST", user.getPanDocument());
                        sharedPref.setInt("PAN_COUNT", user.getPan_counter());
                        sharedPref.setInt("ADHAR_COUNT", user.getAddhar_counter());
                        // sharedPref.set("FNAME", user.getKycDetails().get);


                        Intent i = new Intent(SplashActivity.this, MainActivity.class);
                        startActivity(i);
                        finish();


                    }
                }


                if (progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

            }

            @Override
            public void onFailure(Call<DysplayProfileModel> call, Throwable t) {
                Log.e("TAG", t.toString());

                if (progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
            }
        });

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        TruecallerSDK.getInstance().onActivityResultObtained(this, resultCode, data);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        TruecallerSDK.clear();
    }

}