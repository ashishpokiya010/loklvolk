package com.loklvokl.lokl.activtiy;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Environment;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.loklvokl.lokl.R;
import com.loklvokl.lokl.adapter.AdCatAdapter;
import com.loklvokl.lokl.model.MainCategory;
import com.loklvokl.lokl.model.SignUpResponse;
import com.loklvokl.lokl.util.ApiClient;
import com.loklvokl.lokl.util.ApiInterface;
import com.loklvokl.lokl.util.SharedPref;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddAdActivity6 extends AppCompatActivity implements View.OnClickListener, AdCatAdapter.AdInterface {

    private static final String TAG = "AddAd";
    ProgressBar progressBar;
    ApiInterface apiService;
    ImageView back_btn;
    TextView title_text;
    MainCategory mainCategory = new MainCategory();
    String image1, image2, image3, image4, adtitle, color_id, size_id, mPrice,
            sPrice, main_cat, sub_cat1, sub_cat2, product_discription,
            productCondition,
            countryOfRegion;
    Button btn_submit;
    RadioButton radio1, radio2;
    SharedPref sharedPref;
    TextView tv_my_earning_loklvokl, tv_my_earning_self;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_ad6);
        getINtentData();
        init();

//        loadData();
    }

    private void getINtentData() {
        if (getIntent().hasExtra("TITLE")) {
            title_text.setText(getIntent().getStringExtra("TITLE"));
        }
        if (getIntent().hasExtra("DATA")) {
            mainCategory = (MainCategory) getIntent().getSerializableExtra("DATA");
        }
        image1 = getIntent().getStringExtra("IMAGE1");
        image2 = getIntent().getStringExtra("IMAGE2");
        image3 = getIntent().getStringExtra("IMAGE3");
        image4 = getIntent().getStringExtra("IMAGE4");

        adtitle = getIntent().getStringExtra("AD_TITLE");
        color_id = getIntent().getStringExtra("COLOR_ID");
        size_id = getIntent().getStringExtra("SIZE_ID");

        mPrice = getIntent().getStringExtra("M_PRICE");
        sPrice = getIntent().getStringExtra("S_PRICE");
        main_cat = getIntent().getStringExtra("MAIN_CAT");
        sub_cat1 = getIntent().getStringExtra("SUB_CAT1");
        sub_cat2 = getIntent().getStringExtra("SUB_CAT2");
        product_discription = getIntent().getStringExtra("product_discription");
        productCondition = getIntent().getStringExtra("productCondition");
        countryOfRegion = getIntent().getStringExtra("countryOfRegion");

        System.out.println("SECCLING_PRICE1" + sPrice);
    }

    private void init() {

        sharedPref = new SharedPref(this);
        apiService = ApiClient.getClient().create(ApiInterface.class);
        back_btn = findViewById(R.id.back_btn);
        title_text = findViewById(R.id.title_text);
        title_text.setText("Select Shipping");
        back_btn.setOnClickListener(this);

        progressBar = findViewById(R.id.progressBar);
        radio1 = findViewById(R.id.radio1);
        radio2 = findViewById(R.id.radio2);
        btn_submit = findViewById(R.id.btn_submit);

        tv_my_earning_loklvokl = findViewById(R.id.tv_my_earning_loklvokl);
        tv_my_earning_self = findViewById(R.id.tv_my_earning_self);

        btn_submit.setOnClickListener(this);

        radio2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                radio2.setChecked(false);
                new AlertDialog.Builder(AddAdActivity6.this)
//                .setTitle("Alert")
                        .setMessage("Coming Soon..")
                        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .show();
            }
        });


        int a = Integer.parseInt(sPrice.trim()) - 10;
        tv_my_earning_loklvokl.setText("Rs."+a);
        tv_my_earning_self.setText("Rs."+a);
    }

    /*public Uri getImageUri(Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(this.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }*/

    public static File bitmapToFile(Bitmap bitmap) {
        File file = null;
        try {
            file = new File(Environment.getExternalStorageDirectory() + File.separator + ".png");
            file.createNewFile();

//Convert bitmap to byte array
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 0, bos); // YOU can also save it in JPEG
            byte[] bitmapdata = bos.toByteArray();

//write the bytes in file
            FileOutputStream fos = new FileOutputStream(file);
            fos.write(bitmapdata);
            fos.flush();
            fos.close();
            return file;
        } catch (Exception e) {
            e.printStackTrace();
            return file; // it will return null
        }
    }

    @Override
    public void onClick(View view) {
        if (view == back_btn) {
            onBackPressed();

        } else if (view == btn_submit) {
            btn_submit.setEnabled(false);
            progressBar.setVisibility(View.VISIBLE);
            performSubmit();
        }
    }

    private String convertToString(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        byte[] imgByte = byteArrayOutputStream.toByteArray();
        return Base64.encodeToString(imgByte, Base64.DEFAULT);
    }

    public void performSubmit() {
        File file1 = null;
        File file2 = null;
        File file3 = null;
        File file4 = null;

        String image1 = null, image2 = null, image3 = null, image4 = null;

        if (AddAdActivity1.image_bitmap_list.size() == 1) {
            //img_user.setImageBitmap(AddAdActivity1.image_bitmap_list.get(0));
            //file1 = bitmapToFile(AddAdActivity1.image_bitmap_list.get(0));
            image1 = convertToString(AddAdActivity1.image_bitmap_list.get(0));
        }
        if (AddAdActivity1.image_bitmap_list.size() == 2) {

            //file1 = bitmapToFile(AddAdActivity1.image_bitmap_list.get(0));
            //file2 = bitmapToFile(AddAdActivity1.image_bitmap_list.get(1));

            image1 = convertToString(AddAdActivity1.image_bitmap_list.get(0));
            image2 = convertToString(AddAdActivity1.image_bitmap_list.get(1));

            System.out.println("FILENAMAMAM" + image1);
            System.out.println("FILENAMAMAM2" + image2);
            System.out.println("BITMAPNAME" + AddAdActivity1.image_bitmap_list.get(0));
            System.out.println("BITMAPNAME" + AddAdActivity1.image_bitmap_list.get(1));
        }
        if (AddAdActivity1.image_bitmap_list.size() == 3) {
            //file1 = bitmapToFile(AddAdActivity1.image_bitmap_list.get(0));
            //file2 = bitmapToFile(AddAdActivity1.image_bitmap_list.get(1));
            //file3 = bitmapToFile(AddAdActivity1.image_bitmap_list.get(2));

            image1 = convertToString(AddAdActivity1.image_bitmap_list.get(0));
            image2 = convertToString(AddAdActivity1.image_bitmap_list.get(1));
            image3 = convertToString(AddAdActivity1.image_bitmap_list.get(2));

        }
        if (AddAdActivity1.image_bitmap_list.size() == 4) {
            //file1 = bitmapToFile(AddAdActivity1.image_bitmap_list.get(0));
            //file2 = bitmapToFile(AddAdActivity1.image_bitmap_list.get(1));
            //file3 = bitmapToFile(AddAdActivity1.image_bitmap_list.get(2));
            //file4 = bitmapToFile(AddAdActivity1.image_bitmap_list.get(3));

            image1 = convertToString(AddAdActivity1.image_bitmap_list.get(0));
            image2 = convertToString(AddAdActivity1.image_bitmap_list.get(1));
            image3 = convertToString(AddAdActivity1.image_bitmap_list.get(2));
            image4 = convertToString(AddAdActivity1.image_bitmap_list.get(3));
        }


        MultipartBody.Part bodyImage1 = null;
        if (file1 != null) {
            RequestBody requestFileImage1 =
                    RequestBody.create(
                            MediaType.parse("*/*"),
                            file1
                    );
            bodyImage1 = MultipartBody.Part.createFormData("field_name1", file1.getName(), requestFileImage1);
        }

        MultipartBody.Part bodyImage2 = null;
        if (file2 != null) {
            RequestBody requestFileImage2 =
                    RequestBody.create(
                            MediaType.parse("*/*"),
                            file2
                    );
            bodyImage2 = MultipartBody.Part.createFormData("field_name2", file2.getName(), requestFileImage2);
        }

        MultipartBody.Part bodyImage3 = null;
        if (file3 != null) {
            RequestBody requestFileImage3 =
                    RequestBody.create(
                            MediaType.parse("*/*"),
                            file3
                    );
            bodyImage3 = MultipartBody.Part.createFormData("field_name3", file3.getName(), requestFileImage3);
        }

        MultipartBody.Part bodyImage4 = null;
        if (file4 != null) {
            RequestBody requestFileImage4 =
                    RequestBody.create(
                            MediaType.parse("*/*"),
                            file4
                    );
            bodyImage4 = MultipartBody.Part.createFormData("field_name4", file4.getName(), requestFileImage4);
        }



           /* Call<SignUpResponse> call = apiService.performSellProduct(RequestBody.create(MediaType.parse("text/plain"),
                    sharedPref.getStr("ID")),
                    RequestBody.create(MediaType.parse("text/plain"), main_cat),
                    RequestBody.create(MediaType.parse("text/plain"), sub_cat1),
                    RequestBody.create(MediaType.parse("text/plain"), sub_cat2),
                    RequestBody.create(MediaType.parse("text/plain"), adtitle),
                    RequestBody.create(MediaType.parse("text/plain"), ""),
                    RequestBody.create(MediaType.parse("text/plain"), mPrice),
                    RequestBody.create(MediaType.parse("text/plain"), size_id),
                    RequestBody.create(MediaType.parse("text/plain"), color_id),
                    RequestBody.create(MediaType.parse("text/plain"), sPrice),
                    bodyImage1, bodyImage2, bodyImage3, bodyImage4,
                    RequestBody.create(MediaType.parse("text/plain"), "0"));
*/

        Call<SignUpResponse> call = apiService.performSellProduct(sharedPref.getStr("ID"), main_cat,
                sub_cat1,
                sub_cat2,
                adtitle,
                product_discription, productCondition, countryOfRegion,
                mPrice,
                size_id,
                color_id,
                sPrice,
                image1, image2, image3, image4,
                "0", "0");

        System.out.println("REQUEST" + bodyImage1);

        call.enqueue(new Callback<SignUpResponse>() {
            @Override
            public void onResponse(Call<SignUpResponse> call, Response<SignUpResponse> response) {
                Log.d(TAG, "data: " + response.body());
                progressBar.setVisibility(View.GONE);
                btn_submit.setEnabled(true);
                SignUpResponse model = response.body();
                if (model.getResponse().get(0).getError()) {
                    Toast.makeText(AddAdActivity6.this, model.getResponse().get(0).getMessage(), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(AddAdActivity6.this, model.getResponse().get(0).getMessage(), Toast.LENGTH_SHORT).show();
                    AddAdActivity1.image_bitmap_list.clear();
                    setResult(RESULT_OK);
                    finish();

                }
            }

            @Override
            public void onFailure(Call<SignUpResponse> call, Throwable t) {
                // Log error here since request failed
                btn_submit.setEnabled(true);
                progressBar.setVisibility(View.GONE);
                Log.e(TAG, t.toString());
                Toast.makeText(AddAdActivity6.this, "Fail! Try again.", Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public void itemClick(int position) {
        Intent intent = new Intent(this, AddAdActivity3.class);
        intent.putExtra("TITLE", mainCategory.getCategory().get(position).getCatName());
        intent.putExtra("DATA", mainCategory.getCategory().get(position));
        startActivityForResult(intent, 111);
        overridePendingTransition(R.anim.enter, R.anim.exit);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
    }
}