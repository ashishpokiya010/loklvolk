package com.loklvokl.lokl.activtiy;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.OpenableColumns;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.loklvokl.lokl.R;
import com.loklvokl.lokl.util.Constant;
import com.loklvokl.lokl.util.SharedPref;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;

public class BaseActivity extends AppCompatActivity {

    SharedPref sharedPref;
    TextView text_cart_count;
    Activity mActivity = BaseActivity.this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedPref = new SharedPref(this);
    }

    public boolean isNetworkAvailable() {
        return Constant.getConnectivityStatus(mActivity);
    }

    @Override
    protected void onResume() {
        super.onResume();
        int count = sharedPref.getInt("CART_COUNT");

        text_cart_count = findViewById(R.id.text_cart_count);
        try {
            if (count > 0) {
                text_cart_count.setText(String.valueOf(count));
                text_cart_count.setVisibility(View.VISIBLE);
                Log.d("BaseonResume : "," - - "+count);
            } else {
                text_cart_count.setVisibility(View.GONE);
                Log.d("BaseonResume : "," - - "+count);
            }
        } catch (Resources.NotFoundException | NullPointerException ignored) {
            if (text_cart_count != null)
                text_cart_count.setVisibility(View.GONE);
        }
    }

    public void setCardCount() {
        int count = sharedPref.getInt("CART_COUNT");

        text_cart_count = findViewById(R.id.text_cart_count);
        try {
            if (count > 0) {
                text_cart_count.setText(String.valueOf(count));
                text_cart_count.setVisibility(View.VISIBLE);
            } else {
                text_cart_count.setVisibility(View.GONE);
            }
        } catch (Resources.NotFoundException | NullPointerException ignored) {
            if (text_cart_count != null)
                text_cart_count.setVisibility(View.GONE);
        }
    }

    public void updateCartCount(int notiCOunt) {
        sharedPref.setInt("CART_COUNT", notiCOunt);
    }

    public String getFilePathForN(Uri uri, Context context) {
        Uri returnUri = uri;
        Cursor returnCursor = context.getContentResolver().query(returnUri, null, null, null, null);
        /*
         * Get the column indexes of the data in the Cursor,
         *     * move to the first row in the Cursor, get the data,
         *     * and display it.
         * */
        int nameIndex = returnCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);
        int sizeIndex = returnCursor.getColumnIndex(OpenableColumns.SIZE);
        returnCursor.moveToFirst();
        String name = (returnCursor.getString(nameIndex));
        String size = (Long.toString(returnCursor.getLong(sizeIndex)));
        File file = new File(context.getFilesDir(), name);
        try {
            InputStream inputStream = context.getContentResolver().openInputStream(uri);
            FileOutputStream outputStream = new FileOutputStream(file);
            int read = 0;
            int maxBufferSize = 1 * 1024 * 1024;
            int bytesAvailable = inputStream.available();

            //int bufferSize = 1024;
            int bufferSize = Math.min(bytesAvailable, maxBufferSize);

            final byte[] buffers = new byte[bufferSize];
            while ((read = inputStream.read(buffers)) != -1) {
                outputStream.write(buffers, 0, read);
            }
            Log.e("File Size", "Size " + file.length());
            inputStream.close();
            outputStream.close();
            Log.e("File Path", "Path " + file.getPath());
            Log.e("File Size", "Size " + file.length());
        } catch (Exception e) {
            Log.e("Exception", e.getMessage());
        }
        return file.getPath();
    }
}