package com.loklvokl.lokl.activtiy;

import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.loklvokl.lokl.R;
import com.loklvokl.lokl.model.SignUpResponse;
import com.loklvokl.lokl.model.User;
import com.loklvokl.lokl.util.ApiClient;
import com.loklvokl.lokl.util.ApiInterface;
import com.loklvokl.lokl.util.SharedPref;

import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgotActivity3 extends AppCompatActivity implements View.OnClickListener {
    TextView tv_sign_up, btn_login;

    EditText ed_mobile, ed_password;
    ProgressBar progressBar;
    ApiInterface apiService;
    ApiClient apiClient;


    SharedPref sharedPref;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.forgot_activity3);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        sharedPref = new SharedPref(this);
        apiClient = new ApiClient();
        apiService = ApiClient.getClient().create(ApiInterface.class);

        init();
    }


    private void performSignIn() {
        String mobile = ed_mobile.getText().toString();
        String pass = ed_password.getText().toString();

        if (mobile.isEmpty()) {
            Toast.makeText(this, "Mobile is required.", Toast.LENGTH_SHORT).show();
        } else if (pass.isEmpty()) {
            Toast.makeText(this, "Password is required.", Toast.LENGTH_SHORT).show();
        } else {
            progressBar.setVisibility(View.VISIBLE);

            apiService = apiClient.getClient().create(ApiInterface.class);

            Call<SignUpResponse> call = apiService.requestLogin(mobile, pass, "");
            apiClient.enqueue(call, new Callback<SignUpResponse>() {
                @Override
                public void onResponse(Call<SignUpResponse> call, Response<SignUpResponse> response) {
                    Log.d("TAG", "data: " + response.body());
                    progressBar.setVisibility(View.GONE);
                    SignUpResponse body = response.body();

                    if (body.getResponse() != null && body.getResponse().size() > 0) {

                        Toast.makeText(ForgotActivity3.this, body.getResponse().get(0).getMessage(), Toast.LENGTH_SHORT).show();
                        if (!body.getResponse().get(0).getError()) {
                            User user = body.getResponse().get(0).getUser().get(0);
                            sharedPref.setStr("MOBILE", user.getMobile());
                            sharedPref.setStr("NAME", user.getUsername());
                            sharedPref.setStr("LNAME", user.getLast_name());
                            sharedPref.setStr("IMAGE", user.getProfile_image());
                            sharedPref.setStr("EMAIL", user.getEmail());
                            sharedPref.setStr("ID", user.getId());
                            sharedPref.setStr("CITY", user.getCity());
                            sharedPref.setStr("GENDER", user.getGender());
                            sharedPref.setStr("DOB", user.getDob());
                        }
                    }
                }

                @Override
                public void onFailure(Call<SignUpResponse> call, Throwable t) {
                    // Log error here since request failed
                    Log.e("TAG", t.toString());
                }
            });

        }

    }

    private boolean idValid(String email) {
        Pattern pattern = Patterns.EMAIL_ADDRESS;
        return pattern.matcher(email).matches();
    }

    public void init() {
        tv_sign_up = findViewById(R.id.btn_sign);
        tv_sign_up.setOnClickListener(this);

        btn_login = findViewById(R.id.btn_login);
        btn_login.setOnClickListener(this);

        progressBar = findViewById(R.id.progressBar);
        ed_mobile = findViewById(R.id.ed_mobile);
        ed_password = findViewById(R.id.ed_password);
    }

    @Override
    public void onClick(View view) {
        if (view == tv_sign_up) {
            finish();
        } else if (view == btn_login) {
//            performSignIn();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
