package com.loklvokl.lokl.activtiy;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.loklvokl.lokl.R;
import com.loklvokl.lokl.adapter.AdCatAdapter;
import com.loklvokl.lokl.model.Category;
import com.loklvokl.lokl.model.SubCategory;
import com.loklvokl.lokl.util.ApiClient;
import com.loklvokl.lokl.util.ApiInterface;

import java.util.ArrayList;
import java.util.List;

public class AddAdActivity3 extends AppCompatActivity implements View.OnClickListener, AdCatAdapter.AdInterface {

    private static final String TAG = "AddAd";
    ProgressBar progressBar;
    ApiInterface apiService;
    AdCatAdapter adCatAdapter;
    RecyclerView recycle_view;
    ImageView back_btn;
    TextView title_text;
    Category mainCategory = new Category();
    String main_cat, sub_cat1;
    String image1, image2, image3, image4;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_ad);

        init();
        getINtentData();
        loadData();
    }

    private void getINtentData() {
        if (getIntent().hasExtra("TITLE")) {
            title_text.setText(getIntent().getStringExtra("TITLE"));
        }
        if (getIntent().hasExtra("DATA")) {
            mainCategory = (Category) getIntent().getSerializableExtra("DATA");
        }
        main_cat = getIntent().getStringExtra("MAIN_CAT");
        sub_cat1 = getIntent().getStringExtra("SUB_CAT1");

        image1 = getIntent().getStringExtra("IMAGE1");
        image2 = getIntent().getStringExtra("IMAGE2");
        image3 = getIntent().getStringExtra("IMAGE3");
        image4 = getIntent().getStringExtra("IMAGE4");
    }

    private void init() {
        apiService = ApiClient.getClient().create(ApiInterface.class);
        progressBar = findViewById(R.id.progressBar);
        back_btn = findViewById(R.id.back_btn);
        back_btn.setOnClickListener(this);
        title_text = findViewById(R.id.title_text);
        title_text.setText("Select Category");

        recycle_view = findViewById(R.id.recycle_view);
        recycle_view.setLayoutManager(new GridLayoutManager(this, 3));
    }

    private void loadData() {

        progressBar.setVisibility(View.GONE);

        List<SubCategory> subCategory = new ArrayList<>();
        for (SubCategory mainCategory : mainCategory.getSubCategory()) {
            subCategory.add(new SubCategory(mainCategory.getSubCatId(), mainCategory.getSubCatName(), mainCategory.getImage()));
        }

        adCatAdapter = new AdCatAdapter(AddAdActivity3.this, subCategory);
        recycle_view.setAdapter(adCatAdapter);

    }

    @Override
    public void onClick(View view) {
        if (view == back_btn) {
            onBackPressed();
        }
    }

    @Override
    public void itemClick(int position) {
        Intent intent = new Intent(this, AddAdActivity4.class);
        intent.putExtra("TITLE", mainCategory.getSubCategory().get(position).getSubCatName());
        intent.putExtra("ID", mainCategory.getSubCategory().get(position).getSubCatId());

        intent.putExtra("MAIN_CAT", main_cat);
        intent.putExtra("SUB_CAT1", sub_cat1);
        intent.putExtra("SUB_CAT2", mainCategory.getSubCategory().get(position).getSubCatId());
        intent.putExtra("IMAGE1", image1);
        intent.putExtra("IMAGE2", image2);
        intent.putExtra("IMAGE3", image3);
        intent.putExtra("IMAGE4", image4);
        startActivityForResult(intent, 111);
        overridePendingTransition(R.anim.enter, R.anim.exit);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 111 && resultCode == RESULT_OK) {
            setResult(RESULT_OK);
            finish();
        }
    }
}