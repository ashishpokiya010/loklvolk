package com.loklvokl.lokl.activtiy;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.loklvokl.lokl.R;
import com.loklvokl.lokl.adapter.UserOrderAdapter;
import com.loklvokl.lokl.model.OrderProduct;
import com.loklvokl.lokl.model.UserOrderList;
import com.loklvokl.lokl.util.ApiClient;
import com.loklvokl.lokl.util.ApiInterface;
import com.loklvokl.lokl.util.Constant;
import com.loklvokl.lokl.util.SharedPref;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyOrderActivity extends BaseActivity implements View.OnClickListener {

    ImageView back_btn;
    TextView title_text;
    ProgressBar progressBar;
    ApiClient apiClient;
    SharedPref sharedPref;
    ApiInterface apiService;
    RecyclerView recycle_view;
    UserOrderAdapter orderAdapter;
    EditText searchView;
    List<OrderProduct> orderProducts = new ArrayList<>();
    LinearLayout list_layout, no_data_txt;
    ImageView imgDetailWishlist;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_order);

        sharedPref = new SharedPref(this);
        apiClient = new ApiClient();
        apiService = ApiClient.getClient().create(ApiInterface.class);

        init();
        loadDAta();
    }

    private void init() {
        findViewById(R.id.img_detail_noti).setVisibility(View.GONE);
        title_text = findViewById(R.id.title_text);
        title_text.setText("My Orders");
        back_btn = findViewById(R.id.back_btn);
        back_btn.setOnClickListener(this);
        progressBar = findViewById(R.id.progressBar);
        searchView = findViewById(R.id.searchView);
        recycle_view = findViewById(R.id.recycle_view);
        list_layout = findViewById(R.id.list_layout);
        no_data_txt = findViewById(R.id.list_no_data_found);

        imgDetailWishlist = findViewById(R.id.img_detail_wishlist);
        imgDetailWishlist.setVisibility(View.GONE);

        recycle_view.setLayoutManager(new GridLayoutManager(this, 1));

        searchView.setHint("Search Order");
        searchView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                filter(s.toString());
            }
        });
    }


    private void loadDAta() {
        progressBar.setVisibility(View.VISIBLE);

        apiService = apiClient.getClient().create(ApiInterface.class);

        Call<UserOrderList> call = apiService.getUserProductList(sharedPref.getStr("ID"));

        apiClient.enqueue(call, new Callback<UserOrderList>() {
            @Override
            public void onResponse(Call<UserOrderList> call, Response<UserOrderList> response) {
                progressBar.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    String responseData = response.body().getOrderProduct().get(0).getError();
                    System.out.println("NULL>>>" + responseData);
                    if (responseData == null) {
                        no_data_txt.setVisibility(View.GONE);
                        list_layout.setVisibility(View.VISIBLE);
                        UserOrderList detailModel = response.body();
                        orderProducts.addAll(detailModel.getOrderProduct());
                        orderAdapter = new UserOrderAdapter(MyOrderActivity.this, orderProducts);
                        recycle_view.setAdapter(orderAdapter);
                    } else {
                        Constant.toast(getApplicationContext(), response.body().getOrderProduct().get(0).getMessage());
                        no_data_txt.setVisibility(View.VISIBLE);
                        list_layout.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onFailure(Call<UserOrderList> call, Throwable t) {
                // Log error here since request failed
            }
        });
    }


    private void filter(String text) {
        List<OrderProduct> temp = new ArrayList();
        for (OrderProduct d : orderProducts) {
            //or use .equal(text) with you want equal match
            //use .toLowerCase() for better matches
            if (d.getProductName().toLowerCase().contains(text.toLowerCase())) {
                temp.add(d);
            }
        }
        orderAdapter.updateList(temp);
    }

    @Override
    public void onClick(View view) {
        if (view == back_btn) {
            Log.d("BackPressed ; ","MyOrder");
            this.finish();
        }
    }
}