package com.loklvokl.lokl.activtiy;

import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageDecoder;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.loklvokl.lokl.R;
import com.loklvokl.lokl.adapter.AdCatAdapter;
import com.loklvokl.lokl.adapter.ImageAdapter;
import com.loklvokl.lokl.model.AdProductDetail;
import com.loklvokl.lokl.util.ApiClient;
import com.loklvokl.lokl.util.ApiInterface;
import com.yalantis.ucrop.UCrop;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class AddAdActivity1 extends AppCompatActivity
        implements View.OnClickListener, ImageAdapter.AdInterface {

    private static final String TAG = "AddAd";
    ProgressBar progressBar;
    ApiInterface apiService;
    // AdCatAdapter adCatAdapter;
    ImageView back_btn;
    TextView title_text, upload_image;
    //String subcat_id;
    //AdProductDetail adProductDetail;
    ProgressDialog pd;
    RelativeLayout main_Layout;
    LinearLayout top_layout;
    ImageView img_user, img_user2, img_user3, img_user4;
    RelativeLayout relative_user1, relative_user2, relative_user3, relative_user4;
    List<String> image_paths = new ArrayList<>();
    public static List<Bitmap> image_bitmap_list;
    ImageView btn_next;
    ImageView btn_camera, btn_gallery;
    ImageView top_img;

    ImageView remove_img_user, remove_img_user2, remove_img_user3, remove_img_user4;
    RecyclerView recycle_view;


    public static final int RequestPermissionCodeImage = 700;
    public static final int RequestPermissionCodeCamera = 800;

    public static final int RequestPermissionCode = 900;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_ad1);

        init();

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
    }


    private void init() {
        image_bitmap_list = new ArrayList<>();
        apiService = ApiClient.getClient().create(ApiInterface.class);
        progressBar = findViewById(R.id.progressBar);
        back_btn = findViewById(R.id.back_btn);
        back_btn.setOnClickListener(this);
        title_text = findViewById(R.id.title_text);
        upload_image = findViewById(R.id.upload_image);
        img_user = findViewById(R.id.img_user);
        img_user2 = findViewById(R.id.img_user2);
        img_user3 = findViewById(R.id.img_user3);
        img_user4 = findViewById(R.id.img_user4);
        top_img = findViewById(R.id.top_img);

        recycle_view = findViewById(R.id.recycle_view);
        recycle_view.setNestedScrollingEnabled(false);


        remove_img_user = findViewById(R.id.remove_img_user);
        remove_img_user2 = findViewById(R.id.remove_img_user2);
        remove_img_user3 = findViewById(R.id.remove_img_user3);
        remove_img_user4 = findViewById(R.id.remove_img_user4);

        relative_user1 = findViewById(R.id.relative_user);
        relative_user2 = findViewById(R.id.relative_user2);
        relative_user3 = findViewById(R.id.relative_user3);
        relative_user4 = findViewById(R.id.relative_user4);

        btn_next = findViewById(R.id.btn_next);
        btn_next.setOnClickListener(this);
        btn_camera = findViewById(R.id.btn_camera);
        btn_camera.setOnClickListener(this);
        btn_gallery = findViewById(R.id.btn_gallery);
        btn_gallery.setOnClickListener(this);


        main_Layout = findViewById(R.id.main_Layout);
        top_layout = findViewById(R.id.top_layout);

        remove_img_user.setOnClickListener(this);
        remove_img_user2.setOnClickListener(this);
        remove_img_user3.setOnClickListener(this);
        remove_img_user4.setOnClickListener(this);

        img_user.setOnClickListener(this);
        img_user2.setOnClickListener(this);
        img_user3.setOnClickListener(this);
        img_user4.setOnClickListener(this);

    }


    @Override
    public void onClick(View view) {
        if (view == back_btn) {
            onBackPressed();
        } else if (view == upload_image) {
        } else if (view == btn_next) {
            if (image_paths.size() <= 0) {
                Toast.makeText(this, "Select minimum 1 image", Toast.LENGTH_SHORT).show();
            } else {
                System.out.println("IMG_PATH_SIZE" + image_paths.size());
                Intent intent = new Intent(this, AddAdActivity.class);
                intent.putExtra("IMAGE1", String.valueOf(image_bitmap_list.size() > 0 ? image_bitmap_list.get(0) : ""));
                intent.putExtra("IMAGE2", image_paths.size() > 1 ? image_paths.get(1) : "");
                intent.putExtra("IMAGE3", image_paths.size() > 2 ? image_paths.get(2) : "");
                intent.putExtra("IMAGE4", image_paths.size() > 3 ? image_paths.get(3) : "");
                startActivityForResult(intent, 112);
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        } else if (view == btn_gallery) {

            if (checkAndRequestPermissions()) {
                Intent intent = new Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(intent, 200);
            } else {
                RequestMultiplePermissionImage();
            }
        } else if (view == btn_camera) {
            if (checkAndRequestPermissions()) {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                File f = new File(android.os.Environment.getExternalStorageDirectory(), "temp.jpg");
                intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
                startActivityForResult(intent, 100);
            } else {
                RequestMultiplePermissionCamera();
            }
        }
    }

    public boolean checkAndRequestPermissions() {
        int OnePermissionResult = ContextCompat.checkSelfPermission(getApplicationContext(), READ_EXTERNAL_STORAGE);
        int TwoPermissionResult = ContextCompat.checkSelfPermission(getApplicationContext(), WRITE_EXTERNAL_STORAGE);
        int ThreePermissionResult = ContextCompat.checkSelfPermission(getApplicationContext(), CAMERA);
        return OnePermissionResult == PackageManager.PERMISSION_GRANTED &&
                TwoPermissionResult == PackageManager.PERMISSION_GRANTED &&
                ThreePermissionResult == PackageManager.PERMISSION_GRANTED;
    }

    private void RequestMultiplePermissionCamera() {
        ActivityCompat.requestPermissions(AddAdActivity1.this, new String[]
                {
                        READ_EXTERNAL_STORAGE,
                        WRITE_EXTERNAL_STORAGE,
                        CAMERA,
                }, RequestPermissionCodeCamera);
    }

    private void RequestMultiplePermissionImage() {
        ActivityCompat.requestPermissions(AddAdActivity1.this, new String[]
                {
                        READ_EXTERNAL_STORAGE,
                        WRITE_EXTERNAL_STORAGE,
                        CAMERA,
                }, RequestPermissionCodeImage);
    }


    public AddAdActivity1() {
        super();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case RequestPermissionCodeImage:
                if (grantResults.length > 0) {
                    boolean ReadinternalPermission = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    boolean WriteExternalStorageStatePermission = grantResults[1] == PackageManager.PERMISSION_GRANTED;
                    boolean CameraStorageStatePermission = grantResults[2] == PackageManager.PERMISSION_GRANTED;
                    if (WriteExternalStorageStatePermission && ReadinternalPermission && CameraStorageStatePermission) {
                        Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(intent, 201);
                    }
                }
                break;
            case RequestPermissionCodeCamera:
                if (grantResults.length > 0) {
                    boolean ReadinternalPermission = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    boolean WriteExternalStorageStatePermission = grantResults[1] == PackageManager.PERMISSION_GRANTED;
                    boolean CameraStorageStatePermission = grantResults[2] == PackageManager.PERMISSION_GRANTED;
                    if (WriteExternalStorageStatePermission && ReadinternalPermission && CameraStorageStatePermission) {
                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        File f = new File(android.os.Environment.getExternalStorageDirectory(), "temp.jpg");
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
                        startActivityForResult(intent, 101);
                    }
                }
                break;
        }

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        System.out.println("Image_Size>>>>" + requestCode);

        if (resultCode == RESULT_OK) {
            if (requestCode == 100) {
                File f = new File(Environment.getExternalStorageDirectory().toString());
                for (File temp : f.listFiles()) {
                    if (temp.getName().equals("temp.jpg")) {
                        f = temp;
                        break;
                    }
                }
                Uri uri = Uri.fromFile(f);
                System.out.println("UUUU" + uri);
                cropImage(uri);
            } else if (requestCode == 101) {
                File f = new File(Environment.getExternalStorageDirectory().toString());
                for (File temp : f.listFiles()) {
                    if (temp.getName().equals("temp.jpg")) {
                        f = temp;
                        break;
                    }
                }
                Uri uri = Uri.fromFile(f);
                System.out.println("UUUU" + uri);
                cropImage(uri);
            } else if (requestCode == 200) {
                Uri selectedImage = data.getData();
                cropImage(selectedImage);
            } else if (requestCode == 201) {
                Uri selectedImage = data.getData();
                cropImage(selectedImage);
            } else if (requestCode == 112) {
                setResult(RESULT_OK);
                finish();
            } else if (resultCode == RESULT_OK && requestCode == UCrop.REQUEST_CROP) {
                final Uri selectedImage = UCrop.getOutput(data);
                System.out.println("Image_Size>>>>" + selectedImage);
                Bitmap bitmap = loadFromUri(selectedImage);
                getDropboxIMGSize(selectedImage);


                long fileSizeInBytes = byteSizeOf(bitmap);
                long fileSizeInKB = fileSizeInBytes / 1024;
                long fileSizeInMB = fileSizeInKB / 1024;
                //System.out.println("Image_Size>>>>" + getImageUri(this, bitmap));

                File file = new File(selectedImage.getPath());
                if (fileSizeInMB > 1) {
                    Toast.makeText(this, "Image size is more than 1MB", Toast.LENGTH_SHORT).show();
                } else {
                    if (image_bitmap_list.size() <= 3) {
                        image_paths.add(file.getAbsolutePath());
                        top_img.setImageBitmap(bitmap);
                        image_bitmap_list.add(bitmap);
                        //updateImages(image_paths, selectedImage);
                        setImages(image_bitmap_list);
                    } else {
                        Toast.makeText(this, "You can select 4 only! ", Toast.LENGTH_SHORT).show();
                    }
                }

            } else if (resultCode == UCrop.RESULT_ERROR) {
                final Throwable cropError = UCrop.getError(data);
                Log.d("tag", "" + cropError.toString());
                Toast.makeText(this, "Error", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public static int byteSizeOf(Bitmap bitmap) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            return bitmap.getAllocationByteCount();
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR1) {
            return bitmap.getByteCount();
        } else {
            return bitmap.getRowBytes() * bitmap.getHeight();
        }
    }

    private void getDropboxIMGSize(Uri uri) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(new File(uri.getPath()).getAbsolutePath(), options);
        int imageHeight = options.outHeight;
        int imageWidth = options.outWidth;

        System.out.println("HEIGHT_WIDTH" + imageHeight);
        System.out.println("HEIGHT_WIDTH" + imageWidth);

    }

    public Bitmap loadFromUri(Uri photoUri) {
        Bitmap bitmap = null;
        Bitmap imageBitmap = null;
        try {
            // check version of Android on device
            if (Build.VERSION.SDK_INT > 27) {
                // on newer versions of Android, use the new decodeBitmap method
                ImageDecoder.Source source = ImageDecoder.createSource(this.getContentResolver(), photoUri);
                bitmap = ImageDecoder.decodeBitmap(source);
            } else {
                // support older versions of Android by using getBitmap
                bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), photoUri);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        imageBitmap = bitmap.copy(Bitmap.Config.ARGB_8888, true);
        return imageBitmap;
    }


    private static final String SAMPLE_CROPPED_IMAGE_NAME = "SampleCropImage";
    private int IMAGE_COMPRESSION = 100;
    private boolean lockAspectRatio = false, setBitmapMaxWidthHeight = false;
    private int ASPECT_RATIO_X = 1, ASPECT_RATIO_Y = 1, bitmapMaxWidth = 500, bitmapMaxHeight = 500;

    private void cropImage(Uri sourceUri) {

        File f = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/Pictures/" + SAMPLE_CROPPED_IMAGE_NAME + ".jpg");

        Uri destinationUri = Uri.fromFile(f);
        UCrop.Options options = new UCrop.Options();
        options.setCompressionQuality(IMAGE_COMPRESSION);
        options.setToolbarColor(ContextCompat.getColor(this, R.color.colorPrimary));
        options.setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimary));
        options.setActiveWidgetColor(ContextCompat.getColor(this, R.color.colorPrimary));
        options.setToolbarTitle("Edit");

        if (!lockAspectRatio)
            options.withAspectRatio(ASPECT_RATIO_X, ASPECT_RATIO_Y);

        if (!setBitmapMaxWidthHeight)
            options.withMaxResultSize(bitmapMaxWidth, bitmapMaxHeight);

        UCrop.of(sourceUri, destinationUri)
                .withOptions(options)
                .start(this);
    }

    private void setImages(List<Bitmap> bitmap) {
        ImageAdapter imageAdapter = new ImageAdapter(this, bitmap);
        recycle_view.setLayoutManager(new LinearLayoutManager(this,
                LinearLayoutManager.HORIZONTAL, false));
        recycle_view.setAdapter(imageAdapter);

        if (bitmap.size() > 0) {
            btn_next.setVisibility(View.VISIBLE);
            top_img.setVisibility(View.VISIBLE);
        } else {
            btn_next.setVisibility(View.GONE);
            top_img.setVisibility(View.GONE);
        }
    }

    @Override
    public void itemClick(int position) {
        System.out.println("Remove item");
        image_bitmap_list.remove(position);
        image_paths.remove(position);
        setImages(image_bitmap_list);
    }

    @Override
    public void itemClickAll(int position) {
        System.out.println("itemAll");
        if (image_bitmap_list.size() > 0) {
            top_img.setImageBitmap(image_bitmap_list.get(position));
        }
    }
}