package com.loklvokl.lokl.activtiy;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.loklvokl.lokl.R;

public class OrderSuccessActivity extends AppCompatActivity {


    TextView txt_book, date;
    ImageView back_btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_success);


        back_btn = findViewById(R.id.back_btn);
        txt_book = findViewById(R.id.txt_book);
        date = findViewById(R.id.date);

        date.setText(getIntent().getStringExtra("date"));
        txt_book.setText("Your Order is Booked With \n ID : " + getIntent().getStringExtra("order_id"));

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });

        Handler handler = new Handler();

        handler.postDelayed(new Runnable() {
            public void run() {
                onBackPressed();
            }
        }, 8000);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

}