package com.loklvokl.lokl.activtiy;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.phone.SmsRetriever;
import com.google.android.gms.auth.api.phone.SmsRetrieverClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.loklvokl.lokl.R;
import com.loklvokl.lokl.model.CheckLogin;
import com.loklvokl.lokl.model.DysplayProfileModel;
import com.loklvokl.lokl.model.GetOtp;
import com.loklvokl.lokl.model.SignUpResponse;
import com.loklvokl.lokl.model.User;
import com.loklvokl.lokl.model.UserProfile;
import com.loklvokl.lokl.receiver.AppSignatureHashHelper;
import com.loklvokl.lokl.receiver.SMSReceiver;
import com.loklvokl.lokl.util.ApiClient;
import com.loklvokl.lokl.util.ApiInterface;
import com.loklvokl.lokl.util.Constant;
import com.loklvokl.lokl.util.MyCountDownTimer;
import com.loklvokl.lokl.util.Progress;
import com.loklvokl.lokl.util.SharedPref;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EnterOtpActivity extends AppCompatActivity implements View.OnClickListener, SMSReceiver.OTPReceiveListener {

    private ImageView back_btn;
    private TextView title_text, tv_resend_otp, title_text_details, tv_otp_secods;
    private EditText et_otp_one, et_otp_two, et_otp_three, et_otp_four;
    private ConstraintLayout cl_verify;
    private SMSReceiver smsReceiver;
    private String number, otp;

    private ProgressDialog progressDialog;
    private ApiInterface apiService;
    private SharedPref sharedPref;
    private MyCountDownTimer myCountDownTimer;

    private String fcm_token;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enter_otp);

        bind();
        init();
        listner();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        myCountDownTimer.onFinish();
        if (smsReceiver != null) {
            unregisterReceiver(smsReceiver);
        }
    }

    private void init() {

        apiService = ApiClient.getClient().create(ApiInterface.class);
        sharedPref = new SharedPref(this);

        number = getIntent().getStringExtra("number");
        otp = getIntent().getStringExtra("otp");
        title_text_details.setText("Sit back and relax while we verify your phone number +91 " + number);

        AppSignatureHashHelper appSignatureHashHelper = new AppSignatureHashHelper(this);
        Log.e("TAG", "init: " + appSignatureHashHelper.getAppSignatures().get(0));
        getFCMToken();
        startSMSListener();
    }

    private void getFCMToken() {
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            return;
                        }
                        // Get the Instance ID token //
                        String token = task.getResult().getToken();
                        sharedPref.setStr("FCM_TOKEN", token);
                        String msg = getString(R.string.msg_token_fmt, token);
                        Log.d("TAG", msg);

                    }
                });
    }

    private void startSMSListener() {

        try {
            smsReceiver = new SMSReceiver();
            smsReceiver.setOTPListener(this);

            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(SmsRetriever.SMS_RETRIEVED_ACTION);
            this.registerReceiver(smsReceiver, intentFilter);

            SmsRetrieverClient client = SmsRetriever.getClient(this);

            Task<Void> task = client.startSmsRetriever();
            task.addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    // API successfully started
                }
            });

            task.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    // Fail to start API
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void bind() {

        back_btn = findViewById(R.id.back_btn);
        cl_verify = findViewById(R.id.cl_verify);
        title_text = findViewById(R.id.title_text);
        tv_resend_otp = findViewById(R.id.tv_resend_otp);
        title_text.setText("");

        et_otp_one = findViewById(R.id.et_otp_one);
        tv_otp_secods = findViewById(R.id.tv_otp_secods);
        et_otp_two = findViewById(R.id.et_otp_two);
        et_otp_three = findViewById(R.id.et_otp_three);
        et_otp_four = findViewById(R.id.et_otp_four);
        title_text_details = findViewById(R.id.title_text_details);


    }

    private void listner() {
        cl_verify.setOnClickListener(this);
        back_btn.setOnClickListener(this);
        tv_resend_otp.setOnClickListener(this);

        myCountDownTimer = new MyCountDownTimer(60 * 1000, 1000);
        myCountDownTimer.setSourceActivity(this);
        myCountDownTimer.start();

        et_otp_one.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.toString().length() == 1) {
                    et_otp_two.requestFocus();
                }
            }
        });

        et_otp_two.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.toString().length() == 1) {
                    et_otp_three.requestFocus();
                } else if (editable.toString().length() == 0) {
                    et_otp_one.requestFocus();
                }
            }
        });

        et_otp_three.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.toString().length() == 1) {
                    et_otp_four.requestFocus();
                } else if (editable.toString().length() == 0) {
                    et_otp_two.requestFocus();
                }
            }
        });

        et_otp_four.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                if (editable.toString().length() == 1) {
                    cl_verify.callOnClick();
                } else if (editable.toString().length() == 0) {
                    et_otp_three.requestFocus();
                }
            }
        });

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.tv_resend_otp:
                getOtp(number);
                break;

            case R.id.cl_verify:

                String one = et_otp_one.getText().toString().trim();
                String two = et_otp_two.getText().toString().trim();
                String three = et_otp_three.getText().toString().trim();
                String four = et_otp_four.getText().toString().trim();

                if (!TextUtils.isEmpty(one) && !TextUtils.isEmpty(two) && !TextUtils.isEmpty(three) && !TextUtils.isEmpty(four)) {
                    String s = one + two + three + four;
                    if (s.equals(otp)) {
                        Toast.makeText(this, "Otp varify successfully", Toast.LENGTH_SHORT).show();
                        myCountDownTimer.onFinish();
                        checkLogin(number.replace("+91", ""));
                    } else {
                        Toast.makeText(this, "please enter valid otp", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(this, "please enter valid otp", Toast.LENGTH_SHORT).show();
                }

                break;
            case R.id.back_btn:

                onBackPressed();

                break;
        }
    }

    private void getOtp(String mobile) {
        progressDialog = null;
        progressDialog = Progress.showProgressDialog(this, "please wait.", false);
        progressDialog.show();
        Call<GetOtp> call = apiService.sendOTP(mobile);

        call.enqueue(new Callback<GetOtp>() {
            @Override
            public void onResponse(Call<GetOtp> call, Response<GetOtp> response) {

                GetOtp body = response.body();
                if (body.getResponse().get(0).getError()) {
                    Toast.makeText(EnterOtpActivity.this, body.getResponse().get(0).getMessage(), Toast.LENGTH_SHORT).show();
                } else {

                    clearOtpView();
                    myCountDownTimer.start();
                    otp = String.valueOf(body.getResponse().get(0).getOtp());

                }

                if (progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

            }

            @Override
            public void onFailure(Call<GetOtp> call, Throwable t) {

                if (progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                Toast.makeText(EnterOtpActivity.this, "Fail! Try again.", Toast.LENGTH_SHORT).show();
            }
        });

    }


    private void checkLogin(String number) {

        fcm_token = sharedPref.getStr("FCM_TOKEN");
        Log.d("fcm_token : "," - - "+fcm_token);
        progressDialog = null;
        progressDialog = Progress.showProgressDialog(this, "please wait.", false);
        progressDialog.show();

        Call<CheckLogin> call = apiService.checkLogin(number, Constant.getDeviceId(this), fcm_token);

        call.enqueue(new Callback<CheckLogin>() {
            @Override
            public void onResponse(Call<CheckLogin> call, Response<CheckLogin> response) {
                if (progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                CheckLogin body = response.body();
                if (body.getResponse().get(0).isRegister()) {
                    String userId = body.getResponse().get(0).getUser_id();

//                    sharedPref.setStr("MOBILE", number);
//                    sharedPref.setStr("ID", userId);
//                    sharedPref.setBool("IS_LOGIN", true);
//
//                    Intent i = new Intent(EnterOtpActivity.this, MainActivity.class);
//                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                    startActivity(i);
//                    finish();
                    displayProfile(userId);
                } else {
                    registerUser(number);
                }
            }

            @Override
            public void onFailure(Call<CheckLogin> call, Throwable t) {

                if (progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                Toast.makeText(EnterOtpActivity.this, "Fail! Try again.", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void displayProfile(String user_id) {

        progressDialog = null;
        progressDialog = Progress.showProgressDialog(this, "please wait.", false);
        progressDialog.show();

        Call<DysplayProfileModel> call = apiService.diplayProfile(user_id);

        call.enqueue(new Callback<DysplayProfileModel>() {
            @Override
            public void onResponse(Call<DysplayProfileModel> call, Response<DysplayProfileModel> response) {
                DysplayProfileModel body = response.body();

                if (body.getUserProfile() != null) {
                    if (body.getUserProfile().getUserId() != null) {

//                        Toast.makeText(SplashActivity.this, body., Toast.LENGTH_SHORT).show();
                        UserProfile user = body.getUserProfile();
                        sharedPref.setStr("MOBILE", user.getMobile());
                        sharedPref.setStr("NAME", user.getUsername());
                        sharedPref.setStr("LNAME", user.getLastName());
                        sharedPref.setStr("EMAIL", user.getEmail());
                        sharedPref.setStr("IMAGE", user.getProfileImage());
                        sharedPref.setStr("ID", user.getUserId());
                        sharedPref.setBool("IS_LOGIN", true);
                        sharedPref.setStr("CITY", user.getCity());
                        sharedPref.setStr("GENDER", user.getGender());
                        sharedPref.setStr("DOB", user.getDob());
                        sharedPref.setStr("FNAME", user.getFirstName());
                        sharedPref.setAdharList("ADHAR_LIST", user.getAadharDocument());
                        sharedPref.setPanList("PAN_LIST", user.getPanDocument());
                        sharedPref.setInt("PAN_COUNT", user.getPan_counter());
                        sharedPref.setInt("ADHAR_COUNT", user.getAddhar_counter());
                        // sharedPref.set("FNAME", user.getKycDetails().get);


                        Intent i = new Intent(EnterOtpActivity.this, MainActivity.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(i);
                        finishAffinity();

                    }
                }


                if (progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

            }

            @Override
            public void onFailure(Call<DysplayProfileModel> call, Throwable t) {
                Log.e("TAG", t.toString());

                if (progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
            }
        });
    }

    private void registerUser(String number) {

        progressDialog = null;
        progressDialog = Progress.showProgressDialog(this, "please wait.", false);
        progressDialog.show();

        Call<SignUpResponse> call = apiService.requestSignup("", "", "", number, number, Constant.getDeviceId(this));

        call.enqueue(new Callback<SignUpResponse>() {
            @Override
            public void onResponse(Call<SignUpResponse> call, Response<SignUpResponse> response) {
                if (progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                SignUpResponse body = response.body();

                if (body.getResponse() != null && body.getResponse().size() > 0) {
                    Toast.makeText(EnterOtpActivity.this, body.getResponse().get(0).getMessage(), Toast.LENGTH_SHORT).show();

                    if (!body.getResponse().get(0).getError()) {

                        User user = body.getResponse().get(0).getUser().get(0);

                        sharedPref.setStr("MOBILE", user.getMobile());
                        sharedPref.setStr("ID", user.getId());
                        sharedPref.setBool("IS_LOGIN", true);

                        Intent i = new Intent(EnterOtpActivity.this, MainActivity.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(i);
                        finish();
                    }
                }
            }

            @Override
            public void onFailure(Call<SignUpResponse> call, Throwable t) {
                if (progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                Toast.makeText(EnterOtpActivity.this, "Fail! Try again.", Toast.LENGTH_SHORT).show();
            }
        });

    }


    private void verifyOtp(String mobile, String otp) {
        progressDialog = null;
        progressDialog = Progress.showProgressDialog(this, "please wait.", false);
        progressDialog.show();
        Call<GetOtp> call = apiService.verifyOtp(mobile, otp);

        call.enqueue(new Callback<GetOtp>() {
            @Override
            public void onResponse(Call<GetOtp> call, Response<GetOtp> response) {
                Log.d("TAG", "data: " + response.body());

                GetOtp otp = response.body();
                if (otp.getResponse().get(0).getError()) {
                    Toast.makeText(EnterOtpActivity.this, otp.getResponse().get(0).getMessage(), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(EnterOtpActivity.this, otp.getResponse().get(0).getMessage(), Toast.LENGTH_SHORT).show();
//                    Intent intent = new Intent(EnterOtpActivity.this, EnterOtpActivity.class);
//                    intent.putExtra("number", ed_mobile.getText().toString());
//                    startActivity(intent);
                }


                if (progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

            }

            @Override
            public void onFailure(Call<GetOtp> call, Throwable t) {


                if (progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                Toast.makeText(EnterOtpActivity.this, "Fail! Try again.", Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public void onOTPReceived(String otp) {
        if (smsReceiver != null) {
            String[] chars = new String[otp.length()];
            for (int i = 0; i < otp.length(); i++) {
                chars[i] = Character.toString(otp.charAt(i));
            }
            et_otp_one.setText(chars[0]);
            et_otp_two.setText(chars[1]);
            et_otp_three.setText(chars[2]);
            et_otp_four.setText(chars[3]);

            myCountDownTimer.onFinish();
            smsReceiver = null;
        }
    }

    @Override
    public void onOTPTimeOut() {

    }

    @Override
    public void onOTPReceivedError(String error) {

    }

    public void onCountDownTimerFinishEvent() {
        tv_otp_secods.setVisibility(View.GONE);
        tv_resend_otp.setVisibility(View.VISIBLE);
    }

    public void onCountDownTimerTickEvent(long millisUntilFinished) {

        tv_resend_otp.setVisibility(View.GONE);
        tv_otp_secods.setVisibility(View.VISIBLE);

        long leftSeconds = millisUntilFinished / 1000;

        String sendButtonText = null;

        if (leftSeconds == 0) {

        } else {
            sendButtonText = "Request a new OTP in " + leftSeconds + " seconds";
            tv_otp_secods.setText(sendButtonText);
        }
    }

    private void clearOtpView() {
        et_otp_one.setText("");
        et_otp_two.setText("");
        et_otp_three.setText("");
        et_otp_four.setText("");
    }


}