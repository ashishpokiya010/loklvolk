package com.loklvokl.lokl.activtiy;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.loklvokl.lokl.R;
import com.loklvokl.lokl.model.Booking;
import com.loklvokl.lokl.model.Order;
import com.loklvokl.lokl.util.ApiClient;
import com.loklvokl.lokl.util.ApiInterface;
import com.loklvokl.lokl.util.Constant;
import com.loklvokl.lokl.util.SharedPref;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PaymentActivity extends AppCompatActivity implements View.OnClickListener {


    private static final String TAG = "payment";
    ApiInterface apiService;
    ApiClient apiClient;

    ImageView back_btn;
    TextView title_text;
    RadioButton radio1, radio2, radio3, radio4;
    Order order = new Order();
    String payment_type;
    Button btn_continue;
    SharedPref sharedPref;
    ProgressBar progressBar;
    TextView txt_total, txt_price, txt_price_title, txt_delivery, txt_discount;
    RelativeLayout top_layout, layout_discount;
    private static final int ATTRIB_ORDER = 101;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);

        sharedPref = new SharedPref(this);
        apiClient = new ApiClient();
        apiService = ApiClient.getClient().create(ApiInterface.class);
        init();
        getIntentData();

    }

    private void getIntentData() {
        order = (Order) getIntent().getSerializableExtra("ORDER");

        txt_price.setText(order.getPrice());
        txt_delivery.setText(order.getShipppingCharge());
        txt_total.setText(order.getTotal_price());
        txt_discount.setText(order.getDiscount());
        layout_discount.setVisibility(order.getDiscount() != null &&
                !order.getDiscount().equals("0") ? View.VISIBLE : View.GONE);
        txt_price_title.setText("Price (" + order.getTotal_qty() + " item)");

    }

    private void init() {
        progressBar = findViewById(R.id.progressBar);
        btn_continue = findViewById(R.id.btn_continue);
        btn_continue.setOnClickListener(this);
        back_btn = findViewById(R.id.back_btn);
        back_btn.setOnClickListener(this);
        title_text = findViewById(R.id.title_text);
        title_text.setText("Payment Method");

        radio1 = findViewById(R.id.radio1);
        radio1.setOnClickListener(this);
        radio2 = findViewById(R.id.radio2);
        radio2.setOnClickListener(this);
        radio3 = findViewById(R.id.radio3);
        radio3.setOnClickListener(this);
        radio4 = findViewById(R.id.radio4);
        radio4.setOnClickListener(this);

        txt_total = findViewById(R.id.txt_total);
        txt_price = findViewById(R.id.txt_price);
        txt_price_title = findViewById(R.id.txt_price_title);
        txt_delivery = findViewById(R.id.txt_delivery);
        txt_discount = findViewById(R.id.txt_discount);
        layout_discount = findViewById(R.id.layout_discount);

    }

    @Override
    public void onClick(View view) {
        if (view == back_btn) {
            finish();

        } else if (view == btn_continue) {
            permormOrder();
        } else if (view == radio4) {
            btn_continue.setEnabled(true);
            btn_continue.setAlpha(1.0f);
            payment_type = "COD";

        } else {
            btn_continue.setEnabled(false);
            btn_continue.setAlpha(0.5f);
            payment_type = "";
            displayAlert();
        }
    }

    private void permormOrder() {
        order.setPayment_type(payment_type);
        order.setUser_id(sharedPref.getStr("ID"));
        order.setDevice_id(Constant.getDeviceId(this));

        progressBar.setVisibility(View.VISIBLE);
        apiService = apiClient.getClient().create(ApiInterface.class);

        Call<Booking> call = apiService.performPlaceOrder(order.getUser_id(), order.getAddress_id(), order.getTotal_qty(),
                order.getPrice(), order.getDiscount(), order.getTotal_price(), order.getPayment_type(), order.getDevice_id(), order.getPromo_code());
        apiClient.enqueue(call, new Callback<Booking>() {
            @Override
            public void onResponse(Call<Booking> call, Response<Booking> response) {
                Log.d(TAG, "data: " + new Gson().toJson(response.body()));
                progressBar.setVisibility(View.GONE);
                Booking booking = response.body();

                if (booking != null && !booking.getBooking().get(0).getError()) {
                    Toast.makeText(PaymentActivity.this, booking.getBooking().get(0).getMessage(), Toast.LENGTH_SHORT).show();

                    callSuccessOrder(booking.getBooking().get(0).getOrderId(), booking.getBooking().get(0).getDeliveryEstimatedDate());
                } else {
                    Toast.makeText(PaymentActivity.this, booking != null ? booking.getBooking().get(0).getMessage() : "", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<Booking> call, Throwable t) {
                // Log error here since request failed
                Log.e(TAG, t.toString());
            }
        });

    }

    private void callSuccessOrder(Integer orderId, String deliveryEstimatedDate) {
        Intent intent = new Intent(this, OrderSuccessActivity.class);
        intent.putExtra("order_id", orderId + "");
        intent.putExtra("date", deliveryEstimatedDate);
        startActivityForResult(intent, ATTRIB_ORDER);

    }

    private void displayAlert() {
        new AlertDialog.Builder(this)
//                .setTitle("Alert")
                .setMessage("Coming Soon..")
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ATTRIB_ORDER) {
            sharedPref.setInt("CART_COUNT", 0);
            setResult(RESULT_OK);

            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        }
    }
}