package com.loklvokl.lokl.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.loklvokl.lokl.R;
import com.loklvokl.lokl.model.Address;

import java.util.List;

public class AddressAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<Address> products;
    Context context;
    AddressInterface aInterface;

    public AddressAdapter(Context context, List<Address> data) {
        this.products = data;
        this.context = context;
        aInterface = (AddressInterface) context;
    }

    @Override
    public int getItemCount() {
        return products.size();
    }


    public class ViewHolder2 extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView text_name, text_mobile, text_email, text_address1, text_address2, text_address3, text_edit;
        RadioButton radio_button;

        public ViewHolder2(View itemView) {
            super(itemView);
            this.text_name = itemView.findViewById(R.id.text_name);
            this.text_mobile = itemView.findViewById(R.id.text_mobile);
            this.text_email = itemView.findViewById(R.id.text_email);
            this.text_address1 = itemView.findViewById(R.id.text_address1);
            this.text_address2 = itemView.findViewById(R.id.text_address2);
            this.text_address3 = itemView.findViewById(R.id.text_address3);
            this.radio_button = itemView.findViewById(R.id.radio_button);
            this.text_edit = itemView.findViewById(R.id.text_edit);
            this.text_edit.setOnClickListener(this);

            this.radio_button.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (view == text_edit) {
                aInterface.onEditClick(getAdapterPosition());
            } else {
                aInterface.onItemClick(getAdapterPosition());
            }
        }
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view1 = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.address_item, parent, false);
        return new ViewHolder2(view1);

    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

        ViewHolder2 holder2 = (ViewHolder2) holder;
        Address address = products.get(position);

        holder2.text_name.setText(address.getFullName());
        holder2.text_mobile.setText(address.getMobile());
        holder2.text_email.setText(address.getEmail());
        holder2.text_address1.setText(address.getArea());
        holder2.text_address2.setText(address.getLandmark());
        holder2.text_address3.setText(address.getCity() + ", " + address.getState() + ", " + address.getPincode());

        holder2.radio_button.setChecked(address.getSelected());

    }

    public interface AddressInterface {
        void onItemClick(int adapterPosition);

        void onEditClick(int adapterPosition);
    }

}
