package com.loklvokl.lokl.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import com.loklvokl.lokl.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class PanCardAdapter extends RecyclerView.Adapter<PanCardAdapter.MyViewHolder> {

    private List<String> imageSet;
    Context context;
    PanCardAdapter.AdInterface adInterface;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView imageViewUser, imageViewRemove;
        RelativeLayout relativeLayout;
        ProgressBar progressBar;

        public MyViewHolder(View itemView) {
            super(itemView);
            this.imageViewUser = itemView.findViewById(R.id.img_user);
            this.imageViewRemove = itemView.findViewById(R.id.remove_img_user);
            this.relativeLayout = itemView.findViewById(R.id.relative_user_item);
            this.progressBar = itemView.findViewById(R.id.progressBar);
            this.imageViewRemove.setVisibility(View.GONE);

            imageViewRemove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    popupRemove(getAdapterPosition());
                    //adInterface.itemClickPan(getAdapterPosition());
                }
            });
            relativeLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    adInterface.itemClickPanAll(getAdapterPosition());
                }
            });
        }
    }

    public PanCardAdapter(Context context, List<String> data) {
        this.imageSet = data;
        this.context = context;
        this.adInterface = (PanCardAdapter.AdInterface) context;
    }

    @Override
    public PanCardAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent,
                                                          int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.image_item_adhar, parent, false);

        PanCardAdapter.MyViewHolder myViewHolder = new PanCardAdapter.MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final PanCardAdapter.MyViewHolder holder, final int position) {

       /* URL url = null;
        Bitmap image = null;
        try {
            url = new URL("https://" + imageSet.get(position).getAadharImage());
            image = BitmapFactory.decodeStream(url.openConnection().getInputStream());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        holder.imageViewUser.setImageBitmap(image);
        holder.imageViewRemove.setVisibility(View.VISIBLE);
        holder.progressBar.setVisibility(View.GONE);
*/
        if (imageSet.get(position).endsWith(".jpg") || imageSet.get(position).endsWith(".png")) {
            Picasso.get().load("https://" + imageSet.get(position))
                    .resize(0, 250)
                    .into(holder.imageViewUser, new com.squareup.picasso.Callback() {
                        @Override
                        public void onSuccess() {
                            holder.imageViewRemove.setVisibility(View.VISIBLE);
                            holder.progressBar.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError(Exception e) {
                            holder.imageViewRemove.setVisibility(View.VISIBLE);
                            holder.progressBar.setVisibility(View.GONE);
                        }
                    });
        } else {
            System.out.println("WIWIIWI");
            Picasso.get().load(imageSet.get(position))
                    .resize(0, 250)
                    .into(holder.imageViewUser, new com.squareup.picasso.Callback() {
                        @Override
                        public void onSuccess() {
                            holder.imageViewRemove.setVisibility(View.VISIBLE);
                            holder.progressBar.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError(Exception e) {
                            holder.imageViewRemove.setVisibility(View.VISIBLE);
                            holder.progressBar.setVisibility(View.GONE);
                        }
                    });
        }
    }

    @Override
    public int getItemCount() {
        return imageSet.size();
    }

    public void popupRemove(int position){
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setMessage("Remove document from profile?");
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Remove",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        adInterface.itemClickPan(position);
                    }
                });
        alertDialog.show();
    }

    public interface AdInterface {
        void itemClickPan(int position);

        void itemClickPanAll(int position);
    }
}


