package com.loklvokl.lokl.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.ConstraintSet;
import androidx.recyclerview.widget.RecyclerView;

import com.loklvokl.lokl.activtiy.DealListActivity;
import com.loklvokl.lokl.activtiy.OffersActivity;
import com.loklvokl.lokl.activtiy.ProductDetailActivity;
import com.loklvokl.lokl.R;
import com.loklvokl.lokl.model.Product;
import com.loklvokl.lokl.util.RoundedCornersTransformation;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.squareup.picasso.Transformation;

import java.util.List;

public class ProductDealAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    List<Product> products;
    Context context;
    Boolean isDeals;

    public ProductDealAdapter(Context context, List<Product> data, Boolean isDeals) {
        this.products = data;
        this.context = context;
        this.isDeals = isDeals;
        Log.d("ProductDeal : ","Here  "+products.size());
    }

    public void sortData(List<Product> data){
        this.products.clear();
        this.products.addAll(data);
        this.notifyDataSetChanged();
        Log.d("onScrolled sort : ","Here "+this.products.size()+"  - "+data.size());
    }

    @Override
    public int getItemCount() {
        return products.size();
    }


    public class ViewHolder2 extends RecyclerView.ViewHolder {
        TextView textViewName;
        TextView textViewPrice, textViewPrice0, textOff;
        ProgressBar progressBar;
        ImageView imageViewIcon, image_offer;
        ConstraintLayout parentContsraint;
        LinearLayout main_Layout;

        public ViewHolder2(View itemView) {
            super(itemView);
            this.textViewName = itemView.findViewById(R.id.textView);
            this.textViewPrice = itemView.findViewById(R.id.textViewPrice);
            this.textViewPrice0 = itemView.findViewById(R.id.textViewPrice0);
            this.progressBar = itemView.findViewById(R.id.progressBar);
            this.textOff = itemView.findViewById(R.id.textOff);
            this.main_Layout = itemView.findViewById(R.id.main_Layout);

            this.imageViewIcon = itemView.findViewById(R.id.image);
            this.image_offer = itemView.findViewById(R.id.image_offer);
            this.parentContsraint = itemView.findViewById(R.id.parentContsraint);
            if (!isDeals) {
                this.imageViewIcon.setVisibility(View.GONE);
                this.parentContsraint.setVisibility(View.VISIBLE);
                this.image_offer.setVisibility(View.VISIBLE);
            } else {
                this.parentContsraint.setVisibility(View.GONE);
                this.image_offer.setVisibility(View.GONE);
                this.imageViewIcon.setVisibility(View.VISIBLE);
            }
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    openProductDetail(products.get(getAdapterPosition()).getProductId());
                }
            });
        }
    }

    private void openProductDetail(String productId) {
        Intent intent = new Intent(context, ProductDetailActivity.class);
        intent.putExtra("ID", productId);
        context.startActivity(intent);
    }

    class ViewHolder0 extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView seeall_text;
        TextView title_text;
        RelativeLayout main_Layout, banner_layout;
        LinearLayout seeall_layout;
        ImageView imageView;
        ProgressBar progressBar;

        public ViewHolder0(View itemView) {
            super(itemView);
            this.seeall_text = itemView.findViewById(R.id.seeall_text);
            this.title_text = itemView.findViewById(R.id.title_text);
            this.main_Layout = itemView.findViewById(R.id.main_Layout);
            this.banner_layout = itemView.findViewById(R.id.banner_layout);
            this.imageView = itemView.findViewById(R.id.imageView);
            this.progressBar = itemView.findViewById(R.id.progressBar);
            this.seeall_layout = itemView.findViewById(R.id.seeall_layout);

            banner_layout.setOnClickListener(this);
            seeall_text.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (view == banner_layout) {
                Intent intent = new Intent(context, OffersActivity.class);
                intent.putExtra("BANNER_ID", products.get(getAdapterPosition()).getBannar_id());
                context.startActivity(intent);
            } else if (view == seeall_text) {
                Intent intent = new Intent(context, DealListActivity.class);
                intent.putExtra("BANNER_ID", products.get(getAdapterPosition()).getBannar_id());
                intent.putExtra("TITLE", products.get(getAdapterPosition()).getProductName());
                context.startActivity(intent);
            }
        }
    }

    @Override
    public int getItemViewType(int position) {
        try {
            return Integer.parseInt(products.get(position).getProductId());
        } catch (NumberFormatException e) {
            return 1;
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Log.d("ProductDeal : ","Here  "+viewType);
        if (viewType == 0) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.productdeal_title, parent, false);
            return new ViewHolder0(view);
        }
        View view1 = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.productdeal_item, parent, false);
        return new ViewHolder2(view1);
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        if (holder.getItemViewType() == 0) {
            ViewHolder0 holder0 = (ViewHolder0) holder;
            Product product0 = products.get(position);

            if (product0.getBannar_image().isEmpty()) {
                holder0.title_text.setText(product0.getProductName());
                holder0.main_Layout.setBackgroundColor(Color.parseColor(product0.getColor()));

                holder0.banner_layout.setVisibility(View.GONE);
                holder0.main_Layout.setVisibility(View.VISIBLE);

                if (product0.getBannar_id().isEmpty())
                    holder0.seeall_layout.setVisibility(View.GONE);
                else
                    holder0.seeall_layout.setVisibility(View.VISIBLE);

                if (product0.getProductName().isEmpty())
                    holder0.title_text.setVisibility(View.GONE);
                else
                    holder0.title_text.setVisibility(View.VISIBLE);

            } else {
                holder0.banner_layout.setVisibility(View.VISIBLE);
                holder0.main_Layout.setVisibility(View.GONE);

                holder0.progressBar.setVisibility(View.VISIBLE);
                Picasso.get().load("https://" + product0.getBannar_image())
                        .resize(0, 250)
                        .into(holder0.imageView, new Callback() {
                            @Override
                            public void onSuccess() {
                                holder0.progressBar.setVisibility(View.GONE);
                            }

                            @Override
                            public void onError(Exception e) {
                                holder0.progressBar.setVisibility(View.GONE);
                            }
                        });
            }
        } else {
            ViewHolder2 holder2 = (ViewHolder2) holder;
            Product product = products.get(position);

            Log.d("ProductDeal : "," - - "+product.getProductId());
            holder2.main_Layout.setBackgroundColor(Color.parseColor(products.get(position).getColor()));
            holder2.textViewName.setText(product.getProductName());
            holder2.textViewPrice.setText("Rs." + product.getProductSellingPrice());
            holder2.textViewPrice0.setText("Rs." + product.getProductMrp());
            holder2.textViewPrice0.setPaintFlags(holder2.textViewPrice0.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            holder2.textOff.setText("(" + product.getOff() + " OFF)");

            if (isDeals) {
                Picasso.get().load("https://" + product.getProductImage())
                        .resize(0, 250)
                        .into(holder2.imageViewIcon, new Callback() {
                            @Override
                            public void onSuccess() {
                                holder2.progressBar.setVisibility(View.GONE);
                            }

                            @Override
                            public void onError(Exception e) {
                                holder2.progressBar.setVisibility(View.GONE);
                            }
                        });
            } else {
                Log.d("isDeals : ","- - "+product.getProductImage());
                final Transformation transformation = new RoundedCornersTransformation();
                Picasso.get().load("https://" + product.getProductImage())
                        .transform(transformation)
                        .into(holder2.image_offer, new Callback() {
                            @Override
                            public void onSuccess() {
                                holder2.progressBar.setVisibility(View.GONE);

                                //Drawable drawable = holder2.image_offer.getDrawable();
                                Bitmap bitmap = drawableToBitmap(holder2.image_offer.getDrawable());
                                String ratio = String.format("%d:%d", bitmap.getWidth(), bitmap.getHeight());
                                ConstraintSet set = new ConstraintSet();
                                set.clone(holder2.parentContsraint);
                                set.setDimensionRatio(holder2.image_offer.getId(), ratio);
                                set.applyTo(holder2.parentContsraint);
                            }

                            @Override
                            public void onError(Exception e) {
                                holder2.progressBar.setVisibility(View.GONE);
                            }
                        });
                        /*.into(new Target(){
                            @Override
                            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                                holder2.image_offer.setImageBitmap(bitmap);
                                holder2.progressBar.setVisibility(View.GONE);
                                String ratio = String.format("%d:%d", bitmap.getWidth(), bitmap.getHeight());
                                Log.d("ratio : ", "" + ratio);
                                ConstraintSet set = new ConstraintSet();
                                set.clone(holder2.parentContsraint);
                                set.setDimensionRatio(holder2.image_offer.getId(), ratio);
                                set.applyTo(holder2.parentContsraint);
                            }

                            @Override
                            public void onBitmapFailed(Exception e, Drawable errorDrawable) {
                                holder2.progressBar.setVisibility(View.GONE);
                            }

                            @Override
                            public void onPrepareLoad(Drawable placeHolderDrawable) {

                            }
                        });*/
            }
        }
    }

    public static Bitmap drawableToBitmap (Drawable drawable) {
        Bitmap bitmap = null;

        if (drawable instanceof BitmapDrawable) {
            BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
            if(bitmapDrawable.getBitmap() != null) {
                return bitmapDrawable.getBitmap();
            }
        }

        if(drawable.getIntrinsicWidth() <= 0 || drawable.getIntrinsicHeight() <= 0) {
            bitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888); // Single color bitmap will be created of 1x1 pixel
        } else {
            bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        }

        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);
        return bitmap;
    }

}
