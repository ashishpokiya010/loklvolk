package com.loklvokl.lokl.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.loklvokl.lokl.R;
import com.loklvokl.lokl.model.CatItemInterface;
import com.loklvokl.lokl.model.SubCategory;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.List;

public class AllSubCategoryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<SubCategory> products;
    Context context;
    public CatItemInterface itemInterface;


    public AllSubCategoryAdapter(Context context, List<SubCategory> data) {
        this.products = data;
        this.context = context;
        itemInterface = (CatItemInterface) context;
    }

    @Override
    public int getItemCount() {
        return products.size();
    }


    public class ViewHolder2 extends RecyclerView.ViewHolder {
        TextView textViewName;
        TextView textViewPrice, textViewPrice0, textOff;
        ProgressBar progressBar;
        ImageView imageViewIcon;

        public ViewHolder2(View itemView) {
            super(itemView);
            this.textViewName = itemView.findViewById(R.id.textView);
            this.textViewPrice = itemView.findViewById(R.id.textViewPrice);
            this.textViewPrice0 = itemView.findViewById(R.id.textViewPrice0);
            this.progressBar = itemView.findViewById(R.id.progressBar);
            this.imageViewIcon = itemView.findViewById(R.id.image);
            this.textOff = itemView.findViewById(R.id.textOff);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    itemInterface.ItemClick(getAdapterPosition(), products.get(getAdapterPosition()).getSubCatId(), products.get(getAdapterPosition()).getSubCatName());
                }
            });
        }
    }

    class ViewHolder0 extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView seeall_text;
        TextView title_text;
        TextView line_text;

        public ViewHolder0(View itemView) {
            super(itemView);
            this.seeall_text = itemView.findViewById(R.id.seeall_text);
            this.title_text = itemView.findViewById(R.id.title_text);
            this.line_text = itemView.findViewById(R.id.line_text);
            seeall_text.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            itemInterface.SeeAllClick(getAdapterPosition(), products.get(getAdapterPosition()).getSubCatId(), products.get(getAdapterPosition()).getSubCatName());
        }
    }

    @Override
    public int getItemViewType(int position) {
        return Integer.parseInt(products.get(position).getId());
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case 0:
                View view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.allcat_title, parent, false);
                return new ViewHolder0(view);
            default:
                View view1 = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.allsubcat_item, parent, false);
                return new ViewHolder2(view1);
        }
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        switch (holder.getItemViewType()) {
            case 0:
                ViewHolder0 holder0 = (ViewHolder0) holder;
                holder0.title_text.setText(products.get(position).getSubCatName());
                if (position == 0) {
                    holder0.line_text.setVisibility(View.GONE);
                }
                break;

            default:
                ViewHolder2 holder2 = (ViewHolder2) holder;
                SubCategory product = products.get(position);


                holder2.textViewName.setText(product.getSubCatName());


                Picasso.get().load("https://" + product.getImage())
                        .resize(0, 250)
                        .into(holder2.imageViewIcon, new Callback() {
                            @Override
                            public void onSuccess() {
                                holder2.progressBar.setVisibility(View.GONE);
                            }

                            @Override
                            public void onError(Exception e) {
                                holder2.progressBar.setVisibility(View.GONE);
                            }
                        });
                break;
        }
    }


}
