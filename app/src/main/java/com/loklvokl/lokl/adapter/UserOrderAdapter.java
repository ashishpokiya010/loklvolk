package com.loklvokl.lokl.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.loklvokl.lokl.R;
import com.loklvokl.lokl.activtiy.UserOrderDetailActivity;
import com.loklvokl.lokl.model.OrderProduct;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.List;

public class UserOrderAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<OrderProduct> products;
    Context context;

    public UserOrderAdapter(Context context, List<OrderProduct> data) {
        this.products = data;
        this.context = context;
    }

    @Override
    public int getItemCount() {
        return products.size();
    }

    public void updateList(List<OrderProduct> data) {
        this.products = data;
        notifyDataSetChanged();

    }


    public class ViewHolder2 extends RecyclerView.ViewHolder {
        ImageView image, status_icn;
        ProgressBar progressBar;
        TextView orderid_txt, name_txt, date_txt, price_txt, more_txt, status_txt;
        RatingBar orderRatingBar;


        public ViewHolder2(View itemView) {
            super(itemView);
            orderid_txt = itemView.findViewById(R.id.orderid_txt);
            name_txt = itemView.findViewById(R.id.name_txt);
            date_txt = itemView.findViewById(R.id.date_txt);
            price_txt = itemView.findViewById(R.id.price_txt);
            progressBar = itemView.findViewById(R.id.progressBar);
            image = itemView.findViewById(R.id.image);
            status_txt = itemView.findViewById(R.id.status_txt);
            more_txt = itemView.findViewById(R.id.more_txt);
            status_icn = itemView.findViewById(R.id.status_icn);
            orderRatingBar = itemView.findViewById(R.id.orderRatingBar);
        }
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view1 = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.user_order_item, parent, false);
        return new ViewHolder2(view1);

    }

    @SuppressLint("ResourceType")
    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

        ViewHolder2 holder2 = (ViewHolder2) holder;
        OrderProduct product = products.get(position);

        holder2.status_txt.setText(product.getOrderStatus());
        holder2.orderid_txt.setText("Order #" + product.getOrderId());
        holder2.name_txt.setText(product.getProductName());
        holder2.price_txt.setText("Rs " + product.getPrice());
        holder2.date_txt.setText(product.getOrderDate());
        System.out.println("RATINT" + product.getRating());
        if (product.getRating() == null) {
            holder2.orderRatingBar.setVisibility(View.GONE);
        } else {
            holder2.orderRatingBar.setRating(Integer.parseInt(product.getRating()));
            holder2.orderRatingBar.setVisibility(View.VISIBLE);
        }
        holder2.status_icn.setColorFilter(Color.parseColor(product.getColor()));

        if (product.getProductImage() != null) {
            Picasso.get().load("https://" + product.getProductImage())
                    .resize(0, 250)
                    .into(holder2.image, new Callback() {
                        @Override
                        public void onSuccess() {
                            holder2.progressBar.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError(Exception e) {
                            holder2.progressBar.setVisibility(View.GONE);
                        }
                    });
        }
        holder2.more_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, UserOrderDetailActivity.class);
                intent.putExtra("OrderDetailId", product.getOrder_details_id());
                intent.putExtra("Rating", product.getRating());
                intent.putExtra("UserId", product.getUser_id());
                context.startActivity(intent);
            }
        });
    }
}
