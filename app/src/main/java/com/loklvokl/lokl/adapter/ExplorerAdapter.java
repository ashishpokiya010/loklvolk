package com.loklvokl.lokl.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.loklvokl.lokl.R;
import com.loklvokl.lokl.model.ExplorerCategory;
import com.loklvokl.lokl.model.ItemInterface;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ExplorerAdapter extends RecyclerView.Adapter<ExplorerAdapter.MyViewHolder> {

    private List<ExplorerCategory> dataSet;
    Context context;
    ItemInterface itemInterface;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView textViewName;
        ProgressBar progressBar;

        ImageView imageViewIcon;

        public MyViewHolder(View itemView) {
            super(itemView);
            this.textViewName = itemView.findViewById(R.id.textView);
            this.progressBar = itemView.findViewById(R.id.progressBar);
            this.imageViewIcon = (ImageView) itemView.findViewById(R.id.image);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    itemInterface.ItemClick(getAdapterPosition(), dataSet.get(getAdapterPosition()).getId(), dataSet.get(getAdapterPosition()).getMainCatName());
                }
            });
        }
    }

    public ExplorerAdapter(Context context, List<ExplorerCategory> data) {
        this.dataSet = data;
        this.context = context;
        this.itemInterface = (ItemInterface) context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent,
                                           int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.explore_view, parent, false);

        //view.setOnClickListener(MainActivity.myOnClickListener);

        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {

        TextView textViewName = holder.textViewName;
        ImageView imageView = holder.imageViewIcon;
        ProgressBar progressBar = holder.progressBar;

        textViewName.setText(dataSet.get(listPosition).getMainCatName());

        Picasso.get().load("https://" + dataSet.get(listPosition).getImage()).into(imageView, new Callback() {
            @Override
            public void onSuccess() {
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onError(Exception e) {
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }
}
