package com.loklvokl.lokl.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.loklvokl.lokl.activtiy.ProductDetailActivity;
import com.loklvokl.lokl.R;
import com.loklvokl.lokl.model.CartDetail.Product;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.List;

public class CartAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<Product> products;
    Context context;
    CartInterface cartInterface;

    public CartAdapter(Context context, List<Product> data) {
        this.products = data;
        this.context = context;
        cartInterface = (CartInterface) context;
    }

    @Override
    public int getItemCount() {
        return products.size();
    }


    public class ViewHolder2 extends RecyclerView.ViewHolder {
        TextView textViewName, txt_qty, textColor, txt_minus, txt_plus;
        TextView textViewDesc;
        TextView textViewPrice, textViewPrice0, textOff;
        ProgressBar progressBar;
        ImageView imageViewIcon;
        ImageView imgDelete;
        LinearLayout colorLayout;

        public ViewHolder2(View itemView) {
            super(itemView);
            this.textColor = itemView.findViewById(R.id.textColor);
            this.txt_qty = itemView.findViewById(R.id.txt_qty);
            this.textViewName = itemView.findViewById(R.id.textView);
            this.textViewDesc = itemView.findViewById(R.id.textViewDesc);
            this.textViewPrice = itemView.findViewById(R.id.textViewPrice);
            this.textViewPrice0 = itemView.findViewById(R.id.textViewPrice0);
            this.progressBar = itemView.findViewById(R.id.progressBar);
            this.imageViewIcon = itemView.findViewById(R.id.image);
            this.imgDelete = itemView.findViewById(R.id.imgDelete);
            this.textOff = itemView.findViewById(R.id.textOff);
            this.txt_minus = itemView.findViewById(R.id.txt_minus);
            this.txt_plus = itemView.findViewById(R.id.txt_plus);
            this.colorLayout = itemView.findViewById(R.id.colorLayout);

            txt_minus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int qty = Integer.parseInt(txt_qty.getText().toString()) - 1;
                    if (qty > 0)
                        cartInterface.performMinusQty(getAdapterPosition(), qty + "");

                }
            });
            txt_plus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int qty = Integer.parseInt(txt_qty.getText().toString()) + 1;
                    if (qty <= 5)
                        cartInterface.performPlusQty(getAdapterPosition(), qty + "");
                    else
                        Toast.makeText(context, "Maximum Quantity 5 are allow!", Toast.LENGTH_SHORT).show();

                }
            });

            imgDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    cartInterface.performDelete(getAdapterPosition(), products.get(getAdapterPosition()).getCart_id());
                }
            });
            imageViewIcon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    openProductDetail(products.get(getAdapterPosition()).getProductId());
                }
            });
        }
    }

    private void openProductDetail(String productId) {
        Intent intent = new Intent(context, ProductDetailActivity.class);
        intent.putExtra("ID", productId);
        context.startActivity(intent);
    }

    class ViewHolder0 extends RecyclerView.ViewHolder {
        TextView seeall_text;
        TextView title_text;

        public ViewHolder0(View itemView) {
            super(itemView);
            this.seeall_text = itemView.findViewById(R.id.seeall_text);
            this.title_text = itemView.findViewById(R.id.title_text);
        }
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view1 = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cart_item, parent, false);
        return new ViewHolder2(view1);

    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

        ViewHolder2 holder2 = (ViewHolder2) holder;
        Product product = products.get(position);

        if (product.getSize() != null && !product.getSize().equals("")) {
            holder2.textColor.setText(product.getSize());
            holder2.colorLayout.setVisibility(View.VISIBLE);
        } else {
            holder2.colorLayout.setVisibility(View.INVISIBLE);
        }
        holder2.textViewName.setText(product.getProdcutName());
        holder2.txt_qty.setText("" + product.getQty());
        holder2.textViewDesc.setText(Html.fromHtml(product.getDescription()));
        holder2.textViewPrice.setText("Rs." + product.getProductSellingPrice());
        holder2.textViewPrice0.setText("Rs." + product.getMrp());
        holder2.textViewPrice0.setPaintFlags(holder2.textViewPrice0.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        holder2.textOff.setText("(" + product.getOff() + " OFF)");

        if (product.getImage() != null) {
            Picasso.get().load("https://" + product.getImage())
                    .resize(0, 250)
                    .into(holder2.imageViewIcon, new Callback() {
                        @Override
                        public void onSuccess() {
                            holder2.progressBar.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError(Exception e) {
                            holder2.progressBar.setVisibility(View.GONE);
                        }
                    });
        }
    }

    public interface CartInterface {
        void performPlusQty(int adapterPosition, String qty);

        void performMinusQty(int adapterPosition, String qty);

        void performDelete(int adapterPosition, String id);

    }

}
