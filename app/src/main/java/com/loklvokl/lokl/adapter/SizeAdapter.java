package com.loklvokl.lokl.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.loklvokl.lokl.activtiy.AddAdActivity4;
import com.loklvokl.lokl.activtiy.ProductDetailActivity;
import com.loklvokl.lokl.R;
import com.loklvokl.lokl.model.ItemInterface;
import com.loklvokl.lokl.model.ProductSize;

import java.util.List;

public class SizeAdapter extends RecyclerView.Adapter<SizeAdapter.MyViewHolder> {

    private List<ProductSize> dataSet;
    Context context;
    ItemInterface itemInterface;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView textView;

        public MyViewHolder(View itemView) {
            super(itemView);
            this.textView = itemView.findViewById(R.id.textView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    itemInterface.ItemClick(getAdapterPosition(), dataSet.get(getAdapterPosition()).getSize_value(), "");
                }
            });
        }
    }

    public SizeAdapter(Context context, List<ProductSize> data) {
        this.dataSet = data;
        this.context = context;
        this.itemInterface = (ItemInterface) context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent,
                                           int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.size_view, parent, false);

        //view.setOnClickListener(MainActivity.myOnClickListener);

        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {

        TextView textView = holder.textView;

        textView.setText(dataSet.get(listPosition).getSize_value());


        if (context instanceof ProductDetailActivity && ProductDetailActivity.listSelctSize.get(listPosition) != null && ProductDetailActivity.listSelctSize.get(listPosition)) {
            textView.setTextColor(Color.WHITE);
            textView.setBackgroundResource(R.drawable.rounded_size_select);
        } else if (context instanceof AddAdActivity4 && AddAdActivity4.listSelctSize.get(listPosition) != null && AddAdActivity4.listSelctSize.get(listPosition)) {
            textView.setTextColor(Color.WHITE);
            textView.setBackgroundResource(R.drawable.rounded_size_select);
        } else {
            textView.setTextColor(Color.GRAY);
            textView.setBackgroundResource(R.drawable.rounded_size_normal);
        }

    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }
}
