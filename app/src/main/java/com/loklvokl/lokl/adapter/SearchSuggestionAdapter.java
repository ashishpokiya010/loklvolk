package com.loklvokl.lokl.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.loklvokl.lokl.R;
import com.loklvokl.lokl.model.searchsuggestion.SearchItems;

import java.util.List;

public class SearchSuggestionAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<SearchItems> searchItemsList;
    Context context;
    private OnItemSuggest onItemSuggestClicked;
    private OnItemClicked onItemClicked;

    public SearchSuggestionAdapter(Context context, List<SearchItems> data,
                                   OnItemSuggest onSuggestClick, OnItemClicked onItemClick) {
        this.searchItemsList = data;
        this.context = context;
        this.onItemSuggestClicked = onSuggestClick;
        this.onItemClicked = onItemClick;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.productsearch_suggestion, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        SearchItems search = searchItemsList.get(position);
        ViewHolder holder_view = (ViewHolder) holder;
        holder_view.txt_search_text.setText(""+search.getData());

        holder_view.img_suggestion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onItemSuggestClicked.onItemSuggestClick(position);
            }
        });

        ((ViewHolder) holder).main_Layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onItemClicked.onItemClicked(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return searchItemsList.size();
    }

    public interface OnItemSuggest {
        void onItemSuggestClick(int position);
    }

    public interface OnItemClicked {
        void onItemClicked(int position);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView txt_search_text;
        ImageView img_suggestion;
        RelativeLayout main_Layout;

        public ViewHolder(View itemView) {
            super(itemView);
            this.txt_search_text = itemView.findViewById(R.id.txt_search_text);
            this.img_suggestion = itemView.findViewById(R.id.img_suggestion);
            this.main_Layout = itemView.findViewById(R.id.main_Layout);
        }

    }
}
