package com.loklvokl.lokl.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import com.loklvokl.lokl.R;
import com.loklvokl.lokl.activtiy.UpdateStockActivity;
import com.loklvokl.lokl.activtiy.UserOrderDetailActivity;
import com.loklvokl.lokl.model.product.UpdateStockModel;
import com.loklvokl.lokl.model.product.UserProduct;
import com.loklvokl.lokl.util.ApiClient;
import com.loklvokl.lokl.util.ApiInterface;
import com.loklvokl.lokl.util.Constant;
import com.loklvokl.lokl.util.SharedPref;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

public class UpdateStockAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<UserProduct> products;
    Context context;
    private UpdateStockAdapter.onClick onCLICK;

    SharedPref sharedPref;

    public UpdateStockAdapter(Context context, List<UserProduct> data, onClick countryClick) {
        this.products = data;
        this.context = context;
        this.onCLICK = countryClick;
        sharedPref = new SharedPref(context);
    }

    @Override
    public int getItemCount() {
        return products.size();
    }

    public void updateList(List<UserProduct> data) {
        this.products = data;
        notifyDataSetChanged();
    }


    public class ViewHolder2 extends RecyclerView.ViewHolder {
        ImageView image, status_icn, img_delete;
        ProgressBar progressBar;
        TextView name_txt, desc_txt, price_txt, accual_price_txt, price_discount, current_stock, status_txt, more_txt;


        public ViewHolder2(View itemView) {
            super(itemView);
            name_txt = itemView.findViewById(R.id.name_txt);
            desc_txt = itemView.findViewById(R.id.description_txt);
            price_txt = itemView.findViewById(R.id.price_txt);
            accual_price_txt = itemView.findViewById(R.id.accual_price);
            price_discount = itemView.findViewById(R.id.price_discount);
            current_stock = itemView.findViewById(R.id.stock_txt);
            progressBar = itemView.findViewById(R.id.progressBar);
            image = itemView.findViewById(R.id.image);
            img_delete = itemView.findViewById(R.id.image_delete);
            status_txt = itemView.findViewById(R.id.status_txt);
            more_txt = itemView.findViewById(R.id.more_txt);
            status_icn = itemView.findViewById(R.id.status_icn);
        }
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view1 = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.update_stock_prodyct_item, parent, false);
        return new UpdateStockAdapter.ViewHolder2(view1);

    }

    @SuppressLint("ResourceType")
    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

        UpdateStockAdapter.ViewHolder2 holder2 = (UpdateStockAdapter.ViewHolder2) holder;
        UserProduct product = products.get(position);
        holder2.status_txt.setText(product.getProductStatus());
        holder2.name_txt.setText(product.getProductName());
        holder2.price_txt.setText("Rs " + product.getSellingPrice());
        holder2.accual_price_txt.setText("Rs " + product.getMrp());
        holder2.accual_price_txt.setPaintFlags(holder2.accual_price_txt.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        if (product.getOff() != 0) {
            holder2.price_discount.setText("(" + product.getOff() + "% OFF)");
        }
        holder2.desc_txt.setText(product.getSubCatName());
        holder2.current_stock.setText("Current Stock - " + product.getStock());

        // System.out.println("COOOCOOCO" + product.getOrder_status_color());
        if (product.getProduct_status_color() != null) {
            holder2.status_icn.setColorFilter(Color.parseColor(product.getProduct_status_color()));
        }

        if (product.getProductImage() != null) {
            Picasso.get().load("https://" + product.getProductImage())
                    .resize(0, 250)
                    .into(holder2.image, new Callback() {
                        @Override
                        public void onSuccess() {
                            holder2.progressBar.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError(Exception e) {
                            holder2.progressBar.setVisibility(View.GONE);
                        }
                    });
        }
        holder2.more_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onCLICK.onClick(products.get(position), position);

            }
        });
        holder2.img_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AlertDialog.Builder(context)
                        .setMessage("Want to delete product?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                onCLICK.onDelete(product, position);
                                //deleteProduct(product.getProductId());
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int i) {
                                dialog.dismiss();
                            }
                        })
                        .show();
            }
        });
    }

    /*private void deleteProduct(String product_id) {
        progressBar.setVisibility(View.VISIBLE);
        apiService = apiClient.getClient().create(ApiInterface.class);

        Call<UpdateStockModel> call = apiService.deleteStockProduct(sharedPref.getStr("ID"), product_id);

        apiClient.enqueue(call, new retrofit2.Callback<UpdateStockModel>() {
            @Override
            public void onResponse(Call<UpdateStockModel> call, Response<UpdateStockModel> response) {
                progressBar.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    String responseData = response.body().getUserProduct().get(0).getError();
                    System.out.println("NULL>>>" + responseData);
                    if (responseData == null) {
                        Constant.toast(context, "Unable to delete product");
                    } else {
                        Constant.toast(context, response.body().getUserProduct().get(0).getMessage());
                        if(response.body().getUserProduct().get(0).getError().equals("false")){
                            onCLICK.onDelete(products.get(position),);
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<UpdateStockModel> call, Throwable t) {
                // Log error here since request failed
                Log.d("Failed : "," "+t.getMessage());
                Constant.toast(context, "Failed to delete product");
            }
        });
    }*/

    public interface onClick {
        void onClick(UserProduct userProduct, int position);
        void onDelete(UserProduct userProduct, int position);
    }
}


