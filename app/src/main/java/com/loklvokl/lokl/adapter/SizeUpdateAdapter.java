package com.loklvokl.lokl.adapter;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.loklvokl.lokl.R;
import com.loklvokl.lokl.activtiy.ProductDetailActivity;
import com.loklvokl.lokl.activtiy.UpdateStockActivity;
import com.loklvokl.lokl.activtiy.UpdateStockDetailActivity;
import com.loklvokl.lokl.model.ItemInterface;
import com.loklvokl.lokl.model.product.ProductSize;
import com.loklvokl.lokl.model.sizesbycategory.SizeDetailsItem;

import java.util.List;

public class SizeUpdateAdapter extends RecyclerView.Adapter<SizeUpdateAdapter.MyViewHolder>{
    private List<SizeDetailsItem> productSizes;
    private List<ProductSize> selectedSize;
    Context context;
    ItemInterface itemInterface;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView textView;

        public MyViewHolder(View itemView) {
            super(itemView);
            this.textView = itemView.findViewById(R.id.textView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    itemInterface.ItemClick(getAdapterPosition(), productSizes.get(getAdapterPosition()).getSize(), "");
                }
            });
        }
    }


    public SizeUpdateAdapter(List<ProductSize> selectedSize, List<SizeDetailsItem> data, Context context) {
        this.selectedSize = selectedSize;
        this.productSizes = data;
        this.context = context;
        this.itemInterface = (ItemInterface) context;
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent,
                                                       int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.size_view, parent, false);

        //view.setOnClickListener(MainActivity.myOnClickListener);

        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {

        TextView textView = holder.textView;

        Log.i("Sizes adapter:",productSizes.get(listPosition).getSize());
        textView.setText(productSizes.get(listPosition).getSize());


        if (context instanceof UpdateStockDetailActivity &&
                UpdateStockDetailActivity.listSelctSize.get(listPosition) != null &&
                UpdateStockDetailActivity.listSelctSize.get(listPosition)) {
            textView.setTextColor(Color.WHITE);
            textView.setBackgroundResource(R.drawable.rounded_size_select);
        } else {
            textView.setTextColor(Color.GRAY);
            textView.setBackgroundResource(R.drawable.rounded_size_normal);
        }

    }

    @Override
    public int getItemCount() {
        return productSizes.size();
    }
}
