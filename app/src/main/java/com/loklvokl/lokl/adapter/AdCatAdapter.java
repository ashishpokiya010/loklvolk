package com.loklvokl.lokl.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.loklvokl.lokl.R;
import com.loklvokl.lokl.model.SubCategory;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.List;

public class AdCatAdapter extends RecyclerView.Adapter<AdCatAdapter.MyViewHolder> {

    private List<SubCategory> dataSet;
    Context context;
    AdInterface adInterface;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView textViewName;
        TextView textViewPrice, textViewPrice0, textOff;
        ProgressBar progressBar;
        ImageView imageViewIcon;

        public MyViewHolder(View itemView) {
            super(itemView);
            this.textViewName = itemView.findViewById(R.id.textView);
            this.textViewPrice = itemView.findViewById(R.id.textViewPrice);
            this.textViewPrice0 = itemView.findViewById(R.id.textViewPrice0);
            this.progressBar = itemView.findViewById(R.id.progressBar);
            this.imageViewIcon = itemView.findViewById(R.id.image);
            this.textOff = itemView.findViewById(R.id.textOff);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    adInterface.itemClick(getAdapterPosition());
                }
            });
        }
    }

    public AdCatAdapter(Context context, List<SubCategory> data) {
        this.dataSet = data;
        this.context = context;
        this.adInterface = (AdInterface) context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent,
                                           int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.ad_cat_view, parent, false);

        //view.setOnClickListener(MainActivity.myOnClickListener);

        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        SubCategory product = dataSet.get(position);

        holder.textViewName.setText(product.getSubCatName());
        Picasso.get().load("https://" + product.getImage())
                .resize(0, 250)
                .into(holder.imageViewIcon, new Callback() {
                    @Override
                    public void onSuccess() {
                        holder.progressBar.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError(Exception e) {
                        holder.progressBar.setVisibility(View.GONE);
                    }
                });

    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    public interface AdInterface {
        void itemClick(int position);
    }
}
