package com.loklvokl.lokl.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.loklvokl.lokl.R;

import java.io.ByteArrayOutputStream;
import java.util.List;

public class ImageAdapter extends RecyclerView.Adapter<ImageAdapter.MyViewHolder> {

    private List<Bitmap> imageSet;
    Context context;
    AdInterface adInterface;

    public class MyViewHolder extends RecyclerView.ViewHolder {


        ImageView imageViewUser, imageViewRemove;
        RelativeLayout relativeLayout;

        public MyViewHolder(View itemView) {
            super(itemView);
            this.imageViewUser = itemView.findViewById(R.id.img_user);
            this.imageViewRemove = itemView.findViewById(R.id.remove_img_user);
            this.relativeLayout = itemView.findViewById(R.id.relative_user_item);

            imageViewRemove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    adInterface.itemClick(getAdapterPosition());
                }
            });
            relativeLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    adInterface.itemClickAll(getAdapterPosition());
                }
            });
        }
    }

    public ImageAdapter(Context context, List<Bitmap> data) {
        this.imageSet = data;
        this.context = context;
        this.adInterface = (AdInterface) context;
    }

    @Override
    public ImageAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent,
                                                        int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.image_items, parent, false);

        //view.setOnClickListener(MainActivity.myOnClickListener);

        ImageAdapter.MyViewHolder myViewHolder = new ImageAdapter.MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final ImageAdapter.MyViewHolder holder, final int position) {
        // holder.imageViewUser.Bitmap.Config.ARGB_8888;


        Bitmap bitmap = imageSet.get(position);

        //holder.imageViewUser.setImageBitmap();


        Glide.with(context)
                .asBitmap()
                .load(imageSet.get(position))
                .disallowHardwareConfig()
                .override(200, 200)
                .listener(new RequestListener<Bitmap>() {
                              @Override
                              public boolean onLoadFailed(@Nullable GlideException e, Object o, Target<Bitmap> target, boolean b) {
                                  Toast.makeText(context, "Error in loading", Toast.LENGTH_SHORT).show();
                                  return false;
                              }
                              
                              @Override
                              public boolean onResourceReady(Bitmap bitmap1, Object o, Target<Bitmap> target,
                                                             DataSource dataSource, boolean b) {
                                  ///holder.imageViewUser.setImage(ImageSource.bitmap(bitmap));
                                  holder.imageViewUser.setImageBitmap(bitmap1);
                                  holder.imageViewUser.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
                                  System.out.println("LOAD_BITMAP" + bitmap1);
                                  return false;
                              }
                          }
                ).submit();
    }

    private byte[] bitmapToByte(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        byte[] byteArray = stream.toByteArray();
        return byteArray;
    }

    @Override
    public int getItemCount() {
        return imageSet.size();
    }

    public interface AdInterface {
        void itemClick(int position);

        void itemClickAll(int position);
    }
}

