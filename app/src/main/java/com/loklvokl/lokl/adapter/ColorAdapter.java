package com.loklvokl.lokl.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.recyclerview.widget.RecyclerView;

import com.loklvokl.lokl.activtiy.AddAdActivity4;
import com.loklvokl.lokl.activtiy.ProductDetailActivity;
import com.loklvokl.lokl.R;
import com.loklvokl.lokl.model.ItemInterface;
import com.loklvokl.lokl.model.ProductColor;

import java.util.List;

public class ColorAdapter extends RecyclerView.Adapter<ColorAdapter.MyViewHolder> {

    private List<ProductColor> dataSet;
    Context context;
    ItemInterface itemInterface;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView progressBar;
        ImageView imageViewIcon;

        public MyViewHolder(View itemView) {
            super(itemView);
            this.progressBar = itemView.findViewById(R.id.progressBar);
            this.imageViewIcon = (ImageView) itemView.findViewById(R.id.image);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    itemInterface.ItemClick(getAdapterPosition(), dataSet.get(getAdapterPosition()).getCode(), dataSet.get(getAdapterPosition()).getColor());
                }
            });
        }
    }

    public ColorAdapter(Context context, List<ProductColor> data) {
        this.dataSet = data;
        this.context = context;
        this.itemInterface = (ItemInterface) context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent,
                                           int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.color_view, parent, false);

        //view.setOnClickListener(MainActivity.myOnClickListener);

        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {

        ImageView imageView = holder.imageViewIcon;
        ImageView progressBar = holder.progressBar;

        if (dataSet.get(listPosition).getCode() != null && !dataSet.get(listPosition).getCode().isEmpty() && dataSet.get(listPosition).getCode().contains("#"))
            imageView.setColorFilter(Color.parseColor(dataSet.get(listPosition).getCode()));
        else {
            imageView.setColorFilter(Color.WHITE);
        }

//        Log.e("log_color", ">" + listPosition + "|" + dataSet.get(listPosition).getEnables());
        if (context instanceof ProductDetailActivity && ProductDetailActivity.listSelct.get(listPosition) != null && ProductDetailActivity.listSelct.get(listPosition)) {
            progressBar.setVisibility(View.VISIBLE);

        } else if (context instanceof AddAdActivity4 && AddAdActivity4.listSelct.get(listPosition) != null && AddAdActivity4.listSelct.get(listPosition)) {
            progressBar.setVisibility(View.VISIBLE);

        } else {
            progressBar.setVisibility(View.GONE);
        }

    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }
}
