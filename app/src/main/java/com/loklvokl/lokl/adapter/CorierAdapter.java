package com.loklvokl.lokl.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.loklvokl.lokl.R;
import com.loklvokl.lokl.model.Courier;

import java.util.ArrayList;
import java.util.List;

public class CorierAdapter extends RecyclerView.Adapter<CorierAdapter.MyViewHolder> {


    private List<Courier> couriersDataArrayList;
    Context context;
    //getting the context and product list with constructor
    private onCountryClick onCourierClick;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView textViewName;

        public MyViewHolder(View itemView) {
            super(itemView);
            this.textViewName = itemView.findViewById(R.id.txtCountryNameCode);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onCourierClick.onCountryItemClick(couriersDataArrayList.get(getAdapterPosition()));
                }
            });
        }
    }

    public CorierAdapter(Context mCtx, ArrayList<Courier> arrayList, onCountryClick countryClick) {
        this.context = mCtx;
        this.couriersDataArrayList = arrayList;
        this.onCourierClick = countryClick;
    }

    @Override
    public CorierAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent,
                                                         int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_coureir_selector, parent, false);

        //view.setOnClickListener(MainActivity.myOnClickListener);

        CorierAdapter.MyViewHolder myViewHolder = new CorierAdapter.MyViewHolder(view);
        return myViewHolder;
    }


    @Override
    public void onBindViewHolder(final CorierAdapter.MyViewHolder holder, final int listPosition) {

        holder.textViewName.setText("" + couriersDataArrayList.get(listPosition).getName());
    }


    @Override
    public int getItemCount() {
        return couriersDataArrayList.size();
    }


    public interface onCountryClick {
        void onCountryItemClick(Courier countryData);
    }
}