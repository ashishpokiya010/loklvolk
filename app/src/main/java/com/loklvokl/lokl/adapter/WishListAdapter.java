package com.loklvokl.lokl.adapter;

import android.content.Context;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.loklvokl.lokl.R;
import com.loklvokl.lokl.model.product.Wishlist;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.List;

public class WishListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<Wishlist> products;
    Context context;
    private WishListAdapter.onDeleteClick onDelete;

    public WishListAdapter(Context context, List<Wishlist> data, onDeleteClick countryClick) {
        this.products = data;
        this.context = context;
        this.onDelete = countryClick;
    }

    @Override
    public int getItemCount() {
        return products.size();
    }

    public void updateList(List<Wishlist> data) {
        this.products = data;
        notifyDataSetChanged();
    }


    public class ViewHolder2 extends RecyclerView.ViewHolder {
        ImageView image, deletimage;
        ProgressBar progressBar;
        TextView name_txt, desc_txt, price_txt, accual_price_txt, dicount_text;


        public ViewHolder2(View itemView) {
            super(itemView);
            name_txt = itemView.findViewById(R.id.name_txt);
            desc_txt = itemView.findViewById(R.id.description_txt);
            price_txt = itemView.findViewById(R.id.price_txt);
            accual_price_txt = itemView.findViewById(R.id.accual_price);
            dicount_text = itemView.findViewById(R.id.price_discount);
            progressBar = itemView.findViewById(R.id.progressBar);
            image = itemView.findViewById(R.id.image);
            deletimage = itemView.findViewById(R.id.image_delete);
        }
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view1 = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.wishlist_item, parent, false);
        return new WishListAdapter.ViewHolder2(view1);

    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

        WishListAdapter.ViewHolder2 holder2 = (WishListAdapter.ViewHolder2) holder;
        Wishlist product = products.get(position);
        holder2.name_txt.setText(product.getProductName());
        holder2.price_txt.setText("Rs " + product.getProductSellingPrice());
        holder2.accual_price_txt.setText("Rs " + product.getMrp());
        holder2.accual_price_txt.setPaintFlags(holder2.accual_price_txt.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        holder2.dicount_text.setText("Rs " + product.getMrp());


        if (product.getProduct_image() != null) {
            Picasso.get().load("https://" + product.getProduct_image())
                    .resize(0, 250)
                    .into(holder2.image, new Callback() {
                        @Override
                        public void onSuccess() {
                            holder2.progressBar.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError(Exception e) {
                            holder2.progressBar.setVisibility(View.GONE);
                        }
                    });
        }
        holder2.deletimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onDelete.onDeleteClick(products.get(position));
            }
        });
    }

    public interface onDeleteClick {
        void onDeleteClick(Wishlist wishlist);
    }
}


