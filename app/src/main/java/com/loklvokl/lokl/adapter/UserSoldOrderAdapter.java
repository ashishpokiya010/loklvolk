package com.loklvokl.lokl.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.loklvokl.lokl.activtiy.MySoldOrderActivity;
import com.loklvokl.lokl.R;
import com.loklvokl.lokl.activtiy.UserSoldOrderDetailActivity;
import com.loklvokl.lokl.model.MySoldOrderModel;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.List;

public class UserSoldOrderAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<MySoldOrderModel> products;
    Context context;

    public UserSoldOrderAdapter(Context context, List<MySoldOrderModel> data) {
        this.products = data;
        this.context = context;
    }

    @Override
    public int getItemCount() {
        return products.size();
    }

    public void updateList(List<MySoldOrderModel> data) {
        this.products = data;
        notifyDataSetChanged();

    }


    public class ViewHolder2 extends RecyclerView.ViewHolder {
        ImageView image, status_icn;
        ProgressBar progressBar;
        TextView orderid_txt, name_txt, date_txt, price_txt, more_txt, status_txt, payment_type_txt;


        public ViewHolder2(View itemView) {
            super(itemView);
            orderid_txt = itemView.findViewById(R.id.orderid_txt);
            name_txt = itemView.findViewById(R.id.name_txt);
            date_txt = itemView.findViewById(R.id.date_txt);
            price_txt = itemView.findViewById(R.id.price_txt);
            progressBar = itemView.findViewById(R.id.progressBar);
            image = itemView.findViewById(R.id.image);
            status_txt = itemView.findViewById(R.id.status_txt);
            more_txt = itemView.findViewById(R.id.more_txt);
            status_icn = itemView.findViewById(R.id.status_icn);
            payment_type_txt = itemView.findViewById(R.id.payment_type_txt);
        }
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view1 = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.user_sold_order_items, parent, false);
        return new UserSoldOrderAdapter.ViewHolder2(view1);

    }

    @SuppressLint("ResourceType")
    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

        UserSoldOrderAdapter.ViewHolder2 holder2 = (UserSoldOrderAdapter.ViewHolder2) holder;
        MySoldOrderModel product = products.get(position);
        holder2.status_txt.setText(product.getOrder_status());
        holder2.orderid_txt.setText("Order #" + product.getOrder_id());
        holder2.name_txt.setText(product.getProduct_name());
        holder2.price_txt.setText("Rs " + product.getPrice());
        holder2.date_txt.setText(product.getOrder_date());
        holder2.payment_type_txt.setText(product.getPayment_type());
        // System.out.println("COOOCOOCO" + product.getOrder_status_color());
        if (product.getOrder_status_color() != null) {
            holder2.status_icn.setColorFilter(Color.parseColor(product.getOrder_status_color()));
        }

        if (product.getProduct_image() != null) {
            Picasso.get().load("https://" + product.getProduct_image())
                    .resize(0, 250)
                    .into(holder2.image, new Callback() {
                        @Override
                        public void onSuccess() {
                            holder2.progressBar.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError(Exception e) {
                            holder2.progressBar.setVisibility(View.GONE);
                        }
                    });
        }
        holder2.more_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, UserSoldOrderDetailActivity.class);
                intent.putExtra("order_details_id", product.getOrder_details_id());
                intent.putExtra("c_seller_id", MySoldOrderActivity.userId);

                intent.putExtra("product_image", product.getProduct_image());
                intent.putExtra("order_id", product.getOrder_id());
                intent.putExtra("payment_type", product.getPayment_type());
                intent.putExtra("prod_qty", product.getProd_qty());
                intent.putExtra("c_seller_id", MySoldOrderActivity.userId);
                intent.putExtra("c_seller_id", MySoldOrderActivity.userId);
                context.startActivity(intent);
            }
        });
    }
}

