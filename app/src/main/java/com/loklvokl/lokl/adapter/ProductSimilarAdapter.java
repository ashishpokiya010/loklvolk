package com.loklvokl.lokl.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.loklvokl.lokl.activtiy.ProductDetailActivity;
import com.loklvokl.lokl.R;
import com.loklvokl.lokl.model.Product;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ProductSimilarAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<Product> products;
    Context context;


    public ProductSimilarAdapter(Context context, List<Product> data) {
        this.products = data;
        this.context = context;
    }

    @Override
    public int getItemCount() {
        return products.size();
    }


    public class ViewHolder2 extends RecyclerView.ViewHolder {
        TextView textViewName;
        TextView textViewPrice, textViewPrice0, textOff;
        ProgressBar progressBar;
        ImageView imageViewIcon;
        LinearLayout main_Layout;

        public ViewHolder2(View itemView) {
            super(itemView);
            this.textViewName = itemView.findViewById(R.id.textView);
            this.textViewPrice = itemView.findViewById(R.id.textViewPrice);
            this.textViewPrice0 = itemView.findViewById(R.id.textViewPrice0);
            this.progressBar = itemView.findViewById(R.id.progressBar);
            this.imageViewIcon = itemView.findViewById(R.id.image);
            this.textOff = itemView.findViewById(R.id.textOff);
            this.main_Layout = itemView.findViewById(R.id.main_Layout);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    System.out.println("CLICLCICLI");
                    openProductDetail(products.get(getAdapterPosition()).getProductId());
                }
            });
        }
    }

    private void openProductDetail(String productId) {
        Intent intent = new Intent(context, ProductDetailActivity.class);
        intent.putExtra("ID", productId);
        context.startActivity(intent);
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view1 = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.productsimilar_item, parent, false);
        return new ViewHolder2(view1);
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        ViewHolder2 holder2 = (ViewHolder2) holder;
        Product product = products.get(position);

        holder2.main_Layout.setBackgroundColor(Color.parseColor(products.get(position).getColor()));
        holder2.textViewName.setText(product.getProductName());
        holder2.textViewPrice.setText("Rs." + product.getProductSellingPrice());
        holder2.textViewPrice0.setText("Rs." + product.getProductMrp());
        holder2.textViewPrice0.setPaintFlags(holder2.textViewPrice0.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        holder2.textOff.setText("(" + product.getOff() + " OFF)");

        Picasso.get().load("https://" + product.getProductImage())
                .resize(0, 250)
                .into(holder2.imageViewIcon, new Callback() {
                    @Override
                    public void onSuccess() {
                        holder2.progressBar.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError(Exception e) {
                        holder2.progressBar.setVisibility(View.GONE);
                    }
                });
    }


}
