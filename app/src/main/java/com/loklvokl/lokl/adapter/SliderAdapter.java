package com.loklvokl.lokl.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.loklvokl.lokl.R;
import com.loklvokl.lokl.model.BannerInterface;
import com.loklvokl.lokl.model.Homeslider;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.List;

public class SliderAdapter extends PagerAdapter {
    private Context context;
    private List<Homeslider> homeslider;
    private BannerInterface bannerInterface;

    public SliderAdapter(Context context, List<Homeslider> homeslider) {
        this.context = context;
        this.homeslider = homeslider;
        this.bannerInterface = (BannerInterface) context;

    }

    @Override
    public int getCount() {
        return homeslider.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.item_slider, null);

        ImageView imageView = view.findViewById(R.id.imageView);
        ProgressBar progressBar = view.findViewById(R.id.progressBar);

        Picasso.get().load("https://" + homeslider.get(position).getImage()).into(imageView, new Callback() {
            @Override
            public void onSuccess() {
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onError(Exception e) {
                progressBar.setVisibility(View.GONE);
                Log.e(">>>>>>", ">" + e);
            }
        });
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("bannerId : ","  -- "+homeslider.get(position).getId());
                bannerInterface.bannerClick(homeslider.get(position).getId());
            }
        });

        ViewPager viewPager = (ViewPager) container;
        viewPager.addView(view, 0);


        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ViewPager viewPager = (ViewPager) container;
        View view = (View) object;
        viewPager.removeView(view);
    }
}
