package com.loklvokl.lokl.notification;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.loklvokl.lokl.R;
import com.loklvokl.lokl.activtiy.MainActivity;
import com.loklvokl.lokl.activtiy.ProductDetailActivity;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    @Override
    public void onMessageReceived(@NonNull RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        // Check if message contains a data payload.
        if (remoteMessage != null) {
            Log.d("TAG", "MessageNotification Body: " + remoteMessage.getData().toString());
            //showNotification(remoteMessage.getNotification().getTitle(), remoteMessage.getNotification().getBody());
            showNotification(remoteMessage);
        }
    }

    private void showNotification(RemoteMessage remoteMessage) {

        Bitmap bitmap = null;
        String title = remoteMessage.getData().get("title") != null ? remoteMessage.getData().get("title") : "";
        String body = remoteMessage.getData().get("body") != null ? remoteMessage.getData().get("body") : "";
        String action = remoteMessage.getData().get("click_action") != null ? remoteMessage.getData().get("click_action") : "";
        String product_id = remoteMessage.getData().get("product_id") != null ? remoteMessage.getData().get("product_id") : "";
        String offer_id = remoteMessage.getData().get("offer_id") != null ? remoteMessage.getData().get("offer_id") : "";

        String category_id = remoteMessage.getData().get("cat_id") != null ? remoteMessage.getData().get("cat_id") : "";
        String category_name = remoteMessage.getData().get("cat_name") != null ? remoteMessage.getData().get("cat_name") : "";
        String collection_id = remoteMessage.getData().get("collection_id") != null ? remoteMessage.getData().get("collection_id") : "";
        String collection_name = remoteMessage.getData().get("collection_name") != null ? remoteMessage.getData().get("collection_name") : "";
        String slider_id = remoteMessage.getData().get("sliderid") != null ? remoteMessage.getData().get("sliderid") : "";
        Uri image = Uri.parse(remoteMessage.getData().get("image"));

        // Pass the intent to switch to the MainActivity
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("title", "" + title);
        intent.putExtra("body", "" + body);
        if (image != null) {
            intent.putExtra("image", image);
            bitmap = getBitmapfromUrl("https://"+remoteMessage.getData().get("image"));
        }
        intent.putExtra("click_action", ""+action);
        intent.putExtra("product_id", ""+product_id);
        intent.putExtra("offer_id", ""+offer_id);

        intent.putExtra("cat_id", ""+category_id);
        intent.putExtra("cat_name", ""+category_name);
        intent.putExtra("collection_id", ""+collection_id);
        intent.putExtra("collection_name", ""+collection_name);
        intent.putExtra("slider_id", ""+slider_id);

        String channel_id = getResources().getString(R.string.default_notification_channel_id);
        String channel_name = getResources().getString(R.string.default_notification_channel_name);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder builder;
        if (bitmap != null) {
            builder = new NotificationCompat.Builder(getApplicationContext(), channel_id)
                    .setSmallIcon(R.drawable.icon_app)
                    .setLargeIcon(bitmap)
                    .setStyle(new NotificationCompat.BigPictureStyle()
                            .bigPicture(bitmap))
                    .setContentTitle(title)
                    .setContentText(body)
                    .setAutoCancel(true)
                    .setOnlyAlertOnce(true)
                    .setContentIntent(pendingIntent)
                    .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                    .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));
        } else {
            builder = new NotificationCompat.Builder(getApplicationContext(), channel_id)
                    .setSmallIcon(R.drawable.icon_app)
                    .setContentTitle(title)
                    .setContentText(body)
                    .setAutoCancel(true)
                    .setOnlyAlertOnce(true)
                    .setContentIntent(pendingIntent)
                    .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                    .setStyle(new NotificationCompat.BigTextStyle())
                    .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));
        }
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel notificationChannel = new NotificationChannel(channel_id, channel_name,
                    NotificationManager.IMPORTANCE_DEFAULT);
            notificationManager.createNotificationChannel(notificationChannel);
        }

        notificationManager.notify(0, builder.build());
    }

    public Bitmap getBitmapfromUrl(String imageUrl) {
        try {
            URL url = new URL(imageUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap bitmap = BitmapFactory.decodeStream(input);
            return bitmap;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}
