package com.loklvokl.lokl.cart;

import org.json.JSONArray;

public class ModelCart {
	String status_code, status_message, cartid, id_customer, id_guest,
			secure_key, products, id_product, product_name, product_price,
			id_product_attribute, cart_quantity, image, currency, quantity,
			discount;

	// String cart_product_id, cart_product_attribute_id, cart_product_qty;

	public String getDiscount() {
		return discount;
	}

	public void setDiscount(String discount) {
		this.discount = discount;
	}

	public String getQuantity() {
		return quantity;
	}

	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	JSONArray attribute_name;
	JSONArray attribute_val;

	public String getStatus_code() {
		return status_code;
	}

	public void setStatus_code(String status_code) {
		this.status_code = status_code;
	}

	public String getStatus_message() {
		return status_message;
	}

	public void setStatus_message(String status_message) {
		this.status_message = status_message;
	}

	public String getCartid() {
		return cartid;
	}

	public void setCartid(String cartid) {
		this.cartid = cartid;
	}

	public String getId_customer() {
		return id_customer;
	}

	public void setId_customer(String id_customer) {
		this.id_customer = id_customer;
	}

	public String getId_guest() {
		return id_guest;
	}

	public void setId_guest(String id_guest) {
		this.id_guest = id_guest;
	}

	public String getSecure_key() {
		return secure_key;
	}

	public void setSecure_key(String secure_key) {
		this.secure_key = secure_key;
	}

	public String getProducts() {
		return products;
	}

	public void setProducts(String products) {
		this.products = products;
	}

	public String getId_product() {
		return id_product;
	}

	public void setId_product(String id_product) {
		this.id_product = id_product;
	}

	public String getProduct_name() {
		return product_name;
	}

	public void setProduct_name(String product_name) {
		this.product_name = product_name;
	}

	public String getProduct_price() {
		return product_price;
	}

	public void setProduct_price(String product_price) {
		this.product_price = product_price;
	}

	public String getId_product_attribute() {
		return id_product_attribute;
	}

	public void setId_product_attribute(String id_product_attribute) {
		this.id_product_attribute = id_product_attribute;
	}

	public String getCart_quantity() {
		return cart_quantity;
	}

	public void setCart_quantity(String cart_quantity) {
		this.cart_quantity = cart_quantity;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public JSONArray getAttribute_name() {
		return attribute_name;
	}

	public void setAttribute_name(JSONArray jsonArray) {
		this.attribute_name = jsonArray;
	}

	public JSONArray getAttribute_val() {
		return attribute_val;
	}

	public void setAttribute_val(JSONArray jsonArray) {
		this.attribute_val = jsonArray;
	}
}
