package com.loklvokl.lokl.util;

import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader;

import com.squareup.picasso.Transformation;

public class RoundedCornersTransformation implements Transformation {

    @Override
    public Bitmap transform(Bitmap mbitmap) {
        Bitmap imageRounded = Bitmap.createBitmap(mbitmap.getWidth(), mbitmap.getHeight(), mbitmap.getConfig());
        Canvas canvas = new Canvas(imageRounded);
        Paint mpaint = new Paint();
        mpaint.setAntiAlias(true);
        mpaint.setShader(new BitmapShader(mbitmap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP));
        canvas.drawRoundRect((new RectF(0, 0,
                mbitmap.getWidth(), mbitmap.getHeight())), 25, 25, mpaint); // Round Image Corner 100 100 100 100
        mbitmap.recycle();
        return imageRounded;
    }

    @Override
    public String key() {
        return "RoundedTransformation";
    }
}
