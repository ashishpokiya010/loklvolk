package com.loklvokl.lokl.util;

import com.loklvokl.lokl.model.AdProductDetail;
import com.loklvokl.lokl.model.AddressResponse;
import com.loklvokl.lokl.model.Booking;
import com.loklvokl.lokl.model.CartDetail.CartDetail;
import com.loklvokl.lokl.model.CartResponse;
import com.loklvokl.lokl.model.CatBanner;
import com.loklvokl.lokl.model.CategoryModel;
import com.loklvokl.lokl.model.CheckLogin;
import com.loklvokl.lokl.model.CollectionProduct;
import com.loklvokl.lokl.model.CourierModel;
import com.loklvokl.lokl.model.DataModel;
import com.loklvokl.lokl.model.DysplayProfileModel;
import com.loklvokl.lokl.model.EditProfileResponse;
import com.loklvokl.lokl.model.ExplorerProduct;
import com.loklvokl.lokl.model.GetOtp;
import com.loklvokl.lokl.model.HomeModel;
import com.loklvokl.lokl.model.HomeSliderRequest;
import com.loklvokl.lokl.model.OfferProduct;
import com.loklvokl.lokl.model.OfferProductHome;
import com.loklvokl.lokl.model.PincodeModel;
import com.loklvokl.lokl.model.PrivacyModel;
import com.loklvokl.lokl.model.ProductDealModel;
import com.loklvokl.lokl.model.ProductDetailModel;
import com.loklvokl.lokl.model.SearchModel;
import com.loklvokl.lokl.model.SignUpResponse;
import com.loklvokl.lokl.model.SizeDetail;
import com.loklvokl.lokl.model.SliderProduct;
import com.loklvokl.lokl.model.SoldOrderDetailsModle;
import com.loklvokl.lokl.model.TrueCallerVerify;
import com.loklvokl.lokl.model.UserOrderDetails;
import com.loklvokl.lokl.model.UserOrderList;
import com.loklvokl.lokl.model.UserSoldProductList;
import com.loklvokl.lokl.model.commonresponse.CommonResponse;
import com.loklvokl.lokl.model.product.UpdateStockModel;
import com.loklvokl.lokl.model.product.WishListModel;
import com.loklvokl.lokl.model.searchsuggestion.SearchSuggestion;
import com.loklvokl.lokl.model.shareproductdata.ShareProduct;
import com.loklvokl.lokl.model.sizesbycategory.SizesResponse;

import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

public interface ApiInterface {

    @GET(ApiClient.GET_HOME_DATA)
    Call<HomeModel> getHomeData();

    @GET(ApiClient.GET_PRODUCT_SHARE_DATA)
    Call<ShareProduct> getShareData();

    @GET(ApiClient.GET_ALL_CATEGORY)
    Call<CategoryModel> getAllCategoryData();

    @GET(ApiClient.GET_CAT_DATA)
    //Call<SliderProduct> getCatData(@Query("req_id") String req_id);
    Call<SliderProduct> getCatData(@QueryMap Map<String, String> options);

    @GET(ApiClient.GET_CAT_DATA)
    //Call<ExplorerProduct> getExplorerData(@Query("explorer_category_id") String req_id);
    Call<ExplorerProduct> getExplorerData(@QueryMap Map<String, String> options);

    @GET(ApiClient.GET_CAT_DATA)
    //Call<CollectionProduct> getCollectionData(@Query("collection_id") String req_id);
    Call<CollectionProduct> getCollectionData(@QueryMap Map<String, String> options);

    @GET(ApiClient.GET_CAT_DATA)
    //Call<OfferProduct> getOfferData(@Query("offer_id") String req_id);
    Call<OfferProduct> getOfferData(@QueryMap Map<String, String> options);

    @GET(ApiClient.GET_CAT_DATA)
    //Call<ProductDealModel> getDealData(@Query("deal_id") String req_id);
    Call<ProductDealModel> getDealData(@QueryMap Map<String, String> options);

    @POST(ApiClient.GET_PRODUCT_DETAIL)
    Call<ProductDetailModel> getProductData(@Query("product_id") String product_id);

    @FormUrlEncoded
    @POST(ApiClient.POST_PINCODE)
    Call<PincodeModel> checkPincode(@Field("pincode") String pincode);

    @FormUrlEncoded
    @POST(ApiClient.POST_ADD_CART)
    Call<CartResponse> addToCart(@Field("user_id") String user_id,
                                 @Field("device_id") String device_id,
                                 @Field("pro_id") String pro_id,
                                 @Field("product_qty") String product_qty,
                                 @Field("size") String size_id);

    @FormUrlEncoded
    @POST(ApiClient.POST_ADD_WISHLIST)
    Call<CartResponse> addToWishlist(@Field("user_id") String user_id,
                                     @Field("pro_id") String pro_id, @Field("device_id") String device_id);

    @FormUrlEncoded
    @POST(ApiClient.POST_VIEW_CART)
    Call<CartDetail> getCartDetail(@Field("user_id") String user_id,
                                   @Field("device_id") String device_id);

    @FormUrlEncoded
    @POST(ApiClient.POST_VIEW_CART)
    Call<CartDetail> getCartDetailAction(@Field("user_id") String user_id,
                                         @Field("device_id") String device_id,
                                         @Field("cart_id") String cart_id,
                                         @Field("qty") String qty,
                                         @Field("action") String action);

    @FormUrlEncoded
    @POST(ApiClient.POST_VIEW_CART)
    Call<CartDetail> getCartDetailPromo(@Field("user_id") String user_id,
                                        @Field("device_id") String device_id,
                                        @Field("promo_code") String promo_code);

    @FormUrlEncoded
    @POST(ApiClient.POST_DELETE_CART)
    Call<CartResponse> deleteCart(@Field("delete_id") String delete_id);

    @FormUrlEncoded
    @POST(ApiClient.POST_CART_QTY)
    Call<CartResponse> updateQtyCart(@Field("cart_id") String cart_id, @Field("qty") String qty);

    @FormUrlEncoded
    @POST(ApiClient.POST_REGISTER)
    Call<SignUpResponse> requestSignup(@Field("username") String username, @Field("l_name") String l_name, @Field("email") String email,
                                       @Field("mobile") String mobile, @Field("password") String password, @Field("device_id") String device_id);


    @FormUrlEncoded
    @POST(ApiClient.POST_LOGIN)
    Call<SignUpResponse> requestLogin(@Field("mobile") String mobile, @Field("password") String password, @Field("device_id") String device_id);

    @FormUrlEncoded
    @POST(ApiClient.POST_DISPLAY_USER)
    Call<DysplayProfileModel> diplayProfile(@Field("USERID") String USERID);

    @FormUrlEncoded
    @POST(ApiClient.POST_SOCIAL)
    Call<SignUpResponse> requestSocial(@Field("name") String name, @Field("email") String email,
                                       @Field("fb_id") String fb_id, @Field("google_id") String google_id, @Field("device_id") String device_id);


//    @FormUrlEncoded
//    @POST(ApiClient.POST_SOCIAL)
//    Call<SignUpResponse> requestSocialGoogle(@Field("name") String name, @Field("email") String email,
//                                             @Field("google_id") String google_id, @Field("device_id") String device_id);

    @GET(ApiClient.GET_PRIVACY)
    Call<PrivacyModel> getPrivacy();

    @GET(ApiClient.GET_HELP)
    Call<PrivacyModel> getHelp();

    @GET(ApiClient.GET_TERM)
    Call<PrivacyModel> getTerms();

    @GET(ApiClient.GET_FAQ)
    Call<PrivacyModel> getFaq();

    @FormUrlEncoded
    @POST(ApiClient.POST_ALL_CAT_PRODUCT)
    //Call<HomeSliderRequest> getCatProduct(@Field("sub_cat_id") String sub_cat_id);
    Call<HomeSliderRequest> getCatProduct(@FieldMap Map<String, String> options);

    @FormUrlEncoded
    @POST(ApiClient.POST_CAT_SLIDER_PRODUCT)
    //Call<CatBanner> getCatSLider(@Field("cat_slider_id") String sub_cat_id);
    Call<CatBanner> getCatSLider(@FieldMap Map<String, String> options);


    @FormUrlEncoded
    @POST(ApiClient.POST_EDIT_USER)
    Call<EditProfileResponse> requestUpdateProfile(@Field("username") String username, @Field("l_name") String l_name,
                                                   @Field("email") String email, @Field("USERID") String USERID,
                                                   @Field("image_path") String image_path,
                                                   @Field("gender") String gender, @Field("dob") String dob,
                                                   @Field("city") String city);

    @Multipart
    @POST(ApiClient.POST_EDIT_USER)
    Call<EditProfileResponse> requestUpdateProfileFile(@Part("username") RequestBody username,
                                                       @Part("l_name") RequestBody l_name,
                                                       @Part("email") RequestBody email,
                                                       @Part("USERID") RequestBody USERID,
                                                       @Part() MultipartBody.Part image_path,
                                                       @Part("device_id") RequestBody device_id,
                                                       @Part("gender") RequestBody gender,
                                                       @Part("dob") RequestBody dob,
                                                       @Part("city") RequestBody city,
                                                       @Part("addhar") RequestBody addhar,
                                                       @Part("pan") RequestBody pan,
                                                       @Part() MultipartBody.Part adhaar_image1,
                                                       @Part() MultipartBody.Part adhaar_image2,
                                                       @Part() MultipartBody.Part adhaar_image3,
                                                       @Part() MultipartBody.Part pan_image1,
                                                       @Part() MultipartBody.Part pan_image2,
                                                       @Part() MultipartBody.Part pan_image3);


    @FormUrlEncoded
    @POST(ApiClient.POST_EDIT_USER)
    Call<EditProfileResponse> requestDeleteDoc(@Field("username") String username, @Field("l_name") String l_name,
                                               @Field("email") String email, @Field("USERID") String USERID,
                                               @Field("image_path") String image_path,
                                               @Field("gender") String gender, @Field("dob") String dob,
                                               @Field("device_id") String device_id,
                                               @Field("city") String city,
                                               @Field("addhar") String addhar,
                                               @Field("pan") String pan, @Field("action") String action,
                                               @Field("id") String id);


    //    @FormUrlEncoded
//    @POST(ApiClient.POST_EDIT_USER)
//    Call<EditProfileResponse> requestUpdateProfileFile(@Field("username") String username, @Field("l_name") String l_name,
//                                                       @Field("email") String email, @Field("USERID") String USERID,
//
//                                                       @Field("image_path") File image_path);

    @Multipart
    @POST(ApiClient.POST_EDIT_USER)
    Call<EditProfileResponse> requestDeleteDocFile(@Part("username") RequestBody username,
                                                   @Part("l_name") RequestBody l_name,
                                                   @Part("email") RequestBody email,
                                                   @Part("USERID") RequestBody USERID,
                                                   @Part() MultipartBody.Part image_path,
                                                   @Part("gender") RequestBody gender,
                                                   @Part("dob") RequestBody dob,
                                                   @Part("device_id") RequestBody device_id,
                                                   @Part("city") RequestBody city,
                                                   @Part("addhar") RequestBody addhar,
                                                   @Part("pan") RequestBody pan,
                                                   @Part("action") RequestBody action,
                                                   @Part("id") RequestBody id);


    @FormUrlEncoded
    @POST(ApiClient.POST_GET_ADDRESS)
    Call<AddressResponse> getUserAdress(@Field("user_id") String user_id);


    @FormUrlEncoded
    @POST(ApiClient.POST_ADD_ADDRESS)
    Call<AddressResponse> requestAddAddress(@Field("user_id") String user_id, @Field("fullname") String fullname, @Field("mobile") String mobile,
                                            @Field("pincode") String pincode, @Field("area") String area,
                                            @Field("landmark") String landmark, @Field("city") String city, @Field("state") String state);


    @FormUrlEncoded
    @POST(ApiClient.POST_EDIT_ADDRESS)
    Call<AddressResponse> requestEditAddress(@Field("address_id") String address_id, @Field("user_id") String user_id, @Field("fullname") String fullname, @Field("mobile") String mobile,
                                             @Field("pincode") String pincode, @Field("area") String area,
                                             @Field("landmark") String landmark, @Field("city") String city, @Field("state") String state);


    @POST(ApiClient.GET_SEARCH_DATA)
    Call<SearchModel> getSearchProduct(@QueryMap Map<String, String> options);
    //Call<SearchModel> getSearchProduct(@Query("search_data") String search_data, @Query("device_id") String device_id);

    @FormUrlEncoded
    @POST(ApiClient.GET_SEARCH_SUGGESTION)
    Call<SearchSuggestion> getSearchSuggestion(@Field("search") String str_search);

    @POST(ApiClient.GET_TREND_SEARCH)
    Call<SearchSuggestion> getTrendSearch();

    @GET(ApiClient.GET_OFFER_PRODUCT)
    Call<OfferProductHome> getOfferProduct(@QueryMap Map<String, String> options);
    //Call<OfferProductHome> getOfferProduct();

    @FormUrlEncoded
    @POST(ApiClient.POST_BOOKING)
    Call<Booking> performPlaceOrder(@Field("user_id") String user_id, @Field("address_id") String address_id, @Field("total_qty") String total_qty,
                                    @Field("price") String price, @Field("discount") String discount, @Field("total_price") String total_price,
                                    @Field("payment_type") String payment_type, @Field("device_id") String device_id, @Field("promo_code") String promo_code);


    @FormUrlEncoded
    @POST(ApiClient.ADD_PRO_DETAILS)
    Call<AdProductDetail> performProductDetail(@Field("sub_cat_id") String user_id);

    @FormUrlEncoded
    @POST(ApiClient.TRUE_CALL)
    Call<TrueCallerVerify> verifyTrueCall(@Field("accessToken") String token, @Field("appKey") String appKey);

    @FormUrlEncoded
    @POST(ApiClient.OTP_LOGIN)
    Call<GetOtp> sendOTP(@Field("mobile") String mobile);

    @FormUrlEncoded
    @POST(ApiClient.CHECK_LOGIN)
    Call<CheckLogin> checkLogin(@Field("mobile") String mobile, @Field("device_id") String device_id, @Field("device_token") String fcm_token);

    @FormUrlEncoded
    @POST(ApiClient.VERIFY_OTP)
    Call<GetOtp> verifyOtp(@Field("varify_mobile") String mobile, @Field("otp_verify") String otp);

    @FormUrlEncoded
    @POST(ApiClient.POST_ADD_PRODUCT)
    Call<SignUpResponse> performSellProduct(@Field("user_id") String user_id,
                                            @Field("main_cat_id") String main_cat_id,
                                            @Field("sub_cat1_id") String sub_cat1_id,
                                            @Field("sub_cat2_id") String sub_cat2_id,
                                            @Field("pro_name") String pro_name,
                                            @Field("description") String description,
                                            @Field("select_product_condition") String select_product_condition,
                                            @Field("country_of_origin") String country_of_origin,
                                            @Field("mrp") String mrp,
                                            @Field("size") String size,
                                            @Field("color_id") String color_id,
                                            @Field("selling_price") String selling_price,
                                            @Field("field_name1") String field_name1,
                                            @Field("field_name2") String field_name,
                                            @Field("field_name3") String field_name3,
                                            @Field("field_name4") String field_name4,
                                            @Field("shipping_option") String shipping_option,
                                            @Field("c_comission") String c_comission);

   /* @Multipart
    @POST(ApiClient.POST_ADD_PRODUCT)
    Call<SignUpResponse> performSellProduct(@Part("user_id") RequestBody user_id, @Part("main_cat_id") RequestBody main_cat_id, @Part("sub_cat1_id") RequestBody sub_cat1_id,
                                            @Part("sub_cat2_id") RequestBody sub_cat2_id, @Part("pro_name") RequestBody pro_name, @Part("description") RequestBody description,
                                            @Part("mrp") RequestBody mrp, @Part("size") RequestBody size, @Part("color_id") RequestBody color_id, @Part("selling_price") RequestBody selling_price,
                                            @Part() MultipartBody.Part field_name1, @Part() MultipartBody.Part field_name,
                                            @Part() MultipartBody.Part field_name3, @Part() MultipartBody.Part field_name4,
                                            @Part("shipping_option") RequestBody shipping_option);*/


    @FormUrlEncoded
    @POST(ApiClient.POST_USER_ORDER_LIST)
    Call<UserOrderList> getUserProductList(@Field("user_id") String user_id);

    @FormUrlEncoded
    @POST(ApiClient.POST_USER_SOLD_ORDER_LIST)
    Call<UserSoldProductList> getUserSoldProductList(@Field("c_seller_id") String sellder_id);

    @FormUrlEncoded
    @POST(ApiClient.POST_USER_ORDER_DETAILS)
    Call<UserOrderDetails> getUserOrderDetail(@Field("order_details_id") String order_details, @Field("rating") String rating
            , @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST(ApiClient.POST_USER_ORDER_CANCEL)
    Call<DataModel> getUserOrderCancel(@Field("order_id") String order_id, @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST(ApiClient.POST_USER_ORDER_RETURN)
    Call<DataModel> getUserOrderReturn(@Field("order_id") String order_id, @Field("user_id") String user_id,
                                       @Field("return_reason") String return_reason,
                                       @Field("retern_image1") String image_1, @Field("retern_image2") String image_2,
                                       @Field("retern_image3") String image_3, @Field("retern_image4") String image_4,
                                       @Field("retern_image5") String image_5);

    @FormUrlEncoded
    @POST(ApiClient.POST_USER_SOLD_ORDER_DETAILS)
    Call<SoldOrderDetailsModle> getUserSoldOrderDetails(@Field("order_details_id") String order_id, @Field("c_seller_id") String c_seller_id,
                                                        @Field("courier_id") String courier_id,
                                                        @Field("track_number") String track_number,
                                                        @Field("other_courier_name") String other_courier_name);

    @GET(ApiClient.GET_COURIER_COMPANY_LIST)
    Call<CourierModel> getCourierCompnanyList();

    @FormUrlEncoded
    @POST(ApiClient.POST_UPDATE_STOCK_PRODUCT)
    Call<UpdateStockModel> getUpdateStockProduct(@Field("USERID") String user_id);

    @FormUrlEncoded
    @POST(ApiClient.POST_UPDATE_STOCK_PRODUCT)
    Call<UpdateStockModel> deleteStockProduct(@Field("USERID") String user_id, @Field("delete_id") String delete_id);

    @FormUrlEncoded
    @POST(ApiClient.POST_ORDER_ACCEPT)
    Call<CommonResponse> orderAccepted(@Field("orderid") String order_id);

    @FormUrlEncoded
    @POST(ApiClient.POST_ORDER_DECLINE)
    Call<CommonResponse> orderDeclined(@Field("orderid") String order_id);

    @FormUrlEncoded
    @POST(ApiClient.GET_UPDATE_STOCK_DETAIL_PRODUCT)
    Call<UpdateStockModel> getUpdateStockDetail(@Field("user_product_id") String user_product_id,
                                                @Field("selling_price") String selling_price,
                                                @Field("mrp") String mrp, @Field("stock") String stock,
                                                @Field("size") String size);

    @FormUrlEncoded
    @POST(ApiClient.GET_STOCK_SIZE_PRODUCT)
    Call<SizesResponse> getStockSize(@Field("category_id") String cat_id);

    @FormUrlEncoded
    @POST(ApiClient.POST_GET_WISHLIST)
    Call<WishListModel> getWishList(@Field("user_id") String user_id, @Field("device_id") String device_id);

    @FormUrlEncoded
    @POST(ApiClient.POST_GET_WISHLIST)
    Call<WishListModel> deleteWishListItem(@Field("user_id") String user_id,
                                           @Field("device_id") String device_id,
                                           @Field("action") String action,
                                           @Field("id") String id);

}