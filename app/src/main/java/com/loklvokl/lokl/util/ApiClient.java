package com.loklvokl.lokl.util;

import android.util.Log;

import com.google.gson.Gson;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.logging.HttpLoggingInterceptor;
import okio.Buffer;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClient {

    //public static final String BASE_URL_OLD = "https://360004.in/AndroidClass/vokl_api.php";

    // public static final String BASE_DOMAIN = "https://360004.in";
    public static final String BASE_DOMAIN = "https://loklvokl.co.in";

//    public static final String BASE_URL = BASE_DOMAIN + "/AndroidClass/vokl_api/";
    //public static final String BASE_URL = BASE_DOMAIN + "/AndroidClass/vokl_api_v1/";
    //public static final String BASE_URL = BASE_DOMAIN + "/AndroidClass/vokl_api_v1.6/";
    //public static final String BASE_URL = BASE_DOMAIN + "/AndroidClass/vokl_api_v1.8/";
    public static final String BASE_URL = BASE_DOMAIN + "/AndroidClass/vokl_api_v2.0/";

    public static final String GET_HOME_DATA = "get_homedata";
    public static final String GET_PRODUCT_SHARE_DATA = "product_sharing";
    public static final String GET_CAT_DATA = "get_cat_data";
    public static final String GET_ALL_CATEGORY = "get_all_category";
    public static final String GET_PRODUCT_DETAIL = "get_product_detail";
    public static final String POST_PINCODE = "get_pincode";
    public static final String POST_LOGIN = "login";
    public static final String POST_DISPLAY_USER = "display_profile";
    public static final String POST_REGISTER = "signup";
    public static final String GET_PRIVACY = "policy";
    public static final String GET_HELP = "help";
    public static final String GET_TERM = "term";
    public static final String GET_FAQ = "faq";
    public static final String POST_SOCIAL = "checkfb";
    public static final String POST_ALL_CAT_PRODUCT = "all_category_product";
    public static final String POST_CAT_SLIDER_PRODUCT = "all_category_slider_product";
    public static final String POST_EDIT_USER = "edit_user";
    public static final String POST_ADD_CART = "add_cart";
    public static final String POST_ADD_WISHLIST = "add_wishlist";
    public static final String POST_VIEW_CART = "cart_details";
    public static final String POST_DELETE_CART = "cart_delete";
    public static final String POST_CART_QTY = "quantity";
    public static final String POST_GET_ADDRESS = "User_delivery_address";
    public static final String POST_ADD_ADDRESS = "add_delivery_address";
    public static final String POST_EDIT_ADDRESS = "edit_delivery_address";
    public static final String GET_SEARCH_DATA = "search_product";
    public static final String GET_SEARCH_SUGGESTION  = "search_suggestion";
    public static final String GET_TREND_SEARCH  = "trending_search";

    public static final String GET_OFFER_PRODUCT = "offer_product";
    public static final String POST_BOOKING = "booking";
    public static final String ADD_PRO_DETAILS = "add_product_detils";
    public static final String POST_ADD_PRODUCT = "add_product";
    public static final String POST_USER_ORDER_LIST = "user_order_list";
    public static final String POST_USER_ORDER_DETAILS = "user_order_details";
    public static final String POST_USER_ORDER_CANCEL = "cancel_order_req";
    public static final String POST_USER_ORDER_RETURN = "return_order";
    public static final String POST_USER_SOLD_ORDER_LIST = "user_sold_order_list";
    public static final String POST_USER_SOLD_ORDER_DETAILS = "user_sold_order_details";
    public static final String GET_COURIER_COMPANY_LIST = "courier_company_list";
    public static final String POST_UPDATE_STOCK_PRODUCT = "All_user_product";
    public static final String POST_ORDER_ACCEPT = "c_seller_accept_order";
    public static final String POST_ORDER_DECLINE = "c_seller_reject_order";
    public static final String GET_UPDATE_STOCK_DETAIL_PRODUCT = "edit_user_product";
    public static final String GET_STOCK_SIZE_PRODUCT = "edit_product_size_list";
    public static final String POST_GET_WISHLIST = "wishlist_list";
    public static final String OTP_LOGIN = "otp_login";
    public static final String TRUE_CALL = "true_call";
    public static final String VERIFY_OTP = "verify_otp";
    public static final String CHECK_LOGIN = "check_login";

    private static Retrofit retrofit = null;

    public static Retrofit getClient() {

        OkHttpClient httpClient = null;
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        httpClient = new OkHttpClient.Builder()
                .connectTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .addInterceptor(logging)
                .build();


        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(httpClient)
                    .build();
        }
        return retrofit;
    }

    public <T> void enqueue(Call<T> call, final Callback<T> annoucncementResponseCallback) {

        Log.e("API_request", "> " + call.request().url().toString() + " | " + bodyToString(call.request().body()));
        Log.e("API_header", "> " + call.request().body());

        call.enqueue(new Callback<T>() {
            @Override
            public void onResponse(retrofit2.Call<T> call, retrofit2.Response<T> response) {
                Log.e("API_response: ", "> " + new Gson().toJson(response.body()));
                annoucncementResponseCallback.onResponse(call, response);
            }

            @Override
            public void onFailure(retrofit2.Call<T> call, Throwable t) {
                Log.e("API_response: ", "Err: " + t);
                annoucncementResponseCallback.onFailure(call, t);
            }
        });
    }

    private String bodyToString(final RequestBody request) {
        try {
            final RequestBody copy = request;
            final Buffer buffer = new Buffer();
            if (copy != null)
                copy.writeTo(buffer);
            else
                return "";
            return buffer.readUtf8();
        } catch (final IOException e) {
            return "did not work";
        }
    }
}
