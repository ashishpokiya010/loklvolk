package com.loklvokl.lokl.util;

import android.app.ProgressDialog;
import android.content.Context;


public class Progress {

    public static ProgressDialog showProgressDialog(Context context, String msg, boolean cancelable) {
        ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setMessage(msg);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(cancelable);
        return progressDialog;
    }

}
