package com.loklvokl.lokl.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.provider.Settings;
import android.widget.Toast;

import org.jetbrains.annotations.NotNull;

public class Constant {

    public enum clickAction {
        ProductDetailActivity {
            @NotNull
            public String toString() {
                return "ProductDetailActivity";
            }
        },
        OffersActivity {
            @NotNull
            public String toString() {
                return "OffersActivity";
            }
        },
        CategoryProductActivity {
            @NotNull
            public String toString() {
                return "CategoryProductActivity";
            }
        },
        CollectionProductActivity {
            @NotNull
            public String toString() {
                return "CollectionProductActivity";
            }
        },
        SliderProductActivity {
            @NotNull
            public String toString() {
                return "SliderProductActivity";
            }
        },
    }

    public static String getDeviceId(Context context) {
        return Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);

    }

    public static void toast(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    public static boolean isNetworkAvailable(Context context2) {
        ConnectivityManager connectivity = (ConnectivityManager) context2
                .getSystemService(Context.CONNECTIVITY_SERVICE);

        if (connectivity == null) {
            return false;
        } else {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null) {
                for (int i = 0; i < info.length; i++) {
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public static boolean getConnectivityStatus(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (activeNetwork != null) {
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI) {
                return true;
            } else if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
                return true;
            }
        } else {
            return false;
        }
        return false;
    }
}
