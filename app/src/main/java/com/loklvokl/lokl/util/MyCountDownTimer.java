package com.loklvokl.lokl.util;

import android.os.CountDownTimer;

import com.loklvokl.lokl.activtiy.EnterOtpActivity;

public class MyCountDownTimer extends CountDownTimer {

    // This variable refer to the source activity which use this CountDownTimer object.
    private EnterOtpActivity sourceActivity;

    public MyCountDownTimer(long millisInFuture, long countDownInterval) {
        super(millisInFuture, countDownInterval);
    }

    public void setSourceActivity(EnterOtpActivity sourceActivity) {
        this.sourceActivity = sourceActivity;
    }

    @Override
    public void onTick(long millisUntilFinished) {
        if (this.sourceActivity != null) {
            // Invoke source activity's tick event method.
            this.sourceActivity.onCountDownTimerTickEvent(millisUntilFinished);
        }
    }

    @Override
    public void onFinish() {
        if (this.sourceActivity != null) {
            // Invoke source activity's tick event method.
            this.sourceActivity.onCountDownTimerFinishEvent();
        }
    }
}