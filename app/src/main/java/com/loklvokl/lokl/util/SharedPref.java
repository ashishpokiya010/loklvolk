package com.loklvokl.lokl.util;


import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loklvokl.lokl.model.AadharDocument;
import com.loklvokl.lokl.model.PanDocument;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class SharedPref {

    private static Context context;

    public SharedPref(Context context) {
        this.context = context;
    }

    public final static String PREFS_NAME = "my_prefs";

    public boolean sharedPreferenceExist(String key) {
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, 0);
        if (!prefs.contains(key)) {
            return true;
        } else {
            return false;
        }
    }

    public void setInt(String key, int value) {
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt(key, value);
        editor.apply();
    }

    public int getInt(String key) {
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, 0);
        return prefs.getInt(key, 0);
    }

    public void setStr(String key, String value) {
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(key, value);
        editor.apply();
    }

    public String getStr(String key) {
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, 0);
        return prefs.getString(key, "");
    }

    public void setAdharList(String key, List<AadharDocument> value) {
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor editor = prefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(value);
        editor.putString(key, json);
        editor.apply();
    }

    public ArrayList<AadharDocument> getAdharList(String key) {
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, 0);

        Gson gson = new Gson();
        String json = prefs.getString(key, "");
        Type type = new TypeToken<List<AadharDocument>>() {
        }.getType();
        ArrayList<AadharDocument> arrayList = gson.fromJson(json, type);

        return arrayList;
    }

    public void setPanList(String key, List<PanDocument> value) {
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor editor = prefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(value);
        editor.putString(key, json);
        editor.apply();
    }

    public ArrayList<PanDocument> getPanList(String key) {
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, 0);

        Gson gson = new Gson();
        String json = prefs.getString(key, "");
        Type type = new TypeToken<List<PanDocument>>() {
        }.getType();
        ArrayList<PanDocument> arrayList = gson.fromJson(json, type);

        return arrayList;
    }

    public void setBool(String key, boolean value) {
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(key, value);
        editor.apply();
    }

    public boolean getBool(String key) {
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, 0);
        return prefs.getBoolean(key, false);
    }

    public void clearData() {
        SharedPreferences preferences = context.getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.clear();
        editor.apply();
    }

    public String getCart_id() {
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, 0);
        return prefs.getString("CART_ID", "");
    }

    public String getShareText(){
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, 0);
        return prefs.getString("SHARE_TEXT", "");
    }

    public void setShareText(String text){
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("SHARE_TEXT", text);
        editor.apply();
    }

    public String getVersionName(){
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, 0);
        return prefs.getString("VERSION_NAME", "");
    }

    public void setVersionName(String text){
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("VERSION_NAME", text);
        editor.apply();
    }
}

